"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const language_parser_1 = require("./language.parser");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
exports.Query = {
    Language: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, language_parser_1.parseConfigToLanguage),
    Languages: (_, context) => common_meta_1.configService.fetchConfigByType(configitemtype_enum_1.ConfigItemTypes.LANGUAGE, language_parser_1.parseConfigToLanguage)
};
exports.Mutation = {
    createLanguage: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, language_parser_1.parseLanguageToConfig, language_parser_1.parseConfigToLanguage);
    }
};
//# sourceMappingURL=resolvers.js.map