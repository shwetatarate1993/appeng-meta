declare const _default: {
    configObjectId: string;
    configObjectType: string;
    name: string;
    projectId: number;
    defaultLanguage: boolean;
    order: number;
    code: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: any;
    creationDate: string;
    updatedBy: any;
    updationDate: any;
    deletionDate: any;
    privileges: any[];
    childRelations: any[];
    parentRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        childItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: string;
        deletionDate: string;
    }[];
}[];
export default _default;
//# sourceMappingURL=language.object.d.ts.map