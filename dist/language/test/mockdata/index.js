"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var language_object_1 = require("./language.object");
exports.languageObjectMockData = language_object_1.default;
var configitem_language_1 = require("./configitem.language");
exports.languageConfigitemMockData = configitem_language_1.default;
var configitem_property_language_1 = require("./configitem.property.language");
exports.languagePropertyMockData = configitem_property_language_1.default;
var configitem_relation_language_1 = require("./configitem.relation.language");
exports.languageRelationMockData = configitem_relation_language_1.default;
var configitem_privileges_language_1 = require("./configitem.privileges.language");
exports.languagePrivilegesMockData = configitem_privileges_language_1.default;
//# sourceMappingURL=index.js.map