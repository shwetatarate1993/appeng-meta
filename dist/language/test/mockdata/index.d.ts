export { default as languageObjectMockData } from './language.object';
export { default as languageConfigitemMockData } from './configitem.language';
export { default as languagePropertyMockData } from './configitem.property.language';
export { default as languageRelationMockData } from './configitem.relation.language';
export { default as languagePrivilegesMockData } from './configitem.privileges.language';
//# sourceMappingURL=index.d.ts.map