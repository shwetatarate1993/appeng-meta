"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const language_parser_1 = require("../language.parser");
const mockdata_1 = require("./mockdata");
test('parser language to config', () => {
    const language = models_1.Language.deserialize(mockdata_1.languageObjectMockData[0]);
    const result = language_parser_1.parseLanguageToConfig(language);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItem.configObjectType).toEqual('Language');
    expect(result.configItemProperty[0].propertyName).toEqual('IS_DEFAULT');
    expect(result.configItemProperty[1].propertyName).toEqual('ORDER');
    expect(result.configItemProperty[2].propertyName).toEqual('CODE');
});
test('parser config to language', () => {
    const result = language_parser_1.parseConfigToLanguage(mockdata_1.languageConfigitemMockData[0], mockdata_1.languagePropertyMockData, mockdata_1.languageRelationMockData, mockdata_1.languagePrivilegesMockData);
    expect(result).toHaveProperty('defaultLanguage');
    expect(result).toHaveProperty('code');
    expect(result).toHaveProperty('order');
});
//# sourceMappingURL=language.parser.test.js.map