export declare const Query: {
    Language: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").Language>;
    Languages: (_: any, context: any) => Promise<import("../models").Language[]>;
};
export declare const Mutation: {
    createLanguage: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map