import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { Language } from '../models';
export declare const parseLanguageToConfig: (language: Language) => ConfigMetadata;
export declare const parseConfigToLanguage: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => Language;
//# sourceMappingURL=language.parser.d.ts.map