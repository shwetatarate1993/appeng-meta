"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseLanguageToConfig = (language) => {
    const createdAt = language.creationDate;
    const createdBy = language.createdBy;
    const updatedBy = language.updatedBy;
    const updatedAt = language.updationDate;
    const property = [];
    let itemId;
    if (language.configObjectId !== undefined && language.configObjectId !== ''
        && language.configObjectId !== null) {
        itemId = language.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    // setting default value as false
    property.push(new meta_db_1.ConfigItemProperty('IS_DEFAULT', language.defaultLanguage !== undefined ? (language.defaultLanguage ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    if (language.order !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ORDER', language.order, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (language.code !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('CODE', language.code, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, language.name, configitemtype_enum_1.ConfigItemTypes.LANGUAGE, language.projectId, createdBy, language.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const languagePrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(language.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(language.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(language.privileges, languagePrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, languagePrivileges);
};
exports.parseConfigToLanguage = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.Language(details.get('IS_DEFAULT'), details.get('ORDER'), details.get('CODE'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=language.parser.js.map