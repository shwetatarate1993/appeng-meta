declare const _default: {
    Query: {
        Database: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").Database>;
        RootCompositeEntityNode: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").RootCompositeEntityNode>;
        EventListener: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").EventListener>;
        DataSetRelProperty: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").DataSetRelProperty>;
        ParentGridHeader: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").ParentGridHeader>;
        UILayout: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").UILayout>;
        FormSection: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").FormSection>;
        LogicalEntityOperation: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").LogicalEntityOperation>;
        CustomFormValidation: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").CustomFormValidation>;
        FormDbValidation: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").FormDbValidation>;
        AuditEntity: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").AuditEntity>;
        DatabaseValidation: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").DatabaseValidation>;
        StandardValidation: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").StandardValidation>;
        PhysicalEntity: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").PhysicalEntity>;
        LogicalEntity: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").LogicalEntity>;
        MenuGroup: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").MenuGroup>;
        UiTab: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").UiTab>;
        ColumnDataPreprocessor: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").ColumnDataPreprocessor>;
        DataGrid: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").DataGrid>;
        AuditGrid: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").AuditGrid>;
        NodeBusinessRule: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").NodeBusinessRule>;
        CompositeEntityNode: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").CompositeEntityNode>;
        ChildCompositeEntityNodes: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").CompositeEntityNode[]>;
        CombinedChildCompositeEntityNodes: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").CompositeEntityNode[]>;
        ChildNodesInTree: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").CompositeEntityNode[]>;
        HiddenNodes: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").CompositeEntityNode[]>;
        Language: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").Language>;
        Languages: (_: any, context: any) => Promise<import("./models").Language[]>;
        ButtonPanel: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").ButtonPanel>;
        DataSetRel: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").DataSetRel>;
        DataSetRelEntityColumn: (_: any, { parentEntityId, childEntityId }: {
            parentEntityId: any;
            childEntityId: any;
        }, context: any) => Promise<import("./models").DataSetRelEntityColumns>;
        DBOperation: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").DBOperation>;
        UiCard: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").UiCard>;
        DataGridColumn: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").DataGridColumn>;
        FormGroup: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").FormGroup>;
        MenuButton: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").MenuButton>;
        Menu: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").Menu>;
        SubMenus: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").Menu[]>;
        Action: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").Action>;
        PhysicalColumn: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").PhysicalColumn>;
        LogicalColumn: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").LogicalColumn>;
        CompositeEntity: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").CompositeEntity>;
        FormField: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").FormField>;
        Form: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").Form>;
        Button: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").Button>;
        Datasource: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").Datasource>;
        CardGroup: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").CardGroup>;
        Card: (_: any, { id }: {
            id: any;
        }, context: any) => Promise<import("./models").Card>;
    };
    Mutation: {
        createDatabase: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createRootCompositeEntityNode: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createEventListener: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createDataSetRelProperty: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createParentGridHeader: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createUILayout: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createFormSection: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createLogicalEntityOperation: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createCustomFormValidation: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createFormDbValidation: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createAuditEntity: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createDatabaseValidation: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createStandardValidation: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createPhysicalEntity: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createLogicalEntity: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createMenuGroup: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createUiTab: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createColumnDataPreprocessor: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createDataGrid: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createAuditGrid: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createNodeBusinessRule: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createCompositeEntityNode: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createLanguage: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createButtonPanel: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createDataSetRel: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createDBOperation: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createUiCard: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createDataGridColumn: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createFormGroup: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createMenuButton: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createMenu: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createAction: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createPhysicalColumn: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createLogicalColumn: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createCompositeEntity: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createFormField: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createForm: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createButton: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createDatasource: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createCardGroup: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
        createCard: (parent: any, { input }: {
            input: any;
        }, context: any) => Promise<any>;
    };
    CardGroup: {
        cards: (cardgroup: any, _: any, context: any) => Promise<import("./models").Card[]>;
    };
    Form: {
        formSections: (form: any, _: any, context: any) => Promise<import("./models").FormSection[]>;
        buttonPanels: (form: any, _: any, context: any) => Promise<import("./models").ButtonPanel[]>;
        columnDataPreprocessors: (form: any, _: any, context: any) => Promise<import("./models").ColumnDataPreprocessor[]>;
    };
    FormSection: {
        formFields: (formSection: any, _: any, context: any) => Promise<import("./models").FormField[]>;
    };
    FormField: {
        logicalColumn: (formField: any, _: any, context: any) => Promise<import("./models").LogicalColumn>;
        columnDataPreprocessors: (formField: any, _: any, context: any) => Promise<import("./models").ColumnDataPreprocessor[]>;
    };
    ButtonPanel: {
        buttons: (buttonPanel: any, _: any, context: any) => Promise<import("./models").Button[]>;
        menuButtons: (buttonPanel: any, _: any, context: any) => Promise<import("./models").MenuButton[]>;
    };
    LogicalEntity: {
        physicalEntities: (logicalEntity: any, _: any, context: any) => Promise<import("./models").PhysicalEntity[]>;
        logicalColumns: (logicalEntity: any, _: any, context: any) => Promise<import("./models").LogicalColumn[]>;
        forms: (logicalEntity: any, _: any, context: any) => Promise<import("./models").Form[]>;
        dataGrids: (logicalEntity: any, _: any, context: any) => Promise<import("./models").DataGrid[]>;
        actions: (logicalEntity: any, _: any, context: any) => Promise<import("./models").Action[]>;
        dbOperations: (logicalEntity: any, _: any, context: any) => Promise<import("./models").DBOperation[]>;
        dataSetRels: (logicalEntity: any, _: any, context: any) => Promise<import("./models").DataSetRel[]>;
        formDbValidations: (logicalEntity: any, _: any, context: any) => Promise<import("./models").FormDbValidation[]>;
    };
    PhysicalEntity: {
        auditEntity: (physicalEntity: any, _: any, context: any) => Promise<import("./models").AuditEntity[]>;
        physicalColumns: (physicalEntity: any, _: any, context: any) => Promise<import("./models").PhysicalColumn[]>;
    };
    AuditEntity: {
        auditGrid: (auditEntity: any, _: any, context: any) => Promise<import("./models").AuditGrid[]>;
    };
    DataGrid: {
        dataGridColumns: (dataGrid: any, _: any, context: any) => Promise<import("./models").DataGridColumn[]>;
        actionDataGridColumns: (dataGrid: any, _: any, context: any) => Promise<import("./models").DataGridColumn[]>;
        logicalEntityOperations: (dataGrid: any, _: any, context: any) => Promise<import("./models").LogicalEntityOperation[]>;
        parentGridHeaders: (dataGrid: any, _: any, context: any) => Promise<import("./models").ParentGridHeader[]>;
        buttonPanels: (dataGrid: any, _: any, context: any) => Promise<import("./models").ButtonPanel[]>;
    };
    DataGridColumn: {
        logicalColumn: (dataGridColumn: any, _: any, context: any) => Promise<import("./models").LogicalColumn>;
    };
    ParentGridHeader: {
        parentGridHeader: (parentGridHeader: any, _: any, context: any) => Promise<import("./models").ParentGridHeader[]>;
    };
    CompositeEntity: {
        eventListeners: (compositeEntity: any, _: any, context: any) => Promise<import("./models").EventListener[]>;
        rootCompositeEntityNode: (compositeEntity: any, _: any, context: any) => Promise<import("./models").RootCompositeEntityNode>;
        formGroups: (compositeEntity: any, _: any, context: any) => Promise<import("./models").FormGroup[]>;
        compositeEntityNodes: (compositeEntity: any, _: any, context: any) => Promise<import("./models").CompositeEntityNode[]>;
        uILayouts: (compositeEntity: any, _: any, context: any) => Promise<import("./models").UILayout[]>;
    };
    RootCompositeEntityNode: {
        physicalEntities: (rootCompositeEntityNode: any, _: any, context: any) => Promise<import("./models").PhysicalEntity[]>;
        entity: (rootCompositeEntityNode: any, _: any, context: any) => Promise<import("./models").LogicalEntity[]>;
        insertForm: (rootCompositeEntityNode: any, _: any, context: any) => Promise<import("./models").Form[]>;
        editForm: (rootCompositeEntityNode: any, _: any, context: any) => Promise<import("./models").Form[]>;
        dataGrid: (rootCompositeEntityNode: any, _: any, context: any) => Promise<import("./models").DataGrid[]>;
        nodeBusinessRules: (rootCompositeEntityNode: any, _: any, context: any) => Promise<import("./models").NodeBusinessRule[]>;
    };
    FormGroup: {
        forms: (formGroup: any, _: any, context: any) => Promise<import("./models").Form[]>;
    };
    CompositeEntityNode: {
        physicalDataEntities: (compositeEntityNode: any, _: any, context: any) => Promise<import("./models").PhysicalEntity[]>;
        entity: (compositeEntityNode: any, _: any, context: any) => Promise<import("./models").LogicalEntity>;
        insertForm: (compositeEntityNode: any, _: any, context: any) => Promise<import("./models").Form>;
        editForm: (compositeEntityNode: any, _: any, context: any) => Promise<import("./models").Form>;
        dataGrid: (compositeEntityNode: any, _: any, context: any) => Promise<import("./models").DataGrid>;
        nodeBusinessRules: (compositeEntityNode: any, _: any, context: any) => Promise<import("./models").NodeBusinessRule[]>;
        combinedNodes: (compositeEntityNode: any, _: any, context: any) => Promise<import("./models").CompositeEntityNode[]>;
        children: (compositeEntityNode: any, _: any, context: any) => Promise<import("./models").CompositeEntityNode[]>;
    };
    UILayout: {
        uICards: (uILayout: any, _: any, context: any) => Promise<import("./models").UiCard[]>;
    };
    UiCard: {
        uITabs: (uICard: any, _: any, context: any) => Promise<import("./models").UiTab[]>;
    };
    DataSetRel: {
        logicalEntity: (dataSetRel: any, _: any, context: any) => Promise<import("./models").LogicalEntity[]>;
        dataSetRelProperties: (dataSetRel: any, _: any, context: any) => Promise<import("./models").DataSetRelProperty[]>;
    };
    DataSetRelProperty: {
        logicalColumns: (dataSetRelProperty: any, _: any, context: any) => Promise<import("./models").LogicalColumn[]>;
    };
    LogicalColumn: {
        standardValidations: (logicalColumn: any, _: any, context: any) => Promise<import("./models").StandardValidation[]>;
        databaseValidations: (logicalColumn: any, _: any, context: any) => Promise<import("./models").DatabaseValidation[]>;
        sourceColumn: (logicalColumn: any, _: any, context: any) => Promise<import("./models").LogicalColumn>;
    };
    MenuGroup: {
        menus: (menuGroup: any, _: any, context: any) => Promise<import("./models").Menu[]>;
    };
    Menu: {
        menus: (menu: any, _: any, context: any) => Promise<import("./models").Menu[]>;
    };
};
export default _default;
//# sourceMappingURL=resolvers.d.ts.map