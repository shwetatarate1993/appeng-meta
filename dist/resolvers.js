"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const resolvers_1 = require("./card/resolvers");
const resolvers_2 = require("./cardgroup/resolvers");
const resolvers_3 = require("./datasource/resolvers");
const resolvers_4 = require("./button/resolvers");
const resolvers_5 = require("./form/resolvers");
const resolvers_6 = require("./formfield/resolvers");
const resolvers_7 = require("./compositeentity/resolvers");
const resolvers_8 = require("./logicalcolumn/resolvers");
const resolvers_9 = require("./physicalcolumn/resolvers");
const resolvers_10 = require("./action/resolvers");
const resolvers_11 = require("./menu/resolvers");
const resolvers_12 = require("./menubutton/resolvers");
const resolvers_13 = require("./formgroup/resolvers");
const resolvers_14 = require("./datagridcolumn/resolvers");
const resolvers_15 = require("./uicard/resolvers");
const resolvers_16 = require("./dboperation/resolvers");
const resolvers_17 = require("./datasetrel/resolvers");
const resolvers_18 = require("./buttonpanel/resolvers");
const resolvers_19 = require("./language/resolvers");
const resolvers_20 = require("./compositeentitynode/resolvers");
const resolvers_21 = require("./nodebusinessrule/resolvers");
const resolvers_22 = require("./auditgrid/resolvers");
const resolvers_23 = require("./datagrid/resolvers");
const resolvers_24 = require("./columndatapreprocessor/resolvers");
const resolvers_25 = require("./uitab/resolvers");
const resolvers_26 = require("./menugroup/resolvers");
const resolvers_27 = require("./logicalentity/resolvers");
const resolvers_28 = require("./physicalentity/resolvers");
const resolvers_29 = require("./standardvalidation/resolvers");
const resolvers_30 = require("./databasevalidation/resolvers");
const resolvers_31 = require("./auditentity/resolvers");
const resolvers_32 = require("./formdbvalidation/resolvers");
const resolvers_33 = require("./customformvalidation/resolvers");
const resolvers_34 = require("./logicalentityoperation/resolvers");
const resolvers_35 = require("./formsection/resolvers");
const resolvers_36 = require("./uilayout/resolvers");
const resolvers_37 = require("./parentgridheader/resolvers");
const resolvers_38 = require("./datasetrelproperty/resolvers");
const resolvers_39 = require("./eventlistener/resolvers");
const resolvers_40 = require("./rootcompositeentitynode/resolvers");
const resolvers_41 = require("./database/resolvers");
exports.default = {
    Query: Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign({}, resolvers_1.Query), resolvers_2.Query), resolvers_3.Query), resolvers_4.Query), resolvers_5.Query), resolvers_6.Query), resolvers_7.Query), resolvers_8.Query), resolvers_9.Query), resolvers_10.Query), resolvers_11.Query), resolvers_12.Query), resolvers_13.Query), resolvers_14.Query), resolvers_15.Query), resolvers_16.Query), resolvers_17.Query), resolvers_18.Query), resolvers_19.Query), resolvers_20.Query), resolvers_21.Query), resolvers_22.Query), resolvers_23.Query), resolvers_24.Query), resolvers_25.Query), resolvers_26.Query), resolvers_27.Query), resolvers_28.Query), resolvers_29.Query), resolvers_30.Query), resolvers_31.Query), resolvers_32.Query), resolvers_33.Query), resolvers_34.Query), resolvers_35.Query), resolvers_36.Query), resolvers_37.Query), resolvers_38.Query), resolvers_39.Query), resolvers_40.Query), resolvers_41.Query),
    Mutation: Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign({}, resolvers_1.Mutation), resolvers_2.Mutation), resolvers_3.Mutation), resolvers_4.Mutation), resolvers_5.Mutation), resolvers_6.Mutation), resolvers_7.Mutation), resolvers_8.Mutation), resolvers_9.Mutation), resolvers_10.Mutation), resolvers_11.Mutation), resolvers_12.Mutation), resolvers_13.Mutation), resolvers_14.Mutation), resolvers_15.Mutation), resolvers_16.Mutation), resolvers_17.Mutation), resolvers_18.Mutation), resolvers_19.Mutation), resolvers_20.Mutation), resolvers_21.Mutation), resolvers_22.Mutation), resolvers_23.Mutation), resolvers_24.Mutation), resolvers_25.Mutation), resolvers_26.Mutation), resolvers_27.Mutation), resolvers_28.Mutation), resolvers_29.Mutation), resolvers_30.Mutation), resolvers_31.Mutation), resolvers_32.Mutation), resolvers_33.Mutation), resolvers_34.Mutation), resolvers_35.Mutation), resolvers_36.Mutation), resolvers_37.Mutation), resolvers_38.Mutation), resolvers_39.Mutation), resolvers_40.Mutation), resolvers_41.Mutation),
    CardGroup: resolvers_2.CardGroup,
    Form: resolvers_5.Form,
    FormSection: resolvers_35.FormSection,
    FormField: resolvers_6.FormField,
    ButtonPanel: resolvers_18.ButtonPanel,
    LogicalEntity: resolvers_27.LogicalEntity,
    PhysicalEntity: resolvers_28.PhysicalEntity,
    AuditEntity: resolvers_31.AuditEntity,
    DataGrid: resolvers_23.DataGrid,
    DataGridColumn: resolvers_14.DataGridColumn,
    ParentGridHeader: resolvers_37.ParentGridHeader,
    CompositeEntity: resolvers_7.CompositeEntity,
    RootCompositeEntityNode: resolvers_40.RootCompositeEntityNode,
    FormGroup: resolvers_13.FormGroup,
    CompositeEntityNode: resolvers_20.CompositeEntityNode,
    UILayout: resolvers_36.UILayout,
    UiCard: resolvers_15.UiCard,
    DataSetRel: resolvers_17.DataSetRel,
    DataSetRelProperty: resolvers_38.DataSetRelProperty,
    LogicalColumn: resolvers_8.LogicalColumn,
    MenuGroup: resolvers_26.MenuGroup,
    Menu: resolvers_11.Menu,
};
//# sourceMappingURL=resolvers.js.map