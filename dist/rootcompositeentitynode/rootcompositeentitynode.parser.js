"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseRootCompositeEntityNodeToConfig = (rootCompositeEntityNode) => {
    const createdAt = rootCompositeEntityNode.creationDate;
    const createdBy = rootCompositeEntityNode.createdBy;
    const updatedBy = rootCompositeEntityNode.updatedBy;
    const updatedAt = rootCompositeEntityNode.updationDate;
    const property = [];
    let itemId;
    if (rootCompositeEntityNode.configObjectId !== undefined && rootCompositeEntityNode.configObjectId !== ''
        && rootCompositeEntityNode.configObjectId !== null) {
        itemId = rootCompositeEntityNode.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    // setting default value as false
    if (rootCompositeEntityNode.displayNodeName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('DISPLAY_NODE_NAME', rootCompositeEntityNode.displayNodeName, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (rootCompositeEntityNode.graphTemplate !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('GRAPH_TEMPLATE', rootCompositeEntityNode.graphTemplate, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (rootCompositeEntityNode.order !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ORDER', rootCompositeEntityNode.order, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, rootCompositeEntityNode.name, configitemtype_enum_1.ConfigItemTypes.COMPOSITEENTITYNODE, rootCompositeEntityNode.projectId, createdBy, rootCompositeEntityNode.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const rootCompositeEntityNodePrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(rootCompositeEntityNode.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(rootCompositeEntityNode.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(rootCompositeEntityNode.privileges, rootCompositeEntityNodePrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, rootCompositeEntityNodePrivileges);
};
exports.parseConfigToRootCompositeEntityNode = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.RootCompositeEntityNode(details.get('DISPLAY_NODE_NAME'), details.get('GRAPH_TEMPLATE'), details.get('ORDER'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=rootcompositeentitynode.parser.js.map