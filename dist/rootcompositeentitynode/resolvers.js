"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const datagrid_parser_1 = require("../datagrid/datagrid.parser");
const form_parser_1 = require("../form/form.parser");
const logicalentity_parser_1 = require("../logicalentity/logicalentity.parser");
const nodebusinessrule_parser_1 = require("../nodebusinessrule/nodebusinessrule.parser");
const physicalentity_parser_1 = require("../physicalentity/physicalentity.parser");
const rootcompositeentitynode_parser_1 = require("./rootcompositeentitynode.parser");
exports.Query = {
    RootCompositeEntityNode: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, rootcompositeentitynode_parser_1.parseConfigToRootCompositeEntityNode),
};
exports.RootCompositeEntityNode = {
    physicalEntities: (rootCompositeEntityNode, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(rootCompositeEntityNode.configObjectId, relationtype_enum_1.RelationType.ROOTCOMPOSITEENTITYNODE_PHYSICALENTITY, physicalentity_parser_1.parseConfigToPhysicalEntity),
    entity: (rootCompositeEntityNode, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(rootCompositeEntityNode.configObjectId, relationtype_enum_1.RelationType.ROOTCOMPOSITEENTITYNODE_CHILDLOGICALENTITY, logicalentity_parser_1.parseConfigToLogicalEntity),
    insertForm: (rootCompositeEntityNode, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(rootCompositeEntityNode.configObjectId, relationtype_enum_1.RelationType.ROOTCOMPOSITEENTITYNODE_INSERTFORM, form_parser_1.parseConfigToForm),
    editForm: (rootCompositeEntityNode, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(rootCompositeEntityNode.configObjectId, relationtype_enum_1.RelationType.ROOTCOMPOSITEENTITYNODE_EDITFORM, form_parser_1.parseConfigToForm),
    dataGrid: (rootCompositeEntityNode, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(rootCompositeEntityNode.configObjectId, relationtype_enum_1.RelationType.ROOTCOMPOSITEENTITYNODE_DATAGRID, datagrid_parser_1.parseConfigToDataGrid),
    nodeBusinessRules: (rootCompositeEntityNode, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(rootCompositeEntityNode.configObjectId, relationtype_enum_1.RelationType.ROOTCOMPOSITEENTITYNODE_NODEBUSINESSRULE, nodebusinessrule_parser_1.parseConfigToNodeBusinessRule),
};
exports.Mutation = {
    createRootCompositeEntityNode: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, rootcompositeentitynode_parser_1.parseRootCompositeEntityNodeToConfig, rootcompositeentitynode_parser_1.parseConfigToRootCompositeEntityNode);
    },
};
//# sourceMappingURL=resolvers.js.map