export declare const Query: {
    RootCompositeEntityNode: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").RootCompositeEntityNode>;
};
export declare const RootCompositeEntityNode: {
    physicalEntities: (rootCompositeEntityNode: any, _: any, context: any) => Promise<import("../models").PhysicalEntity[]>;
    entity: (rootCompositeEntityNode: any, _: any, context: any) => Promise<import("../models").LogicalEntity[]>;
    insertForm: (rootCompositeEntityNode: any, _: any, context: any) => Promise<import("../models").Form[]>;
    editForm: (rootCompositeEntityNode: any, _: any, context: any) => Promise<import("../models").Form[]>;
    dataGrid: (rootCompositeEntityNode: any, _: any, context: any) => Promise<import("../models").DataGrid[]>;
    nodeBusinessRules: (rootCompositeEntityNode: any, _: any, context: any) => Promise<import("../models").NodeBusinessRule[]>;
};
export declare const Mutation: {
    createRootCompositeEntityNode: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map