"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        propertyId: '24342c8e-68f3-48d7-a090-e951fb41a018',
        propertyName: 'GRAPH_TEMPLATE',
        propertyValue: 'Default',
        itemId: 'b46bd5d6-f213-48b0-b4e5-a939843d9adb',
        createdBy: 'SYSTEM',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '7d25b86e-ec35-4c7f-90d2-0e50d658cb20',
        propertyName: 'DISPLAY_NODE_NAME',
        propertyValue: 'en-!lAnGuAge!-Employee Info-!sEpArAToR!-gu-!lAnGuAge!-કર્મચારીની માહિતી',
        itemId: 'b46bd5d6-f213-48b0-b4e5-a939843d9adb',
        createdBy: 'SYSTEM',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '81d0188d-baff-4beb-9017-cf2df068ff50',
        propertyName: 'ORDER',
        propertyValue: 5,
        itemId: 'b46bd5d6-f213-48b0-b4e5-a939843d9adb',
        createdBy: 'SYSTEM',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
];
//# sourceMappingURL=configitem.property.rootcompositeentitynode.js.map