"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var rootcompositeentitynode_1 = require("./rootcompositeentitynode");
exports.rootCompositeEntityNodeObjectMockData = rootcompositeentitynode_1.default;
var configitem_rootcompositeentitynode_1 = require("./configitem.rootcompositeentitynode");
exports.rootCompositeEntityNodeConfigitemMockData = configitem_rootcompositeentitynode_1.default;
var configitem_property_rootcompositeentitynode_1 = require("./configitem.property.rootcompositeentitynode");
exports.rootCompositeEntityNodePropertyMockData = configitem_property_rootcompositeentitynode_1.default;
var configitem_relation_rootcompositeentitynode_1 = require("./configitem.relation.rootcompositeentitynode");
exports.rootCompositeEntityNodeRelationMockData = configitem_relation_rootcompositeentitynode_1.default;
var configitem_privileges_rootcompositeentitynode_1 = require("./configitem.privileges.rootcompositeentitynode");
exports.rootCompositeEntityNodePrivilegesMockData = configitem_privileges_rootcompositeentitynode_1.default;
//# sourceMappingURL=index.js.map