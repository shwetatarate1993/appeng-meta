"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const rootcompositeentitynode_parser_1 = require("../rootcompositeentitynode.parser");
const mockdata_1 = require("./mockdata");
test('parser RootCompositeEntityNode to config', () => {
    const rootCompositeEntityNode = models_1.RootCompositeEntityNode.deserialize(mockdata_1.rootCompositeEntityNodeObjectMockData[0]);
    const result = rootcompositeentitynode_parser_1.parseRootCompositeEntityNodeToConfig(rootCompositeEntityNode);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItem.configObjectType).toEqual('CompositeEntityNode');
});
test('parser config to RootCompositeEntityNode', () => {
    const result = rootcompositeentitynode_parser_1.parseConfigToRootCompositeEntityNode(mockdata_1.rootCompositeEntityNodeConfigitemMockData[0], mockdata_1.rootCompositeEntityNodePropertyMockData, mockdata_1.rootCompositeEntityNodeRelationMockData, mockdata_1.rootCompositeEntityNodePrivilegesMockData);
    expect(result).toHaveProperty('displayNodeName');
    expect(result).toHaveProperty('order', 5);
    expect(result).toHaveProperty('graphTemplate', 'Default');
});
//# sourceMappingURL=rootcompositeentitynode.parser.test.js.map