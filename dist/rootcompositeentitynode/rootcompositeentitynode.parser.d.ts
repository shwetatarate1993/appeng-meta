import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { RootCompositeEntityNode } from '../models';
export declare const parseRootCompositeEntityNodeToConfig: (rootCompositeEntityNode: RootCompositeEntityNode) => ConfigMetadata;
export declare const parseConfigToRootCompositeEntityNode: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => RootCompositeEntityNode;
//# sourceMappingURL=rootcompositeentitynode.parser.d.ts.map