"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseUiTabToConfig = (uiTab) => {
    const createdAt = uiTab.creationDate;
    const createdBy = uiTab.createdBy;
    const updatedBy = uiTab.updatedBy;
    const updatedAt = uiTab.updationDate;
    const property = [];
    let itemId;
    if (uiTab.configObjectId !== undefined && uiTab.configObjectId !== ''
        && uiTab.configObjectId !== null) {
        itemId = uiTab.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (uiTab.order !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ORDER', uiTab.order, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (uiTab.displayName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('DISPLAY_NAME', uiTab.displayName, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (uiTab.viewType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('VIEWTYPE', uiTab.viewType, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, uiTab.name, configitemtype_enum_1.ConfigItemTypes.UITAB, uiTab.projectId, createdBy, uiTab.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const uiTabPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(uiTab.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(uiTab.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(uiTab.privileges, uiTabPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, uiTabPrivileges);
};
exports.parseConfigToUiTab = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.UiTab(details.get('ORDER'), details.get('DISPLAY_NAME'), details.get('VIEWTYPE'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=uitab.parser.js.map