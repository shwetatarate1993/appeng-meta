"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const uitab_parser_1 = require("./uitab.parser");
exports.Query = {
    UiTab: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, uitab_parser_1.parseConfigToUiTab),
};
exports.Mutation = {
    createUiTab: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, uitab_parser_1.parseUiTabToConfig, uitab_parser_1.parseConfigToUiTab);
    },
};
//# sourceMappingURL=resolvers.js.map