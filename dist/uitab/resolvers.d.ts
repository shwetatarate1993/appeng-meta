export declare const Query: {
    UiTab: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").UiTab>;
};
export declare const Mutation: {
    createUiTab: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map