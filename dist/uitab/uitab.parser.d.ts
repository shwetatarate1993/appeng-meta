import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { UiTab } from '../models';
export declare const parseUiTabToConfig: (uiTab: UiTab) => ConfigMetadata;
export declare const parseConfigToUiTab: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => UiTab;
//# sourceMappingURL=uitab.parser.d.ts.map