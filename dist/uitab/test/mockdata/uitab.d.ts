declare const _default: {
    configObjectId: string;
    configObjectType: string;
    name: string;
    order: number;
    projectId: number;
    displayName: string;
    viewType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: any;
    creationDate: string;
    updatedBy: any;
    updationDate: any;
    deletionDate: any;
    privileges: any[];
    childRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        childItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: string;
        deletionDate: string;
    }[];
    parentRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        childItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: string;
        deletionDate: string;
    }[];
}[];
export default _default;
//# sourceMappingURL=uitab.d.ts.map