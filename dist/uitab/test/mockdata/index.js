"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var uitab_1 = require("./uitab");
exports.uitabMockData = uitab_1.default;
var configitem_uitab_1 = require("./configitem.uitab");
exports.configItemUiTabMockData = configitem_uitab_1.default;
var configitem_privileges_uitab_1 = require("./configitem.privileges.uitab");
exports.configItemPrivilegesUiTabMockData = configitem_privileges_uitab_1.default;
var configitem_relation_uitab_1 = require("./configitem.relation.uitab");
exports.configItemRelationsUiTabMockData = configitem_relation_uitab_1.default;
var configitem_property_uitab_1 = require("./configitem.property.uitab");
exports.configItemPropertyUiTabMockData = configitem_property_uitab_1.default;
//# sourceMappingURL=index.js.map