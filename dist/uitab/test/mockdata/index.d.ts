export { default as uitabMockData } from './uitab';
export { default as configItemUiTabMockData } from './configitem.uitab';
export { default as configItemPrivilegesUiTabMockData } from './configitem.privileges.uitab';
export { default as configItemRelationsUiTabMockData } from './configitem.relation.uitab';
export { default as configItemPropertyUiTabMockData } from './configitem.property.uitab';
//# sourceMappingURL=index.d.ts.map