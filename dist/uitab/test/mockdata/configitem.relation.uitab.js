"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        relationId: '2036ab3e-0c07-42f1-85ef-7731bdc1599d',
        relationType: 'UITab_Form',
        parentItemId: '1263587b-b398-4ad5-a3a1-717d0063e0bd',
        childItemId: '1183a076-a0eb-4a81-8203-a4a00ca4e2df',
        createdBy: '1111',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
    },
    {
        relationId: '5b36631b-2fa8-4116-9f6b-a963ba6b49c7',
        relationType: 'UITab_CompositeEntityNode',
        parentItemId: '1263587b-b398-4ad5-a3a1-717d0063e0bd',
        childItemId: '453ffb38-8b42-44c0-911f-c5b0cc99c630',
        createdBy: '1111',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
    },
    {
        relationId: 'b3a3619c-9526-4c47-bfc6-0b240a5cfb25',
        relationType: 'UICard_UITab',
        parentItemId: '23e43852-9ba7-4d77-8da0-197bfa19fe69',
        childItemId: '1263587b-b398-4ad5-a3a1-717d0063e0bd',
        createdBy: '1111',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
    },
];
//# sourceMappingURL=configitem.relation.uitab.js.map