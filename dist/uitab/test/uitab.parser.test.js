"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const uitab_parser_1 = require("../uitab.parser");
const mockdata_1 = require("./mockdata");
test('parser uitab object to config', () => {
    const uitab = models_1.UiTab.deserialize(mockdata_1.uitabMockData[0]);
    const result = uitab_parser_1.parseUiTabToConfig(uitab);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItem.configObjectType).toEqual('UITab');
});
test('parser config to uitab object', () => {
    const result = uitab_parser_1.parseConfigToUiTab(mockdata_1.configItemUiTabMockData[0], mockdata_1.configItemPropertyUiTabMockData, mockdata_1.configItemRelationsUiTabMockData, mockdata_1.configItemPrivilegesUiTabMockData);
    expect(result).toHaveProperty('order', '5');
    expect(result).toHaveProperty('displayName', 'Modal');
    expect(result).toHaveProperty('viewType', 'Form');
    expect(result.privileges.length === 0).toBe(true);
    expect(result.parentRelations[0].relationType === 'UICard_UITab').toBe(true);
    expect(result.childRelations[0].relationType === 'UITab_Form').toBe(true);
    expect(result.childRelations[1].relationType === 'UITab_CompositeEntityNode').toBe(true);
});
//# sourceMappingURL=uitab.parser.test.js.map