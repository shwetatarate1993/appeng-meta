export { default as customFormValidationMockData } from './customformvalidation';
export { default as customFormValidationConfigitemMockData } from './configitem.customformvalidation';
export { default as customFormValidationPropertyMockData } from './configitem.property.customformvalidation';
export { default as customFormValidationConfigRelationsMockData } from './configitem.relation.customformvalidation';
export { default as customFormValidationConfigPrivilegesMockData } from './configitem.privileges.customformvalidation';
//# sourceMappingURL=index.d.ts.map