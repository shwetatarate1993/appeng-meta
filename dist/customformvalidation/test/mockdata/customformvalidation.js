"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: 'a450972d-9f51-4f8d-835f-a557f98eb7cd',
        configObjectType: 'CustomFormValidationObject',
        name: 'test',
        createdBy: '1121',
        isDeleted: 0,
        itemDescription: null,
        creationDate: null,
        projectId: 1,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
        validationMsg: 'test',
        validatorBeanId: 'test',
        validationType: 'test',
        privileges: [],
        parentRelations: [
            {
                relationId: '80fdb0ed-8a7c-42dc-8b58-4d4cb3703192',
                relationType: 'Form_ValidationObject',
                parentItemId: 'd895dd58-aeb3-48ab-b5de-b5fd3bac9f5d',
                childItemId: 'a450972d-9f51-4f8d-835f-a557f98eb7cd',
                createdBy: '1115',
                isDeleted: 0,
                creationDate: null,
                updatedBy: '1115',
                updationDate: null,
                deletionDate: null,
            },
        ],
    },
];
//# sourceMappingURL=customformvalidation.js.map