"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var customformvalidation_1 = require("./customformvalidation");
exports.customFormValidationMockData = customformvalidation_1.default;
var configitem_customformvalidation_1 = require("./configitem.customformvalidation");
exports.customFormValidationConfigitemMockData = configitem_customformvalidation_1.default;
var configitem_property_customformvalidation_1 = require("./configitem.property.customformvalidation");
exports.customFormValidationPropertyMockData = configitem_property_customformvalidation_1.default;
var configitem_relation_customformvalidation_1 = require("./configitem.relation.customformvalidation");
exports.customFormValidationConfigRelationsMockData = configitem_relation_customformvalidation_1.default;
var configitem_privileges_customformvalidation_1 = require("./configitem.privileges.customformvalidation");
exports.customFormValidationConfigPrivilegesMockData = configitem_privileges_customformvalidation_1.default;
//# sourceMappingURL=index.js.map