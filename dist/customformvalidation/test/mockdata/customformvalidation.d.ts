declare const _default: {
    configObjectId: string;
    configObjectType: string;
    name: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: any;
    creationDate: any;
    projectId: number;
    updatedBy: string;
    updationDate: any;
    deletionDate: any;
    validationMsg: string;
    validatorBeanId: string;
    validationType: string;
    privileges: any[];
    parentRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        childItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: any;
        updatedBy: string;
        updationDate: any;
        deletionDate: any;
    }[];
}[];
export default _default;
//# sourceMappingURL=customformvalidation.d.ts.map