"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const customformvalidation_parser_1 = require("../customformvalidation.parser");
const mockdata_1 = require("./mockdata");
test('parser customformvalidation to config', () => {
    const customFormValidation = models_1.CustomFormValidation.deserialize(mockdata_1.customFormValidationMockData[0]);
    const result = customformvalidation_parser_1.parseCustomFormValidationToConfig(customFormValidation);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItem.configObjectType).toEqual('CustomFormValidationObject');
    expect(result.configItemRelation.length === 1).toBe(true);
    expect(result.configItemPrivilege.length === 0).toBe(true);
    expect(result.configItemProperty.length === 8).toBe(true);
});
test('parser config to customformvalidation ', () => {
    const result = customformvalidation_parser_1.parseConfigToCustomFormValidation(mockdata_1.customFormValidationConfigitemMockData[0], mockdata_1.customFormValidationPropertyMockData, mockdata_1.customFormValidationConfigRelationsMockData, mockdata_1.customFormValidationConfigPrivilegesMockData);
    expect(result).toHaveProperty('validationMsg');
    expect(result).toHaveProperty('validationMsg', 'test');
    expect(result.privileges.length === 0).toBe(true);
});
//# sourceMappingURL=customformvalidation.parser.test.js.map