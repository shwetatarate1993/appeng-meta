export declare const Query: {
    CustomFormValidation: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").CustomFormValidation>;
};
export declare const Mutation: {
    createCustomFormValidation: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map