"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const customformvalidation_parser_1 = require("./customformvalidation.parser");
exports.Query = {
    CustomFormValidation: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, customformvalidation_parser_1.parseConfigToCustomFormValidation),
};
exports.Mutation = {
    createCustomFormValidation: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, customformvalidation_parser_1.parseCustomFormValidationToConfig, customformvalidation_parser_1.parseConfigToCustomFormValidation);
    },
};
//# sourceMappingURL=resolvers.js.map