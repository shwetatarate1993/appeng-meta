import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { CustomFormValidation } from '../models';
export declare const parseCustomFormValidationToConfig: (customFormValidation: CustomFormValidation) => ConfigMetadata;
export declare const parseConfigToCustomFormValidation: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => CustomFormValidation;
//# sourceMappingURL=customformvalidation.parser.d.ts.map