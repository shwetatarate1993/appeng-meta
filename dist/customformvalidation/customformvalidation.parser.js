"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseCustomFormValidationToConfig = (customFormValidation) => {
    const createdAt = customFormValidation.creationDate;
    const createdBy = customFormValidation.createdBy;
    const updatedBy = customFormValidation.updatedBy;
    const updatedAt = customFormValidation.updationDate;
    const property = [];
    let itemId;
    if (customFormValidation.configObjectId !== undefined &&
        customFormValidation.configObjectId !== '' && customFormValidation.configObjectId !== null) {
        itemId = customFormValidation.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (customFormValidation.validatorBeanId !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'MODE', customFormValidation.mode, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (customFormValidation.validatorBeanId !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'VALIDATOR_BEAN_ID', customFormValidation.validatorBeanId, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (customFormValidation.validationType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'VALIDATION_TYPE', customFormValidation.validationType, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (customFormValidation.validationMsg !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'VALIDATION_MSG', customFormValidation.validationMsg, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'IS_CONDITION_AVAILABLE', customFormValidation.isConditionAvailable !== undefined ?
        (customFormValidation.isConditionAvailable ? '1' : '0') : '0', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    if (customFormValidation.conditionExpression !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'CONDITION_EXPRESSION', customFormValidation.conditionExpression, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (customFormValidation.conditionFields !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'CONDITION_FIELDS', customFormValidation.conditionFields, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (customFormValidation.description !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'DESCRIPTION', customFormValidation.description, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, customFormValidation.name, configitemtype_enum_1.ConfigItemTypes.CUSTOMFORMVALIDATIONOBJECT, customFormValidation.projectId, createdBy, customFormValidation.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const customFormValidationPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(customFormValidation.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(customFormValidation.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(customFormValidation.privileges, customFormValidationPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, customFormValidationPrivileges);
};
exports.parseConfigToCustomFormValidation = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.CustomFormValidation(details.get('MODE'), details.get('VALIDATOR_BEAN_ID'), details.get('VALIDATION_TYPE'), details.get('VALIDATION_MSG'), details.get('IS_CONDITION_AVAILABLE'), details.get('CONDITION_EXPRESSION'), details.get('CONDITION_FIELDS'), details.get('DESCRIPTION'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=customformvalidation.parser.js.map