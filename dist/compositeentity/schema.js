"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = __importDefault(require("../base"));
const common_schema_1 = __importDefault(require("../common.schema"));
const CompositeEntity = `

input CompositeEntityInput {

    graphTemplate: String
    isFormLevelOperationAllowed: Boolean
    isReleaseAreaNotApplicable: Boolean
    logicalEntityId: String
    multiEntityOrder: String
    isMenuRequired: Boolean
    isNodeTreeRequired: Boolean
    rootDataSetId: String
    configObjectId: ID
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilegeInput]
    parentRelations : [ConfigItemRelationInput]
    childRelations : [ConfigItemRelationInput]
}

extend type Query {
    CompositeEntity(id: ID!): CompositeEntity
   }

extend type Mutation {
    createCompositeEntity (input: CompositeEntityInput): CompositeEntity
}

type CompositeEntity {

    graphTemplate: String
    isFormLevelOperationAllowed: Boolean
    isReleaseAreaNotApplicable: Boolean
    logicalEntityId: String
    multiEntityOrder: String
    isMenuRequired: Boolean
    isNodeTreeRequired: Boolean
    rootDataSetId: String
    configObjectId: ID
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilege]
    parentRelations : [ConfigItemRelation]
    childRelations : [ConfigItemRelation]
    rootCompositeEntityNode : CompositeEntityNode
    eventListeners : [EventListener]
    formGroups : [FormGroup]
    compositeEntityNodes : [CompositeEntityNode]
    uILayouts : [UILayout]
}
`;
exports.default = () => [CompositeEntity, base_1.default, common_schema_1.default];
//# sourceMappingURL=schema.js.map