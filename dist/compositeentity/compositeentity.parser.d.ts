import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { CompositeEntity } from '../models';
export declare const parseCompositeEntityToConfig: (compositeEntity: CompositeEntity) => ConfigMetadata;
export declare const parseConfigToCompositeEntity: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => CompositeEntity;
//# sourceMappingURL=compositeentity.parser.d.ts.map