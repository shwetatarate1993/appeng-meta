"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseCompositeEntityToConfig = (compositeEntity) => {
    const createdAt = compositeEntity.creationDate;
    const createdBy = compositeEntity.createdBy;
    const updatedBy = compositeEntity.updatedBy;
    const updatedAt = compositeEntity.updationDate;
    const property = [];
    let itemId;
    if (compositeEntity.configObjectId !== undefined && compositeEntity.configObjectId !== ''
        && compositeEntity.configObjectId !== null) {
        itemId = compositeEntity.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (compositeEntity.graphTemplate !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'GRAPH_TEMPLATE', compositeEntity.graphTemplate, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (compositeEntity.isFormLevelOperationAllowed !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'FORM_LEVEL_OPERATION_ALLOWED', compositeEntity.isFormLevelOperationAllowed !== undefined ?
            (compositeEntity.isFormLevelOperationAllowed ? '1' : '0') : '1', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (compositeEntity.isReleaseAreaNotApplicable !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'RELEASE_AREA_NOT_APPLICABLE', compositeEntity.isReleaseAreaNotApplicable !== undefined ?
            (compositeEntity.isReleaseAreaNotApplicable ? '1' : '0') : '1', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (compositeEntity.logicalEntityId !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'LOGICAL_ENTITY_ID', compositeEntity.logicalEntityId, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (compositeEntity.multiEntityOrder !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'MULTIENTITY_ORDER', compositeEntity.multiEntityOrder, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (compositeEntity.isMenuRequired !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'IS_MENU_REQUIRED', compositeEntity.isMenuRequired !== undefined ? (compositeEntity.isMenuRequired ? '1' : '0') : '0', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (compositeEntity.isNodeTreeRequired !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'IS_NODE_TREE_REQUIRED', compositeEntity.isNodeTreeRequired !== undefined ? (compositeEntity.isNodeTreeRequired ? '1' : '0') : '0', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (compositeEntity.rootDataSetId !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'ROOT_DATASET_ID', compositeEntity.rootDataSetId, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, compositeEntity.name, configitemtype_enum_1.ConfigItemTypes.COMPOSITEENTITY, compositeEntity.projectId, createdBy, compositeEntity.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const compositeEntityPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(compositeEntity.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(compositeEntity.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(compositeEntity.privileges, compositeEntityPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, compositeEntityPrivileges);
};
exports.parseConfigToCompositeEntity = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.CompositeEntity(details.get('GRAPH_TEMPLATE'), details.get('FORM_LEVEL_OPERATION_ALLOWED'), details.get('RELEASE_AREA_NOT_APPLICABLE'), details.get('LOGICAL_ENTITY_ID'), details.get('MULTIENTITY_ORDER'), details.get('IS_MENU_REQUIRED'), details.get('IS_NODE_TREE_REQUIRED'), details.get('ROOT_DATASET_ID'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=compositeentity.parser.js.map