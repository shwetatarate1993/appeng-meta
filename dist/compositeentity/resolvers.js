"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const compositeentitynode_parser_1 = require("../compositeentitynode/compositeentitynode.parser");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const eventlistener_parser_1 = require("../eventlistener/eventlistener.parser");
const formgroup_parser_1 = require("../formgroup/formgroup.parser");
const rootcompositeentitynode_parser_1 = require("../rootcompositeentitynode/rootcompositeentitynode.parser");
const uilayout_parser_1 = require("../uilayout/uilayout.parser");
const compositeentity_parser_1 = require("./compositeentity.parser");
exports.Query = {
    CompositeEntity: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, compositeentity_parser_1.parseConfigToCompositeEntity),
};
exports.CompositeEntity = {
    eventListeners: (compositeEntity, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(compositeEntity.configObjectId, relationtype_enum_1.RelationType.COMPOSITEENTITY_EVENT, eventlistener_parser_1.parseConfigToEventListener),
    rootCompositeEntityNode: async (compositeEntity, _, context) => {
        const nodes = await common_meta_1.configService.fetchConfigsByParentIdAndRelationType(compositeEntity.configObjectId, relationtype_enum_1.RelationType.COMPOSITEENTITY_ROOTCOMPOSITEENTITYNODE, rootcompositeentitynode_parser_1.parseConfigToRootCompositeEntityNode);
        return nodes ? nodes[0] : null;
    },
    formGroups: (compositeEntity, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(compositeEntity.configObjectId, relationtype_enum_1.RelationType.COMPOSITEENTITY_FORMGROUP, formgroup_parser_1.parseConfigToFormGroup),
    compositeEntityNodes: (compositeEntity, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(compositeEntity.configObjectId, relationtype_enum_1.RelationType.COMPOSITEENTITY_COMPOSITEENTITYNODE, compositeentitynode_parser_1.parseConfigToCompositeEntityNode),
    uILayouts: (compositeEntity, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(compositeEntity.configObjectId, relationtype_enum_1.RelationType.COMPOSITEENTITY_UILAYOUT, uilayout_parser_1.parseConfigToUILayout),
};
exports.Mutation = {
    createCompositeEntity: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, compositeentity_parser_1.parseCompositeEntityToConfig, compositeentity_parser_1.parseConfigToCompositeEntity);
    },
};
//# sourceMappingURL=resolvers.js.map