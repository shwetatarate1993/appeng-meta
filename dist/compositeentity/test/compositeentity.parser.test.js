"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const compositeentity_parser_1 = require("../compositeentity.parser");
const mockdata_1 = require("./mockdata");
test('parse CompositeEntity object to config', () => {
    const compositeEntity = models_1.CompositeEntity.deserialize(mockdata_1.compositeEntityMockData[0]);
    expect(compositeEntity.parentRelations.length === 0).toBe(true);
    expect(compositeEntity.childRelations.length === 1).toBe(true);
    expect(compositeEntity.name).toEqual('Building Branch-Employee Info');
    expect(compositeEntity.configObjectType).toEqual('CompositeEntity');
    const result = compositeentity_parser_1.parseCompositeEntityToConfig(compositeEntity);
    expect(result.configItem.configObjectType).toEqual('CompositeEntity');
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
});
test('parse config to CompositeEntity object', () => {
    const result = compositeentity_parser_1.parseConfigToCompositeEntity(mockdata_1.configitemMockDataCompositeEntity[0], mockdata_1.compositeEntityPropertyMockData, mockdata_1.compositeEntityRelationMockData, mockdata_1.compositeEntityPrivilegeMockData);
    expect(result).toHaveProperty('graphTemplate');
    expect(result).toHaveProperty('isMenuRequired', true);
    expect(result).toHaveProperty('isReleaseAreaNotApplicable', true);
    expect(result).toHaveProperty('isFormLevelOperationAllowed', true);
    expect(result).toHaveProperty('rootDataSetId', 'e32836c5-5cb0-459c-9a83-1a1d35d231b3');
    expect(result).toHaveProperty('multiEntityOrder');
    expect(result).toHaveProperty('logicalEntityId', 'e32836c5-5cb0-459c-9a83-1a1d35d231b3');
    expect(result.privileges.length === 0).toBe(true);
    expect(result.parentRelations.length === 0).toBe(true);
    expect(result.childRelations.length === 1).toBe(true);
    expect(result.childRelations[0].relationType === 'CompositeEntity_RootCompositeEntityNode').toBe(true);
});
//# sourceMappingURL=compositeentity.parser.test.js.map