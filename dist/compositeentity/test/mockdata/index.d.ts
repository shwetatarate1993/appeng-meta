export { default as compositeEntityMockData } from './compositeentity';
export { default as configitemMockDataCompositeEntity } from './configitem.compositeentity';
export { default as compositeEntityPropertyMockData } from './configitem.property.compositeentity';
export { default as compositeEntityRelationMockData } from './configitem.relation.compositeentity';
export { default as compositeEntityPrivilegeMockData } from './configitem.privilege.compositeentity';
//# sourceMappingURL=index.d.ts.map