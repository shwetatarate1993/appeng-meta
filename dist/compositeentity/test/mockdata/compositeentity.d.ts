declare const _default: {
    configObjectId: string;
    configObjectType: string;
    name: string;
    graphTemplate: any;
    isFormLevelOperationAllowed: boolean;
    isReleaseAreaNotApplicable: boolean;
    isMenuRequired: boolean;
    logicalEntityId: string;
    multiEntityOrder: any;
    rootDataSetId: string;
    privileges: any[];
    parentRelations: any[];
    childRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        childItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: any;
        updatedBy: string;
        updationDate: any;
        deletionDate: any;
    }[];
}[];
export default _default;
//# sourceMappingURL=compositeentity.d.ts.map