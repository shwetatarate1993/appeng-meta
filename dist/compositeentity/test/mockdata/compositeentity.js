"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: '04f76e04-b658-4f33-96a0-89d772eee2d3',
        configObjectType: 'CompositeEntity',
        name: 'Building Branch-Employee Info',
        graphTemplate: null,
        isFormLevelOperationAllowed: true,
        isReleaseAreaNotApplicable: true,
        isMenuRequired: true,
        logicalEntityId: 'e32836c5-5cb0-459c-9a83-1a1d35d231b3',
        multiEntityOrder: null,
        rootDataSetId: 'e32836c5-5cb0-459c-9a83-1a1d35d231b3',
        privileges: [],
        parentRelations: [],
        childRelations: [
            {
                relationId: '202cc928-07e4-485f-b568-b141046a7a2e',
                relationType: 'CompositeEntity_RootCompositeEntityNode',
                parentItemId: '04f76e04-b658-4f33-96a0-89d772eee2d3',
                childItemId: 'b46bd5d6-f213-48b0-b4e5-a939843d9adb',
                createdBy: '1135',
                isDeleted: 0,
                creationDate: null,
                updatedBy: '1135',
                updationDate: null,
                deletionDate: null,
            },
        ],
    },
];
//# sourceMappingURL=compositeentity.js.map