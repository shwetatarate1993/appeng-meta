"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var compositeentity_1 = require("./compositeentity");
exports.compositeEntityMockData = compositeentity_1.default;
var configitem_compositeentity_1 = require("./configitem.compositeentity");
exports.configitemMockDataCompositeEntity = configitem_compositeentity_1.default;
var configitem_property_compositeentity_1 = require("./configitem.property.compositeentity");
exports.compositeEntityPropertyMockData = configitem_property_compositeentity_1.default;
var configitem_relation_compositeentity_1 = require("./configitem.relation.compositeentity");
exports.compositeEntityRelationMockData = configitem_relation_compositeentity_1.default;
var configitem_privilege_compositeentity_1 = require("./configitem.privilege.compositeentity");
exports.compositeEntityPrivilegeMockData = configitem_privilege_compositeentity_1.default;
//# sourceMappingURL=index.js.map