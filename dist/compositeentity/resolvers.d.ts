export declare const Query: {
    CompositeEntity: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").CompositeEntity>;
};
export declare const CompositeEntity: {
    eventListeners: (compositeEntity: any, _: any, context: any) => Promise<import("../models").EventListener[]>;
    rootCompositeEntityNode: (compositeEntity: any, _: any, context: any) => Promise<import("../models").RootCompositeEntityNode>;
    formGroups: (compositeEntity: any, _: any, context: any) => Promise<import("../models").FormGroup[]>;
    compositeEntityNodes: (compositeEntity: any, _: any, context: any) => Promise<import("../models").CompositeEntityNode[]>;
    uILayouts: (compositeEntity: any, _: any, context: any) => Promise<import("../models").UILayout[]>;
};
export declare const Mutation: {
    createCompositeEntity: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map