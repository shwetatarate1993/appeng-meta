"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ConfigItemTypes;
(function (ConfigItemTypes) {
    ConfigItemTypes["MENU"] = "Menu";
    ConfigItemTypes["MENUBUTTON"] = "MenuButton";
    ConfigItemTypes["DATAGRIDCOLUMN"] = "DataGridColumn";
    ConfigItemTypes["UICARD"] = "UICard";
    ConfigItemTypes["DATASETREL"] = "DataSetRel";
    ConfigItemTypes["FORMGROUP"] = "FormGroup";
    ConfigItemTypes["DBOPERATION"] = "DBOperation";
    ConfigItemTypes["CARD"] = "Card";
    ConfigItemTypes["CARDGROUP"] = "CardGroup";
    ConfigItemTypes["COMPOSITEENTITY"] = "CompositeEntity";
    ConfigItemTypes["DATASOURCE"] = "DataSource";
    ConfigItemTypes["BUTTON"] = "Button";
    ConfigItemTypes["FORM"] = "Form";
    ConfigItemTypes["FORMFIELD"] = "FormField";
    ConfigItemTypes["ACTION"] = "Action";
    ConfigItemTypes["ENTITYCOLUMN"] = "EntityColumn";
    ConfigItemTypes["MENUGROUP"] = "MenuGroup";
    ConfigItemTypes["LOGICALENTITY"] = "LogicalEntity";
    ConfigItemTypes["PHYSICALENTITY"] = "PhysicalEntity";
    ConfigItemTypes["STANDARDVALIDATION"] = "StandardValidation";
    ConfigItemTypes["DATABASEVALIDATION"] = "DatabaseValidation";
    ConfigItemTypes["FORMDBVALIDATIONOBJECT"] = "FormDBValidationObject";
    ConfigItemTypes["AUDITENTITY"] = "AuditEntity";
    ConfigItemTypes["CUSTOMFORMVALIDATIONOBJECT"] = "CustomFormValidationObject";
    ConfigItemTypes["BUTTONPANEL"] = "ButtonPanel";
    ConfigItemTypes["COMPOSITEENTITYNODE"] = "CompositeEntityNode";
    ConfigItemTypes["NODEBUSINESSRULE"] = "NodeBusinessRule";
    ConfigItemTypes["AUDITGRID"] = "AuditGrid";
    ConfigItemTypes["LANGUAGE"] = "Language";
    ConfigItemTypes["LOGICALENTITYOPERATION"] = "LogicalEntityOperation";
    ConfigItemTypes["UILAYOUT"] = "UILayout";
    ConfigItemTypes["PARENTGRIDHEADER"] = "ParentGridHeader";
    ConfigItemTypes["DATASETRELPROPERTY"] = "DataSetRelProperty";
    ConfigItemTypes["FORMSECTION"] = "FormSection";
    ConfigItemTypes["UITAB"] = "UITab";
    ConfigItemTypes["DATAGRID"] = "DataGrid";
    ConfigItemTypes["EVENTLISTENER"] = "EventListener";
    ConfigItemTypes["SCHEDULER"] = "Scheduler";
    ConfigItemTypes["SCENARIOOBJECT"] = "ScenarioObject";
    ConfigItemTypes["REPORTOBJECT"] = "ReportObject";
    ConfigItemTypes["SKINCONFIG"] = "SkinConfig";
    ConfigItemTypes["GLOBALCONFIG"] = "GlobalConfig";
    ConfigItemTypes["MODULE"] = "Module";
    ConfigItemTypes["COLUMNDATAPREPROCESSOR"] = "ColumnDataPreprocessor";
})(ConfigItemTypes = exports.ConfigItemTypes || (exports.ConfigItemTypes = {}));
var NodeDisplayType;
(function (NodeDisplayType) {
    NodeDisplayType["COMBINED"] = "1";
    NodeDisplayType["TREE"] = "0";
})(NodeDisplayType = exports.NodeDisplayType || (exports.NodeDisplayType = {}));
var DisplayTypeProp;
(function (DisplayTypeProp) {
    DisplayTypeProp["ADDTOPARENTDISPLAY"] = "ADDTO_PARENT_DISPLAY";
    DisplayTypeProp["ADDEDHIDDEN"] = "ADDED_HIDDEN";
})(DisplayTypeProp = exports.DisplayTypeProp || (exports.DisplayTypeProp = {}));
//# sourceMappingURL=configitemtype.enum.js.map