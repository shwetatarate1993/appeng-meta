"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const card_parser_1 = require("../card.parser");
const mockdata_1 = require("./mockdata");
test('parser card object to config', () => {
    const card = models_1.Card.deserialize(mockdata_1.cardMockData[0]);
    const result = card_parser_1.parseCardObjectToConfig(card);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
});
test('parser config to Card object', () => {
    const result = card_parser_1.parseConfigToCardObject(mockdata_1.configitemMockDataCard[0], mockdata_1.cardPropertyMockData, mockdata_1.configRelationsMockData, mockdata_1.configPrivilegesMockData);
    expect(result).toHaveProperty('dataGridId');
    expect(result).toHaveProperty('cardType', 'Grid');
    expect(result.privileges.length === 4).toBe(true);
});
//# sourceMappingURL=card.parser.test.js.map