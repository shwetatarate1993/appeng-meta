"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var cards_1 = require("./cards");
exports.cards = cards_1.default;
var card_1 = require("./card");
exports.cardMockData = card_1.default;
var configitem_card_1 = require("./configitem.card");
exports.configitemMockDataCard = configitem_card_1.default;
var configitem_property_card_1 = require("./configitem.property.card");
exports.cardPropertyMockData = configitem_property_card_1.default;
var configitem_privileges_card_1 = require("./configitem.privileges.card");
exports.configPrivilegesMockData = configitem_privileges_card_1.default;
var configitem_relation_card_1 = require("./configitem.relation.card");
exports.configRelationsMockData = configitem_relation_card_1.default;
//# sourceMappingURL=index.js.map