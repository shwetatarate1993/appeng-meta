declare const _default: {
    description: any;
    archived: boolean;
    table_id: number;
    database_id: number;
    enable_embedding: boolean;
    collection_id: number;
    query_type: string;
    name: string;
    query_average_duration: number;
    creator_id: number;
    updated_at: string;
    made_public_by_id: any;
    embedding_params: any;
    cache_ttl: any;
    dataset_query: string;
    id: number;
    display: string;
    visualization_settings: string;
    created_at: string;
    public_uuid: any;
}[];
export default _default;
//# sourceMappingURL=cards.d.ts.map