export { default as cards } from './cards';
export { default as cardMockData } from './card';
export { default as configitemMockDataCard } from './configitem.card';
export { default as cardPropertyMockData } from './configitem.property.card';
export { default as configPrivilegesMockData } from './configitem.privileges.card';
export { default as configRelationsMockData } from './configitem.relation.card';
//# sourceMappingURL=index.d.ts.map