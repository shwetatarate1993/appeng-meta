declare const _default: {
    configObjectId: string;
    configObjectType: string;
    name: string;
    displayLabel: string;
    projectId: number;
    dataGridId: string;
    gridObjectId: any;
    searchEnabled: boolean;
    renderingBeanName: string;
    defaultDisplay: boolean;
    cardType: string;
    chartType: any;
    logicalEntityId: string;
    privileges: {
        privilegeType: string;
        itemId: string;
        roleId: number;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: string;
        deletionDate: string;
    }[];
    parentRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        childItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: string;
        deletionDate: string;
    }[];
}[];
export default _default;
//# sourceMappingURL=card.d.ts.map