"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const card_parser_1 = require("./card.parser");
exports.Query = {
    Card: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, card_parser_1.parseConfigToCardObject),
};
exports.Mutation = {
    createCard: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, card_parser_1.parseCardObjectToConfig, card_parser_1.parseConfigToCardObject);
    },
};
//# sourceMappingURL=resolvers.js.map