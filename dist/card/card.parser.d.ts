import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { Card } from '../models';
export declare const parseCardObjectToConfig: (card: Card) => ConfigMetadata;
export declare const parseConfigToCardObject: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => Card;
//# sourceMappingURL=card.parser.d.ts.map