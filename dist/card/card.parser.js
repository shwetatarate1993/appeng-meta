"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseCardObjectToConfig = (card) => {
    const createdAt = card.creationDate;
    const createdBy = card.createdBy;
    const updatedBy = card.updatedBy;
    const updatedAt = card.updationDate;
    const property = [];
    let itemId;
    if (card.configObjectId !== undefined && card.configObjectId !== ''
        && card.configObjectId !== null) {
        itemId = card.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (card.displayLabel !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'DISPLAY_LABEL', card.displayLabel, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (card.renderingBeanName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'RENDERING_BEAN_NAME', card.renderingBeanName, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    // for setting default value as true
    property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'IS_SEARCH_ENABLED', card.searchEnabled !== undefined ? (card.searchEnabled ? '1' : '0') : '1', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    // for setting default value as false
    property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'IS_DEFAULT_DISPLAY', card.defaultDisplay !== undefined ? (card.defaultDisplay ? '1' : '0') : '0', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    if (card.cardType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'CARD_TYPE', card.cardType, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (card.chartType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'CHART_TYPE', card.chartType, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (card.dataGridId !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'DATAGRID_ID', card.dataGridId, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (card.logicalEntityId !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'LOGICAL_ENTITY', card.logicalEntityId, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, card.name, configitemtype_enum_1.ConfigItemTypes.CARD, card.projectId, createdBy, card.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const cardPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(card.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(card.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(card.privileges, cardPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, cardPrivileges);
};
exports.parseConfigToCardObject = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.Card(details.get('DISPLAY_LABEL'), details.get('RENDERING_BEAN_NAME'), details.get('IS_SEARCH_ENABLED'), details.get('IS_DEFAULT_DISPLAY'), details.get('CARD_TYPE'), details.get('CHART_TYPE'), details.get('DATAGRID_ID'), details.get('LOGICAL_ENTITY'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=card.parser.js.map