export declare const Query: {
    Card: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").Card>;
};
export declare const Mutation: {
    createCard: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map