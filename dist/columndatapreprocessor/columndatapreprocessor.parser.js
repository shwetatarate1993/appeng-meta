"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseColumnDataPreprocessorToConfig = (columnDataPreprocessor) => {
    const createdAt = columnDataPreprocessor.creationDate;
    const createdBy = columnDataPreprocessor.createdBy;
    const updatedBy = columnDataPreprocessor.updatedBy;
    const updatedAt = columnDataPreprocessor.updationDate;
    const property = [];
    let itemId;
    if (columnDataPreprocessor.configObjectId !== undefined && columnDataPreprocessor.configObjectId !== ''
        && columnDataPreprocessor.configObjectId !== null) {
        itemId = columnDataPreprocessor.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (columnDataPreprocessor.preProcessorBean !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('EXECUTOR', columnDataPreprocessor.preProcessorBean, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (columnDataPreprocessor.isMultiValue !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_MULTIVALUE', columnDataPreprocessor.isMultiValue, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (columnDataPreprocessor.excecutionType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('EXCECUTION_TYPE', columnDataPreprocessor.excecutionType, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (columnDataPreprocessor.jsCode !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('JS_CODE', columnDataPreprocessor.jsCode, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (columnDataPreprocessor.order !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ORDER', columnDataPreprocessor.order, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, columnDataPreprocessor.name, configitemtype_enum_1.ConfigItemTypes.COLUMNDATAPREPROCESSOR, columnDataPreprocessor.projectId, createdBy, columnDataPreprocessor.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const columndataPreprocessorPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(columnDataPreprocessor.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(columnDataPreprocessor.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(columnDataPreprocessor.privileges, columndataPreprocessorPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, columndataPreprocessorPrivileges);
};
exports.parseConfigToColumnDataPreprocessor = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.ColumnDataPreprocessor(details.get('EXECUTOR'), details.get('IS_MULTIVALUE'), details.get('EXCECUTION_TYPE'), details.get('JS_CODE'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, details.get('ORDER'), configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=columndatapreprocessor.parser.js.map