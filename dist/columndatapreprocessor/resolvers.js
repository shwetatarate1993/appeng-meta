"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const columndatapreprocessor_parser_1 = require("./columndatapreprocessor.parser");
exports.Query = {
    ColumnDataPreprocessor: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, columndatapreprocessor_parser_1.parseConfigToColumnDataPreprocessor),
};
exports.Mutation = {
    createColumnDataPreprocessor: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, columndatapreprocessor_parser_1.parseColumnDataPreprocessorToConfig, columndatapreprocessor_parser_1.parseConfigToColumnDataPreprocessor);
    },
};
//# sourceMappingURL=resolvers.js.map