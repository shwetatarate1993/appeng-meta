"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const columndatapreprocessor_parser_1 = require("../columndatapreprocessor.parser");
const mockdata_1 = require("./mockdata");
test('parser columndatapreprocessor object to config', () => {
    const columndatapreprocessor = models_1.ColumnDataPreprocessor.deserialize(mockdata_1.columnDataPreprocessorMockData[0]);
    const result = columndatapreprocessor_parser_1.parseColumnDataPreprocessorToConfig(columndatapreprocessor);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result).toHaveProperty('configItem');
    expect(result.configItem.configObjectType).toEqual('ColumnDataPreprocessor');
});
test('parser config to columndatapreprocessor object', () => {
    const result = columndatapreprocessor_parser_1.parseConfigToColumnDataPreprocessor(mockdata_1.configItemColumnDataPreprocessorMockData[0], mockdata_1.configItemPropertyColumnDataPreprocessorMockData, mockdata_1.configItemRelationsColumnDataPreprocessorMockData, mockdata_1.configItemPrivilegesColumnDataPreprocessorMockData);
    expect(result).toHaveProperty('isMultiValue');
    expect(result).toHaveProperty('excecutionType', 'Javascript');
    expect(result).toHaveProperty('jsCode');
    expect(result.privileges.length === 0).toBe(true);
    expect(result.parentRelations.length === 1).toBe(true);
    expect(result.childRelations.length === 0).toBe(true);
    expect(result.parentRelations[0].relationType === 'FormField_ColumnDataPreprocessor').toBe(true);
});
//# sourceMappingURL=columndatapreprocessor.parser.test.js.map