"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var configitem_columndatapreprocessor_1 = require("./configitem.columndatapreprocessor");
exports.configItemColumnDataPreprocessorMockData = configitem_columndatapreprocessor_1.default;
var configitem_relation_columndatapreprocessor_1 = require("./configitem.relation.columndatapreprocessor");
exports.configItemRelationsColumnDataPreprocessorMockData = configitem_relation_columndatapreprocessor_1.default;
var configitem_privileges_columndatapreprocessor_1 = require("./configitem.privileges.columndatapreprocessor");
exports.configItemPrivilegesColumnDataPreprocessorMockData = configitem_privileges_columndatapreprocessor_1.default;
var configitem_property_columndatapreprocessor_1 = require("./configitem.property.columndatapreprocessor");
exports.configItemPropertyColumnDataPreprocessorMockData = configitem_property_columndatapreprocessor_1.default;
var columndatapreprocessor_1 = require("./columndatapreprocessor");
exports.columnDataPreprocessorMockData = columndatapreprocessor_1.default;
//# sourceMappingURL=index.js.map