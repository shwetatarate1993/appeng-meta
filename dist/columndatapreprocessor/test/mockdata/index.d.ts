export { default as configItemColumnDataPreprocessorMockData } from './configitem.columndatapreprocessor';
export { default as configItemRelationsColumnDataPreprocessorMockData } from './configitem.relation.columndatapreprocessor';
export { default as configItemPrivilegesColumnDataPreprocessorMockData } from './configitem.privileges.columndatapreprocessor';
export { default as configItemPropertyColumnDataPreprocessorMockData } from './configitem.property.columndatapreprocessor';
export { default as columnDataPreprocessorMockData } from './columndatapreprocessor';
//# sourceMappingURL=index.d.ts.map