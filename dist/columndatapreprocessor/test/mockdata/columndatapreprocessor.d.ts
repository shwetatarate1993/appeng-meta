declare const _default: {
    configObjectId: string;
    configObjectType: string;
    name: string;
    preProcessorBean: string;
    isMultiValue: string;
    excecutionType: string;
    jsCode: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: any;
    creationDate: string;
    projectId: number;
    updatedBy: string;
    updationDate: string;
    deletionDate: string;
    privileges: any[];
    parentRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        childItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: string;
        deletionDate: string;
    }[];
    childRelations: any[];
}[];
export default _default;
//# sourceMappingURL=columndatapreprocessor.d.ts.map