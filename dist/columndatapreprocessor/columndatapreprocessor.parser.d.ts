import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { ColumnDataPreprocessor } from '../models';
export declare const parseColumnDataPreprocessorToConfig: (columnDataPreprocessor: ColumnDataPreprocessor) => ConfigMetadata;
export declare const parseConfigToColumnDataPreprocessor: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => ColumnDataPreprocessor;
//# sourceMappingURL=columndatapreprocessor.parser.d.ts.map