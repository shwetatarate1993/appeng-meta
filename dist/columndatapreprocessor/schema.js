"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = __importDefault(require("../base"));
const common_schema_1 = __importDefault(require("../common.schema"));
const ColumnDataPreprocessor = `

input ColumnDataPreprocessorInput {

    configObjectId: ID
    name: String
    configObjectType: String
    preProcessorBean: String
    isMultiValue: Boolean
    excecutionType: String
    jsCode: String
    order: Int
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilegeInput]
    parentRelations : [ConfigItemRelationInput]
    childRelations : [ConfigItemRelationInput]
}

extend type Query {
    ColumnDataPreprocessor(id: ID!): ColumnDataPreprocessor
   }

extend type Mutation {
    createColumnDataPreprocessor (input: ColumnDataPreprocessorInput): ColumnDataPreprocessor
}

type ColumnDataPreprocessor {
    configObjectId: ID
    name: String
    configObjectType: String
    preProcessorBean: String
    isMultiValue: Boolean
    excecutionType: String
    jsCode: String
    order: Int
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilege]
    parentRelations : [ConfigItemRelation]
    childRelations : [ConfigItemRelation]
}
`;
exports.default = () => [ColumnDataPreprocessor, base_1.default, common_schema_1.default];
//# sourceMappingURL=schema.js.map