export declare const Query: {
    ColumnDataPreprocessor: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").ColumnDataPreprocessor>;
};
export declare const Mutation: {
    createColumnDataPreprocessor: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map