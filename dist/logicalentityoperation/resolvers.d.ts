export declare const Query: {
    LogicalEntityOperation: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").LogicalEntityOperation>;
};
export declare const Mutation: {
    createLogicalEntityOperation: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map