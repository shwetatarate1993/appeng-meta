import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { LogicalEntityOperation } from '../models';
export declare const parseLogicalEntityOperationToConfig: (logicalEntityOperation: LogicalEntityOperation) => ConfigMetadata;
export declare const parseConfigToLogicalEntityOperation: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => LogicalEntityOperation;
//# sourceMappingURL=logicalentityoperation.parser.d.ts.map