"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const logicalentityoperation_parser_1 = require("./logicalentityoperation.parser");
exports.Query = {
    LogicalEntityOperation: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, logicalentityoperation_parser_1.parseConfigToLogicalEntityOperation),
};
exports.Mutation = {
    createLogicalEntityOperation: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, logicalentityoperation_parser_1.parseLogicalEntityOperationToConfig, logicalentityoperation_parser_1.parseConfigToLogicalEntityOperation);
    },
};
//# sourceMappingURL=resolvers.js.map