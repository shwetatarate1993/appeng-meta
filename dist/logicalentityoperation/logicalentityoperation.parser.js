"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseLogicalEntityOperationToConfig = (logicalEntityOperation) => {
    const createdAt = logicalEntityOperation.creationDate;
    const createdBy = logicalEntityOperation.createdBy;
    const updatedBy = logicalEntityOperation.updatedBy;
    const updatedAt = logicalEntityOperation.updationDate;
    const property = [];
    let itemId;
    if (logicalEntityOperation.configObjectId !== undefined && logicalEntityOperation.configObjectId !== ''
        && logicalEntityOperation.configObjectId !== null) {
        itemId = logicalEntityOperation.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (logicalEntityOperation.selectId !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'SELECT_ID', logicalEntityOperation.selectId, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (logicalEntityOperation.gridSelectId !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'GRID_SELECT_ID', logicalEntityOperation.gridSelectId, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (logicalEntityOperation.filteredQuery !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'FILTER_QUERY', logicalEntityOperation.filteredQuery, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (logicalEntityOperation.workAreaSessName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'WORK_AREA_SESS_NAME', logicalEntityOperation.workAreaSessName, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, logicalEntityOperation.name, configitemtype_enum_1.ConfigItemTypes.LOGICALENTITYOPERATION, logicalEntityOperation.projectId, createdBy, logicalEntityOperation.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const logicalentityoperationPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(logicalEntityOperation.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(logicalEntityOperation.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(logicalEntityOperation.privileges, logicalentityoperationPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, logicalentityoperationPrivileges);
};
exports.parseConfigToLogicalEntityOperation = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.LogicalEntityOperation(details.get('SELECT_ID'), details.get('GRID_SELECT_ID'), details.get('FILTER_QUERY'), details.get('WORK_AREA_SESS_NAME'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=logicalentityoperation.parser.js.map