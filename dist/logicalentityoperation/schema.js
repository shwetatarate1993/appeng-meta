"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = __importDefault(require("../base"));
const common_schema_1 = __importDefault(require("../common.schema"));
const LogicalEntityOperation = `

input LogicalEntityOperationInput {

    configObjectId: ID
    name: String
    configObjectType: String
    selectId: String
	gridSelectId: String
	filteredQuery: String
	workAreaSessName: String
	createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilegeInput]
    parentRelations : [ConfigItemRelationInput]
    childRelations : [ConfigItemRelationInput]
}

extend type Query {
    LogicalEntityOperation(id: ID!): LogicalEntityOperation
   }

extend type Mutation {
    createLogicalEntityOperation (input: LogicalEntityOperationInput): LogicalEntityOperation
}

type LogicalEntityOperation {
    configObjectId: ID
    name: String
    configObjectType: String
    selectId: String
	gridSelectId: String
	filteredQuery: String
	workAreaSessName: String
	createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilege]
    parentRelations : [ConfigItemRelation]
    childRelations : [ConfigItemRelation]
}
`;
exports.default = () => [LogicalEntityOperation, base_1.default, common_schema_1.default];
//# sourceMappingURL=schema.js.map