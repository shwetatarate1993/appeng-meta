"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const logicalentityoperation_parser_1 = require("../logicalentityoperation.parser");
const mockdata_1 = require("./mockdata");
test('parser logicalentityoperation object to config', () => {
    const logicalentityoperation = models_1.LogicalEntityOperation.deserialize(mockdata_1.logicalentityoperationMockData[0]);
    const result = logicalentityoperation_parser_1.parseLogicalEntityOperationToConfig(logicalentityoperation);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
});
test('parser config to logicalentityoperation object', () => {
    const result = logicalentityoperation_parser_1.parseConfigToLogicalEntityOperation(mockdata_1.configitemMockDatalogicalentityoperation[0], mockdata_1.logicalentityoperationPropertyMockData, mockdata_1.configRelationslogMockData, mockdata_1.configPrivilegeslogMockData);
    expect(result).toHaveProperty('selectId');
    expect(result).toHaveProperty('gridSelectId');
});
//# sourceMappingURL=logicalentityoperation.parser.test.js.map