"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: '4067d50b-82a3-48da-8196-984f89aaabc8',
        configObjectType: 'LogicalEntityOperation',
        name: 'LogicalEntityOperation',
        selectId: 'SELECT EMPLOYEE_INFO_UUID, EMPLOYEE_INFO_ID, TALUKA_ID,'
            + 'GRAMPANCHAYAT_ID, DISTRICT_ID, CLASS, DESIGNATION, EMPLOYEE_NAME, MOBILENO,'
            + 'EMAIL, TELEPHONE, TYPE_OF_VACANCY, CHARGE_TYPE, DATE_ATTACHED_TO_ABOVE_POSITION,'
            + 'DATE_OF_BIRTH, RETIREMENT_DATE, FINANCIAL_YEAR, CREATEDBY, UPDATEDBY, CREATIONDATE,'
            + 'UPDATIONDATE, ISDELETED, DELETIONDATE FROM BUILDING_BRANCH_EMPLOYEE_INFO  WHERE'
            + 'EMPLOYEE_INFO_UUID =:entityPrimaryKey',
        gridSelectId: 'SELECT einfo.EMPLOYEE_INFO_UUID,'
            + 'einfo.EMPLOYEE_INFO_IDtal.TALUKA_NAME as TALUKA_ID,'
            + 'einfo.GRAMPANCHAYAT_ID, dis.DISTRICT_NAME as DISTRICT_ID,'
            + 'einfo.CLASS, einfo.DESIGNATION, einfo.EMPLOYEE_NAME,'
            + 'einfo.MOBILENO, einfo.EMAIL, einfo.TELEPHONE,einfo.TYPE_OF_VACANCY,'
            + 'einfo.CHARGE_TYPE, einfo.DATE_ATTACHED_TO_ABOVE_POSITION,'
            + 'einfo.DATE_OF_BIRTH,einfo.RETIREMENT_DATE,einfo.FINANCIAL_YEAR,'
            + 'einfo.ISDELETED FROM BUILDING_BRANCH_EMPLOYEE_INFO einfo'
            + 'left join DISTRICT dis on (dis.DISTRICT_UUID=einfo.DISTRICT_ID)'
            + 'left join TALUKA tal on (tal.TALUKA_UUID=einfo.TALUKA_ID)  where einfo.ISDELETED!=1'
            + '<if test="APP_LOGGED_IN_ROLE_ID==17">'
            + 'and tal.TALUKA_UUID=:APP_LOGGED_IN_USER_TALUKA'
            + '</if> '
            + '<if test="APP_LOGGED_IN_ROLE_ID==18">'
            + 'and tal.TALUKA_UUID=:APP_LOGGED_IN_USER_TALUKA'
            + '</if>'
            + '<if test="APP_LOGGED_IN_ROLE_ID==19">'
            + 'and dis.DISTRICT_UUID=:APP_LOGGED_IN_USER_DISTRICT'
            + '</if>'
            + '<if test="APP_LOGGED_IN_ROLE_ID==20">'
            + 'and dis.STATE_ID=:APP_LOGGED_IN_USER_STATE'
            + '</if>'
            + 'and FINANCIAL_YEAR=:APP_LOGGED_IN_YEAR'
            + 'ORDER BY einfo.EMPLOYEE_INFO_ID DESC',
        filteredQuery: null,
        workAreaSessName: 'PRIMARYSPRING',
        privileges: [],
        parentRelations: [
            {
                relationId: '9b3900ae-b393-4457-aeb5-dd734b70636f',
                relationType: 'DataGrid_LogicalEntityOperation',
                parentItemId: '605638de-a49e-4d52-ae0c-80bc845a0770',
                childItemId: '4067d50b-82a3-48da-8196-984f89aaabc8',
                createdBy: '1123',
                isDeleted: 0,
                creationDate: '2018-02-26T00:00:00.000Z',
                updatedBy: '1123',
                updationDate: '2018-04-27T00:00:00.000Z',
                deletionDate: '2018-04-27T00:00:00.000Z',
            },
        ],
    },
];
//# sourceMappingURL=logicalentityoperation.object.js.map