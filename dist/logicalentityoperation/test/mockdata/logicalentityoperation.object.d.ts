declare const _default: {
    configObjectId: string;
    configObjectType: string;
    name: string;
    selectId: string;
    gridSelectId: string;
    filteredQuery: any;
    workAreaSessName: string;
    privileges: any[];
    parentRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        childItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: string;
        deletionDate: string;
    }[];
}[];
export default _default;
//# sourceMappingURL=logicalentityoperation.object.d.ts.map