"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var logicalentityoperation_object_1 = require("./logicalentityoperation.object");
exports.logicalentityoperationMockData = logicalentityoperation_object_1.default;
var configitem_property_logicalentityoperation_1 = require("./configitem.property.logicalentityoperation");
exports.logicalentityoperationPropertyMockData = configitem_property_logicalentityoperation_1.default;
var configitem_logicalentityoperation_1 = require("./configitem.logicalentityoperation");
exports.configitemMockDatalogicalentityoperation = configitem_logicalentityoperation_1.default;
var configitem_privileges_logicalentityoperation_1 = require("./configitem.privileges.logicalentityoperation");
exports.configPrivilegeslogMockData = configitem_privileges_logicalentityoperation_1.default;
var configitem_relation_logicalentityoperation_1 = require("./configitem.relation.logicalentityoperation");
exports.configRelationslogMockData = configitem_relation_logicalentityoperation_1.default;
//# sourceMappingURL=index.js.map