export { default as logicalentityoperationMockData } from './logicalentityoperation.object';
export { default as logicalentityoperationPropertyMockData } from './configitem.property.logicalentityoperation';
export { default as configitemMockDatalogicalentityoperation } from './configitem.logicalentityoperation';
export { default as configPrivilegeslogMockData } from './configitem.privileges.logicalentityoperation';
export { default as configRelationslogMockData } from './configitem.relation.logicalentityoperation';
//# sourceMappingURL=index.d.ts.map