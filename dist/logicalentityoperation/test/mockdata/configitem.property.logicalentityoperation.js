"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        propertyId: 'eaedd93c-f348-4ac3-b6f1-50e3dd000fd8',
        propertyName: 'SELECT_ID',
        propertyValue: 'SELECT EMPLOYEE_INFO_UUID, EMPLOYEE_INFO_ID, TALUKA_ID,'
            + 'GRAMPANCHAYAT_ID, DISTRICT_ID, CLASS, DESIGNATION, EMPLOYEE_NAME, MOBILENO,'
            + 'EMAIL, TELEPHONE, TYPE_OF_VACANCY, CHARGE_TYPE, DATE_ATTACHED_TO_ABOVE_POSITION,'
            + 'DATE_OF_BIRTH, RETIREMENT_DATE, FINANCIAL_YEAR, CREATEDBY, UPDATEDBY, CREATIONDATE,'
            + 'UPDATIONDATE, ISDELETED, DELETIONDATE FROM BUILDING_BRANCH_EMPLOYEE_INFO  WHERE'
            + 'EMPLOYEE_INFO_UUID =:entityPrimaryKey',
        itemId: '4067d50b-82a3-48da-8196-984f89aaabc8',
        createdBy: null,
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '1e599efb-3e14-44d2-8ecd-d1e03b3efb33',
        propertyName: 'GRID_SELECT_ID',
        propertyValue: 'SELECT einfo.EMPLOYEE_INFO_UUID,'
            + 'einfo.EMPLOYEE_INFO_IDtal.TALUKA_NAME as TALUKA_ID,'
            + 'einfo.GRAMPANCHAYAT_ID, dis.DISTRICT_NAME as DISTRICT_ID,'
            + 'einfo.CLASS, einfo.DESIGNATION, einfo.EMPLOYEE_NAME,'
            + 'einfo.MOBILENO, einfo.EMAIL, einfo.TELEPHONE,einfo.TYPE_OF_VACANCY,'
            + 'einfo.CHARGE_TYPE, einfo.DATE_ATTACHED_TO_ABOVE_POSITION,'
            + 'einfo.DATE_OF_BIRTH,einfo.RETIREMENT_DATE,einfo.FINANCIAL_YEAR,'
            + 'einfo.ISDELETED FROM BUILDING_BRANCH_EMPLOYEE_INFO einfo'
            + 'left join DISTRICT dis on (dis.DISTRICT_UUID=einfo.DISTRICT_ID)'
            + 'left join TALUKA tal on (tal.TALUKA_UUID=einfo.TALUKA_ID)  where einfo.ISDELETED!=1'
            + '<if test="APP_LOGGED_IN_ROLE_ID==17">'
            + 'and tal.TALUKA_UUID=:APP_LOGGED_IN_USER_TALUKA'
            + '</if> '
            + '<if test="APP_LOGGED_IN_ROLE_ID==18">'
            + 'and tal.TALUKA_UUID=:APP_LOGGED_IN_USER_TALUKA'
            + '</if>'
            + '<if test="APP_LOGGED_IN_ROLE_ID==19">'
            + 'and dis.DISTRICT_UUID=:APP_LOGGED_IN_USER_DISTRICT'
            + '</if>'
            + '<if test="APP_LOGGED_IN_ROLE_ID==20">'
            + 'and dis.STATE_ID=:APP_LOGGED_IN_USER_STATE'
            + '</if>'
            + 'and FINANCIAL_YEAR=:APP_LOGGED_IN_YEAR'
            + 'ORDER BY einfo.EMPLOYEE_INFO_ID DESC',
        itemId: '4067d50b-82a3-48da-8196-984f89aaabc8',
        createdBy: null,
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '68b221bb-a2c3-4ea7-9585-b0563acbd3b0',
        propertyName: 'FILTER_QUERY',
        propertyValue: 'com.appengine.view.web.component.builder.DataGridViewCardBuilder',
        itemId: '4067d50b-82a3-48da-8196-984f89aaabc8',
        createdBy: null,
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: 'ae8bc12e-91db-4e9b-968c-020f87059fca',
        propertyName: 'WORK_AREA_SESS_NAME',
        propertyValue: 'PRIMARYSPRING',
        itemId: '4067d50b-82a3-48da-8196-984f89aaabc8',
        createdBy: null,
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
];
//# sourceMappingURL=configitem.property.logicalentityoperation.js.map