"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const formdbvalidation_parser_1 = require("./formdbvalidation.parser");
exports.Query = {
    FormDbValidation: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, formdbvalidation_parser_1.parseConfigToFormDbValidation),
};
exports.Mutation = {
    createFormDbValidation: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, formdbvalidation_parser_1.parseFormDbValidationToConfig, formdbvalidation_parser_1.parseConfigToFormDbValidation);
    },
};
//# sourceMappingURL=resolvers.js.map