export declare const Query: {
    FormDbValidation: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").FormDbValidation>;
};
export declare const Mutation: {
    createFormDbValidation: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map