import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { FormDbValidation } from '../models';
export declare const parseFormDbValidationToConfig: (formDbValidation: FormDbValidation) => ConfigMetadata;
export declare const parseConfigToFormDbValidation: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => FormDbValidation;
//# sourceMappingURL=formdbvalidation.parser.d.ts.map