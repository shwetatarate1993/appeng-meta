"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const formdbvalidation_parser_1 = require("../formdbvalidation.parser");
const mockdata_1 = require("./mockdata");
test('parser formdb validation  object to config', () => {
    const formDbValidation = models_1.FormDbValidation.deserialize(mockdata_1.formDbValidationMockData[0]);
    const result = formdbvalidation_parser_1.parseFormDbValidationToConfig(formDbValidation);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItem.configObjectType).toEqual('FormDBValidationObject');
    expect(result.configItemRelation.length === 1).toBe(true);
    expect(result.configItemPrivilege.length === 0).toBe(true);
    expect(result.configItemProperty.length === 12).toBe(true);
});
test('parser config to formdb validation object', () => {
    const result = formdbvalidation_parser_1.parseConfigToFormDbValidation(mockdata_1.formDbValidationConfigitemMockData[0], mockdata_1.formDbValidationPropertyMockData, mockdata_1.formDbValidationConfigRelationsMockData, mockdata_1.formDbValidationConfigPrivilegesMockData);
    expect(result).toHaveProperty('datasourceName');
    expect(result).toHaveProperty('datasourceName', 'PRIMARYSPRING');
    expect(result.privileges.length === 0).toBe(true);
});
//# sourceMappingURL=formdbvalidation.parser.test.js.map