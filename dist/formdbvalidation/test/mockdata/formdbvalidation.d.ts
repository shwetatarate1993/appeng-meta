declare const _default: {
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: any;
    creationDate: any;
    projectId: number;
    updatedBy: string;
    updationDate: any;
    deletionDate: any;
    datasourceName: string;
    fieldIds: string;
    validationExpression: string;
    validationExpressionKeys: string;
    validationMessage: string;
    validationQid: string;
    validationType: string;
    privileges: any[];
    parentRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        childItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: any;
        updatedBy: string;
        updationDate: any;
        deletionDate: any;
    }[];
}[];
export default _default;
//# sourceMappingURL=formdbvalidation.d.ts.map