"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var formdbvalidation_1 = require("./formdbvalidation");
exports.formDbValidationMockData = formdbvalidation_1.default;
var configitem_formdbvalidation_1 = require("./configitem.formdbvalidation");
exports.formDbValidationConfigitemMockData = configitem_formdbvalidation_1.default;
var configitem_property_formdbvalidation_1 = require("./configitem.property.formdbvalidation");
exports.formDbValidationPropertyMockData = configitem_property_formdbvalidation_1.default;
var configitem_relation_formdbvalidation_1 = require("./configitem.relation.formdbvalidation");
exports.formDbValidationConfigRelationsMockData = configitem_relation_formdbvalidation_1.default;
var configitem_privileges_formdbvalidation_1 = require("./configitem.privileges.formdbvalidation");
exports.formDbValidationConfigPrivilegesMockData = configitem_privileges_formdbvalidation_1.default;
//# sourceMappingURL=index.js.map