export { default as formDbValidationMockData } from './formdbvalidation';
export { default as formDbValidationConfigitemMockData } from './configitem.formdbvalidation';
export { default as formDbValidationPropertyMockData } from './configitem.property.formdbvalidation';
export { default as formDbValidationConfigRelationsMockData } from './configitem.relation.formdbvalidation';
export { default as formDbValidationConfigPrivilegesMockData } from './configitem.privileges.formdbvalidation';
//# sourceMappingURL=index.d.ts.map