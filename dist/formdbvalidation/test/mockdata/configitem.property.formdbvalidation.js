"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        propertyId: '437wff38-09d2-41f9-88t4-1de41e618e1',
        propertyName: 'MODE',
        propertyValue: 'abc',
        itemId: '0b949f15-a1d7-4449-8b69-741a01cf8b12',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '437dff38-09d2-41f9-8844-1d6841e618e1',
        propertyName: 'DATASOURCE_NAME',
        propertyValue: 'PRIMARYSPRING',
        itemId: '0b949f15-a1d7-4449-8b69-741a01cf8b12',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    }, {
        propertyId: '4a35cf58-999e-4eb6-ae66-dcb8cea9ee08',
        propertyName: 'FIELD_IDS',
        propertyValue: 'test',
        itemId: '0b949f15-a1d7-4449-8b69-741a01cf8b12',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    }, {
        propertyId: 'caa27617-f783-49a7-a4e0-a06d0885fb2e',
        propertyName: 'VALIDATION_EXPRESSION',
        propertyValue: 'test',
        itemId: '0b949f15-a1d7-4449-8b69-741a01cf8b12',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    }, {
        propertyId: '21478e67-d932-45a0-819c-5624cbe03d6f',
        propertyName: 'VALIDATION_EXPRESSION_KEYS',
        propertyValue: 'test',
        itemId: '0b949f15-a1d7-4449-8b69-741a01cf8b12',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    }, {
        propertyId: '98506dec-0e0f-4f89-a2c3-08e3b0eb6397',
        propertyName: 'VALIDATION_MSG',
        propertyValue: 'test',
        itemId: '0b949f15-a1d7-4449-8b69-741a01cf8b12',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    }, {
        propertyId: '7e4a2c81-a449-43e5-9adc-e72d70b6b913',
        propertyName: 'VALIDATION_QID',
        propertyValue: 'test',
        itemId: '0b949f15-a1d7-4449-8b69-741a01cf8b12',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    }, {
        propertyId: '42d6d043-e271-41e0-9c67-01d3e8b74b2d',
        propertyName: 'VALIDATION_TYPE',
        propertyValue: 'test',
        itemId: '0b949f15-a1d7-4449-8b69-741a01cf8b12',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '7d939586-302f-43b9-b2fe-97fb08979600',
        propertyName: 'IS_CONDITION_AVAILABLE',
        propertyValue: false,
        itemId: '1334f4d5-24c9-4939-86f1-00d5f319d60c',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '7d9586-30s2f-43b9-b2fe-97f8979600',
        propertyName: 'CONDITION_EXPRESSION',
        propertyValue: null,
        itemId: '1334f4d5-24c9-4939-86f1-00d5f319d60c',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '7d9386-302f-43b9-b2fe-97fb08970',
        propertyName: 'CONDITION_FIELDS',
        propertyValue: null,
        itemId: '1334f4d5-24c9-4939-86f1-00d5f319d60c',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '7d9386-302f-43b9-b21fe-97fb8970',
        propertyName: 'DESCRIPTION',
        propertyValue: null,
        itemId: '1334f4d5-24c9-4939-86f1-00d5f319d60c',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
];
//# sourceMappingURL=configitem.property.formdbvalidation.js.map