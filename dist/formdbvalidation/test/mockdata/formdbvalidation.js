"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: '0b949f15-a1d7-4449-8b69-741a01cf8b12',
        name: 'Validation for month',
        configObjectType: 'FormDBValidationObject',
        createdBy: '1121',
        isDeleted: 0,
        itemDescription: null,
        creationDate: null,
        projectId: -1,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
        datasourceName: 'PRIMARYSPRING',
        fieldIds: 'test',
        validationExpression: 'test',
        validationExpressionKeys: 'test',
        validationMessage: 'test',
        validationQid: 'test',
        validationType: 'test',
        privileges: [],
        parentRelations: [
            {
                relationId: '564467f2a-5ab1-11e9-8548-0ee72cgfdgr4',
                relationType: 'LogicalEntity_ValidationObject',
                parentItemId: '0cd70c37-6aaf-4717-97af-6c82cc840d20',
                childItemId: '0b949f15-a1d7-4449-8b69-741a01cf8b12',
                createdBy: '1121',
                isDeleted: 0,
                creationDate: null,
                updatedBy: '1121',
                updationDate: null,
                deletionDate: null,
            },
        ],
    },
];
//# sourceMappingURL=formdbvalidation.js.map