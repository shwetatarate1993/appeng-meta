"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseFormDbValidationToConfig = (formDbValidation) => {
    const createdAt = formDbValidation.creationDate;
    const createdBy = formDbValidation.createdBy;
    const updatedBy = formDbValidation.updatedBy;
    const updatedAt = formDbValidation.updationDate;
    const property = [];
    let itemId;
    if (formDbValidation.configObjectId !== undefined && formDbValidation.configObjectId !== ''
        && formDbValidation.configObjectId !== null) {
        itemId = formDbValidation.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (formDbValidation.datasourceName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'MODE', formDbValidation.mode, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formDbValidation.datasourceName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'DATASOURCE_NAME', formDbValidation.datasourceName, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formDbValidation.validationType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'VALIDATION_TYPE', formDbValidation.validationType, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formDbValidation.validationExpression !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'VALIDATION_EXPRESSION', formDbValidation.validationExpression, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formDbValidation.validationQid !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'VALIDATION_QID', formDbValidation.validationQid, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formDbValidation.validationMessage !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'VALIDATION_MSG', formDbValidation.validationMessage, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formDbValidation.validationExpressionKeys !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'VALIDATION_EXPRESSION_KEYS', formDbValidation.validationExpressionKeys, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formDbValidation.fieldIds !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'FIELD_IDS', formDbValidation.fieldIds, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'IS_CONDITION_AVAILABLE', formDbValidation.isConditionAvailable !== undefined ?
        (formDbValidation.isConditionAvailable ? '1' : '0') : '0', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    if (formDbValidation.conditionExpression !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'CONDITION_EXPRESSION', formDbValidation.conditionExpression, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formDbValidation.conditionFields !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'CONDITION_FIELDS', formDbValidation.conditionFields, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formDbValidation.description !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'DESCRIPTION', formDbValidation.description, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, formDbValidation.name, configitemtype_enum_1.ConfigItemTypes.FORMDBVALIDATIONOBJECT, formDbValidation.projectId, createdBy, formDbValidation.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const formDbValidationPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(formDbValidation.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(formDbValidation.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(formDbValidation.privileges, formDbValidationPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, formDbValidationPrivileges);
};
exports.parseConfigToFormDbValidation = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations != null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.FormDbValidation(configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, details.get('MODE'), details.get('DATASOURCE_NAME'), details.get('VALIDATION_TYPE'), details.get('VALIDATION_EXPRESSION'), details.get('VALIDATION_QID'), details.get('VALIDATION_MSG'), details.get('VALIDATION_EXPRESSION_KEYS'), details.get('FIELD_IDS'), details.get('IS_CONDITION_AVAILABLE'), details.get('CONDITION_EXPRESSION'), details.get('DESCRIPTION'), details.get('CONDITION_FIELDS'), privileges, childRelations, parentRelations);
};
//# sourceMappingURL=formdbvalidation.parser.js.map