"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const action_parser_1 = require("./action.parser");
exports.Query = {
    Action: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, action_parser_1.parseConfigToAction),
};
exports.Mutation = {
    createAction: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, action_parser_1.parseActionToConfig, action_parser_1.parseConfigToAction);
    },
};
//# sourceMappingURL=resolvers.js.map