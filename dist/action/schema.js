"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = __importDefault(require("../base"));
const common_schema_1 = __importDefault(require("../common.schema"));
const Action = `

input ActionInput {

    isProcessAction: Boolean
    expressionVariables: String
    accessiblityExpression: String
    action: String
    formsId: String
    description: String
    formFieldsId: String
    type: String
    configObjectId: ID
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilegeInput]
    parentRelations : [ConfigItemRelationInput]
    childRelations : [ConfigItemRelationInput]
}

extend type Query {
    Action(id: ID!): Action
   }

extend type Mutation {
    createAction (input: ActionInput): Action
}

type Action {

    isProcessAction: Boolean
    expressionVariables: String
    accessiblityExpression: String
    action: String
    formsId: String
    description: String
    formFieldsId: String
    type: String
    statusValue: String
    configObjectId: ID
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilege]
    parentRelations : [ConfigItemRelation]
    childRelations : [ConfigItemRelation]
}
`;
exports.default = () => [Action, base_1.default, common_schema_1.default];
//# sourceMappingURL=schema.js.map