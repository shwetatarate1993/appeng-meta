export declare const Query: {
    Action: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").Action>;
};
export declare const Mutation: {
    createAction: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map