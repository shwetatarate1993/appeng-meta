"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const action_parser_1 = require("../action.parser");
const mockdata_1 = require("./mockdata");
test('parse Action object to config', () => {
    const action = models_1.Action.deserialize(mockdata_1.actionMockData[0]);
    expect(action.configObjectType).toEqual('Action');
    const result = action_parser_1.parseActionToConfig(action);
    expect(result.configItem.configObjectType).toEqual('Action');
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
});
test('parse config to Action object', () => {
    const result = action_parser_1.parseConfigToAction(mockdata_1.configitemMockDataAction[0], mockdata_1.actionPropertyMockData, mockdata_1.actionRelationMockData, mockdata_1.actionPrivilegeMockData);
    expect(result).toHaveProperty('isProcessAction', false);
    expect(result).toHaveProperty('formsId');
    expect(result).toHaveProperty('statusValue', 'Define');
    expect(result).toHaveProperty('type', 'Form Action');
    expect(result).toHaveProperty('accessiblityExpression');
    expect(result).toHaveProperty('description', 'Defined');
    expect(result).toHaveProperty('action', 'Define');
    expect(result).toHaveProperty('expressionVariables');
    expect(result).toHaveProperty('formFieldsId');
    expect(result.privileges.length === 2).toBe(true);
    expect(result.parentRelations.length === 1).toBe(true);
    expect(result.parentRelations[0].relationType === 'LogicalEntity_Action').toBe(true);
});
//# sourceMappingURL=action.parser.test.js.map