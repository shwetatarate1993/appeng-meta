declare const _default: {
    privilegeId: string;
    privilegeType: string;
    itemId: string;
    roleId: number;
    createdBy: string;
    isDeleted: number;
    creationDate: any;
    updatedBy: string;
    updationDate: any;
    deletionDate: any;
}[];
export default _default;
//# sourceMappingURL=configitem.privilege.action.d.ts.map