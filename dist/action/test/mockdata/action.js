"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: 'd5e12d27-00d5-4ed0-adf9-4be488b00e78',
        configObjectType: 'Action',
        name: 'Define',
        isProcessAction: false,
        expressionVariables: null,
        accessiblityExpression: null,
        action: 'Define',
        formsId: null,
        description: 'Defined',
        type: 'Form Action',
        statusValue: 'Define',
        privileges: [
            {
                privilegeId: '163e0034-10eb-11e9-9992-0e5a20b1cdd2',
                privilegeType: 'EDIT',
                itemId: 'd5e12d27-00d5-4ed0-adf9-4be488b00e78',
                roleId: 16,
                createdBy: '1117',
                isDeleted: 0,
                creationDate: null,
                updatedBy: '1111',
                updationDate: null,
                deletionDate: null,
            },
            {
                privilegeId: '681316ea-2b98-11e9-9992-0e5a20b1cdd2',
                privilegeType: 'VIEW',
                itemId: 'd5e12d27-00d5-4ed0-adf9-4be488b00e78',
                roleId: -888,
                createdBy: '1117',
                isDeleted: 0,
                creationDate: null,
                updatedBy: '1111',
                updationDate: null,
                deletionDate: null,
            },
        ],
        parentRelations: [
            {
                relationId: 'de190237-aa09-43b9-bf53-cd64aa5b6a4d',
                relationType: 'LogicalEntity_Action',
                parentItemId: '0a75e462-3bcc-475b-adf0-9876ee6bd2a9',
                childItemId: 'd5e12d27-00d5-4ed0-adf9-4be488b00e78',
                createdBy: '1117',
                isDeleted: 0,
                creationDate: null,
                updatedBy: '1111',
                updationDate: null,
                deletionDate: null,
            },
        ],
    },
];
//# sourceMappingURL=action.js.map