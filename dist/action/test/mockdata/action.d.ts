declare const _default: {
    configObjectId: string;
    configObjectType: string;
    name: string;
    isProcessAction: boolean;
    expressionVariables: any;
    accessiblityExpression: any;
    action: string;
    formsId: any;
    description: string;
    type: string;
    statusValue: string;
    privileges: {
        privilegeId: string;
        privilegeType: string;
        itemId: string;
        roleId: number;
        createdBy: string;
        isDeleted: number;
        creationDate: any;
        updatedBy: string;
        updationDate: any;
        deletionDate: any;
    }[];
    parentRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        childItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: any;
        updatedBy: string;
        updationDate: any;
        deletionDate: any;
    }[];
}[];
export default _default;
//# sourceMappingURL=action.d.ts.map