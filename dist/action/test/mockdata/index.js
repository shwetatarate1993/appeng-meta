"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var action_1 = require("./action");
exports.actionMockData = action_1.default;
var configitem_action_1 = require("./configitem.action");
exports.configitemMockDataAction = configitem_action_1.default;
var configitem_property_action_1 = require("./configitem.property.action");
exports.actionPropertyMockData = configitem_property_action_1.default;
var configitem_relation_action_1 = require("./configitem.relation.action");
exports.actionRelationMockData = configitem_relation_action_1.default;
var configitem_privilege_action_1 = require("./configitem.privilege.action");
exports.actionPrivilegeMockData = configitem_privilege_action_1.default;
//# sourceMappingURL=index.js.map