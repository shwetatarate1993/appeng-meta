"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        privilegeId: '163e0034-10eb-11e9-9992-0e5a20b1cdd2',
        privilegeType: 'EDIT',
        itemId: 'd5e12d27-00d5-4ed0-adf9-4be488b00e78',
        roleId: 16,
        createdBy: '1117',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
    },
    {
        privilegeId: '681316ea-2b98-11e9-9992-0e5a20b1cdd2',
        privilegeType: 'VIEW',
        itemId: 'd5e12d27-00d5-4ed0-adf9-4be488b00e78',
        roleId: -888,
        createdBy: '1117',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
    },
];
//# sourceMappingURL=configitem.privilege.action.js.map