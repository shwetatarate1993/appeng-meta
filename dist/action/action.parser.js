"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseActionToConfig = (action) => {
    const createdAt = action.creationDate;
    const createdBy = action.createdBy;
    const updatedBy = action.updatedBy;
    const updatedAt = action.updationDate;
    const property = [];
    let itemId;
    if (action.configObjectId !== undefined && action.configObjectId !== ''
        && action.configObjectId !== null) {
        itemId = action.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (action.isProcessAction !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'IS_PROCESS_ACTION', action.isProcessAction !== undefined ? (action.isProcessAction ? '1' : '0') : '0', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (action.expressionVariables !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'EXPRESSION_VARIABLE', action.expressionVariables, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (action.accessiblityExpression !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'ACCESSIBLITY_EXPRESSION', action.accessiblityExpression, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (action.action !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'ACTION', action.action, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (action.formsId !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'FORMS_ID', action.formsId, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (action.description !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'DESCRIPTION', action.description, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (action.formFieldsId !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'FORMS_FIELD_ID', action.formFieldsId, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (action.type !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'TYPE', action.type, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (action.statusValue !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'STATUS_VALUE', action.statusValue, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, action.name, configitemtype_enum_1.ConfigItemTypes.ACTION, action.projectId, createdBy, action.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const actionPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(action.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(action.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(action.privileges, actionPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, actionPrivileges);
};
exports.parseConfigToAction = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.Action(details.get('IS_PROCESS_ACTION'), details.get('EXPRESSION_VARIABLE'), details.get('ACCESSIBLITY_EXPRESSION'), details.get('ACTION'), details.get('FORMS_ID'), details.get('DESCRIPTION'), details.get('FORMS_FIELD_ID'), details.get('TYPE'), details.get('STATUS_VALUE'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=action.parser.js.map