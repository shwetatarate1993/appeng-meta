import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { Action } from '../models';
export declare const parseActionToConfig: (action: Action) => ConfigMetadata;
export declare const parseConfigToAction: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => Action;
//# sourceMappingURL=action.parser.d.ts.map