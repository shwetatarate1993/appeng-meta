"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const CommonSchema = `

input ConfigItemPrivilegeInput {
    privilegeId: String
    privilegeType: String
    itemId: String
    roleId: Int
    createdBy: String
    isDeleted: Int
    creationDate: Date
    updatedBy: String
    updationDate: Date
    deletionDate: Date
}

input ConfigItemRelationInput {
    relationId: String
    relationType: String
    parentItemId: String
    childItemId: String
    createdBy: String
    isDeleted: Int
    creationDate: Date
    updatedBy: String
    updationDate: Date
    deletionDate: Date
}

type ConfigItemPrivilege {
    privilegeId: String
    privilegeType: String
    itemId: String
    roleId: Int
    createdBy: String
    isDeleted: Int
    creationDate: Date
    updatedBy: String
    updationDate: Date
    deletionDate: Date
}

type ConfigItemRelation {
    relationId: String
    relationType: String
    parentItemId: String
    childItemId: String
    createdBy: String
    isDeleted: Int
    creationDate: Date
    updatedBy: String
    updationDate: Date
    deletionDate: Date
}`;
exports.default = () => [CommonSchema];
//# sourceMappingURL=common.schema.js.map