"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const datasetrelproperty_parser_1 = require("../datasetrelproperty.parser");
const mockdata_1 = require("./mockdata");
test('parser datasetrelProperty object to config', () => {
    const datasetrelp = models_1.DataSetRelProperty.deserialize(mockdata_1.dataSetRelPropertyMockData[0]);
    const result = datasetrelproperty_parser_1.parseDataSetRelPropertyObjectToConfig(datasetrelp);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(datasetrelp.propertyType).toEqual('INNERJOIN');
});
test('parser config to DataSetRelProperty object', () => {
    const result = datasetrelproperty_parser_1.parseConfigToDataSetRelPropertyObject(mockdata_1.dataSetRelPropertyConfigMockData[0], mockdata_1.dataSetRelPropertyPropertyMockData, mockdata_1.dataSetRelPropertyRelationMockData, mockdata_1.dataSetRelPropertyPrivilegesMockData);
    expect(result).toHaveProperty('propertyType', 'INNERJOIN');
    expect(result).toHaveProperty('name');
    expect(result.privileges.length === 0).toBe(true);
});
//# sourceMappingURL=datasetrelproperty.parser.test.js.map