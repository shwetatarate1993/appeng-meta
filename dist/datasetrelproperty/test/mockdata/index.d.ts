export { default as dataSetRelPropertyMockData } from './datasetrelproperty.object';
export { default as dataSetRelPropertyConfigMockData } from './configitem.datasetrelproperty';
export { default as dataSetRelPropertyPropertyMockData } from './property.datasetrelproperty';
export { default as dataSetRelPropertyPrivilegesMockData } from './privileges.datasetrelproperty';
export { default as dataSetRelPropertyRelationMockData } from './relation.datasetrelproperty';
//# sourceMappingURL=index.d.ts.map