"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: '0102eab2-9b76-4105-a038-e45377a30fed',
        configObjectType: 'DataSetRelProperty',
        name: 'Backlog Attachment',
        propertyType: 'INNERJOIN',
        privileges: [],
        parentRelations: [
            {
                relationId: '47759b6a-f680-4663-874c-86543b894096',
                relationType: 'DataSetRel_DataSetRelProperty',
                parentItemId: '14521583-1413-482a-aa19-e9acd1b225f0',
                childItemId: '0102eab2-9b76-4105-a038-e45377a30fed',
                createdBy: '1115',
                isDeleted: 0,
                creationDate: '2018-02-26T00:00:00.000Z',
                updatedBy: '1115',
                updationDate: '2018-04-27T00:00:00.000Z',
                deletionDate: '2018-04-27T00:00:00.000Z',
            },
        ],
    },
];
//# sourceMappingURL=datasetrelproperty.object.js.map