"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var datasetrelproperty_object_1 = require("./datasetrelproperty.object");
exports.dataSetRelPropertyMockData = datasetrelproperty_object_1.default;
var configitem_datasetrelproperty_1 = require("./configitem.datasetrelproperty");
exports.dataSetRelPropertyConfigMockData = configitem_datasetrelproperty_1.default;
var property_datasetrelproperty_1 = require("./property.datasetrelproperty");
exports.dataSetRelPropertyPropertyMockData = property_datasetrelproperty_1.default;
var privileges_datasetrelproperty_1 = require("./privileges.datasetrelproperty");
exports.dataSetRelPropertyPrivilegesMockData = privileges_datasetrelproperty_1.default;
var relation_datasetrelproperty_1 = require("./relation.datasetrelproperty");
exports.dataSetRelPropertyRelationMockData = relation_datasetrelproperty_1.default;
//# sourceMappingURL=index.js.map