import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { DataSetRelProperty } from '../models';
export declare const parseDataSetRelPropertyObjectToConfig: (dataSetRelProperty: DataSetRelProperty) => ConfigMetadata;
export declare const parseConfigToDataSetRelPropertyObject: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => DataSetRelProperty;
//# sourceMappingURL=datasetrelproperty.parser.d.ts.map