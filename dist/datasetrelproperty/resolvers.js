"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const logicalcolumn_parser_1 = require("../logicalcolumn/logicalcolumn.parser");
const datasetrelproperty_parser_1 = require("./datasetrelproperty.parser");
exports.Query = {
    DataSetRelProperty: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, datasetrelproperty_parser_1.parseConfigToDataSetRelPropertyObject),
};
exports.DataSetRelProperty = {
    logicalColumns: (dataSetRelProperty, _, context) => common_meta_1.configService.fetchConfigsByParentId(dataSetRelProperty.configObjectId, logicalcolumn_parser_1.parseConfigToLogicalColumn),
};
exports.Mutation = {
    createDataSetRelProperty: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, datasetrelproperty_parser_1.parseDataSetRelPropertyObjectToConfig, datasetrelproperty_parser_1.parseConfigToDataSetRelPropertyObject);
    },
};
//# sourceMappingURL=resolvers.js.map