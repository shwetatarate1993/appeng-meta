"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseDataSetRelPropertyObjectToConfig = (dataSetRelProperty) => {
    const createdAt = dataSetRelProperty.creationDate;
    const createdBy = dataSetRelProperty.createdBy;
    const updatedBy = dataSetRelProperty.updatedBy;
    const updatedAt = dataSetRelProperty.updationDate;
    const property = [];
    let itemId;
    if (dataSetRelProperty.configObjectId !== undefined && dataSetRelProperty.configObjectId !== ''
        && dataSetRelProperty.configObjectId !== null) {
        itemId = dataSetRelProperty.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (dataSetRelProperty.propertyType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'PROPERTY_TYPE', dataSetRelProperty.propertyType, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, dataSetRelProperty.name, configitemtype_enum_1.ConfigItemTypes.DATASETRELPROPERTY, dataSetRelProperty.projectId, createdBy, dataSetRelProperty.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const datasetrelpropertyPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(dataSetRelProperty.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(dataSetRelProperty.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(dataSetRelProperty.privileges, datasetrelpropertyPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, datasetrelpropertyPrivileges);
};
exports.parseConfigToDataSetRelPropertyObject = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.DataSetRelProperty(configItem.configObjectId, details.get('PROPERTY_TYPE'), configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=datasetrelproperty.parser.js.map