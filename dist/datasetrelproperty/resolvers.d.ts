export declare const Query: {
    DataSetRelProperty: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").DataSetRelProperty>;
};
export declare const DataSetRelProperty: {
    logicalColumns: (dataSetRelProperty: any, _: any, context: any) => Promise<import("../models").LogicalColumn[]>;
};
export declare const Mutation: {
    createDataSetRelProperty: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map