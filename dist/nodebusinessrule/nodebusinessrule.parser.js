"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseNodeBusinessRuleToConfig = (nodeBusinessRule) => {
    const createdAt = nodeBusinessRule.creationDate;
    const createdBy = nodeBusinessRule.createdBy;
    const updatedBy = nodeBusinessRule.updatedBy;
    const updatedAt = nodeBusinessRule.updationDate;
    const property = [];
    let itemId;
    if (nodeBusinessRule.configObjectId !== undefined && nodeBusinessRule.configObjectId !== ''
        && nodeBusinessRule.configObjectId !== null) {
        itemId = nodeBusinessRule.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (nodeBusinessRule.executionType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('EXECUTION_TYPE', nodeBusinessRule.executionType, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (nodeBusinessRule.rule !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('RULE', nodeBusinessRule.rule, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (nodeBusinessRule.order !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ORDER', nodeBusinessRule.order, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, nodeBusinessRule.name, configitemtype_enum_1.ConfigItemTypes.NODEBUSINESSRULE, nodeBusinessRule.projectId, createdBy, nodeBusinessRule.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const nodeBusinessRulePrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(nodeBusinessRule.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(nodeBusinessRule.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(nodeBusinessRule.privileges, nodeBusinessRulePrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, nodeBusinessRulePrivileges);
};
exports.parseConfigToNodeBusinessRule = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.NodeBusinessRule(details.get('EXECUTION_TYPE'), details.get('RULE'), details.get('ORDER'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=nodebusinessrule.parser.js.map