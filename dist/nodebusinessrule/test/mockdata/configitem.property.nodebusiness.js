"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        propertyId: '898cc09e-6f8d-4eb6-a845-96c0f6ac4d92',
        propertyName: 'EXECUTION_TYPE',
        propertyValue: 'Javascript',
        itemId: 'f8fb0eaf-e42f-47de-b6da-ff92d13b4b61',
        createdBy: '1135',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '67c967d0-8e3e-475a-8280-e1373e9f97b8',
        propertyName: 'RULE',
        propertyValue: 'let a=5; let b=5; let c=a+b;',
        itemId: 'f8fb0eaf-e42f-47de-b6da-ff92d13b4b61',
        createdBy: '1135',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: 'b71e4bff-29e6-4583-aad4-ebe40074e9ee',
        propertyName: 'ORDER',
        propertyValue: '5',
        itemId: 'f8fb0eaf-e42f-47de-b6da-ff92d13b4b61',
        createdBy: '1135',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
];
//# sourceMappingURL=configitem.property.nodebusiness.js.map