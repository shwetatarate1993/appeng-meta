"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: 'f8fb0eaf-e42f-47de-b6da-ff92d13b4b61',
        configObjectType: 'NodeBusinessRule',
        name: 'Total calculation',
        projectId: 0,
        executionType: 'Javascript',
        rule: 'let a=5; let b=5; let c=a+b;',
        order: 5,
        createdBy: '1135',
        isDeleted: 0,
        itemDescription: null,
        creationDate: '2019-04-05T10:40:06.000Z',
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
        privileges: [],
        childRelations: [],
        parentRelations: [
            {
                relationId: '56237bdd-51ba-4a25-8836-d1a764d666ae',
                relationType: 'RootCompositeEntityNode_NodeBusinessRule',
                parentItemId: 'b0f0991f-8fbc-46c6-ac71-d1e4a5fc251a',
                childItemId: 'f8fb0eaf-e42f-47de-b6da-ff92d13b4b61',
                createdBy: '1135',
                isDeleted: 0,
                creationDate: '2018-02-26T00:00:00.000Z',
                updatedBy: '1135',
                updationDate: '2018-02-26T00:00:00.000Z',
                deletionDate: '2018-02-26T00:00:00.000Z',
            },
        ],
    },
];
//# sourceMappingURL=nodebusinessrule.object.js.map