declare const _default: {
    relationId: string;
    relationType: string;
    parentItemId: string;
    childItemId: string;
    createdBy: string;
    isDeleted: number;
    creationDate: any;
    updatedBy: string;
    updationDate: any;
    deletionDate: any;
}[];
export default _default;
//# sourceMappingURL=configitem.relation.nodebusiness.d.ts.map