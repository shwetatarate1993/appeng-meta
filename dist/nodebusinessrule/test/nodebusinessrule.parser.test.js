"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const nodebusinessrule_parser_1 = require("../nodebusinessrule.parser");
const mockdata_1 = require("./mockdata");
test('parser nodeBusinessRule to config', () => {
    const nodeBusinessRule = models_1.NodeBusinessRule.deserialize(mockdata_1.nodeBusinessRuleObjectMockData[0]);
    const result = nodebusinessrule_parser_1.parseNodeBusinessRuleToConfig(nodeBusinessRule);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItem.configObjectType).toEqual('NodeBusinessRule');
    expect(result.configItemProperty[0].propertyName).toEqual('EXECUTION_TYPE');
    expect(result.configItemProperty[1].propertyName).toEqual('RULE');
    expect(result.configItemProperty[2].propertyName).toEqual('ORDER');
});
test('parser config to nodeBusinessRule', () => {
    const result = nodebusinessrule_parser_1.parseConfigToNodeBusinessRule(mockdata_1.nodeBusinessRuleConfigitemMockData[0], mockdata_1.nodeBusinessRulePropertyMockData, mockdata_1.nodeBusinessRuleRelationMockData, mockdata_1.nodeBusinessRulePrivilegesMockData);
    expect(result).toHaveProperty('executionType');
    expect(result).toHaveProperty('rule');
    expect(result).toHaveProperty('order');
});
//# sourceMappingURL=nodebusinessrule.parser.test.js.map