"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const nodebusinessrule_parser_1 = require("./nodebusinessrule.parser");
exports.Query = {
    NodeBusinessRule: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, nodebusinessrule_parser_1.parseConfigToNodeBusinessRule),
};
exports.Mutation = {
    createNodeBusinessRule: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, nodebusinessrule_parser_1.parseNodeBusinessRuleToConfig, nodebusinessrule_parser_1.parseConfigToNodeBusinessRule);
    },
};
//# sourceMappingURL=resolvers.js.map