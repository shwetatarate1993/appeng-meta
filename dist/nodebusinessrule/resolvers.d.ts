export declare const Query: {
    NodeBusinessRule: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").NodeBusinessRule>;
};
export declare const Mutation: {
    createNodeBusinessRule: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map