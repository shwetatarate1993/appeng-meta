import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { NodeBusinessRule } from '../models';
export declare const parseNodeBusinessRuleToConfig: (nodeBusinessRule: NodeBusinessRule) => ConfigMetadata;
export declare const parseConfigToNodeBusinessRule: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => NodeBusinessRule;
//# sourceMappingURL=nodebusinessrule.parser.d.ts.map