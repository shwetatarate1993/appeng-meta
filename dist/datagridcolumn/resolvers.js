"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const logicalcolumn_parser_1 = require("../logicalcolumn/logicalcolumn.parser");
const datagridcolumn_parser_1 = require("./datagridcolumn.parser");
exports.Query = {
    DataGridColumn: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, datagridcolumn_parser_1.parseConfigToDataGridColumn),
};
exports.DataGridColumn = {
    logicalColumn: async (dataGridColumn, _, context) => {
        const columnRel = dataGridColumn.childRelations.find((rel) => rel.relationType === relationtype_enum_1.RelationType.DATAGRIDCOLUMN_ENTITYCOLUMN);
        return await common_meta_1.configService.fetchConfigById(columnRel.childItemId, logicalcolumn_parser_1.parseConfigToLogicalColumn);
    },
};
exports.Mutation = {
    createDataGridColumn: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, datagridcolumn_parser_1.parseDataGridColumnToConfig, datagridcolumn_parser_1.parseConfigToDataGridColumn);
    },
};
//# sourceMappingURL=resolvers.js.map