"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseDataGridColumnToConfig = (dataGridColumn) => {
    const createdAt = dataGridColumn.creationDate;
    const createdBy = dataGridColumn.createdBy;
    const updatedBy = dataGridColumn.updatedBy;
    const updatedAt = dataGridColumn.updationDate;
    const property = [];
    let itemId;
    if (dataGridColumn.configObjectId !== undefined && dataGridColumn.configObjectId !== ''
        && dataGridColumn.configObjectId !== null) {
        itemId = dataGridColumn.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (dataGridColumn.headerName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('HEADER_NAME', dataGridColumn.headerName, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (dataGridColumn.order !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('HEADER_ORDER', dataGridColumn.order, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    property.push(new meta_db_1.ConfigItemProperty('IS_VISIBLE', dataGridColumn.visible !== undefined ? (dataGridColumn.visible ? '1' : '0') : '1', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    // for setting default value as true
    property.push(new meta_db_1.ConfigItemProperty('IS_DISPLAY_DETAIL', dataGridColumn.isDisplayDetail !== undefined ? (dataGridColumn.isDisplayDetail ? '1' : '0') : '1', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    if (dataGridColumn.toolTip !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('TOOLTIP', dataGridColumn.toolTip, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    property.push(new meta_db_1.ConfigItemProperty('IS_ACTION_COLUMN', dataGridColumn.actionColumn !== undefined ? (dataGridColumn.actionColumn ? '1' : '0') : '1', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    if (dataGridColumn.actionColumnType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ACTION_COLUMN_TYPE', dataGridColumn.actionColumnType, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (dataGridColumn.group !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('GROUP', dataGridColumn.group, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (dataGridColumn.icon !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ICON', dataGridColumn.icon, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (dataGridColumn.hrefValue !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('HREF_VALUE', dataGridColumn.hrefValue, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (dataGridColumn.dateFormat !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('DATE_FORMAT', dataGridColumn.dateFormat, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    property.push(new meta_db_1.ConfigItemProperty('IS_HYPERLINK', dataGridColumn.hyperLink !== undefined ? (dataGridColumn.hyperLink ? '1' : '0') : '1', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    property.push(new meta_db_1.ConfigItemProperty('IS_KEY', dataGridColumn.key !== undefined ? (dataGridColumn.key ? '1' : '0') : '1', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    if (dataGridColumn.editabilityRegex !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('EDITABILITY_REGEX', dataGridColumn.editabilityRegex, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    property.push(new meta_db_1.ConfigItemProperty('IS_EXPRESSION_AVAILABLE', dataGridColumn.expressionAvailable !== undefined ? (dataGridColumn.expressionAvailable ? '1' : '0') : '1', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    if (dataGridColumn.width !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('WIDTH', dataGridColumn.width, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    property.push(new meta_db_1.ConfigItemProperty('IS_FIXED_COLUMN', dataGridColumn.fixColumn !== undefined ? (dataGridColumn.fixColumn ? '1' : '0') : '1', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    if (dataGridColumn.goToLink !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('GO_TO_LINK', dataGridColumn.goToLink, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (dataGridColumn.accessibilityRegex !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ACCESSBILITY_REGEX', dataGridColumn.accessibilityRegex, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, dataGridColumn.name, configitemtype_enum_1.ConfigItemTypes.DATAGRIDCOLUMN, dataGridColumn.projectId, createdBy, dataGridColumn.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const DataGridColumnPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(dataGridColumn.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(dataGridColumn.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(dataGridColumn.privileges, DataGridColumnPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, DataGridColumnPrivileges);
};
exports.parseConfigToDataGridColumn = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.DataGridColumn(details.get('HEADER_NAME'), details.get('HEADER_ORDER'), details.get('IS_DISPLAY_DETAIL'), details.get('IS_VISIBLE'), details.get('TOOLTIP'), details.get('IS_ACTION_COLUMN'), details.get('ACTION_COLUMN_TYPE'), details.get('GROUP'), details.get('ICON'), details.get('HREF_VALUE'), details.get('DATE_FORMAT'), details.get('IS_HYPERLINK'), details.get('IS_KEY'), details.get('IS_EXPRESSION_AVAILABLE'), details.get('EDITABILITY_REGEX'), details.get('WIDTH'), details.get('IS_FIXED_COLUMN'), details.get('GO_TO_LINK'), details.get('ACCESSBILITY_REGEX'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=datagridcolumn.parser.js.map