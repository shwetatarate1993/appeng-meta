import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { DataGridColumn } from '../models';
export declare const parseDataGridColumnToConfig: (dataGridColumn: DataGridColumn) => ConfigMetadata;
export declare const parseConfigToDataGridColumn: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => DataGridColumn;
//# sourceMappingURL=datagridcolumn.parser.d.ts.map