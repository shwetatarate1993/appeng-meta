export declare const Query: {
    DataGridColumn: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").DataGridColumn>;
};
export declare const DataGridColumn: {
    logicalColumn: (dataGridColumn: any, _: any, context: any) => Promise<import("../models").LogicalColumn>;
};
export declare const Mutation: {
    createDataGridColumn: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map