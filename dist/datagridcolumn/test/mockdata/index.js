"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var datagridcolumn_object_1 = require("./datagridcolumn.object");
exports.dataGridColumnMockData = datagridcolumn_object_1.default;
var configitem_relation_datagridcolumn_1 = require("./configitem.relation.datagridcolumn");
exports.configRelationsMockDatadataGridColumn = configitem_relation_datagridcolumn_1.default;
var configitem_property_datagridcolumn_1 = require("./configitem.property.datagridcolumn");
exports.dataGridColumnPropertyMockData = configitem_property_datagridcolumn_1.default;
var configitem_datagridcolumn_1 = require("./configitem.datagridcolumn");
exports.configitemMockDatadataGridColumn = configitem_datagridcolumn_1.default;
var configitem_privileges_datagridcolumn_1 = require("./configitem.privileges.datagridcolumn");
exports.configPrivilegesMockDataDataGridColumn = configitem_privileges_datagridcolumn_1.default;
//# sourceMappingURL=index.js.map