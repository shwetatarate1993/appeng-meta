export { default as dataGridColumnMockData } from './datagridcolumn.object';
export { default as configRelationsMockDatadataGridColumn } from './configitem.relation.datagridcolumn';
export { default as dataGridColumnPropertyMockData } from './configitem.property.datagridcolumn';
export { default as configitemMockDatadataGridColumn } from './configitem.datagridcolumn';
export { default as configPrivilegesMockDataDataGridColumn } from './configitem.privileges.datagridcolumn';
//# sourceMappingURL=index.d.ts.map