"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        relationId: 'a98db1dc-ba1b-4c97-8b6b-051d3027091d',
        relationType: 'DataGrid_DataGridColumn',
        parentItemId: '605638de-a49e-4d52-ae0c-80bc845a0770',
        childItemId: '8c2f1ac0-97cb-4dcd-aaf0-2b88ebbb0f7c',
        createdBy: null,
        isDeleted: 0,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
    },
    {
        relationId: 'bf9bd48d-0fe3-4576-ad7d-3f14f5cabc0e',
        relationType: 'DataGridColumn_EntityColumn',
        parentItemId: '8c2f1ac0-97cb-4dcd-aaf0-2b88ebbb0f7c',
        childItemId: '6ad8e5e5-4f29-474f-b336-535449f522a5',
        createdBy: null,
        isDeleted: 0,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
    },
];
//# sourceMappingURL=configitem.relation.datagridcolumn.js.map