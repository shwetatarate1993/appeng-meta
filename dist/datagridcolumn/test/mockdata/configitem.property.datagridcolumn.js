"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        propertyId: '530c5cc8-85ee-4641-9dbc-1dc71c7818c7',
        propertyName: 'HEADER_NAME',
        propertyValue: 'en-!lAnGuAge!-Name-!sEpArAToR!-gu-!lAnGuAge!-નામ',
        itemId: '8c2f1ac0-97cb-4dcd-aaf0-2b88ebbb0f7c',
        createdBy: null,
        isDeleted: 0,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
    },
    {
        propertyId: '414503ef-1dc4-4ec6-8078-11dbf2b5011e',
        propertyName: 'HEADER_ORDER',
        propertyValue: 15,
        itemId: '8c2f1ac0-97cb-4dcd-aaf0-2b88ebbb0f7c',
        createdBy: null,
        isDeleted: 0,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
    },
    {
        propertyId: '8614d62a-c93e-4801-968e-053a52114911',
        propertyName: 'IS_VISIBLE',
        propertyValue: '1',
        itemId: '8c2f1ac0-97cb-4dcd-aaf0-2b88ebbb0f7c',
        createdBy: null,
        isDeleted: 0,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
    },
];
//# sourceMappingURL=configitem.property.datagridcolumn.js.map