"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const datagridcolumn_parser_1 = require("../datagridcolumn.parser");
const mockdata_1 = require("./mockdata");
test('parser Datagridcolumn object to config', () => {
    const datagridcolumn = models_1.DataGridColumn.deserialize(mockdata_1.dataGridColumnMockData[0]);
    const result = datagridcolumn_parser_1.parseDataGridColumnToConfig(datagridcolumn);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItem.configObjectType).toEqual('DataGridColumn');
});
test('parser config to Datagridcolumn object', () => {
    const result = datagridcolumn_parser_1.parseConfigToDataGridColumn(mockdata_1.configitemMockDatadataGridColumn[0], mockdata_1.dataGridColumnPropertyMockData, mockdata_1.configRelationsMockDatadataGridColumn, mockdata_1.configPrivilegesMockDataDataGridColumn);
    expect(result).toHaveProperty('headerName', 'en-!lAnGuAge!-Name-!sEpArAToR!-gu-!lAnGuAge!-નામ');
    expect(result).toHaveProperty('order', 15);
    expect(result).toHaveProperty('visible', true);
    expect(result.privileges.length === 5).toBe(true);
    expect(result.parentRelations.length === 1).toBe(true);
    expect(result.childRelations.length === 1).toBe(true);
    expect(result.parentRelations[0].relationType === 'DataGrid_DataGridColumn').toBe(true);
    expect(result.childRelations[0].relationType === 'DataGridColumn_EntityColumn').toBe(true);
});
//# sourceMappingURL=datagridcolumn.parser.test.js.map