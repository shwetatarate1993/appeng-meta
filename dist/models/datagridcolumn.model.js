'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
class DataGridColumn {
    constructor(headerName, order, isDisplayDetail, visible, toolTip, actionColumn, actionColumnType, group, icon, hrefValue, dateFormat, hyperLink, key, editabilityRegex, expressionAvailable, width, fixColumn, goToLink, accessibilityRegex, itemId, itemName, itemType, projectId, createdBy, itemDescription, creationDate, updatedBy, updationDate, deletionDate, isDeleted, privileges, childRelations, parentRelations) {
        this.configObjectId = itemId;
        this.name = itemName;
        this.configObjectType = itemType;
        this.projectId = projectId;
        this.createdBy = createdBy;
        this.itemDescription = itemDescription;
        this.creationDate = creationDate;
        this.updatedBy = updatedBy;
        this.updationDate = updationDate;
        this.deletionDate = deletionDate;
        this.isDeleted = isDeleted;
        this.order = order;
        this.isDisplayDetail = (typeof isDisplayDetail === 'boolean') ?
            isDisplayDetail : ((isDisplayDetail === '0') ? false : true);
        this.headerName = headerName;
        this.actionColumnType = actionColumnType;
        this.group = group;
        this.icon = icon;
        this.hrefValue = hrefValue;
        this.dateFormat = dateFormat;
        this.width = width;
        this.goToLink = goToLink;
        this.accessibilityRegex = accessibilityRegex;
        this.toolTip = toolTip;
        this.visible = (typeof visible === 'boolean') ?
            visible : ((visible === '0') ? false : true);
        this.hyperLink = (typeof hyperLink === 'boolean') ?
            hyperLink : ((hyperLink === '0') ? false : true);
        this.key = (typeof key === 'boolean') ?
            key : ((key === '0') ? false : true);
        this.editabilityRegex = editabilityRegex;
        this.expressionAvailable = (typeof expressionAvailable === 'boolean') ?
            expressionAvailable : ((expressionAvailable === '0') ? false : true);
        this.fixColumn = (typeof fixColumn === 'boolean') ?
            fixColumn : ((fixColumn === '0') ? false : true);
        this.actionColumn = (typeof actionColumn === 'boolean') ?
            actionColumn : ((actionColumn === '0') ? false : true);
        this.privileges = privileges;
        this.childRelations = childRelations;
        this.parentRelations = parentRelations;
        Object.freeze(this);
    }
    static deserialize(input) {
        const privileges = [];
        const childRelations = [];
        const parentRelations = [];
        if (input.privileges !== undefined && input.privileges !== null) {
            input.privileges.forEach((privilege) => {
                privileges.push(meta_db_1.ConfigItemPrivilege.deserialize(privilege));
            });
        }
        if (input.childRelations !== undefined && input.childRelations !== null) {
            input.childRelations.forEach((relation) => {
                childRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        if (input.parentRelations !== undefined && input.parentRelations !== null) {
            input.parentRelations.forEach((relation) => {
                parentRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        return new DataGridColumn(input.headerName !== undefined ? input.headerName : null, input.order !== undefined ? input.order : null, input.isDisplayDetail !== undefined ? input.isDisplayDetail : null, input.visible !== undefined ? input.visible : null, input.actionColumnType !== undefined ? input.actionColumnType : null, input.group !== undefined ? input.group : null, input.icon !== undefined ? input.icon : null, input.hrefValue !== undefined ? input.hrefValue : null, input.dateFormat !== undefined ? input.dateFormat : null, input.hyperLink !== undefined ? input.hyperLink : null, input.key !== undefined ? input.key : null, input.editabilityRegex !== undefined ? input.editabilityRegex : null, input.expressionAvailable !== undefined ? input.expressionAvailable : null, input.width !== undefined ? input.width : null, input.fixColumn !== undefined ? input.fixColumn : null, input.goToLink !== undefined ? input.goToLink : null, input.accessibilityRegex !== undefined ? input.accessibilityRegex : null, input.toolTip !== undefined ? input.toolTip : null, input.actionColumn !== undefined ? input.actionColumn : null, input.configObjectId !== undefined ? input.configObjectId : null, input.name !== undefined ? input.name : null, input.configObjectType !== undefined ? input.configObjectType : null, input.projectId !== undefined ? input.projectId : null, input.createdBy !== undefined ? input.createdBy : null, input.itemDescription !== undefined ? input.itemDescription : null, input.creationDate !== undefined ? input.creationDate : null, input.updatedBy !== undefined ? input.updatedBy : null, input.updationDate !== undefined ? input.updationDate : null, input.deletionDate !== undefined ? input.deletionDate : null, input.isDeleted !== undefined ? input.isDeleted : null, privileges, childRelations, parentRelations);
    }
}
exports.default = DataGridColumn;
//# sourceMappingURL=datagridcolumn.model.js.map