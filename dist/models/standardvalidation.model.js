'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
class StandardValidation {
    constructor(itemId, itemName, itemType, projectId, createdBy, itemDescription, creationDate, updatedBy, updationDate, deletionDate, isDeleted, mode, defaultErrorMessage, validationType, regex, isConditionAvailable, conditionExpression, conditionFields, description, privileges, childRelations, parentRelations) {
        this.configObjectId = itemId;
        this.name = itemName;
        this.configObjectType = itemType;
        this.projectId = projectId;
        this.createdBy = createdBy;
        this.itemDescription = itemDescription;
        this.creationDate = creationDate;
        this.updatedBy = updatedBy;
        this.updationDate = updationDate;
        this.deletionDate = deletionDate;
        this.isDeleted = isDeleted;
        this.defaultErrorMessage = defaultErrorMessage;
        this.validationType = validationType;
        this.regex = regex;
        this.isConditionAvailable = (typeof isConditionAvailable === 'boolean') ?
            isConditionAvailable : ((isConditionAvailable === '0') ? false : true);
        this.conditionExpression = conditionExpression;
        this.conditionFields = conditionFields;
        this.description = description;
        this.privileges = privileges;
        this.childRelations = childRelations;
        this.parentRelations = parentRelations;
        Object.freeze(this);
    }
    static deserialize(input) {
        const privileges = [];
        const childRelations = [];
        const parentRelations = [];
        if (input.privileges !== undefined && input.privileges !== null) {
            input.privileges.forEach((privilege) => {
                privileges.push(meta_db_1.ConfigItemPrivilege.deserialize(privilege));
            });
        }
        if (input.childRelations !== undefined && input.childRelations !== null) {
            input.childRelations.forEach((relation) => {
                childRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        if (input.parentRelations !== undefined && input.parentRelations !== null) {
            input.parentRelations.forEach((relation) => {
                parentRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        return new StandardValidation(input.configObjectId !== undefined ? input.configObjectId : null, input.name !== undefined ? input.name : null, input.configObjectType !== undefined ? input.configObjectType : null, input.projectId !== undefined ? input.projectId : null, input.createdBy !== undefined ? input.createdBy : null, input.itemDescription !== undefined ? input.itemDescription : null, input.creationDate !== undefined ? input.creationDate : null, input.updatedBy !== undefined ? input.updatedBy : null, input.updationDate !== undefined ? input.updationDate : null, input.deletionDate !== undefined ? input.deletionDate : null, input.isDeleted !== undefined ? input.isDeleted : null, input.mode !== undefined ? input.mode : null, input.defaultErrorMessage !== undefined ? input.defaultErrorMessage : null, input.validationType !== undefined ? input.validationType : null, input.regex !== undefined ? input.regex : null, input.isConditionAvailable !== undefined ? input.isConditionAvailable : null, input.conditionExpression !== undefined ? input.conditionExpression : null, input.conditionFields !== undefined ? input.conditionFields : null, input.description !== undefined ? input.description : null, privileges, childRelations, parentRelations);
    }
}
exports.default = StandardValidation;
//# sourceMappingURL=standardvalidation.model.js.map