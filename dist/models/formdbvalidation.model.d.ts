import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class FormDbValidation implements ConfigItem {
    static deserialize(input: any): FormDbValidation;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    mode: string;
    datasourceName: string;
    validationType: string;
    validationExpression: string;
    validationQid: string;
    validationMessage: string;
    validationExpressionKeys: string;
    fieldIds: string;
    isConditionAvailable: boolean;
    conditionExpression: string;
    conditionFields: string;
    description: string;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, mode: string, datasourceName: string, validationType: string, validationExpression: string, validationQid: string, validationMessage: string, validationExpressionKeys: string, fieldIds: string, isConditionAvailable: any, conditionExpression: string, description: string, conditionFields: string, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=formdbvalidation.model.d.ts.map