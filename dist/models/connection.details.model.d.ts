export default class ConnectionDetails {
    host: string;
    port: number;
    dbname: string;
    user: string;
    password: string;
    constructor(host: string, port: number, dbname: string, user: string, password: string);
}
//# sourceMappingURL=connection.details.model.d.ts.map