'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
class FormSection {
    constructor(objectId, headerLabel, order, displayName, accessibilityRegex, editabilityRegex, tabGroup, isRenderOnRepeat, requiredFormfields, componentsPerRow, expressionAvailable, itemName, itemType, projectId, createdBy, itemDescription, creationDate, updatedBy, updationDate, deletionDate, isDeleted, privileges, childRelations, parentRelations) {
        this.configObjectId = objectId;
        this.headerLabel = headerLabel;
        this.order = order;
        this.displayName = displayName;
        this.accessibilityRegex = accessibilityRegex;
        this.editabilityRegex = editabilityRegex;
        this.tabGroup = tabGroup;
        this.isRenderOnRepeat = (typeof isRenderOnRepeat === 'boolean') ?
            isRenderOnRepeat : ((isRenderOnRepeat === '0') ? false : true);
        this.componentsPerRow = componentsPerRow;
        this.expressionAvailable = (typeof expressionAvailable === 'boolean') ?
            expressionAvailable : ((expressionAvailable === '0') ? false : true);
        this.name = itemName;
        this.configObjectType = itemType;
        this.projectId = projectId;
        this.createdBy = createdBy;
        this.itemDescription = itemDescription;
        this.creationDate = creationDate;
        this.updatedBy = updatedBy;
        this.updationDate = updationDate;
        this.deletionDate = deletionDate;
        this.isDeleted = isDeleted;
        this.privileges = privileges;
        this.childRelations = childRelations;
        this.parentRelations = parentRelations;
        Object.freeze(this);
    }
    static deserialize(input) {
        const privileges = [];
        const childRelations = [];
        const parentRelations = [];
        if (input.privileges !== undefined && input.privileges !== null) {
            input.privileges.forEach((privilege) => {
                privileges.push(meta_db_1.ConfigItemPrivilege.deserialize(privilege));
            });
        }
        if (input.childRelations !== undefined && input.childRelations !== null) {
            input.childRelations.forEach((relation) => {
                childRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        if (input.parentRelations !== undefined && input.parentRelations !== null) {
            input.parentRelations.forEach((relation) => {
                parentRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        return new FormSection(input.configObjectId !== undefined ? input.configObjectId : null, input.headerLabel !== undefined ? input.headerLabel : null, input.order !== undefined ? input.order : null, input.displayName !== undefined ? input.displayName : null, input.accessibilityRegex !== undefined ? input.accessibilityRegex : null, input.editabilityRegex !== undefined ? input.editabilityRegex : null, input.tabGroup !== undefined ? input.tabGroup : null, input.isRenderOnRepeat !== undefined ? input.isRenderOnRepeat : null, input.requiredFormfields !== undefined ? input.requiredFormfields : null, input.componentsPerRow !== undefined ? input.componentsPerRow : null, input.expressionAvailable !== undefined ? input.expressionAvailable : null, input.name !== undefined ? input.name : null, input.configObjectType !== undefined ? input.configObjectType : null, input.projectId !== undefined ? input.projectId : null, input.createdBy !== undefined ? input.createdBy : null, input.itemDescription !== undefined ? input.itemDescription : null, input.creationDate !== undefined ? input.creationDate : null, input.updatedBy !== undefined ? input.updatedBy : null, input.updationDate !== undefined ? input.updationDate : null, input.deletionDate !== undefined ? input.deletionDate : null, input.isDeleted !== undefined ? input.isDeleted : null, privileges, childRelations, parentRelations);
    }
}
exports.default = FormSection;
//# sourceMappingURL=formsection.model.js.map