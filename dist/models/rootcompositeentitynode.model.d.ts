import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class RootCompositeEntityNode implements ConfigItem {
    static deserialize(input: any): RootCompositeEntityNode;
    displayNodeName: string;
    graphTemplate: string;
    order: number;
    expressionAvailble: boolean;
    expressionFieldString: string;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(displayNodeName: string, graphTemplate: string, order: number, itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=rootcompositeentitynode.model.d.ts.map