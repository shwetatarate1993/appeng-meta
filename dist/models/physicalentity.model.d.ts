import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class PhsicalEntity implements ConfigItem {
    static deserialize(input: any): PhsicalEntity;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    expressionAvailable: boolean;
    accessibilityRegex: string;
    isMultivalueMapping: boolean;
    isPrimaryEntity: boolean;
    order: number;
    dbType: string;
    dbTypeName: string;
    schemaName: string;
    releaseAreaSessName: string;
    workAreaSessName: string;
    insertQID: string;
    updateQID: string;
    sequenceQID: string;
    singleSelectQID: string;
    multiSelectQID: string;
    deleteQID: string;
    isAuditRequired: boolean;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(expressionAvailable: any, accessibilityRegex: string, isMultivalueMapping: any, isPrimaryEntity: any, order: number, dbType: string, dbTypeName: string, schemaName: string, releaseAreaSessName: string, workAreaSessName: string, insertQID: string, updateQID: string, sequenceQID: string, singleSelectQID: string, multiSelectQID: string, deleteQID: string, isAuditRequired: any, itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=physicalentity.model.d.ts.map