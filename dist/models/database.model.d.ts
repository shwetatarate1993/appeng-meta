import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
import { ConnectionDetails } from '../models';
export default class Database implements ConfigItem {
    static deserialize(input: any): Database;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    engine: string;
    features: string;
    details: ConnectionDetails;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, engine: string, features: string, host: string, port: number, dbname: string, user: string, password: string, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=database.model.d.ts.map