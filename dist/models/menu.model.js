'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
class Menu {
    constructor(displayLabel, hrefBaseUrl, menuClass, header, order, isLabel, objectId, isOpenInNewTab, submenuContainerClass, subMenu, report, customUrl, componentsPerRow, linkType, itemId, itemName, itemType, projectId, createdBy, itemDescription, creationDate, updatedBy, updationDate, deletionDate, isDeleted, privileges, childRelations, parentRelations) {
        this.configObjectId = itemId;
        this.name = itemName;
        this.configObjectType = itemType;
        this.projectId = projectId;
        this.createdBy = createdBy;
        this.itemDescription = itemDescription;
        this.creationDate = creationDate;
        this.updatedBy = updatedBy;
        this.updationDate = updationDate;
        this.deletionDate = deletionDate;
        this.isDeleted = isDeleted;
        this.displayLabel = displayLabel;
        this.hrefBaseUrl = hrefBaseUrl;
        this.menuClass = menuClass;
        this.header = header;
        this.order = order;
        this.isLabel = (typeof isLabel === 'boolean') ?
            isLabel : ((isLabel === '0') ? false : true);
        this.objectId = objectId;
        this.isOpenInNewTab = (typeof isOpenInNewTab === 'boolean') ?
            isOpenInNewTab : ((isOpenInNewTab === '0') ? false : true);
        this.submenuContainerClass = submenuContainerClass;
        this.subMenu = (typeof subMenu === 'boolean') ?
            subMenu : ((subMenu === '0') ? false : true);
        this.report = report;
        this.customUrl = customUrl;
        this.componentsPerRow = componentsPerRow;
        this.linkType = linkType;
        this.privileges = privileges;
        this.childRelations = childRelations;
        this.parentRelations = parentRelations;
        Object.freeze(this);
    }
    static deserialize(input) {
        const privileges = [];
        const childRelations = [];
        const parentRelations = [];
        if (input.privileges !== undefined && input.privileges !== null) {
            input.privileges.forEach((privilege) => {
                privileges.push(meta_db_1.ConfigItemPrivilege.deserialize(privilege));
            });
        }
        if (input.childRelations !== undefined && input.childRelations !== null) {
            input.childRelations.forEach((relation) => {
                childRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        if (input.parentRelations !== undefined && input.parentRelations !== null) {
            input.parentRelations.forEach((relation) => {
                parentRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        return new Menu(input.displayLabel !== undefined ? input.displayLabel : null, input.hrefBaseUrl !== undefined ? input.hrefBaseUrl : null, input.menuClass !== undefined ? input.menuClass : null, input.header !== undefined ? input.header : null, input.order !== undefined ? input.order : null, input.isLabel !== undefined ? input.isLabel : null, input.objectId !== undefined ? input.objectId : null, input.isOpenInNewTab !== undefined ? input.isOpenInNewTab : null, input.submenuContainerClass !== undefined ? input.submenuContainerClass : null, input.subMenu !== undefined ? input.subMenu : null, input.report !== undefined ? input.report : null, input.customUrl !== undefined ? input.customUrl : null, input.componentsPerRow !== undefined ? input.componentsPerRow : null, input.linkType !== undefined ? input.linkType : null, input.configObjectId !== undefined ? input.configObjectId : null, input.name !== undefined ? input.name : null, input.configObjectType !== undefined ? input.configObjectType : null, input.projectId !== undefined ? input.projectId : null, input.createdBy !== undefined ? input.createdBy : null, input.itemDescription !== undefined ? input.itemDescription : null, input.creationDate !== undefined ? input.creationDate : null, input.updatedBy !== undefined ? input.updatedBy : null, input.updationDate !== undefined ? input.updationDate : null, input.deletionDate !== undefined ? input.deletionDate : null, input.isDeleted !== undefined ? input.isDeleted : null, privileges, childRelations, parentRelations);
    }
}
exports.default = Menu;
//# sourceMappingURL=menu.model.js.map