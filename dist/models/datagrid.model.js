'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
class DataGrid {
    constructor(defaultOrdering, gridType, isHavingAdvanceFilterForm, scroll, swimlaneRequired, modalRequired, isRowReOrder, itemId, itemName, itemType, projectId, createdBy, itemDescription, creationDate, updatedBy, updationDate, deletionDate, isDeleted, privileges, childRelations, parentRelations) {
        this.configObjectId = itemId;
        this.name = itemName;
        this.configObjectType = itemType;
        this.projectId = projectId;
        this.createdBy = createdBy;
        this.itemDescription = itemDescription;
        this.creationDate = creationDate;
        this.updatedBy = updatedBy;
        this.updationDate = updationDate;
        this.deletionDate = deletionDate;
        this.isDeleted = isDeleted;
        this.defaultOrdering = (typeof defaultOrdering === 'boolean') ?
            defaultOrdering : ((defaultOrdering === '1') ? true : false);
        this.gridType = gridType;
        this.isHavingAdvanceFilterForm = (typeof isHavingAdvanceFilterForm === 'boolean') ?
            isHavingAdvanceFilterForm : ((isHavingAdvanceFilterForm === '0') ? false : true);
        this.scroll = (typeof scroll === 'boolean') ?
            scroll : ((scroll === '0') ? false : true);
        this.swimlaneRequired = (typeof swimlaneRequired === 'boolean') ?
            swimlaneRequired : ((swimlaneRequired === '0') ? false : true);
        this.modalRequired = (typeof modalRequired === 'boolean') ?
            modalRequired : ((modalRequired === '0') ? false : true);
        this.isRowReOrder = (typeof isRowReOrder === 'boolean') ?
            isRowReOrder : ((isRowReOrder === '0') ? false : true);
        this.privileges = privileges;
        this.childRelations = childRelations;
        this.parentRelations = parentRelations;
        Object.freeze(this);
    }
    static deserialize(input) {
        const privileges = [];
        const childRelations = [];
        const parentRelations = [];
        if (input.privileges !== undefined && input.privileges !== null) {
            input.privileges.forEach((privilege) => {
                privileges.push(meta_db_1.ConfigItemPrivilege.deserialize(privilege));
            });
        }
        if (input.childRelations !== undefined && input.childRelations !== null) {
            input.childRelations.forEach((relation) => {
                childRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        if (input.parentRelations !== undefined && input.parentRelations !== null) {
            input.parentRelations.forEach((relation) => {
                parentRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        return new DataGrid(input.defaultOrdering !== undefined ? input.defaultOrdering : null, input.gridType !== undefined ? input.gridType : null, input.isHavingAdvanceFilterForm !== undefined ? input.isHavingAdvanceFilterForm : null, input.scroll !== undefined ? input.scroll : null, input.swimlaneRequired !== undefined ? input.swimlaneRequired : null, input.modalRequired !== undefined ? input.modalRequired : null, input.isRowReOrder !== undefined ? input.isRowReOrder : null, input.configObjectId !== undefined ? input.configObjectId : null, input.name !== undefined ? input.name : null, input.configObjectType !== undefined ? input.configObjectType : null, input.projectId !== undefined ? input.projectId : null, input.createdBy !== undefined ? input.createdBy : null, input.itemDescription !== undefined ? input.itemDescription : null, input.creationDate !== undefined ? input.creationDate : null, input.updatedBy !== undefined ? input.updatedBy : null, input.updationDate !== undefined ? input.updationDate : null, input.deletionDate !== undefined ? input.deletionDate : null, input.isDeleted !== undefined ? input.isDeleted : null, privileges, childRelations, parentRelations);
    }
}
exports.default = DataGrid;
//# sourceMappingURL=datagrid.model.js.map