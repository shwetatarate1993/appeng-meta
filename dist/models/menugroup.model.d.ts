import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class MenuGroup implements ConfigItem {
    static deserialize(input: any): MenuGroup;
    name: string;
    configObjectId: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    leftMenuRequired: boolean;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    usedInAllProjects: boolean;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(usedInAllProjects: any, itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, leftMenuRequired: any, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=menugroup.model.d.ts.map