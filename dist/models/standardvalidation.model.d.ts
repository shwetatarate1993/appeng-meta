import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class StandardValidation implements ConfigItem {
    static deserialize(input: any): StandardValidation;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    mode: string;
    defaultErrorMessage: string;
    validationType: string;
    regex: string;
    isConditionAvailable: boolean;
    conditionExpression: string;
    conditionFields: string;
    description: string;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, mode: string, defaultErrorMessage: string, validationType: string, regex: string, isConditionAvailable: any, conditionExpression: string, conditionFields: string, description: string, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=standardvalidation.model.d.ts.map