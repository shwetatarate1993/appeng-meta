import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class Card implements ConfigItem {
    static deserialize(input: any): Card;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    displayLabel: string;
    gridObjectId: string;
    renderingBeanName: string;
    searchEnabled: boolean;
    defaultDisplay: boolean;
    cardType: string;
    chartType: string;
    dataGridId: string;
    logicalEntityId: string;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(displayLabel: string, renderingBeanName: string, searchEnabled: any, defaultDisplay: any, cardType: string, chartType: string, dataGridId: string, logicalEntityId: string, itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=card.model.d.ts.map