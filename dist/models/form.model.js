'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
class Form {
    constructor(isRepeatable, tabRequiredInFormsection, tab, order, formType, expressionAvailable, accessibilityRegex, editabilityRegex, expressionFieldString, formLabel, defaultDataString, maxRepeatation, defaultRepeatation, repeatationStyle, isDeletionAllowed, addAllowed, itemId, itemName, itemType, projectId, createdBy, itemDescription, creationDate, updatedBy, updationDate, deletionDate, isDeleted, privileges, childRelations, parentRelations) {
        this.isRepeatable = (typeof isRepeatable === 'boolean') ?
            isRepeatable : ((isRepeatable === '1') ? true : false);
        this.tabRequiredInFormsection = (typeof tabRequiredInFormsection === 'boolean') ?
            tabRequiredInFormsection : ((tabRequiredInFormsection === '1') ? true : false);
        this.tab = tab;
        this.order = order;
        this.formType = formType;
        this.accessibilityRegex = accessibilityRegex;
        this.editabilityRegex = editabilityRegex;
        this.expressionAvailable = (typeof expressionAvailable === 'boolean') ?
            expressionAvailable : ((expressionAvailable === '1') ? true : false);
        this.expressionFieldString = expressionFieldString;
        this.formLabel = formLabel;
        this.defaultDataString = defaultDataString;
        this.maxRepeatation = maxRepeatation;
        this.defaultRepeatation = defaultRepeatation;
        this.repeatationStyle = repeatationStyle;
        this.isDeletionAllowed = (typeof isDeletionAllowed === 'boolean') ?
            isDeletionAllowed : ((isDeletionAllowed === '1') ? true : false);
        this.addAllowed = (typeof addAllowed === 'boolean') ?
            addAllowed : ((addAllowed === '1') ? true : false);
        this.configObjectId = itemId;
        this.name = itemName;
        this.configObjectType = itemType;
        this.projectId = projectId;
        this.createdBy = createdBy;
        this.itemDescription = itemDescription;
        this.creationDate = creationDate;
        this.updatedBy = updatedBy;
        this.updationDate = updationDate;
        this.deletionDate = deletionDate;
        this.isDeleted = isDeleted;
        this.privileges = privileges;
        this.childRelations = childRelations;
        this.parentRelations = parentRelations;
        Object.freeze(this);
    }
    static deserialize(input) {
        const privileges = [];
        const childRelations = [];
        const parentRelations = [];
        if (input.privileges !== undefined && input.privileges !== null) {
            input.privileges.forEach((privilege) => {
                privileges.push(meta_db_1.ConfigItemPrivilege.deserialize(privilege));
            });
        }
        if (input.childRelations !== undefined && input.childRelations !== null) {
            input.childRelations.forEach((relation) => {
                childRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        if (input.parentRelations !== undefined && input.parentRelations !== null) {
            input.parentRelations.forEach((relation) => {
                parentRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        return new Form(input.isRepeatable !== undefined ? input.isRepeatable : null, input.tabRequiredInFormsection !== undefined ? input.tabRequiredInFormsection : null, input.tab !== undefined ? input.tab : null, input.order !== undefined ? input.order : null, input.formType !== undefined ? input.formType : null, input.accessibilityRegex !== undefined ? input.accessibilityRegex : null, input.editabilityRegex !== undefined ? input.editabilityRegex : null, input.expressionAvailable !== undefined ? input.expressionAvailable : null, input.expressionFieldString !== undefined ? input.expressionFieldString : null, input.formLabel !== undefined ? input.formLabel : null, input.defaultDataString !== undefined ? input.defaultDataString : null, input.maxRepeatation !== undefined ? input.maxRepeatation : null, input.defaultRepeatation !== undefined ? input.defaultRepeatation : null, input.repeatationStyle !== undefined ? input.repeatationStyle : null, input.isDeletionAllowed !== undefined ? input.isDeletionAllowed : null, input.addAllowed !== undefined ? input.addAllowed : null, input.configObjectId !== undefined ? input.configObjectId : null, input.name !== undefined ? input.name : null, input.configObjectType !== undefined ? input.configObjectType : null, input.projectId !== undefined ? input.projectId : null, input.createdBy !== undefined ? input.createdBy : null, input.itemDescription !== undefined ? input.itemDescription : null, input.creationDate !== undefined ? input.creationDate : null, input.updatedBy !== undefined ? input.updatedBy : null, input.updationDate !== undefined ? input.updationDate : null, input.deletionDate !== undefined ? input.deletionDate : null, input.isDeleted !== undefined ? input.isDeleted : null, privileges, childRelations, parentRelations);
    }
}
exports.default = Form;
//# sourceMappingURL=form.model.js.map