import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class CustomFormValidation implements ConfigItem {
    static deserialize(input: any): CustomFormValidation;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    mode: string;
    validatorBeanId: string;
    validationType: string;
    validationMsg: string;
    isConditionAvailable: boolean;
    conditionExpression: string;
    conditionFields: string;
    description: string;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(mode: string, validatorBeanId: string, validationType: string, validationMsg: string, isConditionAvailable: any, conditionExpression: string, conditionFields: string, description: string, itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=customformvalidation.model.d.ts.map