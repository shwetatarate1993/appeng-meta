import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class CompositeEntity implements ConfigItem {
    static deserialize(input: any): CompositeEntity;
    graphTemplate: string;
    isFormLevelOperationAllowed: boolean;
    isReleaseAreaNotApplicable: boolean;
    logicalEntityId: string;
    multiEntityOrder: string;
    isMenuRequired: boolean;
    isNodeTreeRequired: boolean;
    rootDataSetId: string;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(graphTemplate: string, isFormLevelOperationAllowed: any, isReleaseAreaNotApplicable: any, logicalEntityId: string, multiEntityOrder: string, isMenuRequired: any, isNodeTreeRequired: any, rootDataSetId: string, itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=compositeentity.model.d.ts.map