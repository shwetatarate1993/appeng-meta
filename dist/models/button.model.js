'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
class Button {
    constructor(label, order, buttonClass, buttonAlignment, accessibilityRegex, editabilityRegex, expressionAvailable, expressionFieldString, itemId, itemName, itemType, projectId, createdBy, itemDescription, creationDate, updatedBy, updationDate, deletionDate, isDeleted, privileges, childRelations, parentRelations) {
        this.label = label;
        this.order = order;
        this.buttonClass = buttonClass;
        this.buttonAlignment = buttonAlignment;
        this.accessibilityRegex = accessibilityRegex;
        this.editabilityRegex = editabilityRegex;
        this.expressionAvailable = (typeof expressionAvailable === 'boolean') ?
            expressionAvailable : ((expressionAvailable === '1') ? true : false);
        this.expressionFieldString = expressionFieldString;
        this.configObjectId = itemId;
        this.name = itemName;
        this.configObjectType = itemType;
        this.projectId = projectId;
        this.createdBy = createdBy;
        this.itemDescription = itemDescription;
        this.creationDate = creationDate;
        this.updatedBy = updatedBy;
        this.updationDate = updationDate;
        this.deletionDate = deletionDate;
        this.isDeleted = isDeleted;
        this.privileges = privileges;
        this.childRelations = childRelations;
        this.parentRelations = parentRelations;
        Object.freeze(this);
    }
    static deserialize(input) {
        const privileges = [];
        const childRelations = [];
        const parentRelations = [];
        if (input.privileges !== undefined && input.privileges !== null) {
            input.privileges.forEach((privilege) => {
                privileges.push(meta_db_1.ConfigItemPrivilege.deserialize(privilege));
            });
        }
        if (input.childRelations !== undefined && input.childRelations !== null) {
            input.childRelations.forEach((relation) => {
                childRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        if (input.parentRelations !== undefined && input.parentRelations !== null) {
            input.parentRelations.forEach((relation) => {
                parentRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        return new Button(input.label !== undefined ? input.label : null, input.order !== undefined ? input.order : null, input.buttonClass !== undefined ? input.buttonClass : null, input.buttonAlignment !== undefined ? input.buttonAlignment : null, input.accessibilityRegex !== undefined ? input.accessibilityRegex : null, input.editabilityRegex !== undefined ? input.editabilityRegex : null, input.expressionAvailable !== undefined ? input.expressionAvailable : null, input.expressionFieldString !== undefined ? input.expressionFieldString : null, input.configObjectId !== undefined ? input.configObjectId : null, input.name !== undefined ? input.name : null, input.configObjectType !== undefined ? input.configObjectType : null, input.projectId !== undefined ? input.projectId : null, input.createdBy !== undefined ? input.createdBy : null, input.itemDescription !== undefined ? input.itemDescription : null, input.creationDate !== undefined ? input.creationDate : null, input.updatedBy !== undefined ? input.updatedBy : null, input.updationDate !== undefined ? input.updationDate : null, input.deletionDate !== undefined ? input.deletionDate : null, input.isDeleted !== undefined ? input.isDeleted : null, privileges, childRelations, parentRelations);
    }
}
exports.default = Button;
//# sourceMappingURL=button.model.js.map