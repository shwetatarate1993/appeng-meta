'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
class PhsicalEntity {
    constructor(expressionAvailable, accessibilityRegex, isMultivalueMapping, isPrimaryEntity, order, dbType, dbTypeName, schemaName, releaseAreaSessName, workAreaSessName, insertQID, updateQID, sequenceQID, singleSelectQID, multiSelectQID, deleteQID, isAuditRequired, itemId, itemName, itemType, projectId, createdBy, itemDescription, creationDate, updatedBy, updationDate, deletionDate, isDeleted, privileges, childRelations, parentRelations) {
        this.expressionAvailable = (expressionAvailable ? ((typeof expressionAvailable === 'boolean') ?
            expressionAvailable : ((expressionAvailable === '0')) ? false : true) : false);
        this.accessibilityRegex = accessibilityRegex;
        this.isMultivalueMapping = (typeof isMultivalueMapping === 'boolean') ?
            isMultivalueMapping : ((isMultivalueMapping === '0') ? false : true);
        this.isPrimaryEntity = (isPrimaryEntity ? ((typeof isPrimaryEntity === 'boolean') ?
            isPrimaryEntity : ((isPrimaryEntity === '0') ? false : true)) : false);
        this.order = order;
        this.dbType = dbType;
        this.dbTypeName = dbTypeName;
        this.schemaName = schemaName;
        this.releaseAreaSessName = releaseAreaSessName;
        this.workAreaSessName = workAreaSessName;
        this.insertQID = insertQID;
        this.updateQID = updateQID;
        this.sequenceQID = sequenceQID;
        this.singleSelectQID = singleSelectQID;
        this.multiSelectQID = multiSelectQID;
        this.deleteQID = deleteQID;
        this.isAuditRequired = (typeof isAuditRequired === 'boolean') ?
            isAuditRequired : ((isAuditRequired === '0') ? false : true);
        this.configObjectId = itemId;
        this.name = itemName;
        this.configObjectType = itemType;
        this.projectId = projectId;
        this.createdBy = createdBy;
        this.itemDescription = itemDescription;
        this.creationDate = creationDate;
        this.updatedBy = updatedBy;
        this.updationDate = updationDate;
        this.deletionDate = deletionDate;
        this.isDeleted = isDeleted;
        this.privileges = privileges;
        this.childRelations = childRelations;
        this.parentRelations = parentRelations;
        Object.freeze(this);
    }
    static deserialize(input) {
        const privileges = [];
        const childRelations = [];
        const parentRelations = [];
        if (input.privileges !== undefined && input.privileges !== null) {
            input.privileges.forEach((privilege) => {
                privileges.push(meta_db_1.ConfigItemPrivilege.deserialize(privilege));
            });
        }
        if (input.childRelations !== undefined && input.childRelations !== null) {
            input.childRelations.forEach((relation) => {
                childRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        if (input.parentRelations !== undefined && input.parentRelations !== null) {
            input.parentRelations.forEach((relation) => {
                parentRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        return new PhsicalEntity(input.expressionAvailable !== undefined ? input.expressionAvailable : null, input.accessibilityRegex !== undefined ? input.accessibilityRegex : null, input.isMultivalueMapping !== undefined ? input.isMultivalueMapping : null, input.isPrimaryEntity !== undefined ? input.isPrimaryEntity : null, input.order !== undefined ? input.order : null, input.dbType !== undefined ? input.dbType : null, input.dbTypeName !== undefined ? input.dbTypeName : null, input.schemaName !== undefined ? input.schemaName : null, input.releaseAreaSessName !== undefined ? input.releaseAreaSessName : null, input.workAreaSessName !== undefined ? input.workAreaSessName : null, input.insertQID !== undefined ? input.insertQID : null, input.updateQID !== undefined ? input.updateQID : null, input.sequenceQID !== undefined ? input.sequenceQID : null, input.singleSelectQID !== undefined ? input.singleSelectQID : null, input.multiSelectQID !== undefined ? input.multiSelectQID : null, input.deleteQID !== undefined ? input.deleteQID : null, input.isAuditRequired !== undefined ? input.isAuditRequired : null, input.configObjectId !== undefined ? input.configObjectId : null, input.name !== undefined ? input.name : null, input.configObjectType !== undefined ? input.configObjectType : null, input.projectId !== undefined ? input.projectId : null, input.createdBy !== undefined ? input.createdBy : null, input.itemDescription !== undefined ? input.itemDescription : null, input.creationDate !== undefined ? input.creationDate : null, input.updatedBy !== undefined ? input.updatedBy : null, input.updationDate !== undefined ? input.updationDate : null, input.deletionDate !== undefined ? input.deletionDate : null, input.isDeleted !== undefined ? input.isDeleted : null, privileges, childRelations, parentRelations);
    }
}
exports.default = PhsicalEntity;
//# sourceMappingURL=physicalentity.model.js.map