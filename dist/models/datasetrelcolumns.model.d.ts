import { LogicalColumn } from '../models';
export default class DataSetRelColumns {
    parentColumn: LogicalColumn;
    childColumn: LogicalColumn;
}
//# sourceMappingURL=datasetrelcolumns.model.d.ts.map