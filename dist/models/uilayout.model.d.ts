import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class UILayout implements ConfigItem {
    static deserialize(input: any): UILayout;
    configObjectId: string;
    uiLayoutType: string;
    targetDevice: string;
    mode: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(itemId: string, uiType: string, targetDevice: string, mode: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=uilayout.model.d.ts.map