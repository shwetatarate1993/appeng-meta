'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
class FormGroup {
    constructor(formGroupName, formOrder, formType, repeatableForms, tabs, readOnlyAccessible, readOnly, tabbedStructure, itemId, itemName, itemType, projectId, createdBy, itemDescription, creationDate, updatedBy, updationDate, deletionDate, isDeleted, privileges, childRelations, parentRelations) {
        this.configObjectId = itemId;
        this.name = itemName;
        this.configObjectType = itemType;
        this.projectId = projectId;
        this.createdBy = createdBy;
        this.itemDescription = itemDescription;
        this.creationDate = creationDate;
        this.updatedBy = updatedBy;
        this.updationDate = updationDate;
        this.deletionDate = deletionDate;
        this.isDeleted = isDeleted;
        this.formGroupName = formGroupName;
        this.formOrder = formOrder;
        this.formType = formType;
        this.tabs = tabs;
        this.repeatableForms = repeatableForms;
        this.readOnlyAccessible = (typeof readOnlyAccessible === 'boolean') ?
            readOnlyAccessible : ((readOnlyAccessible === '0') ? false : true);
        this.readOnly = (typeof readOnly === 'boolean') ?
            readOnly : ((readOnly === '0') ? false : true);
        this.tabbedStructure = (typeof tabbedStructure === 'boolean') ?
            tabbedStructure : ((tabbedStructure === '0') ? false : true);
        this.privileges = privileges;
        this.childRelations = childRelations;
        this.parentRelations = parentRelations;
        Object.freeze(this);
    }
    static deserialize(input) {
        const privileges = [];
        const childRelations = [];
        const parentRelations = [];
        if (input.privileges !== undefined && input.privileges !== null) {
            input.privileges.forEach((privilege) => {
                privileges.push(meta_db_1.ConfigItemPrivilege.deserialize(privilege));
            });
        }
        if (input.childRelations !== undefined && input.childRelations !== null) {
            input.childRelations.forEach((relation) => {
                childRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        if (input.parentRelations !== undefined && input.parentRelations !== null) {
            input.parentRelations.forEach((relation) => {
                parentRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        return new FormGroup(input.formGroupName !== undefined ? input.formGroupName : null, input.formOrder !== undefined ? input.formOrder : null, input.formType !== undefined ? input.formType : null, input.repeatableForms !== undefined ? input.repeatableForms : null, input.tabs !== undefined ? input.tabs : null, input.readOnlyAccessible !== undefined ? input.readOnlyAccessible : null, input.readOnly !== undefined ? input.readOnly : null, input.tabbedStructure !== undefined ? input.tabbedStructure : null, input.configObjectId !== undefined ? input.configObjectId : null, input.name !== undefined ? input.name : null, input.configObjectType !== undefined ? input.configObjectType : null, input.projectId !== undefined ? input.projectId : null, input.createdBy !== undefined ? input.createdBy : null, input.itemDescription !== undefined ? input.itemDescription : null, input.creationDate !== undefined ? input.creationDate : null, input.updatedBy !== undefined ? input.updatedBy : null, input.updationDate !== undefined ? input.updationDate : null, input.deletionDate !== undefined ? input.deletionDate : null, input.isDeleted !== undefined ? input.isDeleted : null, privileges, childRelations, parentRelations);
    }
}
exports.default = FormGroup;
//# sourceMappingURL=formgroup.model.js.map