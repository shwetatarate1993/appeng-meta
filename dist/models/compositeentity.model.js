'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
class CompositeEntity {
    constructor(graphTemplate, isFormLevelOperationAllowed, isReleaseAreaNotApplicable, logicalEntityId, multiEntityOrder, isMenuRequired, isNodeTreeRequired, rootDataSetId, itemId, itemName, itemType, projectId, createdBy, itemDescription, creationDate, updatedBy, updationDate, deletionDate, isDeleted, privileges, childRelations, parentRelations) {
        this.graphTemplate = graphTemplate;
        this.isFormLevelOperationAllowed = (typeof isFormLevelOperationAllowed === 'boolean') ?
            isFormLevelOperationAllowed : ((isFormLevelOperationAllowed === '1') ? true : false);
        this.isReleaseAreaNotApplicable = (typeof isReleaseAreaNotApplicable === 'boolean') ?
            isReleaseAreaNotApplicable : ((isReleaseAreaNotApplicable === '1') ? true : false);
        this.logicalEntityId = logicalEntityId;
        this.multiEntityOrder = multiEntityOrder;
        this.isMenuRequired = (typeof isMenuRequired === 'boolean') ?
            isMenuRequired : ((isMenuRequired === '1') ? true : false);
        this.isNodeTreeRequired = (isNodeTreeRequired ? ((typeof isNodeTreeRequired === 'boolean') ?
            isNodeTreeRequired : ((isNodeTreeRequired === '1') ? true : false)) : false);
        this.rootDataSetId = rootDataSetId;
        this.configObjectId = itemId;
        this.name = itemName;
        this.configObjectType = itemType;
        this.projectId = projectId;
        this.createdBy = createdBy;
        this.itemDescription = itemDescription;
        this.creationDate = creationDate;
        this.updatedBy = updatedBy;
        this.updationDate = updationDate;
        this.deletionDate = deletionDate;
        this.isDeleted = isDeleted;
        this.privileges = privileges;
        this.childRelations = childRelations;
        this.parentRelations = parentRelations;
        Object.freeze(this);
    }
    static deserialize(input) {
        const privileges = [];
        const childRelations = [];
        const parentRelations = [];
        if (input.privileges !== undefined && input.privileges !== null) {
            input.privileges.forEach((privilege) => {
                privileges.push(meta_db_1.ConfigItemPrivilege.deserialize(privilege));
            });
        }
        if (input.childRelations !== undefined && input.childRelations !== null) {
            input.childRelations.forEach((relation) => {
                childRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        if (input.parentRelations !== undefined && input.parentRelations !== null) {
            input.parentRelations.forEach((relation) => {
                parentRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        return new CompositeEntity(input.graphTemplate !== undefined ? input.graphTemplate : null, input.isFormLevelOperationAllowed !== undefined ? input.isFormLevelOperationAllowed : null, input.isReleaseAreaNotApplicable !== undefined ? input.isReleaseAreaNotApplicable : null, input.logicalEntityId !== undefined ? input.logicalEntityId : null, input.multiEntityOrder !== undefined ? input.multiEntityOrder : null, input.isMenuRequired !== undefined ? input.isMenuRequired : null, input.isNodeTreeRequired !== undefined ? input.isNodeTreeRequired : null, input.rootDataSetId !== undefined ? input.rootDataSetId : null, input.configObjectId !== undefined ? input.configObjectId : null, input.name !== undefined ? input.name : null, input.configObjectType !== undefined ? input.configObjectType : null, input.projectId !== undefined ? input.projectId : null, input.createdBy !== undefined ? input.createdBy : null, input.itemDescription !== undefined ? input.itemDescription : null, input.creationDate !== undefined ? input.creationDate : null, input.updatedBy !== undefined ? input.updatedBy : null, input.updationDate !== undefined ? input.updationDate : null, input.deletionDate !== undefined ? input.deletionDate : null, input.isDeleted !== undefined ? input.isDeleted : null, privileges, childRelations, parentRelations);
    }
}
exports.default = CompositeEntity;
//# sourceMappingURL=compositeentity.model.js.map