import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class ColumnDataPreprocessor implements ConfigItem {
    static deserialize(input: any): ColumnDataPreprocessor;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    order: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    preProcessorBean: string;
    isMultiValue: boolean;
    excecutionType: string;
    jsCode: string;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(preProcessorBean: string, isMultiValue: any, excecutionType: string, jsCode: string, itemId: string, itemName: string, itemType: string, projectId: number, order: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=columndatapreprocessor.model.d.ts.map