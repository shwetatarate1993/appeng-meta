import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class AuditGrid implements ConfigItem {
    static deserialize(input: any): AuditGrid;
    selectQueryId: string;
    selectQueryDetail: string;
    dataSourceName: string;
    tableType: string;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(selectQueryId: string, selectQueryDetail: string, dataSourceName: string, tableType: string, itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=auditgrid.model.d.ts.map