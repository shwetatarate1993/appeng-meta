'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
class UiCard {
    constructor(height, width, order, accessibilityRegex, expressionAvailble, viewType, displayLabel, expressionFields, itemId, itemName, itemType, projectId, createdBy, itemDescription, creationDate, updatedBy, updationDate, deletionDate, isDeleted, privileges, childRelations, parentRelations) {
        this.configObjectId = itemId;
        this.name = itemName;
        this.configObjectType = itemType;
        this.projectId = projectId;
        this.createdBy = createdBy;
        this.itemDescription = itemDescription;
        this.creationDate = creationDate;
        this.updatedBy = updatedBy;
        this.updationDate = updationDate;
        this.deletionDate = deletionDate;
        this.isDeleted = isDeleted;
        this.height = height;
        this.width = width;
        this.order = order;
        this.accessibilityRegex = accessibilityRegex;
        this.expressionAvailble = (typeof expressionAvailble === 'boolean') ?
            expressionAvailble : ((expressionAvailble === '0') ? false : true);
        this.viewType = viewType;
        this.displayLabel = (typeof displayLabel === 'boolean') ?
            displayLabel : ((displayLabel === '0') ? false : true);
        this.expressionFields = expressionFields;
        this.privileges = privileges;
        this.childRelations = childRelations;
        this.parentRelations = parentRelations;
        Object.freeze(this);
    }
    static deserialize(input) {
        const privileges = [];
        const childRelations = [];
        const parentRelations = [];
        if (input.privileges !== undefined && input.privileges !== null) {
            input.privileges.forEach((privilege) => {
                privileges.push(meta_db_1.ConfigItemPrivilege.deserialize(privilege));
            });
        }
        if (input.childRelations !== undefined && input.childRelations !== null) {
            input.childRelations.forEach((relation) => {
                childRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        if (input.parentRelations !== undefined && input.parentRelations !== null) {
            input.parentRelations.forEach((relation) => {
                parentRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        return new UiCard(input.height !== undefined ? input.height : null, input.width !== undefined ? input.width : null, input.order !== undefined ? input.order : null, input.accessibilityRegex !== undefined ? input.accessibilityRegex : null, input.expressionAvailble !== undefined ? input.expressionAvailble : null, input.viewType !== undefined ? input.viewType : null, input.displayLabel !== undefined ? input.displayLabel : null, input.expressionFields !== undefined ? input.expressionFields : null, input.configObjectId !== undefined ? input.configObjectId : null, input.name !== undefined ? input.name : null, input.configObjectType !== undefined ? input.configObjectType : null, input.projectId !== undefined ? input.projectId : null, input.createdBy !== undefined ? input.createdBy : null, input.itemDescription !== undefined ? input.itemDescription : null, input.creationDate !== undefined ? input.creationDate : null, input.updatedBy !== undefined ? input.updatedBy : null, input.updationDate !== undefined ? input.updationDate : null, input.deletionDate !== undefined ? input.deletionDate : null, input.isDeleted !== undefined ? input.isDeleted : null, privileges, childRelations, parentRelations);
    }
}
exports.default = UiCard;
//# sourceMappingURL=uicard.model.js.map