import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class LogicalEntity implements ConfigItem {
    static deserialize(input: any): LogicalEntity;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    dbTypeName: string;
    supportedFlavor: string;
    generateSkeleton: boolean;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, dbTypeName: string, supportedFlavor: string, generateSkeleton: any, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=logicalentity.model.d.ts.map