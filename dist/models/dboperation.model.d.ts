import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class DBOperation implements ConfigItem {
    static deserialize(input: any): DBOperation;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    mode: string;
    selectId: string;
    gridSelectId: string;
    filteredGridSelectQuery: string;
    workAreaSessName: string;
    customCss: string;
    type: string;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(mode: string, selectId: string, gridSelectId: string, filteredGridSelectQuery: string, workAreaSessName: string, customCss: string, type: string, itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=dboperation.model.d.ts.map