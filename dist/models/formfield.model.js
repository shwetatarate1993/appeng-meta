'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
class FormField {
    constructor(label, order, type, displayType, isMandatory, isButtonTextBox, accessibilityRegex, editabilityRegex, expressionAvailable, refreshFormOnChange, expressionFieldString, itemId, itemName, itemType, projectId, createdBy, itemDescription, isMultiLingual, formatDate, isApplyDateFormat, resetOnRefresh, rowspan, colspan, hasToolTipIcon, toolTipText, isSearchBoxRequired, isHyperlink, gotoLink, uploadLabel, outDbcodeUploadFileTempId, outDbcodeUploadFileName, outDbcodeUploadFileType, isRefreshFormWithUploadData, valueType, isReadOnly, creationDate, updatedBy, updationDate, deletionDate, isDeleted, privileges, childRelations, parentRelations, multivalueList, selectItemsReferenceID, placeHolder, dataSourceName, defaultValue, starColor, starCount, mask, maskChar, isSearchable, isDisabled, isMulti, showYearDropdown, showMonthDropdown, dateFormat, dropdownMode, timeFormat, showTimeSelect, showTimeSelectOnly, timeIntervals, timeCaption) {
        this.label = label;
        this.order = order;
        this.type = type;
        this.displayType = displayType;
        this.isMandatory = (typeof isMandatory === 'boolean') ?
            isMandatory : ((isMandatory === '1') ? true : false);
        this.isButtonTextBox = (typeof isButtonTextBox === 'boolean') ?
            isButtonTextBox : ((isButtonTextBox === '1') ? true : false);
        this.accessibilityRegex = accessibilityRegex;
        this.editabilityRegex = editabilityRegex;
        this.expressionAvailable = (typeof expressionAvailable === 'boolean') ?
            expressionAvailable : ((expressionAvailable === '1') ? true : false);
        this.refreshFormOnChange = (typeof refreshFormOnChange === 'boolean') ?
            refreshFormOnChange : ((refreshFormOnChange === '1') ? true : false);
        this.expressionFieldString = expressionFieldString;
        this.configObjectId = itemId;
        this.name = itemName;
        this.configObjectType = itemType;
        this.projectId = projectId;
        this.createdBy = createdBy;
        this.itemDescription = itemDescription;
        this.creationDate = creationDate;
        this.updatedBy = updatedBy;
        this.updationDate = updationDate;
        this.deletionDate = deletionDate;
        this.isDeleted = isDeleted;
        this.privileges = privileges;
        this.childRelations = childRelations;
        this.parentRelations = parentRelations;
        this.multivalueList = multivalueList;
        this.selectItemsReferenceID = selectItemsReferenceID;
        this.placeHolder = placeHolder;
        this.dataSourceName = dataSourceName;
        this.defaultValue = defaultValue;
        this.isMultiLingual = (typeof isMultiLingual === 'boolean') ?
            isMultiLingual : ((isMultiLingual === '1') ? true : false);
        this.formatDate = formatDate;
        this.isApplyDateFormat = (typeof isApplyDateFormat === 'boolean') ?
            isApplyDateFormat : ((isApplyDateFormat === '1') ? true : false);
        this.resetOnRefresh = resetOnRefresh;
        this.rowspan = rowspan;
        this.colspan = colspan;
        this.hasToolTipIcon = (typeof hasToolTipIcon === 'boolean') ?
            hasToolTipIcon : ((hasToolTipIcon === '1') ? true : false);
        this.toolTipText = toolTipText;
        this.isSearchBoxRequired = (typeof isSearchBoxRequired === 'boolean') ?
            isSearchBoxRequired : ((isSearchBoxRequired === '1') ? true : false);
        this.isHyperlink = (typeof isHyperlink === 'boolean') ?
            isHyperlink : ((isHyperlink === '1') ? true : false);
        this.gotoLink = gotoLink;
        this.uploadLabel = uploadLabel;
        this.outDbcodeUploadFileTempId = outDbcodeUploadFileTempId;
        this.outDbcodeUploadFileName = outDbcodeUploadFileName;
        this.outDbcodeUploadFileType = outDbcodeUploadFileType;
        this.isRefreshFormWithUploadData = (typeof isRefreshFormWithUploadData === 'boolean') ?
            isRefreshFormWithUploadData : ((isRefreshFormWithUploadData === '1') ? true : false);
        this.valueType = valueType;
        this.isReadOnly = (typeof isReadOnly === 'boolean') ?
            isReadOnly : ((isReadOnly === '1') ? true : false);
        this.starColor = starColor;
        this.starCount = starCount;
        this.mask = mask;
        this.maskChar = maskChar;
        this.isSearchable = (typeof isSearchable === 'boolean') ?
            isSearchable : ((isSearchable === '1') ? true : false);
        this.isDisabled = (typeof isDisabled === 'boolean') ?
            isDisabled : ((isDisabled === '1') ? true : false);
        this.isMulti = (typeof isMulti === 'boolean') ?
            isMulti : ((isMulti === '1') ? true : false);
        this.showYearDropdown = (typeof showYearDropdown === 'boolean') ?
            showYearDropdown : ((showYearDropdown === '1') ? true : false);
        this.showMonthDropdown = (typeof showMonthDropdown === 'boolean') ?
            showMonthDropdown : ((showMonthDropdown === '1') ? true : false);
        this.dateFormat = dateFormat;
        this.dropdownMode = dropdownMode;
        this.timeFormat = timeFormat;
        this.showTimeSelect = (typeof showTimeSelect === 'boolean') ?
            showTimeSelect : ((showTimeSelect === '1') ? true : false);
        this.showTimeSelectOnly = (typeof showTimeSelectOnly === 'boolean') ?
            showTimeSelectOnly : ((showTimeSelectOnly === '1') ? true : false);
        this.timeIntervals = timeIntervals;
        this.timeCaption = timeCaption;
        Object.freeze(this);
    }
    static deserialize(input) {
        const privileges = [];
        const childRelations = [];
        const parentRelations = [];
        if (input.privileges !== undefined && input.privileges !== null) {
            input.privileges.forEach((privilege) => {
                privileges.push(meta_db_1.ConfigItemPrivilege.deserialize(privilege));
            });
        }
        if (input.childRelations !== undefined && input.childRelations !== null) {
            input.childRelations.forEach((relation) => {
                childRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        if (input.parentRelations !== undefined && input.parentRelations !== null) {
            input.parentRelations.forEach((relation) => {
                parentRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        return new FormField(input.label !== undefined ? input.label : null, input.order !== undefined ? input.order : null, input.type !== undefined ? input.type : null, input.displayType !== undefined ? input.displayType : null, input.isMandatory !== undefined ? input.isMandatory : null, input.isButtonTextBox !== undefined ? input.isButtonTextBox : null, input.accessibilityRegex !== undefined ? input.accessibilityRegex : null, input.editabilityRegex !== undefined ? input.editabilityRegex : null, input.expressionAvailable !== undefined ? input.expressionAvailable : null, input.refreshFormOnChange !== undefined ? input.refreshFormOnChange : null, input.expressionFieldString !== undefined ? input.expressionFieldString : null, input.configObjectId !== undefined ? input.configObjectId : null, input.name !== undefined ? input.name : null, input.configObjectType !== undefined ? input.configObjectType : null, input.projectId !== undefined ? input.projectId : null, input.createdBy !== undefined ? input.createdBy : null, input.itemDescription !== undefined ? input.itemDescription : null, input.isMultiLingual !== undefined ? input.isMultiLingual : null, input.formatDate !== undefined ? input.formatDate : null, input.isApplyDateFormat !== undefined ? input.isApplyDateFormat : null, input.resetOnRefresh !== undefined ? input.resetOnRefresh : null, input.rowspan !== undefined ? input.rowspan : null, input.colspan !== undefined ? input.colspan : null, input.hasToolTipIcon !== undefined ? input.hasToolTipIcon : null, input.toolTipText !== undefined ? input.toolTipText : null, input.isHyperlink !== undefined ? input.isHyperlink : null, input.gotoLink !== undefined ? input.gotoLink : null, input.isSearchBoxRequired !== undefined ? input.isSearchBoxRequired : null, input.uploadLabel !== undefined ? input.uploadLabel : null, input.outDbcodeUploadFileTempId !== undefined ? input.outDbcodeUploadFileTempId : null, input.outDbcodeUploadFileName !== undefined ? input.outDbcodeUploadFileName : null, input.outDbcodeUploadFileType !== undefined ? input.outDbcodeUploadFileType : null, input.isRefreshFormWithUploadData !== undefined ? input.isRefreshFormWithUploadData : null, input.valueType !== undefined ? input.valueType : null, input.isReadOnly !== undefined ? input.isReadOnly : null, input.creationDate !== undefined ? input.creationDate : null, input.updatedBy !== undefined ? input.updatedBy : null, input.updationDate !== undefined ? input.updationDate : null, input.deletionDate !== undefined ? input.deletionDate : null, input.isDeleted !== undefined ? input.isDeleted : null, privileges, childRelations, parentRelations, input.multivalueList !== undefined ? input.multivalueList : null, input.selectItemsReferenceID !== undefined ? input.selectItemsReferenceID : null, input.placeHolder !== undefined ? input.placeHolder : null, input.dataSourceName !== undefined ? input.dataSourceName : null, input.defaultValue !== undefined ? input.defaultValue : null, input.starColor !== undefined ? input.starColor : null, input.starCount !== undefined ? input.starCount : null, input.mask !== undefined ? input.mask : null, input.maskChar !== undefined ? input.maskChar : null, input.isSearchable !== undefined ? input.isSearchable : null, input.isDisabled !== undefined ? input.isDisabled : null, input.isMulti !== undefined ? input.isMulti : null, input.showYearDropdown !== undefined ? input.showYearDropdown : null, input.showMonthDropdown !== undefined ? input.showMonthDropdown : null, input.dateFormat !== undefined ? input.dateFormat : null, input.dropdownMode !== undefined ? input.dropdownMode : null, input.timeFormat !== undefined ? input.timeFormat : null, input.showTimeSelect !== undefined ? input.showTimeSelect : null, input.showTimeSelectOnly !== undefined ? input.showTimeSelectOnly : null, input.timeIntervals !== undefined ? input.timeIntervals : null, input.timeCaption !== undefined ? input.timeCaption : null);
    }
}
exports.default = FormField;
//# sourceMappingURL=formfield.model.js.map