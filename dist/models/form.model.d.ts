import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class Form implements ConfigItem {
    static deserialize(input: any): Form;
    isRepeatable: boolean;
    tabRequiredInFormsection: boolean;
    tab: string;
    order: number;
    formType: string;
    accessibilityRegex: string;
    editabilityRegex: string;
    expressionAvailable: any;
    expressionFieldString: string;
    formLabel: string;
    defaultDataString: string;
    maxRepeatation: number;
    defaultRepeatation: number;
    repeatationStyle: string;
    isDeletionAllowed: boolean;
    addAllowed: boolean;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(isRepeatable: any, tabRequiredInFormsection: any, tab: string, order: number, formType: string, expressionAvailable: any, accessibilityRegex: string, editabilityRegex: string, expressionFieldString: string, formLabel: string, defaultDataString: string, maxRepeatation: number, defaultRepeatation: number, repeatationStyle: string, isDeletionAllowed: any, addAllowed: any, itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=form.model.d.ts.map