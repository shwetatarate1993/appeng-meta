import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class Button implements ConfigItem {
    static deserialize(input: any): Button;
    label: string;
    order: number;
    buttonClass: string;
    buttonAlignment: string;
    accessibilityRegex: string;
    editabilityRegex: string;
    expressionAvailable: boolean;
    expressionFieldString: string;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(label: string, order: number, buttonClass: string, buttonAlignment: string, accessibilityRegex: string, editabilityRegex: string, expressionAvailable: any, expressionFieldString: string, itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=button.model.d.ts.map