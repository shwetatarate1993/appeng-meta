import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class ButtonPanel implements ConfigItem {
    static deserialize(input: any): ButtonPanel;
    defaultType: string;
    buttonPanelPosition: string;
    mode: string;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(defaultType: string, buttonPanelPosition: string, mode: string, itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=buttonpanel.model.d.ts.map