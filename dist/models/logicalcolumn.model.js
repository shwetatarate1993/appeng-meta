'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
class LogicalColumn {
    constructor(isPrimaryKey, isDisplayColumn, dataType, isVerticalFieldKey, dbCode, length, dbType, isMandatory, jsonName, mode, isUnique, isDerived, itemId, itemName, itemType, projectId, createdBy, itemDescription, creationDate, updatedBy, updationDate, deletionDate, isDeleted, privileges, childRelations, parentRelations) {
        this.isPrimaryKey = (typeof isPrimaryKey === 'boolean') ?
            isPrimaryKey : ((isPrimaryKey === '1') ? true : false);
        this.isDisplayColumn = (typeof isDisplayColumn === 'boolean') ?
            isDisplayColumn : ((isDisplayColumn === '1') ? true : false);
        this.dataType = dataType;
        this.isVerticalFieldKey = (typeof isVerticalFieldKey === 'boolean') ?
            isVerticalFieldKey : ((isVerticalFieldKey === '1') ? true : false);
        this.dbCode = dbCode;
        this.length = length;
        this.dbType = dbType;
        this.isMandatory = (typeof isMandatory === 'boolean') ?
            isMandatory : ((isMandatory === '1') ? true : false);
        this.jsonName = jsonName;
        this.mode = mode;
        this.isUnique = (typeof isUnique === 'boolean') ?
            isUnique : ((isUnique === '1') ? true : false);
        this.isDerived = (typeof isDerived === 'boolean') ?
            isDerived : ((isDerived === '1') ? true : false);
        this.configObjectId = itemId;
        this.name = itemName;
        this.configObjectType = itemType;
        this.projectId = projectId;
        this.createdBy = createdBy;
        this.itemDescription = itemDescription;
        this.creationDate = creationDate;
        this.updatedBy = updatedBy;
        this.updationDate = updationDate;
        this.deletionDate = deletionDate;
        this.isDeleted = isDeleted;
        this.privileges = privileges;
        this.childRelations = childRelations;
        this.parentRelations = parentRelations;
        Object.freeze(this);
    }
    static deserialize(input) {
        const privileges = [];
        const childRelations = [];
        const parentRelations = [];
        if (input.privileges !== undefined && input.privileges !== null) {
            input.privileges.forEach((privilege) => {
                privileges.push(meta_db_1.ConfigItemPrivilege.deserialize(privilege));
            });
        }
        if (input.childRelations !== undefined && input.childRelations !== null) {
            input.childRelations.forEach((relation) => {
                childRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        if (input.parentRelations !== undefined && input.parentRelations !== null) {
            input.parentRelations.forEach((relation) => {
                parentRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        return new LogicalColumn(input.isPrimaryKey !== undefined ? input.isPrimaryKey : null, input.isDisplayColumn !== undefined ? input.isDisplayColumn : null, input.dataType !== undefined ? input.dataType : null, input.isVerticalFieldKey !== undefined ? input.isVerticalFieldKey : null, input.dbCode !== undefined ? input.dbCode : null, input.length !== undefined ? input.length : null, input.dbType !== undefined ? input.dbType : null, input.isMandatory !== undefined ? input.isMandatory : null, input.jsonName !== undefined ? input.jsonName : null, input.mode !== undefined ? input.mode : null, input.isUnique !== undefined ? input.isUnique : null, input.isDerived !== undefined ? input.isDerived : null, input.configObjectId !== undefined ? input.configObjectId : null, input.name !== undefined ? input.name : null, input.configObjectType !== undefined ? input.configObjectType : null, input.projectId !== undefined ? input.projectId : null, input.createdBy !== undefined ? input.createdBy : null, input.itemDescription !== undefined ? input.itemDescription : null, input.creationDate !== undefined ? input.creationDate : null, input.updatedBy !== undefined ? input.updatedBy : null, input.updationDate !== undefined ? input.updationDate : null, input.deletionDate !== undefined ? input.deletionDate : null, input.isDeleted !== undefined ? input.isDeleted : null, privileges, childRelations, parentRelations);
    }
}
exports.default = LogicalColumn;
//# sourceMappingURL=logicalcolumn.model.js.map