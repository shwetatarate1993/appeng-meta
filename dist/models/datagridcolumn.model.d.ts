import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class DataGridColumn implements ConfigItem {
    static deserialize(input: any): DataGridColumn;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    order: number;
    isDisplayDetail: boolean;
    visible: boolean;
    headerName: string;
    actionColumnType: string;
    group: string;
    icon: string;
    hrefValue: string;
    dateFormat: string;
    hyperLink: boolean;
    key: boolean;
    editabilityRegex: string;
    expressionAvailable: boolean;
    width: string;
    fixColumn: boolean;
    goToLink: string;
    accessibilityRegex: string;
    toolTip: string;
    actionColumn: boolean;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(headerName: string, order: number, isDisplayDetail: any, visible: any, toolTip: string, actionColumn: any, actionColumnType: string, group: string, icon: string, hrefValue: string, dateFormat: string, hyperLink: any, key: any, editabilityRegex: string, expressionAvailable: any, width: string, fixColumn: any, goToLink: string, accessibilityRegex: string, itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=datagridcolumn.model.d.ts.map