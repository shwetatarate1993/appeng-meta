"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ConnectionDetails {
    constructor(host, port, dbname, user, password) {
        this.host = host;
        this.port = port;
        this.dbname = dbname;
        this.user = user;
        this.password = password;
        Object.freeze(this);
    }
}
exports.default = ConnectionDetails;
//# sourceMappingURL=connection.details.model.js.map