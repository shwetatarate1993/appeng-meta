import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class PhysicalColumn implements ConfigItem {
    static deserialize(input: any): PhysicalColumn;
    isKey: boolean;
    isDisplayColumn: boolean;
    isPrimaryKey: boolean;
    dataType: string;
    dbCode: string;
    length: number;
    dbType: string;
    jsonName: string;
    isLogicalColumnRequired: boolean;
    isUnique: boolean;
    isVirtualColumn: boolean;
    isMultiValueField: boolean;
    isPhysicalColumnMandatory: boolean;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(isKey: any, isDisplayColumn: any, isPrimaryKey: any, dataType: string, dbCode: string, length: number, dbType: string, jsonName: string, isLogicalColumnRequired: any, isUnique: any, isVirtualColumn: any, isMultiValueField: any, isPhysicalColumnMandatory: any, itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=physicalcolumn.model.d.ts.map