import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class Menu implements ConfigItem {
    static deserialize(input: any): Menu;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    displayLabel: string;
    hrefBaseUrl: string;
    menuClass: string;
    header: string;
    order: number;
    isLabel: boolean;
    objectId: string;
    isOpenInNewTab: boolean;
    submenuContainerClass: string;
    subMenu: boolean;
    report: string;
    customUrl: string;
    componentsPerRow: number;
    linkType: string;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(displayLabel: string, hrefBaseUrl: string, menuClass: string, header: string, order: number, isLabel: any, objectId: string, isOpenInNewTab: any, submenuContainerClass: string, subMenu: any, report: string, customUrl: string, componentsPerRow: number, linkType: string, itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=menu.model.d.ts.map