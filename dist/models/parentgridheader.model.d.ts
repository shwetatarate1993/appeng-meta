import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class ParentGridHeader implements ConfigItem {
    static deserialize(input: any): ParentGridHeader;
    configObjectId: string;
    order: number;
    headerName: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(itemId: string, order: number, headerName: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=parentgridheader.model.d.ts.map