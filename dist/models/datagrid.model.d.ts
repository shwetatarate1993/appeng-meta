import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class DataGrid implements ConfigItem {
    static deserialize(input: any): DataGrid;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    defaultOrdering: boolean;
    gridType: string;
    isHavingAdvanceFilterForm: boolean;
    scroll: boolean;
    swimlaneRequired: boolean;
    modalRequired: boolean;
    isRowReOrder: boolean;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(defaultOrdering: any, gridType: string, isHavingAdvanceFilterForm: any, scroll: any, swimlaneRequired: any, modalRequired: any, isRowReOrder: any, itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=datagrid.model.d.ts.map