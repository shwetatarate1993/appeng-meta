'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
class Card {
    constructor(displayLabel, renderingBeanName, searchEnabled, defaultDisplay, cardType, chartType, dataGridId, logicalEntityId, itemId, itemName, itemType, projectId, createdBy, itemDescription, creationDate, updatedBy, updationDate, deletionDate, isDeleted, privileges, childRelations, parentRelations) {
        this.configObjectId = itemId;
        this.name = itemName;
        this.configObjectType = itemType;
        this.projectId = projectId;
        this.createdBy = createdBy;
        this.itemDescription = itemDescription;
        this.creationDate = creationDate;
        this.updatedBy = updatedBy;
        this.updationDate = updationDate;
        this.deletionDate = deletionDate;
        this.isDeleted = isDeleted;
        this.displayLabel = displayLabel;
        this.renderingBeanName = renderingBeanName;
        this.searchEnabled = (typeof searchEnabled === 'boolean') ?
            searchEnabled : ((searchEnabled === '0') ? false : true);
        this.defaultDisplay = (typeof defaultDisplay === 'boolean') ?
            defaultDisplay : ((defaultDisplay === '0') ? false : true);
        this.cardType = cardType;
        this.chartType = chartType;
        this.dataGridId = dataGridId;
        this.logicalEntityId = logicalEntityId;
        this.privileges = privileges;
        this.childRelations = childRelations;
        this.parentRelations = parentRelations;
        Object.freeze(this);
    }
    static deserialize(input) {
        const privileges = [];
        const childRelations = [];
        const parentRelations = [];
        if (input.privileges !== undefined && input.privileges !== null) {
            input.privileges.forEach((privilege) => {
                privileges.push(meta_db_1.ConfigItemPrivilege.deserialize(privilege));
            });
        }
        if (input.childRelations !== undefined && input.childRelations !== null) {
            input.childRelations.forEach((relation) => {
                childRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        if (input.parentRelations !== undefined && input.parentRelations !== null) {
            input.parentRelations.forEach((relation) => {
                parentRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        return new Card(input.displayLabel !== undefined ? input.displayLabel : null, input.renderingBeanName !== undefined ? input.renderingBeanName : null, input.searchEnabled !== undefined ? input.searchEnabled : null, input.defaultDisplay !== undefined ? input.defaultDisplay : null, input.cardType !== undefined ? input.cardType : null, input.chartType !== undefined ? input.chartType : null, input.dataGridId !== undefined ? input.dataGridId : null, input.logicalEntityId !== undefined ? input.logicalEntityId : null, input.configObjectId !== undefined ? input.configObjectId : null, input.name !== undefined ? input.name : null, input.configObjectType !== undefined ? input.configObjectType : null, input.projectId !== undefined ? input.projectId : null, input.createdBy !== undefined ? input.createdBy : null, input.itemDescription !== undefined ? input.itemDescription : null, input.creationDate !== undefined ? input.creationDate : null, input.updatedBy !== undefined ? input.updatedBy : null, input.updationDate !== undefined ? input.updationDate : null, input.deletionDate !== undefined ? input.deletionDate : null, input.isDeleted !== undefined ? input.isDeleted : null, privileges, childRelations, parentRelations);
    }
}
exports.default = Card;
//# sourceMappingURL=card.model.js.map