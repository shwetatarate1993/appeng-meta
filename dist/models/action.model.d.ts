import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class Action implements ConfigItem {
    static deserialize(input: any): Action;
    isProcessAction: boolean;
    expressionVariables: string;
    accessiblityExpression: string;
    action: string;
    formsId: string;
    description: string;
    formFieldsId: string;
    type: string;
    statusValue: string;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(isProcessAction: any, expressionVariables: string, accessiblityExpression: string, action: string, formsId: string, description: string, formFieldsId: string, type: any, statusValue: string, itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=action.model.d.ts.map