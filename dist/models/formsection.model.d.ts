import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class FormSection implements ConfigItem {
    static deserialize(input: any): FormSection;
    configObjectId: string;
    headerLabel: string;
    order: number;
    displayName: string;
    accessibilityRegex: string;
    editabilityRegex: string;
    tabGroup: string;
    isRenderOnRepeat: boolean;
    requiredFormfields: string;
    componentsPerRow: number;
    expressionAvailable: boolean;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(objectId: string, headerLabel: string, order: number, displayName: string, accessibilityRegex: string, editabilityRegex: string, tabGroup: string, isRenderOnRepeat: any, requiredFormfields: string, componentsPerRow: number, expressionAvailable: any, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=formsection.model.d.ts.map