import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class MenuButton implements ConfigItem {
    static deserialize(input: any): MenuButton;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    defaultType: string;
    menuLabel: string;
    position: string;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(defaultType: string, menuLabel: string, position: string, itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=menubutton.model.d.ts.map