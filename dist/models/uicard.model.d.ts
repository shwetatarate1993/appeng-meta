import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class UiCard implements ConfigItem {
    static deserialize(input: any): UiCard;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    height: number;
    width: number;
    order: number;
    accessibilityRegex: string;
    expressionAvailble: boolean;
    viewType: string;
    displayLabel: boolean;
    expressionFields: string;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(height: number, width: number, order: number, accessibilityRegex: string, expressionAvailble: any, viewType: string, displayLabel: any, expressionFields: string, itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=uicard.model.d.ts.map