import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class LogicalColumn implements ConfigItem {
    static deserialize(input: any): LogicalColumn;
    isPrimaryKey: boolean;
    isDisplayColumn: boolean;
    dataType: string;
    isVerticalFieldKey: boolean;
    dbCode: string;
    length: number;
    dbType: string;
    isMandatory: boolean;
    jsonName: string;
    mode: string;
    isUnique: boolean;
    isDerived: boolean;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(isPrimaryKey: any, isDisplayColumn: any, dataType: string, isVerticalFieldKey: any, dbCode: string, length: number, dbType: string, isMandatory: any, jsonName: string, mode: string, isUnique: any, isDerived: any, itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=logicalcolumn.model.d.ts.map