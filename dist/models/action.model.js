'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
class Action {
    constructor(isProcessAction, expressionVariables, accessiblityExpression, action, formsId, description, formFieldsId, type, statusValue, itemId, itemName, itemType, projectId, createdBy, itemDescription, creationDate, updatedBy, updationDate, deletionDate, isDeleted, privileges, childRelations, parentRelations) {
        this.isProcessAction = (typeof isProcessAction === 'boolean') ?
            isProcessAction : ((isProcessAction === '1') ? true : false);
        this.expressionVariables = expressionVariables;
        this.accessiblityExpression = accessiblityExpression;
        this.action = action;
        this.formsId = formsId;
        this.description = description;
        this.formFieldsId = formFieldsId;
        this.type = type;
        this.statusValue = statusValue;
        this.configObjectId = itemId;
        this.name = itemName;
        this.configObjectType = itemType;
        this.projectId = projectId;
        this.createdBy = createdBy;
        this.itemDescription = itemDescription;
        this.creationDate = creationDate;
        this.updatedBy = updatedBy;
        this.updationDate = updationDate;
        this.deletionDate = deletionDate;
        this.isDeleted = isDeleted;
        this.privileges = privileges;
        this.childRelations = childRelations;
        this.parentRelations = parentRelations;
        Object.freeze(this);
    }
    static deserialize(input) {
        const privileges = [];
        const childRelations = [];
        const parentRelations = [];
        if (input.privileges !== undefined && input.privileges !== null) {
            input.privileges.forEach((privilege) => {
                privileges.push(meta_db_1.ConfigItemPrivilege.deserialize(privilege));
            });
        }
        if (input.childRelations !== undefined && input.childRelations !== null) {
            input.childRelations.forEach((relation) => {
                childRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        if (input.parentRelations !== undefined && input.parentRelations !== null) {
            input.parentRelations.forEach((relation) => {
                parentRelations.push(meta_db_1.ConfigItemRelation.deserialize(relation));
            });
        }
        return new Action(input.isProcessAction !== undefined ? input.isProcessAction : null, input.expressionVariables !== undefined ? input.expressionVariables : null, input.accessiblityExpression !== undefined ? input.accessiblityExpression : null, input.action !== undefined ? input.action : null, input.formsId !== undefined ? input.formsId : null, input.description !== undefined ? input.description : null, input.formFieldsId !== undefined ? input.formFieldsId : null, input.type !== undefined ? input.type : null, input.statusValue !== undefined ? input.statusValue : null, input.configObjectId !== undefined ? input.configObjectId : null, input.name !== undefined ? input.name : null, input.configObjectType !== undefined ? input.configObjectType : null, input.projectId !== undefined ? input.projectId : null, input.createdBy !== undefined ? input.createdBy : null, input.itemDescription !== undefined ? input.itemDescription : null, input.creationDate !== undefined ? input.creationDate : null, input.updatedBy !== undefined ? input.updatedBy : null, input.updationDate !== undefined ? input.updationDate : null, input.deletionDate !== undefined ? input.deletionDate : null, input.isDeleted !== undefined ? input.isDeleted : null, privileges, childRelations, parentRelations);
    }
}
exports.default = Action;
//# sourceMappingURL=action.model.js.map