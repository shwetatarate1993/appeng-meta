import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class LogicalEntityOperation implements ConfigItem {
    static deserialize(input: any): LogicalEntityOperation;
    configObjectId: string;
    name: string;
    configObjectType: string;
    selectId: string;
    gridSelectId: string;
    filteredQuery: string;
    workAreaSessName: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(selectId: string, gridSelectId: string, filteredQuery: string, workAreaSessName: string, itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=logicalentityoperation.model.d.ts.map