import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class CompositeEntityNode implements ConfigItem {
    static deserialize(input: any): CompositeEntityNode;
    addToParentDisplay: boolean;
    addToParentGrid: boolean;
    addToParentEditForm: boolean;
    addToParentInsertForm: boolean;
    displayNodeName: string;
    graphTemplate: string;
    accessibilityRegex: string;
    order: number;
    expressionAvailable: boolean;
    editabilityRegex: string;
    expressionFieldString: string;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(addToParentDisplay: any, addToParentGrid: any, addToParentEditForm: any, addToParentInsertForm: any, displayNodeName: string, graphTemplate: string, accessibilityRegex: string, order: number, expressionAvailable: any, editabilityRegex: string, expressionFieldString: string, itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=compositeentitynode.model.d.ts.map