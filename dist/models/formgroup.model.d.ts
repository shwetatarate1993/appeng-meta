import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
export default class FormGroup implements ConfigItem {
    static deserialize(input: any): FormGroup;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    formGroupName: string;
    formOrder: string;
    formType: string;
    tabs: string;
    repeatableForms: string;
    readOnlyAccessible: boolean;
    readOnly: boolean;
    tabbedStructure: boolean;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
    constructor(formGroupName: string, formOrder: string, formType: string, repeatableForms: string, tabs: string, readOnlyAccessible: any, readOnly: any, tabbedStructure: any, itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=formgroup.model.d.ts.map