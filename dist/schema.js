"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_tools_1 = require("graphql-tools");
const schema_1 = __importDefault(require("./action/schema"));
const schema_2 = __importDefault(require("./auditentity/schema"));
const schema_3 = __importDefault(require("./auditgrid/schema"));
const base_1 = __importDefault(require("./base"));
const schema_4 = __importDefault(require("./button/schema"));
const schema_5 = __importDefault(require("./buttonpanel/schema"));
const schema_6 = __importDefault(require("./card/schema"));
const schema_7 = __importDefault(require("./cardgroup/schema"));
const schema_8 = __importDefault(require("./columndatapreprocessor/schema"));
const schema_9 = __importDefault(require("./compositeentity/schema"));
const schema_10 = __importDefault(require("./compositeentitynode/schema"));
const schema_11 = __importDefault(require("./customformvalidation/schema"));
const schema_12 = __importDefault(require("./database/schema"));
const schema_13 = __importDefault(require("./databasevalidation/schema"));
const schema_14 = __importDefault(require("./datagrid/schema"));
const schema_15 = __importDefault(require("./datagridcolumn/schema"));
const schema_16 = __importDefault(require("./datasetrel/schema"));
const schema_17 = __importDefault(require("./datasetrelproperty/schema"));
const schema_18 = __importDefault(require("./datasource/schema"));
const schema_19 = __importDefault(require("./dboperation/schema"));
const schema_20 = __importDefault(require("./eventlistener/schema"));
const schema_21 = __importDefault(require("./form/schema"));
const schema_22 = __importDefault(require("./formdbvalidation/schema"));
const schema_23 = __importDefault(require("./formfield/schema"));
const schema_24 = __importDefault(require("./formgroup/schema"));
const schema_25 = __importDefault(require("./formsection/schema"));
const schema_26 = __importDefault(require("./language/schema"));
const schema_27 = __importDefault(require("./logicalcolumn/schema"));
const schema_28 = __importDefault(require("./logicalentity/schema"));
const schema_29 = __importDefault(require("./logicalentityoperation/schema"));
const schema_30 = __importDefault(require("./menu/schema"));
const schema_31 = __importDefault(require("./menubutton/schema"));
const schema_32 = __importDefault(require("./menugroup/schema"));
const schema_33 = __importDefault(require("./nodebusinessrule/schema"));
const schema_34 = __importDefault(require("./parentgridheader/schema"));
const schema_35 = __importDefault(require("./physicalcolumn/schema"));
const schema_36 = __importDefault(require("./physicalentity/schema"));
const resolvers_1 = __importDefault(require("./resolvers"));
const schema_37 = __importDefault(require("./rootcompositeentitynode/schema"));
const schema_38 = __importDefault(require("./standardvalidation/schema"));
const schema_39 = __importDefault(require("./uicard/schema"));
const schema_40 = __importDefault(require("./uilayout/schema"));
const schema_41 = __importDefault(require("./uitab/schema"));
exports.default = graphql_tools_1.makeExecutableSchema({
    typeDefs: [base_1.default, schema_6.default, schema_7.default, schema_18.default, schema_4.default, schema_21.default,
        schema_23.default, schema_9.default, schema_27.default, schema_1.default,
        schema_30.default, schema_31.default, schema_15.default, schema_24.default, schema_39.default, schema_19.default, schema_16.default,
        schema_5.default, schema_26.default, schema_10.default, schema_33.default, schema_3.default,
        schema_14.default, schema_8.default, schema_41.default, schema_32.default, schema_28.default, schema_36.default,
        schema_38.default, schema_13.default, schema_2.default, schema_22.default, schema_11.default,
        schema_29.default, schema_25.default, schema_40.default, schema_34.default, schema_17.default, schema_20.default,
        schema_37.default, schema_35.default, schema_12.default],
    resolvers: resolvers_1.default,
});
//# sourceMappingURL=schema.js.map