"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = __importDefault(require("../base"));
const common_schema_1 = __importDefault(require("../common.schema"));
const DataGrid = `

input DataGridInput {

    configObjectId: ID
    defaultOrdering:Boolean
    gridType:String
    isHavingAdvanceFilterForm:Boolean
    scroll: Boolean
    swimlaneRequired:Boolean
    modalRequired:Boolean
    isRowReOrder:Boolean
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    creationDate: Date
    projectId: Int
    itemDescription: String
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilegeInput]
    parentRelations : [ConfigItemRelationInput]
    childRelations : [ConfigItemRelationInput]
}

extend type Query {
    DataGrid(id: ID!): DataGrid
   }

extend type Mutation {
    createDataGrid (input: DataGridInput): DataGrid
}

type DataGrid {
    configObjectId: ID
    defaultOrdering:Boolean
    gridType:String
    isHavingAdvanceFilterForm:Boolean
    scroll:Boolean
    swimlaneRequired:Boolean
    modalRequired:Boolean
    isRowReOrder:Boolean
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    creationDate: Date
    projectId: Int
    itemDescription: String
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilege]
    parentRelations : [ConfigItemRelation]
    childRelations : [ConfigItemRelation]
    dataGridColumns : [DataGridColumn]
    actionDataGridColumns : [DataGridColumn]
    logicalEntityOperations : [LogicalEntityOperation]
    parentGridHeaders : [ParentGridHeader]
    buttonPanels : [ButtonPanel]
}
`;
exports.default = () => [DataGrid, base_1.default, common_schema_1.default];
//# sourceMappingURL=schema.js.map