"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const buttonpanel_parser_1 = require("../buttonpanel/buttonpanel.parser");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const datagridcolumn_parser_1 = require("../datagridcolumn/datagridcolumn.parser");
const logicalentityoperation_parser_1 = require("../logicalentityoperation/logicalentityoperation.parser");
const parentgridheader_parser_1 = require("../parentgridheader/parentgridheader.parser");
const datagrid_parser_1 = require("./datagrid.parser");
exports.Query = {
    DataGrid: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, datagrid_parser_1.parseConfigToDataGrid),
};
exports.DataGrid = {
    dataGridColumns: async (dataGrid, _, context) => {
        const columnsIds = dataGrid.childRelations.filter((rel) => rel.relationType === relationtype_enum_1.RelationType.DATAGRID_DATAGRIDCOLUMN)
            .map((rel) => rel.childItemId);
        return columnsIds ?
            await common_meta_1.configService.fetchConfigItemsForIdList(columnsIds, datagridcolumn_parser_1.parseConfigToDataGridColumn) : [];
    },
    actionDataGridColumns: async (dataGrid, _, context) => {
        const columnsIds = dataGrid.childRelations.filter((rel) => rel.relationType === relationtype_enum_1.RelationType.DATAGRID_ACTIONDATAGRIDCOLUMN)
            .map((rel) => rel.childItemId);
        return columnsIds ?
            await common_meta_1.configService.fetchConfigItemsForIdList(columnsIds, datagridcolumn_parser_1.parseConfigToDataGridColumn) : [];
    },
    logicalEntityOperations: (dataGrid, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(dataGrid.configObjectId, relationtype_enum_1.RelationType.DATAGRID_LOGICALENTITYOPERATION, logicalentityoperation_parser_1.parseConfigToLogicalEntityOperation),
    parentGridHeaders: (dataGrid, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(dataGrid.configObjectId, relationtype_enum_1.RelationType.DATAGRID_PARENTGRIDHEADER, parentgridheader_parser_1.parseConfigToParentGridHeaderObject),
    buttonPanels: async (dataGrid, _, context) => {
        const buttonPanelIds = dataGrid.childRelations.filter((rel) => rel.relationType === relationtype_enum_1.RelationType.DATAGRID_BUTTONPANEL)
            .map((rel) => rel.childItemId);
        return buttonPanelIds ?
            await common_meta_1.configService.fetchConfigItemsForIdList(buttonPanelIds, buttonpanel_parser_1.parseConfigToButtonPanel) : [];
    },
};
exports.Mutation = {
    createDataGrid: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, datagrid_parser_1.parseDataGridToConfig, datagrid_parser_1.parseConfigToDataGrid);
    },
};
//# sourceMappingURL=resolvers.js.map