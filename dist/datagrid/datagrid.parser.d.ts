import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { DataGrid } from '../models';
export declare const parseDataGridToConfig: (dataGrid: DataGrid) => ConfigMetadata;
export declare const parseConfigToDataGrid: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => DataGrid;
//# sourceMappingURL=datagrid.parser.d.ts.map