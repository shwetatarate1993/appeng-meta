export declare const Query: {
    DataGrid: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").DataGrid>;
};
export declare const DataGrid: {
    dataGridColumns: (dataGrid: any, _: any, context: any) => Promise<import("../models").DataGridColumn[]>;
    actionDataGridColumns: (dataGrid: any, _: any, context: any) => Promise<import("../models").DataGridColumn[]>;
    logicalEntityOperations: (dataGrid: any, _: any, context: any) => Promise<import("../models").LogicalEntityOperation[]>;
    parentGridHeaders: (dataGrid: any, _: any, context: any) => Promise<import("../models").ParentGridHeader[]>;
    buttonPanels: (dataGrid: any, _: any, context: any) => Promise<import("../models").ButtonPanel[]>;
};
export declare const Mutation: {
    createDataGrid: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map