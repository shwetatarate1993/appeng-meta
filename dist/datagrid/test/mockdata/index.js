"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var datagrid_1 = require("./datagrid");
exports.datagridMockData = datagrid_1.default;
var datagrid_2 = require("./datagrid");
exports.configItemDataGridMockData = datagrid_2.default;
var configitem_privileges_datagrid_1 = require("./configitem.privileges.datagrid");
exports.configItemPrivilegesDataGridMockData = configitem_privileges_datagrid_1.default;
var configitem_relation_datagrid_1 = require("./configitem.relation.datagrid");
exports.configItemRelationsDataGridMockData = configitem_relation_datagrid_1.default;
var configitem_property_datagrid_1 = require("./configitem.property.datagrid");
exports.configItemPropertyDataGridMockData = configitem_property_datagrid_1.default;
//# sourceMappingURL=index.js.map