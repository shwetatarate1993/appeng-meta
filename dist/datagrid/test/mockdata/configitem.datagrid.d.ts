declare const _default: {
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: any;
    projectId: number;
    updatedBy: string;
    updationDate: any;
    deletionDate: any;
}[];
export default _default;
//# sourceMappingURL=configitem.datagrid.d.ts.map