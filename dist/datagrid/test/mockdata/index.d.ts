export { default as datagridMockData } from './datagrid';
export { default as configItemDataGridMockData } from './datagrid';
export { default as configItemPrivilegesDataGridMockData } from './configitem.privileges.datagrid';
export { default as configItemRelationsDataGridMockData } from './configitem.relation.datagrid';
export { default as configItemPropertyDataGridMockData } from './configitem.property.datagrid';
//# sourceMappingURL=index.d.ts.map