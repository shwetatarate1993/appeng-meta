"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const datagrid_parser_1 = require("../datagrid.parser");
const mockdata_1 = require("./mockdata");
test('parser datagrid object to config', () => {
    const datagrid = models_1.DataGrid.deserialize(mockdata_1.datagridMockData[0]);
    const result = datagrid_parser_1.parseDataGridToConfig(datagrid);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItem.configObjectType).toEqual('DataGrid');
});
test('parser config to datagrid object', () => {
    const result = datagrid_parser_1.parseConfigToDataGrid(mockdata_1.configItemDataGridMockData[0], mockdata_1.configItemPropertyDataGridMockData, mockdata_1.configItemRelationsDataGridMockData, mockdata_1.configItemPrivilegesDataGridMockData);
    expect(result).toHaveProperty('gridType', 'GENERAL_GRID');
    expect(result).toHaveProperty('isHavingAdvanceFilterForm', false);
    expect(result).toHaveProperty('scroll', false);
    expect(result).toHaveProperty('defaultOrdering', true);
    expect(result).toHaveProperty('isRowReOrder', false);
    expect(result).toHaveProperty('swimlaneRequired', false);
    expect(result).toHaveProperty('modalRequired', false);
    expect(result.privileges.length === 5).toBe(true);
    expect(result.childRelations[0].relationType === 'DataGrid_DataGridColumn').toBe(true);
    expect(result.childRelations[1].relationType === 'DataGrid_DataGridColumn').toBe(true);
    expect(result.childRelations[2].relationType === 'DataGrid_DataGridColumn').toBe(true);
    expect(result.childRelations[3].relationType === 'DataGrid_DataGridColumn').toBe(true);
    expect(result.childRelations[4].relationType === 'DataGrid_DataGridColumn').toBe(true);
    expect(result.childRelations[5].relationType === 'DataGrid_DataGridColumn').toBe(true);
    expect(result.childRelations[6].relationType === 'DataGrid_DataGridColumn').toBe(true);
    expect(result.childRelations[7].relationType === 'DataGrid_DataGridColumn').toBe(true);
    expect(result.childRelations[8].relationType === 'DataGrid_LogicalEntityOperation').toBe(true);
    expect(result.childRelations[9].relationType === 'DataGrid_ButtonPanel').toBe(true);
    expect(result.childRelations[10].relationType === 'DataGrid_ActionDataGridColumn').toBe(true);
    expect(result.parentRelations[0].relationType === 'LogicalEntity_DataGrid').toBe(true);
    expect(result.parentRelations[1].relationType === 'RootCompositeEntityNode_DataGrid').toBe(true);
});
//# sourceMappingURL=datagrid.parser.test.js.map