"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseDataGridToConfig = (dataGrid) => {
    const createdAt = dataGrid.creationDate;
    const createdBy = dataGrid.createdBy;
    const updatedBy = dataGrid.updatedBy;
    const updatedAt = dataGrid.updationDate;
    const property = [];
    let itemId;
    if (dataGrid.configObjectId !== undefined && dataGrid.configObjectId !== ''
        && dataGrid.configObjectId !== null) {
        itemId = dataGrid.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    // for setting default value as true
    property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'DEFAULT_ORDERING', dataGrid.defaultOrdering !== undefined ? (dataGrid.defaultOrdering ? '1' : '0') : '1', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    if (dataGrid.gridType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'GRID_TYPE', dataGrid.gridType, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    // for setting default value as true
    property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'IS_ADVANCE_FILTER_FORM', dataGrid.isHavingAdvanceFilterForm !== undefined ? (dataGrid.isHavingAdvanceFilterForm ? '1' : '0') : '0', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    // for setting default value as true
    property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'IS_SCROLL', dataGrid.scroll !== undefined ? (dataGrid.scroll ? '1' : '0') : '0', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    // for setting default value as true
    property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'IS_SWIMLANE_REQUIRED', dataGrid.swimlaneRequired !== undefined ? (dataGrid.swimlaneRequired ? '1' : '0') : '0', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    // for setting default value as true
    property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'IS_MODAL_REQUIRED', dataGrid.modalRequired !== undefined ? (dataGrid.modalRequired ? '1' : '0') : '0', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    // for setting default value as true
    property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'IS_ROW_REORDER', dataGrid.isRowReOrder !== undefined ? (dataGrid.isRowReOrder ? '1' : '0') : '0', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    const configItem = new meta_db_1.ConfigItemModel(itemId, dataGrid.name, configitemtype_enum_1.ConfigItemTypes.DATAGRID, dataGrid.projectId, createdBy, dataGrid.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const datagridPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(dataGrid.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(dataGrid.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(dataGrid.privileges, datagridPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, datagridPrivileges);
};
exports.parseConfigToDataGrid = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.DataGrid(details.get('DEFAULT_ORDERING'), details.get('GRID_TYPE'), details.get('IS_ADVANCE_FILTER_FORM'), details.get('IS_SCROLL'), details.get('IS_SWIMLANE_REQUIRED'), details.get('IS_MODAL_REQUIRED'), details.get('IS_ROW_REORDER'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=datagrid.parser.js.map