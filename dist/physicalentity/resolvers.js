"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const auditentity_parser_1 = require("../auditentity/auditentity.parser");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const physicalcolumn_parser_1 = require("../physicalcolumn/physicalcolumn.parser");
const physicalentity_parser_1 = require("./physicalentity.parser");
exports.Query = {
    PhysicalEntity: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, physicalentity_parser_1.parseConfigToPhysicalEntity),
};
exports.PhysicalEntity = {
    auditEntity: (physicalEntity, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(physicalEntity.configObjectId, relationtype_enum_1.RelationType.PHYSICALENTITY_AUDITENTITY, auditentity_parser_1.parseConfigToAuditEntity),
    physicalColumns: (physicalEntity, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(physicalEntity.configObjectId, relationtype_enum_1.RelationType.PHYSICALENTITY_ENTITYCOLUMN, physicalcolumn_parser_1.parseConfigToPhysicalColumn),
};
exports.Mutation = {
    createPhysicalEntity: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, physicalentity_parser_1.parsePhysicalEntityToConfig, physicalentity_parser_1.parseConfigToPhysicalEntity);
    },
};
//# sourceMappingURL=resolvers.js.map