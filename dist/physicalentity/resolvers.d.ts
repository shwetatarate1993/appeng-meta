export declare const Query: {
    PhysicalEntity: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").PhysicalEntity>;
};
export declare const PhysicalEntity: {
    auditEntity: (physicalEntity: any, _: any, context: any) => Promise<import("../models").AuditEntity[]>;
    physicalColumns: (physicalEntity: any, _: any, context: any) => Promise<import("../models").PhysicalColumn[]>;
};
export declare const Mutation: {
    createPhysicalEntity: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map