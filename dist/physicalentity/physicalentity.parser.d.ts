import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { PhysicalEntity } from '../models';
export declare const parsePhysicalEntityToConfig: (physicalEntity: PhysicalEntity) => ConfigMetadata;
export declare const parseConfigToPhysicalEntity: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => PhysicalEntity;
//# sourceMappingURL=physicalentity.parser.d.ts.map