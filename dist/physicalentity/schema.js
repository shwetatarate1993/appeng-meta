"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = __importDefault(require("../base"));
const common_schema_1 = __importDefault(require("../common.schema"));
const PhysicalEntity = `

input PhysicalEntityInput {

    configObjectId: ID
    compositeEntityId : String
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    expressionAvailable: Boolean
    accessibilityRegex: String
    isMultivalueMapping: Boolean
    isPrimaryEntity: Boolean
    order: Int
    dbType: String
    dbTypeName: String
    schemaName: String
    releaseAreaSessName: String
    workAreaSessName: String
    insertQID: String
    updateQID: String
    sequenceQID: String
    singleSelectQID: String
    multiSelectQID: String
    deleteQID: String
    isAuditRequired: Boolean
    privileges : [ConfigItemPrivilegeInput]
    parentRelations : [ConfigItemRelationInput]
    childRelations : [ConfigItemRelationInput]
}


extend type Query {
    PhysicalEntity(id: ID!): PhysicalEntity
   }

extend type Mutation {
    createPhysicalEntity (input: PhysicalEntityInput): PhysicalEntity
}

type PhysicalEntity {
    configObjectId: ID
    compositeEntityId : String
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    expressionAvailable: Boolean
    accessibilityRegex: String
    isMultivalueMapping: Boolean
    isPrimaryEntity:Boolean
    order: Int
    dbType: String
    dbTypeName: String
    schemaName: String
    releaseAreaSessName: String
    workAreaSessName: String
    insertQID: String
    updateQID: String
    sequenceQID: String
    singleSelectQID: String
    multiSelectQID: String
    deleteQID: String
    isAuditRequired: Boolean
    privileges : [ConfigItemPrivilege]
    parentRelations : [ConfigItemRelation]
    childRelations : [ConfigItemRelation]
    auditEntity : [AuditEntity]
    physicalColumns : [PhysicalColumn]
}
`;
exports.default = () => [PhysicalEntity, base_1.default, common_schema_1.default];
//# sourceMappingURL=schema.js.map