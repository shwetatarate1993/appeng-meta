"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: '4893f5af-d8c5-4afe-99ec-fc3f480784ae',
        name: 'PhysicalEntity_Building Branch-EmployeeInfo',
        configObjectType: 'PhysicalEntity',
        createdBy: '1121',
        isDeleted: 0,
        itemDescription: null,
        creationDate: null,
        projectId: 1,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
        dbType: 'Table',
        schemaName: 'ddoPortalapp',
        order: 5,
        dbTypeName: 'BUILDING_BRANCH_EMPLOYEE_INFO',
        releaseAreaSessName: 'test',
        workAreaSessName: 'test',
        insertQID: 'test',
        updateQID: 'test',
        sequenceQID: 'test',
        singleSelectQID: 'test',
        multiSelectQID: 'test',
        deleteQID: 'test',
        isAuditRequired: false,
        privileges: [],
        childRelations: [
            {
                relationId: '0e5a0762-8760-47e7-88c8-8a6c3d12ce04',
                relationType: 'PhysicalEntity_EntityColumn',
                parentItemId: '4893f5af-d8c5-4afe-99ec-fc3f480784ae',
                childItemId: '9b0bfecd-bb93-4988-86b2-91e3da36e5ce',
                createdBy: '1121',
                isDeleted: 0,
                creationDate: null,
                updatedBy: '1121',
                updationDate: null,
                deletionDate: null,
            },
        ],
    },
];
//# sourceMappingURL=physicalentity.js.map