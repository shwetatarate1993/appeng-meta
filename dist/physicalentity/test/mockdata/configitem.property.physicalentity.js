"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        propertyId: '7c70f7bc-12ef-4205-ace7-aabc1a43e121',
        propertyName: 'DBTYPE',
        propertyValue: 'Table',
        itemId: '4893f5af-d8c5-4afe-99ec-fc3f480784ae',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    }, {
        propertyId: '5ee117da-5d5a-477c-bdf8-f186c51f777a',
        propertyName: 'DBTYPENAME',
        propertyValue: 'BUILDING_BRANCH_EMPLOYEE_INFO',
        itemId: '4893f5af-d8c5-4afe-99ec-fc3f480784ae',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    }, {
        propertyId: '339b19f8-fbaf-420d-af2a-442128f12896',
        propertyName: 'ORDER',
        propertyValue: 5,
        itemId: '4893f5af-d8c5-4afe-99ec-fc3f480784ae',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    }, {
        propertyId: '3b999347-bbfd-4c43-ab13-703fb00e6570',
        propertyName: 'SCHEMA_NAME',
        propertyValue: 'ddoPortalapp',
        itemId: '4893f5af-d8c5-4afe-99ec-fc3f480784ae',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    }, {
        propertyId: '313960b5-41c1-49e1-828c-7abd9a4f3440',
        propertyName: 'RELEASE_AREA_SESS_NAME',
        propertyValue: 'test',
        itemId: '4893f5af-d8c5-4afe-99ec-fc3f480784ae',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    }, {
        propertyId: '5f53c26a-d1bc-43a1-afd4-de9cc7c0fbb8',
        propertyName: 'WORK_AREA_SESS_NAME',
        propertyValue: 'test',
        itemId: '4893f5af-d8c5-4afe-99ec-fc3f480784ae',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    }, {
        propertyId: 'a1c2875d-55d5-4d2c-b8b7-eeb38456e67b',
        propertyName: 'INSERT_QID',
        propertyValue: 'test',
        itemId: '4893f5af-d8c5-4afe-99ec-fc3f480784ae',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    }, {
        propertyId: '51e29bd7-e751-4141-ad91-71132910a8fc',
        propertyName: 'UPDATE_QID',
        propertyValue: 'test',
        itemId: '4893f5af-d8c5-4afe-99ec-fc3f480784ae',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    }, {
        propertyId: '8cf1aac3-d9c7-45b9-83a5-b4a7fe0835ac',
        propertyName: 'SEQUENCE_QID',
        propertyValue: 'test',
        itemId: '4893f5af-d8c5-4afe-99ec-fc3f480784ae',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    }, {
        propertyId: '87260051-9967-4cc5-b51f-3407afdec311',
        propertyName: 'SINGLE_SELECT_QID',
        propertyValue: 'test',
        itemId: '4893f5af-d8c5-4afe-99ec-fc3f480784ae',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    }, {
        propertyId: '699615d8-bbe7-4383-83a8-d5961a57d659',
        propertyName: 'MULTI_SELECT_QID',
        propertyValue: 'test',
        itemId: '4893f5af-d8c5-4afe-99ec-fc3f480784ae',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    }, {
        propertyId: '8b175687-6a2d-4ec1-aa4e-c108d98b850c',
        propertyName: 'DELETE_QID',
        propertyValue: 'test',
        itemId: '4893f5af-d8c5-4afe-99ec-fc3f480784ae',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    }, {
        propertyId: 'f6946400-3072-403d-bb14-7210483392ba',
        propertyName: 'IS_AUDIT_REQUIRED',
        propertyValue: false,
        itemId: '4893f5af-d8c5-4afe-99ec-fc3f480784ae',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '69905d8-bbe7-4383-83t8-d5961a57d659',
        propertyName: 'IS_EXPRESSION_AVAILABLE',
        propertyValue: 'false',
        itemId: '4893f5af-d8c5-4afe-99ec-fc3f480784ae',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '8b175687-6aad-4ec1-aa4e-c1erd98b850c',
        propertyName: 'ACCESSBILITY_REGEX',
        propertyValue: null,
        itemId: '4893f5af-d8c5-4afe-99ec-fc3f480784ae',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: 'f646400-3072-403d-bb14-72104392ba',
        propertyName: 'IS_MULITIVALUE_MAPPING_ADS',
        propertyValue: false,
        itemId: '4893f5af-d8c5-4afe-99ec-fc3f480784ae',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
];
//# sourceMappingURL=configitem.property.physicalentity.js.map