export { default as physicalEntityMockData } from './physicalentity';
export { default as physicalEntityConfigitemMockData } from './configitem.physicalentity';
export { default as physicalEntityPropertyMockData } from './configitem.property.physicalentity';
export { default as physicalEntityConfigRelationsMockData } from './configitem.relation.physicalentity';
export { default as physicalEntityConfigPrivilegesMockData } from './configitem.privileges.physicalentity';
//# sourceMappingURL=index.d.ts.map