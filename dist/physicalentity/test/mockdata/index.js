"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var physicalentity_1 = require("./physicalentity");
exports.physicalEntityMockData = physicalentity_1.default;
var configitem_physicalentity_1 = require("./configitem.physicalentity");
exports.physicalEntityConfigitemMockData = configitem_physicalentity_1.default;
var configitem_property_physicalentity_1 = require("./configitem.property.physicalentity");
exports.physicalEntityPropertyMockData = configitem_property_physicalentity_1.default;
var configitem_relation_physicalentity_1 = require("./configitem.relation.physicalentity");
exports.physicalEntityConfigRelationsMockData = configitem_relation_physicalentity_1.default;
var configitem_privileges_physicalentity_1 = require("./configitem.privileges.physicalentity");
exports.physicalEntityConfigPrivilegesMockData = configitem_privileges_physicalentity_1.default;
//# sourceMappingURL=index.js.map