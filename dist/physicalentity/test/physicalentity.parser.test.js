"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const physicalentity_parser_1 = require("../physicalentity.parser");
const mockdata_1 = require("./mockdata");
test('parser physical entity object to config', () => {
    const physicalEntity = models_1.PhysicalEntity.deserialize(mockdata_1.physicalEntityMockData[0]);
    const result = physicalentity_parser_1.parsePhysicalEntityToConfig(physicalEntity);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItem.configObjectType).toEqual('PhysicalEntity');
    expect(result.configItemRelation.length === 1).toBe(true);
    expect(result.configItemPrivilege.length === 0).toBe(true);
    expect(result.configItemProperty.length === 17).toBe(true);
});
test('parser config to physical entity object', () => {
    const result = physicalentity_parser_1.parseConfigToPhysicalEntity(mockdata_1.physicalEntityConfigitemMockData[0], mockdata_1.physicalEntityPropertyMockData, mockdata_1.physicalEntityConfigRelationsMockData, mockdata_1.physicalEntityConfigPrivilegesMockData);
    expect(result).toHaveProperty('configObjectId', '4893f5af-d8c5-4afe-99ec-fc3f480784ae');
    expect(result.privileges.length === 0).toBe(true);
});
//# sourceMappingURL=physicalentity.parser.test.js.map