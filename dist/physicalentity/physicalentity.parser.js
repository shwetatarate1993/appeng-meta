"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parsePhysicalEntityToConfig = (physicalEntity) => {
    const createdAt = physicalEntity.creationDate;
    const createdBy = physicalEntity.createdBy;
    const updatedBy = physicalEntity.updatedBy;
    const updatedAt = physicalEntity.updationDate;
    const property = [];
    let itemId;
    if (physicalEntity.configObjectId !== undefined && physicalEntity.configObjectId !== ''
        && physicalEntity.configObjectId !== null) {
        itemId = physicalEntity.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    property.push(new meta_db_1.ConfigItemProperty('IS_EXPRESSION_AVAILABLE', physicalEntity.expressionAvailable !== undefined ? (physicalEntity.expressionAvailable ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    if (physicalEntity.deleteQID !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ACCESSBILITY_REGEX', physicalEntity.accessibilityRegex, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    property.push(new meta_db_1.ConfigItemProperty('IS_MULITIVALUE_MAPPING_ADS', physicalEntity.isMultivalueMapping !== undefined ? (physicalEntity.isMultivalueMapping ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    property.push(new meta_db_1.ConfigItemProperty('IS_PRIMARY_ENTITY', physicalEntity.isPrimaryEntity !== undefined ? (physicalEntity.isPrimaryEntity ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    if (physicalEntity.order !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ORDER', physicalEntity.order, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (physicalEntity.dbType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('DBTYPE', physicalEntity.dbType, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (physicalEntity.dbTypeName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('DBTYPENAME', physicalEntity.dbTypeName, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (physicalEntity.schemaName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('SCHEMA_NAME', physicalEntity.schemaName, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (physicalEntity.releaseAreaSessName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('RELEASE_AREA_SESS_NAME', physicalEntity.releaseAreaSessName, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (physicalEntity.workAreaSessName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('WORK_AREA_SESS_NAME', physicalEntity.workAreaSessName, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (physicalEntity.insertQID !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('INSERT_QID', physicalEntity.insertQID, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (physicalEntity.updateQID !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('UPDATE_QID', physicalEntity.updateQID, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (physicalEntity.sequenceQID !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('SEQUENCE_QID', physicalEntity.sequenceQID, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (physicalEntity.singleSelectQID !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('SINGLE_SELECT_QID', physicalEntity.singleSelectQID, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (physicalEntity.multiSelectQID !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('MULTI_SELECT_QID', physicalEntity.multiSelectQID, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (physicalEntity.deleteQID !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('DELETE_QID', physicalEntity.deleteQID, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    // for setting default value as false
    property.push(new meta_db_1.ConfigItemProperty('IS_AUDIT_REQUIRED', physicalEntity.isAuditRequired !== undefined ? (physicalEntity.isAuditRequired ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    const configItem = new meta_db_1.ConfigItemModel(itemId, physicalEntity.name, configitemtype_enum_1.ConfigItemTypes.PHYSICALENTITY, physicalEntity.projectId, createdBy, physicalEntity.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const physicalEntityPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(physicalEntity.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(physicalEntity.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(physicalEntity.privileges, physicalEntityPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, physicalEntityPrivileges);
};
exports.parseConfigToPhysicalEntity = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.PhysicalEntity(details.get('IS_EXPRESSION_AVAILABLE'), details.get('ACCESSBILITY_REGEX'), details.get('IS_MULITIVALUE_MAPPING_ADS'), details.get('IS_PRIMARY_ENTITY'), details.get('ORDER'), details.get('DBTYPE'), details.get('DBTYPENAME'), details.get('SCHEMA_NAME'), details.get('RELEASE_AREA_SESS_NAME'), details.get('WORK_AREA_SESS_NAME'), details.get('INSERT_QID'), details.get('UPDATE_QID'), details.get('SEQUENCE_QID'), details.get('SINGLE_SELECT_QID'), details.get('MULTI_SELECT_QID'), details.get('DELETE_QID'), details.get('IS_AUDIT_REQUIRED'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=physicalentity.parser.js.map