"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const action_parser_1 = require("../action/action.parser");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const datagrid_parser_1 = require("../datagrid/datagrid.parser");
const datasetrel_parser_1 = require("../datasetrel/datasetrel.parser");
const dboperation_parser_1 = require("../dboperation/dboperation.parser");
const form_parser_1 = require("../form/form.parser");
const formdbvalidation_parser_1 = require("../formdbvalidation/formdbvalidation.parser");
const logicalcolumn_parser_1 = require("../logicalcolumn/logicalcolumn.parser");
const physicalentity_parser_1 = require("../physicalentity/physicalentity.parser");
const logicalentity_parser_1 = require("./logicalentity.parser");
exports.Query = {
    LogicalEntity: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, logicalentity_parser_1.parseConfigToLogicalEntity),
};
exports.LogicalEntity = {
    physicalEntities: (logicalEntity, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(logicalEntity.configObjectId, relationtype_enum_1.RelationType.LOGICALENTITY_PHYSICALENTITY, physicalentity_parser_1.parseConfigToPhysicalEntity),
    logicalColumns: async (logicalEntity, _, context) => {
        const columnsIds = logicalEntity.childRelations.filter((rel) => rel.relationType === relationtype_enum_1.RelationType.LOGICALENTITY_ENTITYCOLUMN)
            .map((rel) => rel.childItemId);
        return columnsIds ?
            await common_meta_1.configService.fetchConfigItemsForIdList(columnsIds, logicalcolumn_parser_1.parseConfigToLogicalColumn) : [];
    },
    forms: (logicalEntity, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(logicalEntity.configObjectId, relationtype_enum_1.RelationType.LOGICALENTITY_FORM, form_parser_1.parseConfigToForm),
    dataGrids: (logicalEntity, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(logicalEntity.configObjectId, relationtype_enum_1.RelationType.LOGICALENTITY_DATAGRID, datagrid_parser_1.parseConfigToDataGrid),
    actions: (logicalEntity, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(logicalEntity.configObjectId, relationtype_enum_1.RelationType.LOGICALENTITY_ACTION, action_parser_1.parseConfigToAction),
    dbOperations: (logicalEntity, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(logicalEntity.configObjectId, relationtype_enum_1.RelationType.LOGICALENTITY_DBOPERATION, dboperation_parser_1.parseConfigToDBOperation),
    dataSetRels: (logicalEntity, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(logicalEntity.configObjectId, relationtype_enum_1.RelationType.LOGICALENTITY_DATASETREL, datasetrel_parser_1.parseConfigToDataSetRel),
    formDbValidations: async (logicalEntity, _, context) => {
        const validationIds = logicalEntity.childRelations.filter((rel) => rel.relationType === relationtype_enum_1.RelationType.LOGICALENTITY_VALIDATIONOBJECT)
            .map((rel) => rel.childItemId);
        return validationIds ?
            await common_meta_1.configService.fetchConfigItemsForIdList(validationIds, formdbvalidation_parser_1.parseConfigToFormDbValidation) : [];
    },
};
exports.Mutation = {
    createLogicalEntity: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, logicalentity_parser_1.parseLogicalEntityToConfig, logicalentity_parser_1.parseConfigToLogicalEntity);
    },
};
//# sourceMappingURL=resolvers.js.map