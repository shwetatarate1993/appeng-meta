"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseLogicalEntityToConfig = (logicalEntity) => {
    const createdAt = logicalEntity.creationDate;
    const createdBy = logicalEntity.createdBy;
    const updatedBy = logicalEntity.updatedBy;
    const updatedAt = logicalEntity.updationDate;
    const property = [];
    let itemId;
    if (logicalEntity.configObjectId !== undefined && logicalEntity.configObjectId !== ''
        && logicalEntity.configObjectId !== null) {
        itemId = logicalEntity.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (logicalEntity.dbTypeName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'DBTYPENAME', logicalEntity.dbTypeName, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'GENERATE_SKELETON', logicalEntity.generateSkeleton !== undefined ? (logicalEntity.generateSkeleton ? '1' : '0') : '1', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    if (logicalEntity.supportedFlavor !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'SUPPORTED_FLAVOR', logicalEntity.supportedFlavor, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, logicalEntity.name, configitemtype_enum_1.ConfigItemTypes.LOGICALENTITY, logicalEntity.projectId, createdBy, logicalEntity.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const logicalEntityPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(logicalEntity.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(logicalEntity.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(logicalEntity.privileges, logicalEntityPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, logicalEntityPrivileges);
};
exports.parseConfigToLogicalEntity = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations != null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.LogicalEntity(configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, details.get('DBTYPENAME'), details.get('SUPPORTED_FLAVOR'), details.get('GENERATE_SKELETON'), privileges, childRelations, parentRelations);
};
//# sourceMappingURL=logicalentity.parser.js.map