"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const logicalentity_parser_1 = require("../logicalentity.parser");
const mockdata_1 = require("./mockdata");
test('parser logical entity object to config', () => {
    const logicalEntity = models_1.LogicalEntity.deserialize(mockdata_1.logicalEntityMockData[0]);
    const result = logicalentity_parser_1.parseLogicalEntityToConfig(logicalEntity);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItem.configObjectType).toEqual('LogicalEntity');
    expect(result.configItemRelation.length === 1).toBe(true);
    expect(result.configItemPrivilege.length === 0).toBe(true);
    expect(result.configItemProperty.length === 3).toBe(true);
});
test('parser config to logical entity object', () => {
    const result = logicalentity_parser_1.parseConfigToLogicalEntity(mockdata_1.logicalEntityConfigitemMockData[0], mockdata_1.logicalEntityPropertyMockData, mockdata_1.logicalEntityConfigRelationsMockData, mockdata_1.logicalEntityConfigPrivilegesMockData);
    expect(result).toHaveProperty('dbTypeName');
    expect(result).toHaveProperty('dbTypeName', 'Building Branch-EmployeeInfo');
    expect(result.privileges.length === 0).toBe(true);
});
//# sourceMappingURL=logicalentity.parser.test.js.map