"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var logicalentity_1 = require("./logicalentity");
exports.logicalEntityMockData = logicalentity_1.default;
var configitem_logicalentity_1 = require("./configitem.logicalentity");
exports.logicalEntityConfigitemMockData = configitem_logicalentity_1.default;
var configitem_property_logicalentity_1 = require("./configitem.property.logicalentity");
exports.logicalEntityPropertyMockData = configitem_property_logicalentity_1.default;
var configitem_relation_logicalentity_1 = require("./configitem.relation.logicalentity");
exports.logicalEntityConfigRelationsMockData = configitem_relation_logicalentity_1.default;
var configitem_privileges_logicalentity_1 = require("./configitem.privileges.logicalentity");
exports.logicalEntityConfigPrivilegesMockData = configitem_privileges_logicalentity_1.default;
//# sourceMappingURL=index.js.map