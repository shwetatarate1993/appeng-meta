"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        propertyId: 'c3c0e963-f74a-48e0-9247-fe8de25d9b20',
        propertyName: 'GENERATE_SKELETON',
        propertyValue: true,
        itemId: 'e32836c5-5cb0-459c-9a83-1a1d35d231b3',
        createdBy: '1121',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '8a4b7f55-b4d3-417a-93aa-141bc5999406',
        propertyName: 'SUPPORTED_FLAVOR',
        propertyValue: null,
        itemId: 'e32836c5-5cb0-459c-9a83-1a1d35d231b3',
        createdBy: '1121',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '2230f184-1031-4826-855e-bae31c33de11',
        propertyName: 'DBTYPENAME',
        propertyValue: 'Building Branch-EmployeeInfo',
        itemId: 'e32836c5-5cb0-459c-9a83-1a1d35d231b3',
        createdBy: '1121',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
];
//# sourceMappingURL=configitem.property.logicalentity.js.map