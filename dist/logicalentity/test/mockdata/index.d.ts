export { default as logicalEntityMockData } from './logicalentity';
export { default as logicalEntityConfigitemMockData } from './configitem.logicalentity';
export { default as logicalEntityPropertyMockData } from './configitem.property.logicalentity';
export { default as logicalEntityConfigRelationsMockData } from './configitem.relation.logicalentity';
export { default as logicalEntityConfigPrivilegesMockData } from './configitem.privileges.logicalentity';
//# sourceMappingURL=index.d.ts.map