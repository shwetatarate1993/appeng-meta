"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: 'e32836c5-5cb0-459c-9a83-1a1d35d231b3',
        name: 'Building Branch-EmployeeInfo',
        configObjectType: 'LogicalEntity',
        createdBy: '1121',
        isDeleted: 0,
        itemDescription: null,
        creationDate: null,
        projectId: 1,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
        dbTypeName: 'Building Branch-EmployeeInfo',
        supportedFlavor: null,
        generateSkeleton: true,
        privileges: [],
        childRelations: [
            {
                relationId: 'f95d6ba4-6fd4-4133-a3ac-a1ccec2ca8be',
                relationType: 'LogicalEntity_PhysicalEntity',
                parentItemId: 'e368efb9-ec7e-4071-96f1-91c51a57d195',
                childItemId: '4893f5af-d8c5-4afe-99ec-fc3f480784ae',
                createdBy: '1121',
                isDeleted: 0,
                creationDate: null,
                updatedBy: '1121',
                updationDate: null,
                deletionDate: null,
            },
        ],
    },
];
//# sourceMappingURL=logicalentity.js.map