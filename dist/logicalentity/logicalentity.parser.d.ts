import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { LogicalEntity } from '../models';
export declare const parseLogicalEntityToConfig: (logicalEntity: LogicalEntity) => ConfigMetadata;
export declare const parseConfigToLogicalEntity: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => LogicalEntity;
//# sourceMappingURL=logicalentity.parser.d.ts.map