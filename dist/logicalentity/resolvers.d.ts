export declare const Query: {
    LogicalEntity: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").LogicalEntity>;
};
export declare const LogicalEntity: {
    physicalEntities: (logicalEntity: any, _: any, context: any) => Promise<import("../models").PhysicalEntity[]>;
    logicalColumns: (logicalEntity: any, _: any, context: any) => Promise<import("../models").LogicalColumn[]>;
    forms: (logicalEntity: any, _: any, context: any) => Promise<import("../models").Form[]>;
    dataGrids: (logicalEntity: any, _: any, context: any) => Promise<import("../models").DataGrid[]>;
    actions: (logicalEntity: any, _: any, context: any) => Promise<import("../models").Action[]>;
    dbOperations: (logicalEntity: any, _: any, context: any) => Promise<import("../models").DBOperation[]>;
    dataSetRels: (logicalEntity: any, _: any, context: any) => Promise<import("../models").DataSetRel[]>;
    formDbValidations: (logicalEntity: any, _: any, context: any) => Promise<import("../models").FormDbValidation[]>;
};
export declare const Mutation: {
    createLogicalEntity: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map