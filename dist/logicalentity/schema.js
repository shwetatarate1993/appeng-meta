"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = __importDefault(require("../base"));
const common_schema_1 = __importDefault(require("../common.schema"));
const LogicalEntity = `

input LogicalEntityInput {

    name: String
    configObjectId: ID
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    dbTypeName: String
    supportedFlavor: String
    generateSkeleton: Boolean
    privileges : [ConfigItemPrivilegeInput]
    parentRelations : [ConfigItemRelationInput]
    childRelations : [ConfigItemRelationInput]
}

extend type Query {
    LogicalEntity(id: ID!): LogicalEntity
   }

extend type Mutation {
    createLogicalEntity (input: LogicalEntityInput): LogicalEntity
}

type LogicalEntity {

    name: String
    configObjectId: ID
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    dbTypeName: String
    supportedFlavor: String
    generateSkeleton: Boolean
    privileges : [ConfigItemPrivilege]
    parentRelations : [ConfigItemRelation]
    childRelations : [ConfigItemRelation]
    physicalEntities : [PhysicalEntity]
    logicalColumns : [LogicalColumn]
    forms : [Form]
    dataGrids : [DataGrid]
    actions : [Action]
    dbOperations : [DBOperation]
    dataSetRels : [DataSetRel]
    formDbValidations : [FormDbValidation]
    customFormValidations : [CustomFormValidation]
}
`;
exports.default = () => [LogicalEntity, base_1.default, common_schema_1.default];
//# sourceMappingURL=schema.js.map