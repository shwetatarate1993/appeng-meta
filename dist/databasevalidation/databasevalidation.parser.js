"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseDatabaseValidationToConfig = (databaseValidation) => {
    const createdAt = databaseValidation.creationDate;
    const createdBy = databaseValidation.createdBy;
    const updatedBy = databaseValidation.updatedBy;
    const updatedAt = databaseValidation.updationDate;
    const property = [];
    let itemId;
    if (databaseValidation.configObjectId !== undefined && databaseValidation.configObjectId !== ''
        && databaseValidation.configObjectId !== null) {
        itemId = databaseValidation.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (databaseValidation.datasourceName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'MODE', databaseValidation.mode, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (databaseValidation.datasourceName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'DATASOURCE_NAME', databaseValidation.datasourceName, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (databaseValidation.validationType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'VALIDATION_TYPE', databaseValidation.validationType, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (databaseValidation.validationExpression !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'VALIDATION_EXPRESSION', databaseValidation.validationExpression, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (databaseValidation.validationQid !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'VALIDATION_QID', databaseValidation.validationQid, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (databaseValidation.validationMessage !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'VALIDATION_MSG', databaseValidation.validationMessage, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (databaseValidation.validationExpressionKeys !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'VALIDATION_EXPRESSION_KEYS', databaseValidation.validationExpressionKeys, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'IS_CONDITION_AVAILABLE', databaseValidation.isConditionAvailable !== undefined ?
        (databaseValidation.isConditionAvailable ? '1' : '0') : '0', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    if (databaseValidation.conditionExpression !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'CONDITION_EXPRESSION', databaseValidation.conditionExpression, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (databaseValidation.conditionFields !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'CONDITION_FIELDS', databaseValidation.conditionFields, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (databaseValidation.description !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'DESCRIPTION', databaseValidation.description, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, databaseValidation.name, configitemtype_enum_1.ConfigItemTypes.DATABASEVALIDATION, databaseValidation.projectId, createdBy, databaseValidation.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const databaseValidationPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(databaseValidation.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(databaseValidation.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(databaseValidation.privileges, databaseValidationPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, databaseValidationPrivileges);
};
exports.parseConfigToDatabaseValidation = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations != null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.DatabaseValidation(configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, details.get('MODE'), details.get('DATASOURCE_NAME'), details.get('VALIDATION_TYPE'), details.get('VALIDATION_EXPRESSION'), details.get('VALIDATION_QID'), details.get('VALIDATION_MSG'), details.get('VALIDATION_EXPRESSION_KEYS'), details.get('IS_CONDITION_AVAILABLE'), details.get('CONDITION_EXPRESSION'), details.get('CONDITION_FIELDS'), details.get('DESCRIPTION'), privileges, childRelations, parentRelations);
};
//# sourceMappingURL=databasevalidation.parser.js.map