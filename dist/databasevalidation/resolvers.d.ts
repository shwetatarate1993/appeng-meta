export declare const Query: {
    DatabaseValidation: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").DatabaseValidation>;
};
export declare const Mutation: {
    createDatabaseValidation: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map