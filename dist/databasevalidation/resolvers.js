"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const databasevalidation_parser_1 = require("./databasevalidation.parser");
exports.Query = {
    DatabaseValidation: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, databasevalidation_parser_1.parseConfigToDatabaseValidation),
};
exports.Mutation = {
    createDatabaseValidation: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, databasevalidation_parser_1.parseDatabaseValidationToConfig, databasevalidation_parser_1.parseConfigToDatabaseValidation);
    },
};
//# sourceMappingURL=resolvers.js.map