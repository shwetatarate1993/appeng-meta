"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const databasevalidation_parser_1 = require("../databasevalidation.parser");
const mockdata_1 = require("./mockdata");
test('parser database validation  to config', () => {
    const databaseValidation = models_1.DatabaseValidation.deserialize(mockdata_1.databaseValidationMockData[0]);
    const result = databasevalidation_parser_1.parseDatabaseValidationToConfig(databaseValidation);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItem.configObjectType).toEqual('DatabaseValidation');
    expect(result.configItemRelation.length === 1).toBe(true);
    expect(result.configItemPrivilege.length === 0).toBe(true);
    expect(result.configItemProperty.length === 11).toBe(true);
});
test('parser config to database validation ', () => {
    const result = databasevalidation_parser_1.parseConfigToDatabaseValidation(mockdata_1.databaseValidationConfigitemMockData[0], mockdata_1.databaseValidationPropertyMockData, mockdata_1.databaseValidationConfigRelationsMockData, mockdata_1.databaseValidationConfigPrivilegesMockData);
    expect(result).toHaveProperty('datasourceName');
    expect(result.privileges.length === 0).toBe(true);
});
//# sourceMappingURL=databasevalidation.parser.test.js.map