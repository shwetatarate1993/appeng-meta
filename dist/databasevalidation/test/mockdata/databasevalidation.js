"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: '0ece272a-422d-402b-903d-51c37bee92ad',
        name: 'Wizard Logical Entity Name should not be same',
        configObjectType: 'DatabaseValidation',
        createdBy: '1121',
        isDeleted: 0,
        itemDescription: null,
        creationDate: null,
        projectId: -1,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
        datasourceName: 'test',
        validationType: 'test',
        validationExpression: 'test',
        validationQid: 'test',
        validationMessage: 'test',
        validationExpressionKeys: 'test',
        privileges: [],
        parentRelations: [
            {
                relationId: '3c7e07ae-8ba3-499c-b19d-14a351ce8a76',
                relationType: 'FormField_ValidationObject',
                parentItemId: 'ac7d01fc-7572-44f2-b195-244bb03d4b47',
                childItemId: '0ece272a-422d-402b-903d-51c37bee92ad',
                createdBy: '1121',
                isDeleted: 0,
                creationDate: null,
                updatedBy: '1121',
                updationDate: null,
                deletionDate: null,
            },
        ],
    },
];
//# sourceMappingURL=databasevalidation.js.map