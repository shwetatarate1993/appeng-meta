"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: '0ece272a-422d-402b-903d-51c37bee92ad',
        name: 'Wizard Logical Entity Name should not be same',
        configObjectType: 'DatabaseValidation',
        createdBy: '1121',
        isDeleted: 0,
        itemDescription: null,
        creationDate: null,
        projectId: 1,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
    },
];
//# sourceMappingURL=configitem.databasevalidation.js.map