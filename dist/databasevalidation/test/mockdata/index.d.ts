export { default as databaseValidationMockData } from './databasevalidation';
export { default as databaseValidationConfigitemMockData } from './configitem.databasevalidation';
export { default as databaseValidationPropertyMockData } from './configitem.property.databasevalidation';
export { default as databaseValidationConfigRelationsMockData } from './configitem.relation.databasevalidation';
export { default as databaseValidationConfigPrivilegesMockData } from './configitem.privileges.databasevalidation';
//# sourceMappingURL=index.d.ts.map