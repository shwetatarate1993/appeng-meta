"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var databasevalidation_1 = require("./databasevalidation");
exports.databaseValidationMockData = databasevalidation_1.default;
var configitem_databasevalidation_1 = require("./configitem.databasevalidation");
exports.databaseValidationConfigitemMockData = configitem_databasevalidation_1.default;
var configitem_property_databasevalidation_1 = require("./configitem.property.databasevalidation");
exports.databaseValidationPropertyMockData = configitem_property_databasevalidation_1.default;
var configitem_relation_databasevalidation_1 = require("./configitem.relation.databasevalidation");
exports.databaseValidationConfigRelationsMockData = configitem_relation_databasevalidation_1.default;
var configitem_privileges_databasevalidation_1 = require("./configitem.privileges.databasevalidation");
exports.databaseValidationConfigPrivilegesMockData = configitem_privileges_databasevalidation_1.default;
//# sourceMappingURL=index.js.map