import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { DatabaseValidation } from '../models';
export declare const parseDatabaseValidationToConfig: (databaseValidation: DatabaseValidation) => ConfigMetadata;
export declare const parseConfigToDatabaseValidation: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => DatabaseValidation;
//# sourceMappingURL=databasevalidation.parser.d.ts.map