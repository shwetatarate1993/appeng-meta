import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { EventListener } from '../models';
export declare const parseEventListenerToConfig: (eventListener: EventListener) => ConfigMetadata;
export declare const parseConfigToEventListener: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => EventListener;
//# sourceMappingURL=eventlistener.parser.d.ts.map