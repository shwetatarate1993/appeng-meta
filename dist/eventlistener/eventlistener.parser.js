"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseEventListenerToConfig = (eventListener) => {
    const createdAt = eventListener.creationDate;
    const createdBy = eventListener.createdBy;
    const updatedBy = eventListener.updatedBy;
    const updatedAt = eventListener.updationDate;
    const property = [];
    let itemId;
    if (eventListener.configObjectId !== undefined && eventListener.configObjectId !== ''
        && eventListener.configObjectId !== null) {
        itemId = eventListener.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (eventListener.eventClass !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'CLASS', eventListener.eventClass, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (eventListener.eventType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'EVENT', eventListener.eventType, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (eventListener.isBackgroundListener !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'IS_BACKGROUND_LISTENER', eventListener.isBackgroundListener !== undefined ? (eventListener.isBackgroundListener ? '1' : '0') : '0', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, eventListener.name, configitemtype_enum_1.ConfigItemTypes.EVENTLISTENER, eventListener.projectId, createdBy, eventListener.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const eventListenerPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(eventListener.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(eventListener.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(eventListener.privileges, eventListenerPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, eventListenerPrivileges);
};
exports.parseConfigToEventListener = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.EventListener(details.get('CLASS'), details.get('EVENT'), details.get('IS_BACKGROUND_LISTENER'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=eventlistener.parser.js.map