"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const eventlistener_parser_1 = require("./eventlistener.parser");
exports.Query = {
    EventListener: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, eventlistener_parser_1.parseConfigToEventListener),
};
exports.Mutation = {
    createEventListener: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, eventlistener_parser_1.parseEventListenerToConfig, eventlistener_parser_1.parseConfigToEventListener);
    },
};
//# sourceMappingURL=resolvers.js.map