export { default as eventListenerMockData } from './eventlistener';
export { default as eventListenerConfigItemMockData } from './configitem.eventlistener';
export { default as eventListenerPropertyMockData } from './configitem.property.eventlistener';
export { default as eventListenerPrivilegeMockData } from './configitem.privilege.eventlistener';
export { default as eventListenerRelationMockData } from './configitem.relation.eventlistener';
//# sourceMappingURL=index.d.ts.map