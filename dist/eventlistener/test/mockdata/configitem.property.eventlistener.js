"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        propertyId: '4bbf56a3-9faa-4fff-a19a-59c2ae0e16f9',
        propertyName: 'CLASS',
        propertyValue: 'com.appengine.inboxfeaturemanagement.event.listener.ReleaseItemGenerator',
        itemId: '45a700e8-97ce-4779-aff2-36f1487fe888',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: 'b7f56163-b3e6-45a0-97e0-614553ff8924',
        propertyName: 'IS_BACKGROUND_LISTENER',
        propertyValue: true,
        itemId: '45a700e8-97ce-4779-aff2-36f1487fe888',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: 'fba19a9d-8d29-4b4c-9c99-1f3dc8bd6a8e',
        propertyName: 'EVENT',
        propertyValue: 'DataOperationEvent',
        itemId: '45a700e8-97ce-4779-aff2-36f1487fe888',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
];
//# sourceMappingURL=configitem.property.eventlistener.js.map