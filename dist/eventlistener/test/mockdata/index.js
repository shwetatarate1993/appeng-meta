"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var eventlistener_1 = require("./eventlistener");
exports.eventListenerMockData = eventlistener_1.default;
var configitem_eventlistener_1 = require("./configitem.eventlistener");
exports.eventListenerConfigItemMockData = configitem_eventlistener_1.default;
var configitem_property_eventlistener_1 = require("./configitem.property.eventlistener");
exports.eventListenerPropertyMockData = configitem_property_eventlistener_1.default;
var configitem_privilege_eventlistener_1 = require("./configitem.privilege.eventlistener");
exports.eventListenerPrivilegeMockData = configitem_privilege_eventlistener_1.default;
var configitem_relation_eventlistener_1 = require("./configitem.relation.eventlistener");
exports.eventListenerRelationMockData = configitem_relation_eventlistener_1.default;
//# sourceMappingURL=index.js.map