declare const _default: {
    configObjectId: string;
    configObjectType: string;
    name: string;
    isBackgroundListener: boolean;
    eventClass: string;
    eventType: string;
    privileges: any[];
    parentRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        childItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: any;
        updatedBy: string;
        updationDate: any;
        deletionDate: any;
    }[];
    childRelations: any[];
}[];
export default _default;
//# sourceMappingURL=eventlistener.d.ts.map