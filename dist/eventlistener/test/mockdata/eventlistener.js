"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: '45a700e8-97ce-4779-aff2-36f1487fe888',
        configObjectType: 'EventListener',
        name: 'ReleaseItemGenerator',
        isBackgroundListener: false,
        eventClass: 'com.appengine.inboxfeaturemanagement.event.listener.ReleaseItemGenerator',
        eventType: 'DataOperationEvent',
        privileges: [],
        parentRelations: [
            {
                relationId: '1b11ss-0728-43c7-a228-a228a228a211',
                relationType: 'CompositeEntity_Event',
                parentItemId: '111211-0728-43c7-a228-18d0eba211111',
                childItemId: '111111-0728-43c7-a228-18d0eba11111',
                createdBy: '1111',
                isDeleted: 0,
                creationDate: null,
                updatedBy: '1111',
                updationDate: null,
                deletionDate: null,
            },
        ],
        childRelations: [],
    },
];
//# sourceMappingURL=eventlistener.js.map