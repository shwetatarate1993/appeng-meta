"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const eventlistener_parser_1 = require("../eventlistener.parser");
const mockdata_1 = require("./mockdata");
test('parse EventListener object to config', () => {
    const eventListener = models_1.EventListener.deserialize(mockdata_1.eventListenerMockData[0]);
    expect(eventListener.configObjectType).toEqual('EventListener');
    const result = eventlistener_parser_1.parseEventListenerToConfig(eventListener);
    expect(result.configItem.configObjectType).toEqual('EventListener');
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
});
test('parse config to EventListener object', () => {
    const result = eventlistener_parser_1.parseConfigToEventListener(mockdata_1.eventListenerConfigItemMockData[0], mockdata_1.eventListenerPropertyMockData, mockdata_1.eventListenerRelationMockData, mockdata_1.eventListenerPrivilegeMockData);
    expect(result).toHaveProperty('eventClass', 'com.appengine.inboxfeaturemanagement.event.listener.ReleaseItemGenerator');
    expect(result).toHaveProperty('eventType', 'DataOperationEvent');
    expect(result).toHaveProperty('isBackgroundListener', true);
    expect(result.privileges.length === 0).toBe(true);
    expect(result.parentRelations.length === 1).toBe(true);
    expect(result.childRelations.length === 0).toBe(true);
    expect(result.parentRelations[0].relationType === 'CompositeEntity_Event').toBe(true);
});
//# sourceMappingURL=eventlistener.parser.test.js.map