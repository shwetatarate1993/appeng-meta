export declare const Query: {
    EventListener: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").EventListener>;
};
export declare const Mutation: {
    createEventListener: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map