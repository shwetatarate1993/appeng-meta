"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var standardvalidation_1 = require("./standardvalidation");
exports.standardValidationMockData = standardvalidation_1.default;
var configitem_standardvalidation_1 = require("./configitem.standardvalidation");
exports.standardValidationConfigitemMockData = configitem_standardvalidation_1.default;
var configitem_property_standardvalidation_1 = require("./configitem.property.standardvalidation");
exports.standardValidationPropertyMockData = configitem_property_standardvalidation_1.default;
var configitem_relation_standardvalidation_1 = require("./configitem.relation.standardvalidation");
exports.standardValidationConfigRelationsMockData = configitem_relation_standardvalidation_1.default;
var configitem_privileges_standardvalidation_1 = require("./configitem.privileges.standardvalidation");
exports.standardValidationConfigPrivilegesMockData = configitem_privileges_standardvalidation_1.default;
//# sourceMappingURL=index.js.map