"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: '1334f4d5-24c9-4939-86f1-00d5f319d60c',
        name: 'Year Validation',
        configObjectType: 'StandardValidation',
        createdBy: '1121',
        isDeleted: 0,
        itemDescription: null,
        creationDate: null,
        projectId: 1,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
        validationType: 'StandardValidation',
        defaultErrorMessage: 'test',
        regex: '5',
        isConditionAvailable: false,
        privileges: [],
        parentRelations: [
            {
                relationId: 'b7331994-76f8-11e9-8548-0ee72c6ddce6',
                relationType: 'EntityColumn_ValidationObject',
                parentItemId: '957bbc75-e812-4586-b18a-112016850001',
                childItemId: '1334f4d5-24c9-4939-86f1-00d5f319d60c',
                createdBy: '1121',
                isDeleted: 0,
                creationDate: null,
                updatedBy: '1121',
                updationDate: null,
                deletionDate: null,
            },
        ],
    },
];
//# sourceMappingURL=standardvalidation.js.map