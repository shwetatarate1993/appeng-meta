export { default as standardValidationMockData } from './standardvalidation';
export { default as standardValidationConfigitemMockData } from './configitem.standardvalidation';
export { default as standardValidationPropertyMockData } from './configitem.property.standardvalidation';
export { default as standardValidationConfigRelationsMockData } from './configitem.relation.standardvalidation';
export { default as standardValidationConfigPrivilegesMockData } from './configitem.privileges.standardvalidation';
//# sourceMappingURL=index.d.ts.map