"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const standardvalidation_parser_1 = require("../standardvalidation.parser");
const mockdata_1 = require("./mockdata");
test('parser standard validation object to config', () => {
    const standardValidation = models_1.StandardValidation.deserialize(mockdata_1.standardValidationMockData[0]);
    const result = standardvalidation_parser_1.parseStandardValidationToConfig(standardValidation);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItemProperty.length === 7).toBe(true);
});
test('parser config to standard validation object', () => {
    const result = standardvalidation_parser_1.parseConfigToStandardValidation(mockdata_1.standardValidationConfigitemMockData[0], mockdata_1.standardValidationPropertyMockData, mockdata_1.standardValidationConfigRelationsMockData, mockdata_1.standardValidationConfigPrivilegesMockData);
    expect(result).toHaveProperty('validationType');
    expect(result).toHaveProperty('validationType', 'StandardValidation');
    expect(result.privileges.length === 0).toBe(true);
});
//# sourceMappingURL=standardvalidation.parser.test.js.map