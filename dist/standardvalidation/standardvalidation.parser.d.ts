import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { StandardValidation } from '../models';
export declare const parseStandardValidationToConfig: (standardValidation: StandardValidation) => ConfigMetadata;
export declare const parseConfigToStandardValidation: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => StandardValidation;
//# sourceMappingURL=standardvalidation.parser.d.ts.map