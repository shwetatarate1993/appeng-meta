"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseStandardValidationToConfig = (standardValidation) => {
    const createdAt = standardValidation.creationDate;
    const createdBy = standardValidation.createdBy;
    const updatedBy = standardValidation.updatedBy;
    const updatedAt = standardValidation.updationDate;
    const property = [];
    let itemId;
    if (standardValidation.configObjectId !== undefined && standardValidation.configObjectId !== ''
        && standardValidation.configObjectId !== null) {
        itemId = standardValidation.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (standardValidation.mode !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'MODE', standardValidation.mode, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (standardValidation.defaultErrorMessage !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'DEFAULT_ERROR_MSG', standardValidation.defaultErrorMessage, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (standardValidation.validationType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'VALIDATION_TYPE', standardValidation.validationType, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (standardValidation.regex !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'REGEX', standardValidation.regex, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'IS_CONDITION_AVAILABLE', standardValidation.isConditionAvailable !== undefined ?
        (standardValidation.isConditionAvailable ? '1' : '0') : '0', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    if (standardValidation.conditionExpression !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'CONDITION_EXPRESSION', standardValidation.conditionExpression, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (standardValidation.conditionFields !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'CONDITION_FIELDS', standardValidation.conditionFields, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (standardValidation.description !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'DESCRIPTION', standardValidation.description, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, standardValidation.name, configitemtype_enum_1.ConfigItemTypes.STANDARDVALIDATION, standardValidation.projectId, createdBy, standardValidation.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const standardValidationPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(standardValidation.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(standardValidation.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(standardValidation.privileges, standardValidationPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, standardValidationPrivileges);
};
exports.parseConfigToStandardValidation = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations != null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.StandardValidation(configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, details.get('MODE'), details.get('DEFAULT_ERROR_MSG'), details.get('VALIDATION_TYPE'), details.get('REGEX'), details.get('IS_CONDITION_AVAILABLE'), details.get('CONDITION_EXPRESSION'), details.get('CONDITION_FIELDS'), details.get('DESCRIPTION'), privileges, childRelations, parentRelations);
};
//# sourceMappingURL=standardvalidation.parser.js.map