export declare const Query: {
    StandardValidation: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").StandardValidation>;
};
export declare const Mutation: {
    createStandardValidation: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map