"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = __importDefault(require("../base"));
const common_schema_1 = __importDefault(require("../common.schema"));
const StandardValidation = `

input StandardValidationInput {

    name: String
    configObjectId: ID
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    mode: String
    defaultErrorMessage: String
    validationType: String
    regex: String
    isConditionAvailable: Boolean
    conditionExpression: String
    conditionFields: String
    description: String
    privileges : [ConfigItemPrivilegeInput]
    parentRelations : [ConfigItemRelationInput]
    childRelations : [ConfigItemRelationInput]
}

extend type Query {
    StandardValidation(id: ID!): StandardValidation
   }

extend type Mutation {
    createStandardValidation (input: StandardValidationInput): StandardValidation
}

type StandardValidation {

    name: String
    configObjectId: ID
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    mode: String
    defaultErrorMessage: String
    validationType: String
    regex: String
    isConditionAvailable: Boolean
    conditionExpression: String
    conditionFields: String
    description: String
    privileges : [ConfigItemPrivilege]
    parentRelations : [ConfigItemRelation]
    childRelations : [ConfigItemRelation]}
`;
exports.default = () => [StandardValidation, base_1.default, common_schema_1.default];
//# sourceMappingURL=schema.js.map