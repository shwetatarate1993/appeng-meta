"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const standardvalidation_parser_1 = require("./standardvalidation.parser");
exports.Query = {
    StandardValidation: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, standardvalidation_parser_1.parseConfigToStandardValidation),
};
exports.Mutation = {
    createStandardValidation: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, standardvalidation_parser_1.parseStandardValidationToConfig, standardvalidation_parser_1.parseConfigToStandardValidation);
    },
};
//# sourceMappingURL=resolvers.js.map