import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { FormField } from '../models';
export declare const parseFormFieldToConfig: (formField: FormField) => ConfigMetadata;
export declare const parseConfigToFormField: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => FormField;
//# sourceMappingURL=formfield.parser.d.ts.map