"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = __importDefault(require("../base"));
const common_schema_1 = __importDefault(require("../common.schema"));
const FormField = `
input FormFieldInput {
    label: String
    order: Int
    type: String
    displayType: String
    isMandatory: Boolean
    isButtonTextBox: Boolean
    expressionAvailable : Boolean
    refreshFormOnChange : Boolean
    accessibilityRegex: String
    editabilityRegex : String
    expressionFieldString : String
    configObjectId: ID
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilegeInput]
    parentRelations : [ConfigItemRelationInput]
    childRelations : [ConfigItemRelationInput]
    multivalueList : String
    selectItemsReferenceID : String
    placeHolder :String
    dataSourceName :String
    defaultValue : String
    isMultiLingual : Boolean
    formatDate : String
    isApplyDateFormat : Boolean
    resetOnRefresh : String
    rowspan : String
    colspan : String
    hasToolTipIcon : Boolean
    toolTipText : String
    isSearchBoxRequired : Boolean
    isHyperlink : Boolean
    gotoLink : String
    uploadLabel : String
    outDbcodeUploadFileTempId : String
    outDbcodeUploadFileName : String
    outDbcodeUploadFileType : String
    isRefreshFormWithUploadData : Boolean
    valueType : String
    isReadOnly : Boolean
    starCount : Int
    starColor : String
    mask : String
    maskChar :String
    isSearchable : Boolean
    isDisabled : Boolean
    isMulti : Boolean
    showYearDropdown : Boolean
    showMonthDropdown : Boolean
    dateFormat : String
    dropdownMode : String
    timeFormat : String
    showTimeSelect : Boolean
    showTimeSelectOnly : Boolean
    timeIntervals : Int
    timeCaption : String
}
extend type Query {
    FormField(id: ID!): FormField
}
extend type Mutation {
    createFormField (input: FormFieldInput): FormField
}
type FormField {
    label: String
    order: Int
    type: String
    displayType: String
    isMandatory: Boolean
    isButtonTextBox: Boolean
    expressionAvailable : Boolean
    refreshFormOnChange : Boolean
    accessibilityRegex: String
    editabilityRegex : String
    expressionFieldString : String
    configObjectId: ID
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilege]
    parentRelations : [ConfigItemRelation]
    childRelations : [ConfigItemRelation]
    logicalColumn: LogicalColumn
    columnDataPreprocessors: [ColumnDataPreprocessor]
    multivalueList : String
    selectItemsReferenceID : String
    placeHolder :String
    dataSourceName :String
    defaultValue : String
    isMultiLingual : Boolean
    formatDate : String
    isApplyDateFormat : Boolean
    resetOnRefresh : String
    rowspan : String
    colspan : String
    hasToolTipIcon : Boolean
    toolTipText : String
    isSearchBoxRequired : Boolean
    isHyperlink : Boolean
    gotoLink : String
    uploadLabel : String
    outDbcodeUploadFileTempId : String
    outDbcodeUploadFileName : String
    outDbcodeUploadFileType : String
    isRefreshFormWithUploadData : Boolean
    valueType : String
    isReadOnly : Boolean
    starCount : Int
    starColor : String
    mask : String
    maskChar :String
    isSearchable : Boolean
    isDisabled : Boolean
    isMulti : Boolean
    showYearDropdown : Boolean
    showMonthDropdown : Boolean
    dateFormat : String
    dropdownMode : String
    timeFormat : String
    showTimeSelect : Boolean
    showTimeSelectOnly : Boolean
    timeIntervals : Int
    timeCaption : String
}
`;
exports.default = () => [FormField, base_1.default, common_schema_1.default];
//# sourceMappingURL=schema.js.map