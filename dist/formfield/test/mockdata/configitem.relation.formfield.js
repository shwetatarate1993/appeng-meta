"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        relationId: 'c318aac4-7959-4ee2-82dd-3f7a00c21011',
        relationType: 'FormSection_FormField',
        parentItemId: 'a0d0370b-6e1d-430b-8852-7a254844189d',
        childItemId: 'fba9a3df-1c3a-4997-a688-292f3ecaf66e',
        createdBy: '1135',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1135',
        updationDate: null,
        deletionDate: null,
    },
    {
        relationId: '0bce53ce-d173-46c1-9251-34fed42b239f',
        relationType: 'FormField_EntityColumn',
        parentItemId: 'fba9a3df-1c3a-4997-a688-292f3ecaf66e',
        childItemId: '6ad8e5e5-4f29-474f-b336-535449f522a5',
        createdBy: '1135',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1135',
        updationDate: null,
        deletionDate: null,
    },
];
//# sourceMappingURL=configitem.relation.formfield.js.map