"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        privilegeId: '295f4890-94f0-4fe6-b591-390f7ddb751d',
        privilegeType: 'EDIT',
        itemId: 'fba9a3df-1c3a-4997-a688-292f3ecaf66e',
        roleId: 20,
        createdBy: 'SYSTEM',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
    },
    {
        privilegeId: '5cbbb619-dbcc-4f2e-9371-2ce8c9c111c3',
        privilegeType: 'EDIT',
        itemId: 'fba9a3df-1c3a-4997-a688-292f3ecaf66e',
        roleId: 18,
        createdBy: 'SYSTEM',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
    },
    {
        privilegeId: 'a2ab4b15-7304-4142-b10d-95e04ba0eb56',
        privilegeType: 'EDIT',
        itemId: 'fba9a3df-1c3a-4997-a688-292f3ecaf66e',
        roleId: 17,
        createdBy: 'SYSTEM',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
    },
    {
        privilegeId: 'bd2e2a1b-515c-4f4d-9a76-105923c6a063',
        privilegeType: 'EDIT',
        itemId: 'fba9a3df-1c3a-4997-a688-292f3ecaf66e',
        roleId: 16,
        createdBy: 'SYSTEM',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
    },
    {
        privilegeId: 'd5955e50-5f2b-4fd6-ba21-7fb3ed871a49',
        privilegeType: 'EDIT',
        itemId: 'fba9a3df-1c3a-4997-a688-292f3ecaf66e',
        roleId: 19,
        createdBy: 'SYSTEM',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
    },
];
//# sourceMappingURL=configitem.privilege.formfield.js.map