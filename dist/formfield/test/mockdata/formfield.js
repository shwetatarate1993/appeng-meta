"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: 'fba9a3df-1c3a-4997-a688-292f3ecaf66e',
        configObjectType: 'FormField',
        name: 'Name',
        order: '45',
        type: 'TextBox',
        label: 'en-!lAnGuAge!-Name-!sEpArAToR!-gu-!lAnGuAge!-નામ',
        privileges: [
            {
                privilegeId: '295f4890-94f0-4fe6-b591-390f7ddb751d',
                privilegeType: 'EDIT',
                itemId: 'fba9a3df-1c3a-4997-a688-292f3ecaf66e',
                roleId: 20,
                createdBy: 'SYSTEM',
                isDeleted: 0,
                creationDate: null,
                updatedBy: '1111',
                updationDate: null,
                deletionDate: null,
            },
            {
                privilegeId: '5cbbb619-dbcc-4f2e-9371-2ce8c9c111c3',
                privilegeType: 'EDIT',
                itemId: 'fba9a3df-1c3a-4997-a688-292f3ecaf66e',
                roleId: 18,
                createdBy: 'SYSTEM',
                isDeleted: 0,
                creationDate: null,
                updatedBy: '1111',
                updationDate: null,
                deletionDate: null,
            },
            {
                privilegeId: 'a2ab4b15-7304-4142-b10d-95e04ba0eb56',
                privilegeType: 'EDIT',
                itemId: 'fba9a3df-1c3a-4997-a688-292f3ecaf66e',
                roleId: 17,
                createdBy: 'SYSTEM',
                isDeleted: 0,
                creationDate: null,
                updatedBy: '1111',
                updationDate: null,
                deletionDate: null,
            },
            {
                privilegeId: 'bd2e2a1b-515c-4f4d-9a76-105923c6a063',
                privilegeType: 'EDIT',
                itemId: 'fba9a3df-1c3a-4997-a688-292f3ecaf66e',
                roleId: 16,
                createdBy: 'SYSTEM',
                isDeleted: 0,
                creationDate: null,
                updatedBy: '1111',
                updationDate: null,
                deletionDate: null,
            },
            {
                privilegeId: 'd5955e50-5f2b-4fd6-ba21-7fb3ed871a49',
                privilegeType: 'EDIT',
                itemId: 'fba9a3df-1c3a-4997-a688-292f3ecaf66e',
                roleId: 19,
                createdBy: 'SYSTEM',
                isDeleted: 0,
                creationDate: null,
                updatedBy: '1111',
                updationDate: null,
                deletionDate: null,
            },
        ],
        parentRelations: [
            {
                relationId: 'c318aac4-7959-4ee2-82dd-3f7a00c21011',
                relationType: 'FormSection_FormField',
                parentItemId: 'a0d0370b-6e1d-430b-8852-7a254844189d',
                childItemId: 'fba9a3df-1c3a-4997-a688-292f3ecaf66e',
                createdBy: '1135',
                isDeleted: 0,
                creationDate: null,
                updatedBy: '1135',
                updationDate: null,
                deletionDate: null,
            },
        ],
        childRelations: [
            {
                relationId: '0bce53ce-d173-46c1-9251-34fed42b239f',
                relationType: 'FormField_EntityColumn',
                parentItemId: 'fba9a3df-1c3a-4997-a688-292f3ecaf66e',
                childItemId: '6ad8e5e5-4f29-474f-b336-535449f522a5',
                createdBy: '1135',
                isDeleted: 0,
                creationDate: null,
                updatedBy: '1135',
                updationDate: null,
                deletionDate: null,
            },
        ],
    },
];
//# sourceMappingURL=formfield.js.map