"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var formfield_1 = require("./formfield");
exports.formFieldMockData = formfield_1.default;
var configitem_formfield_1 = require("./configitem.formfield");
exports.configitemMockDataFormField = configitem_formfield_1.default;
var configitem_property_formfield_1 = require("./configitem.property.formfield");
exports.formFieldPropertyMockData = configitem_property_formfield_1.default;
var configitem_relation_formfield_1 = require("./configitem.relation.formfield");
exports.formFieldRelationMockData = configitem_relation_formfield_1.default;
var configitem_privilege_formfield_1 = require("./configitem.privilege.formfield");
exports.formFieldPrivilegeMockData = configitem_privilege_formfield_1.default;
//# sourceMappingURL=index.js.map