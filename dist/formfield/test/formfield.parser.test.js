"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const formfield_parser_1 = require("../formfield.parser");
const mockdata_1 = require("./mockdata");
test('parse formfield object to config', () => {
    const formField = models_1.FormField.deserialize(mockdata_1.formFieldMockData[0]);
    expect(formField.parentRelations.length === 1).toBe(true);
    expect(formField.childRelations.length === 1).toBe(true);
    expect(formField.label).toEqual('en-!lAnGuAge!-Name-!sEpArAToR!-gu-!lAnGuAge!-નામ');
    expect(formField.configObjectType).toEqual('FormField');
    const result = formfield_parser_1.parseFormFieldToConfig(formField);
    expect(result.configItem.configObjectType).toEqual('FormField');
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
});
test('parse config to formfield object', () => {
    const result = formfield_parser_1.parseConfigToFormField(mockdata_1.configitemMockDataFormField[0], mockdata_1.formFieldPropertyMockData, mockdata_1.formFieldRelationMockData, mockdata_1.formFieldPrivilegeMockData);
    expect(result).toHaveProperty('label', 'en-!lAnGuAge!-Name-!sEpArAToR!-gu-!lAnGuAge!-નામ');
    expect(result).toHaveProperty('order', '45');
    expect(result).toHaveProperty('type', 'TextBox');
    expect(result).toHaveProperty('isMandatory', false);
    expect(result).toHaveProperty('expressionAvailable', false);
    expect(result).toHaveProperty('refreshFormOnChange', false);
    expect(result).toHaveProperty('accessibilityRegex');
    expect(result).toHaveProperty('editabilityRegex');
    expect(result).toHaveProperty('expressionFieldString');
    expect(result.privileges.length === 5).toBe(true);
    expect(result.parentRelations.length === 1).toBe(true);
    expect(result.childRelations.length === 1).toBe(true);
    expect(result.parentRelations[0].relationType === 'FormSection_FormField').toBe(true);
    expect(result.childRelations[0].relationType === 'FormField_EntityColumn').toBe(true);
});
//# sourceMappingURL=formfield.parser.test.js.map