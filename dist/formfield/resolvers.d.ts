export declare const Query: {
    FormField: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").FormField>;
};
export declare const FormField: {
    logicalColumn: (formField: any, _: any, context: any) => Promise<import("../models").LogicalColumn>;
    columnDataPreprocessors: (formField: any, _: any, context: any) => Promise<import("../models").ColumnDataPreprocessor[]>;
};
export declare const Mutation: {
    createFormField: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map