"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseFormFieldToConfig = (formField) => {
    const createdAt = formField.creationDate;
    const createdBy = formField.createdBy;
    const updatedBy = formField.updatedBy;
    const updatedAt = formField.updationDate;
    const property = [];
    let itemId;
    if (formField.configObjectId !== undefined && formField.configObjectId !== ''
        && formField.configObjectId !== null) {
        itemId = formField.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (formField.label !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('LABEL', formField.label, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.order !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ORDER', formField.order, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.type !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('TYPE', formField.type, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.displayType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('DISPLAY_TYPE', formField.displayType, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.isMandatory !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_MANDATORY', formField.isMandatory !== undefined ? (formField.isMandatory ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.isButtonTextBox !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_BUTTON_TEXTBOX', formField.isButtonTextBox !== undefined ? (formField.isButtonTextBox ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.expressionAvailable !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_EXPRESSION_AVAILABLE', formField.expressionAvailable !== undefined ? (formField.expressionAvailable ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.refreshFormOnChange !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_REFRESH_FORM_ON_CHANGE', formField.refreshFormOnChange !== undefined ? (formField.refreshFormOnChange ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.accessibilityRegex !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ACCESSBILITY_REGEX', formField.accessibilityRegex, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.editabilityRegex !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('EDITABILITY_REGEX', formField.editabilityRegex, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.expressionFieldString !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('EXPRESSION_FIELD_STRING', formField.expressionFieldString, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.multivalueList !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('MULTI_VALUE_LIST', formField.multivalueList, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.selectItemsReferenceID !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('SELECT_ITEMS_REFERENCE_ID', formField.selectItemsReferenceID, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.placeHolder !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('PLACE_HOLDER', formField.placeHolder, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.dataSourceName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('DATASOURCE_NAME', formField.dataSourceName, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.defaultValue !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('DEFAULT_VALUE', formField.defaultValue, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.isMultiLingual !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_MULTI_LINGUAL', formField.isMultiLingual !== undefined ? (formField.isMultiLingual ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.dataSourceName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('FORMAT_DATE', formField.formatDate, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.isApplyDateFormat !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_APPLY_DATA_FORMAT', formField.isApplyDateFormat !== undefined ? (formField.isApplyDateFormat ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.resetOnRefresh !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('RESET_ON_REFRESH', formField.resetOnRefresh, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.rowspan !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ROWSPAN', formField.toolTipText, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.colspan !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('COLSPAN', formField.toolTipText, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.hasToolTipIcon !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('HAS_TOOL_TIP_ICON', formField.hasToolTipIcon !== undefined ? (formField.hasToolTipIcon ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.toolTipText !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('TOOLTIP_TEXT', formField.toolTipText, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.isSearchBoxRequired !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_SEARCH_BOX_REQUIRED', formField.isSearchBoxRequired !== undefined ? (formField.isSearchBoxRequired ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.isHyperlink !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_HYPERLINK', formField.isHyperlink !== undefined ? (formField.isHyperlink ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.gotoLink !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('GO_TO_LINK', formField.gotoLink, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.uploadLabel !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('UPLOAD_LABEL', formField.uploadLabel, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.outDbcodeUploadFileTempId !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('OUT_DBCODE_UPLOAD_FILE_TEMP_ID', formField.outDbcodeUploadFileTempId, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.outDbcodeUploadFileName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('OUT_DBCODE_UPLOAD_FILE_NAME', formField.outDbcodeUploadFileName, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.outDbcodeUploadFileType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('OUT_DBCODE_UPLOAD_FILE_TYPE', formField.outDbcodeUploadFileType, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.isRefreshFormWithUploadData !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_REFRESH_FORM_WITH_UPLOAD_DATA', formField.isRefreshFormWithUploadData !== undefined ?
            (formField.isRefreshFormWithUploadData ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.valueType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('VALUE_TYPE', formField.valueType, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.isReadOnly !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_READONLY', formField.isReadOnly !== undefined ? (formField.isReadOnly ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.starColor !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('STAR_COLOR', formField.starColor, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.starCount !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('STAR_COUNT', formField.starCount, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.mask !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('MASK', formField.mask, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.maskChar !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('MASK_CHAR', formField.maskChar, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.isSearchable !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_SEARCHABLE', formField.isSearchable !== undefined ? (formField.isSearchable ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.isDisabled !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_DISABLED', formField.isDisabled !== undefined ? (formField.isDisabled ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.isMulti !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_MULTI', formField.isMulti !== undefined ? (formField.isMulti ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.showYearDropdown !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('SHOW_YEAR_DROPDOWN', formField.showYearDropdown !== undefined ? (formField.showYearDropdown ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.showMonthDropdown !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('SHOW_MONTH_DROPDOWN', formField.showMonthDropdown !== undefined ? (formField.showMonthDropdown ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.dateFormat !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('DATE_FORMAT', formField.dateFormat, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.dropdownMode !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('DROPDOWN_MODE', formField.dropdownMode, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.timeFormat !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('TIME_FORMAT', formField.timeFormat, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.showTimeSelect !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('SHOW_TIME_SELECT', formField.showTimeSelect !== undefined ? (formField.showTimeSelect ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.showTimeSelectOnly !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('SHOW_TIME_SELECT_ONLY', formField.showTimeSelectOnly !== undefined ? (formField.showTimeSelectOnly ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.timeIntervals !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('TIME_INTERVALS', formField.timeIntervals, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formField.timeCaption !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('TIME_CAPTION', formField.timeCaption, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, formField.name, configitemtype_enum_1.ConfigItemTypes.FORMFIELD, formField.projectId, createdBy, formField.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const formFieldPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(formField.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(formField.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(formField.privileges, formFieldPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, formFieldPrivileges);
};
exports.parseConfigToFormField = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.FormField(details.get('LABEL'), details.get('ORDER'), details.get('TYPE'), details.get('DISPLAY_TYPE'), details.get('IS_MANDATORY'), details.get('IS_BUTTON_TEXTBOX'), details.get('ACCESSBILITY_REGEX'), details.get('EDITABILITY_REGEX'), details.get('IS_EXPRESSION_AVAILABLE'), details.get('IS_REFRESH_FORM_ON_CHANGE'), details.get('EXPRESSION_FIELD_STRING'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, details.get('IS_MULTI_LINGUAL'), details.get('FORMAT_DATE'), details.get('IS_APPLY_DATA_FORMAT'), details.get('RESET_ON_REFRESH'), details.get('ROWSPAN'), details.get('COLSPAN'), details.get('HAS_TOOL_TIP_ICON'), details.get('TOOLTIP_TEXT'), details.get('IS_SEARCH_BOX_REQUIRED'), details.get('IS_HYPERLINK'), details.get('GO_TO_LINK'), details.get('UPLOAD_LABEL'), details.get('OUT_DBCODE_UPLOAD_FILE_TEMP_ID'), details.get('OUT_DBCODE_UPLOAD_FILE_NAME'), details.get('OUT_DBCODE_UPLOAD_FILE_TYPE'), details.get('IS_REFRESH_FORM_WITH_UPLOAD_DATA'), details.get('VALUE_TYPE'), details.get('IS_READONLY'), configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations, details.get('MULTI_VALUE_LIST'), details.get('SELECT_ITEMS_REFERENCE_ID'), details.get('PLACE_HOLDER'), details.get('DATASOURCE_NAME'), details.get('DEFAULT_VALUE'), details.get('STAR_COLOR'), details.get('STAR_COUNT'), details.get('MASK'), details.get('MASK_CHAR'), details.get('IS_SEARCHABLE'), details.get('IS_DISABLED'), details.get('IS_MULTI'), details.get('SHOW_YEAR_DROPDOWN'), details.get('SHOW_MONTH_DROPDOWN'), details.get('DATE_FORMAT'), details.get('DROPDOWN_MODE'), details.get('TIME_FORMAT'), details.get('SHOW_TIME_SELECT'), details.get('SHOW_TIME_SELECT_ONLY'), details.get('TIME_INTERVALS'), details.get('TIME_CAPTION'));
};
//# sourceMappingURL=formfield.parser.js.map