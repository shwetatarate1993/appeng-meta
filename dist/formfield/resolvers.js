"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const columndatapreprocessor_parser_1 = require("../columndatapreprocessor/columndatapreprocessor.parser");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const logicalcolumn_parser_1 = require("../logicalcolumn/logicalcolumn.parser");
const formfield_parser_1 = require("./formfield.parser");
exports.Query = {
    FormField: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, formfield_parser_1.parseConfigToFormField),
};
exports.FormField = {
    logicalColumn: async (formField, _, context) => {
        const columnRel = formField.childRelations.find((rel) => rel.relationType === relationtype_enum_1.RelationType.FORMFIELD_ENTITYCOLUMN);
        if (columnRel) {
            return await common_meta_1.configService.fetchConfigById(columnRel.childItemId, logicalcolumn_parser_1.parseConfigToLogicalColumn);
        }
        else {
            return null;
        }
    },
    columnDataPreprocessors: (formField, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(formField.configObjectId, relationtype_enum_1.RelationType.FORMFIELD_COLUMNDATAPREPROCESSOR, columndatapreprocessor_parser_1.parseConfigToColumnDataPreprocessor),
};
exports.Mutation = {
    createFormField: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, formfield_parser_1.parseFormFieldToConfig, formfield_parser_1.parseConfigToFormField);
    },
};
//# sourceMappingURL=resolvers.js.map