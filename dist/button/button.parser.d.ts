import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { Button } from '../models';
export declare const parseButtonToConfig: (button: Button) => ConfigMetadata;
export declare const parseConfigToButton: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => Button;
//# sourceMappingURL=button.parser.d.ts.map