export declare const Query: {
    Button: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").Button>;
};
export declare const Mutation: {
    createButton: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map