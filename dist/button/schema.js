"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = __importDefault(require("../base"));
const common_schema_1 = __importDefault(require("../common.schema"));
const Button = `

input ButtonInput {

    label : String
    order : Int
    buttonClass : String
    buttonAlignment : String
    accessibilityRegex : String
    editabilityRegex : String
    expressionAvailable : Boolean
    expressionFieldString : String
    configObjectId: ID
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilegeInput]
    parentRelations : [ConfigItemRelationInput]
    childRelations : [ConfigItemRelationInput]
}

extend type Query {
    Button(id: ID!): Button
   }

extend type Mutation {
    createButton (input: ButtonInput): Button
}

type Button {
    label : String
    order : Int
    buttonClass : String
    buttonAlignment : String
    accessibilityRegex : String
    editabilityRegex : String
    expressionAvailable : Boolean
    expressionFieldString : String
    configObjectId: ID
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilege]
    parentRelations : [ConfigItemRelation]
    childRelations : [ConfigItemRelation]
}
`;
exports.default = () => [Button, base_1.default, common_schema_1.default];
//# sourceMappingURL=schema.js.map