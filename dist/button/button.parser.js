"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseButtonToConfig = (button) => {
    const createdAt = button.creationDate;
    const createdBy = button.createdBy;
    const updatedBy = button.updatedBy;
    const updatedAt = button.updationDate;
    const property = [];
    let itemId;
    if (button.configObjectId !== undefined && button.configObjectId !== ''
        && button.configObjectId !== null) {
        itemId = button.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (button.label !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('LABEL', button.label, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (button.order !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ORDER', button.order, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (button.buttonClass !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('BUTTON_CLASS', button.buttonClass, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (button.buttonAlignment !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('BUTTON_ALIGNMENT', button.buttonAlignment, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (button.accessibilityRegex !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ACCESSBILITY_REGEX', button.accessibilityRegex, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (button.editabilityRegex !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('EDITABILITY_REGEX', button.editabilityRegex, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (button.expressionAvailable !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_EXPRESSION_AVAILABLE', button.expressionAvailable !== undefined ? (button.expressionAvailable ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (button.expressionFieldString !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('EXPRESSION_FIELD_STRING', button.expressionFieldString, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, button.name, configitemtype_enum_1.ConfigItemTypes.BUTTON, button.projectId, createdBy, button.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const buttonPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(button.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(button.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(button.privileges, buttonPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, buttonPrivileges);
};
exports.parseConfigToButton = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.Button(details.get('LABEL'), details.get('ORDER'), details.get('BUTTON_CLASS'), details.get('BUTTON_ALIGNMENT'), details.get('ACCESSBILITY_REGEX'), details.get('EDITABILITY_REGEX'), details.get('IS_EXPRESSION_AVAILABLE'), details.get('EXPRESSION_FIELD_STRING'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=button.parser.js.map