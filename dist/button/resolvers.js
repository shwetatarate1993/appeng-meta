"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const button_parser_1 = require("./button.parser");
exports.Query = {
    Button: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, button_parser_1.parseConfigToButton),
};
exports.Mutation = {
    createButton: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, button_parser_1.parseButtonToConfig, button_parser_1.parseConfigToButton);
    },
};
//# sourceMappingURL=resolvers.js.map