"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const button_parser_1 = require("../button.parser");
const mockdata_1 = require("./mockdata");
test('parse button object to config', () => {
    const button = models_1.Button.deserialize(mockdata_1.buttonMockData[0]);
    expect(button.configObjectType).toEqual('Button');
    const result = button_parser_1.parseButtonToConfig(button);
    expect(result.configItem.configObjectType).toEqual('Button');
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
});
test('parse config to button object', () => {
    const result = button_parser_1.parseConfigToButton(mockdata_1.configitemMockDataButton[0], mockdata_1.buttonPropertyMockData, mockdata_1.buttonRelationMockData, mockdata_1.buttonPrivilegeMockData);
    expect(result).toHaveProperty('label', 'Add');
    expect(result).toHaveProperty('order', '5');
    expect(result).toHaveProperty('buttonClass', 'single-entity-insert');
    expect(result).toHaveProperty('buttonAlignment', 'Top');
    expect(result).toHaveProperty('accessibilityRegex');
    expect(result).toHaveProperty('editabilityRegex');
    expect(result).toHaveProperty('expressionAvailable', false);
    expect(result).toHaveProperty('expressionFieldString');
    expect(result.privileges.length === 3).toBe(true);
    expect(result.parentRelations.length === 1).toBe(true);
    expect(result.parentRelations[0].relationType === 'ButtonPanel_Button').toBe(true);
});
//# sourceMappingURL=button.parser.test.js.map