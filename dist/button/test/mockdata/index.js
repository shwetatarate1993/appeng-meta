"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var button_1 = require("./button");
exports.buttonMockData = button_1.default;
var configitem_button_1 = require("./configitem.button");
exports.configitemMockDataButton = configitem_button_1.default;
var configitem_property_button_1 = require("./configitem.property.button");
exports.buttonPropertyMockData = configitem_property_button_1.default;
var configitem_relation_button_1 = require("./configitem.relation.button");
exports.buttonRelationMockData = configitem_relation_button_1.default;
var configitem_privilege_button_1 = require("./configitem.privilege.button");
exports.buttonPrivilegeMockData = configitem_privilege_button_1.default;
//# sourceMappingURL=index.js.map