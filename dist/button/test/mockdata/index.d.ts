export { default as buttonMockData } from './button';
export { default as configitemMockDataButton } from './configitem.button';
export { default as buttonPropertyMockData } from './configitem.property.button';
export { default as buttonRelationMockData } from './configitem.relation.button';
export { default as buttonPrivilegeMockData } from './configitem.privilege.button';
//# sourceMappingURL=index.d.ts.map