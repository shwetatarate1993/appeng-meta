declare const _default: {
    configObjectId: string;
    configObjectType: string;
    name: string;
    label: string;
    order: string;
    buttonClass: string;
    buttonAlignment: string;
    accessibilityRegex: string;
    editabilityRegex: string;
    expressionAvailable: boolean;
    expressionFieldString: string;
    privileges: {
        privilegeType: string;
        itemId: string;
        roleId: number;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: string;
        deletionDate: string;
    }[];
    parentRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        childItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: string;
        deletionDate: string;
    }[];
}[];
export default _default;
//# sourceMappingURL=button.d.ts.map