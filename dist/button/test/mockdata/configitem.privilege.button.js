"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        privilegeId: '12d43324-e28e-4c27-ae2d-aaf2321e2197',
        privilegeType: 'VIEW',
        itemId: '1bc9d82d-1234-43c7-a228-5e0eba9071c1',
        roleId: 19,
        createdBy: '1111',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
    },
    {
        privilegeId: '12271fce-0a2a-4c47-8058-27910470bb18',
        privilegeType: 'EDIT',
        itemId: '1bc9d82d-1234-43c7-a228-5e0eba9071c1',
        roleId: 17,
        createdBy: '1111',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
    },
    {
        privilegeId: '12212cb3-333e-4216-98c7-955219411efd',
        privilegeType: 'EDIT',
        itemId: '1bc9d82d-1234-43c7-a228-5e0eba9071c1',
        roleId: 16,
        createdBy: '1111',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
    },
];
//# sourceMappingURL=configitem.privilege.button.js.map