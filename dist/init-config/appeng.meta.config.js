"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
class AppengMetaConfig {
    static configure() {
        common_meta_1.CommonMetaConfig.configure();
    }
    constructor() {
        /** No Op */
    }
}
exports.default = AppengMetaConfig;
//# sourceMappingURL=appeng.meta.config.js.map