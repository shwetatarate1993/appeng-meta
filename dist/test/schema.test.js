"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const init_config_1 = require("../init-config");
const schema_1 = __importDefault(require("../schema"));
const query_constants_1 = require("./query-constants");
const sleep = (time) => new Promise((resolve) => setTimeout(resolve, time));
beforeEach(async () => {
    init_config_1.AppengMetaConfig.configure();
    await sleep(5000);
    console.log('this is after the delay');
});
test('CE query', () => {
    console.log('hello CE');
    const context = {};
    return graphql_1.graphql(schema_1.default, query_constants_1.CompositeEntityQuery, null, context).then((results) => {
        console.log(JSON.stringify(results));
    });
});
// test('CEN query',  () => {
//     console.log('hello CEN')
//      const context = {};
//      return  graphql(schema, CompositeEntityNodeQuery, null, context).then(results => {
//         console.log(JSON.stringify(results))
//     });
// });
// test('MenuGroup query',  () => {
//     console.log('hello MenuGroup')
//      const context = {};
//      return  graphql(schema, MenuGroupQuery, null, context).then(results => {
//         console.log(JSON.stringify(results))
//     });
// });
// test('RootNode query',  () => {
//     console.log('hello MenuGroup')
//      const context = {};
//      return  graphql(schema, ChildNodeInTree, null, context).then(results => {
//         console.log(JSON.stringify(results))
//     });
// });
//# sourceMappingURL=schema.test.js.map