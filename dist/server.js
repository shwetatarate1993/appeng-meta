"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cors_1 = __importDefault(require("cors"));
const express_1 = __importDefault(require("express"));
const express_graphql_1 = __importDefault(require("express-graphql"));
const context_1 = __importDefault(require("./context"));
const init_config_1 = require("./init-config");
const schema_1 = __importDefault(require("./schema"));
init_config_1.AppengMetaConfig.configure();
const app = express_1.default();
app.use(cors_1.default());
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});
app.use('/graphql', express_graphql_1.default((request) => ({
    schema: schema_1.default,
    context: context_1.default(request),
    graphiql: true,
})));
app.listen(8897);
//# sourceMappingURL=server.js.map