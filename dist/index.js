"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// in src/index.js
const graphql_1 = require("graphql");
const init_config_1 = require("./init-config");
const schema_1 = __importDefault(require("./schema"));
var init_config_2 = require("./init-config");
exports.AppengMetaConfig = init_config_2.AppengMetaConfig;
init_config_1.AppengMetaConfig.configure();
const CompositeEntityQuery = '{ CompositeEntity(id:"04f76e04-b658-4f33-96a0-89d772eee2d3") { name  } }';
const graphqlquery = async (querystring) => {
    console.log('hello CE');
    const context = {};
    const result = await graphql_1.graphql(schema_1.default, querystring, null, context);
    console.log(JSON.stringify(result));
    return result;
};
exports.default = graphqlquery;
//# sourceMappingURL=index.js.map