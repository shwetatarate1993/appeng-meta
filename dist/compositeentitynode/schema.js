"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = __importDefault(require("../base"));
const common_schema_1 = __importDefault(require("../common.schema"));
const CompositeEntityNode = `

input CompositeEntityNodeInput {
    configObjectId: ID
    addToParentDisplay: Boolean
    addToParentGrid: Boolean
    addToParentEditForm: Boolean
    addToParentInsertForm: Boolean
    displayNodeName: String
    graphTemplate: String
    accessibilityRegex: String
    order: Int
    expressionAvailable: Boolean
    editabilityRegex : String
    expressionFieldString: String
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilegeInput]
    parentRelations : [ConfigItemRelationInput]
    childRelations : [ConfigItemRelationInput]
}

extend type Query {
    CompositeEntityNode (id: ID!): CompositeEntityNode
    ChildCompositeEntityNodes (id: ID!): [CompositeEntityNode]
    CombinedChildCompositeEntityNodes (id: ID!): [CompositeEntityNode]
    ChildNodesInTree (id: ID!): [CompositeEntityNode]
    HiddenNodes (id: ID!): [CompositeEntityNode]
}

extend type Mutation {
    createCompositeEntityNode (input: CompositeEntityNodeInput): CompositeEntityNode
}

type CompositeEntityNode {
    configObjectId: ID
    addToParentDisplay: Boolean
    addToParentGrid: Boolean
    addToParentEditForm: Boolean
    addToParentInsertForm: Boolean
    displayNodeName: String
    graphTemplate: String
    accessibilityRegex: String
    order: Int
    expressionAvailable: Boolean
    editabilityRegex : String
    expressionFieldString: String
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilege]
    parentRelations : [ConfigItemRelation]
    childRelations : [ConfigItemRelation]
    physicalDataEntities : [PhysicalEntity]
    entity : LogicalEntity
    insertForm : Form
    editForm : Form
    dataGrid : DataGrid
    nodeBusinessRules : [NodeBusinessRule]
    combinedNodes : [CompositeEntityNode]
    children : [CompositeEntityNode]
}
`;
exports.default = () => [CompositeEntityNode, base_1.default, common_schema_1.default];
//# sourceMappingURL=schema.js.map