declare const _default: {
    configObjectId: string;
    configObjectType: string;
    name: string;
    projectId: number;
    addToParentDisplay: boolean;
    addToParentGrid: boolean;
    addToParentEditForm: boolean;
    addToParentInsertForm: boolean;
    displayNodeName: string;
    graphTemplate: string;
    accessibilityRegex: string;
    order: number;
    expressionAvailble: boolean;
    expressionFieldString: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: any;
    creationDate: string;
    updatedBy: any;
    updationDate: any;
    deletionDate: any;
    privileges: any[];
    childRelations: any[];
    parentRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        childItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: string;
        deletionDate: string;
    }[];
}[];
export default _default;
//# sourceMappingURL=compositeentitynode.object.d.ts.map