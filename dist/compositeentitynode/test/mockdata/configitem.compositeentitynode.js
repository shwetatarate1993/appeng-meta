"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: 'f645ccab-8133-11e9-8548-0ee72c6ddce6',
        name: 'Composite Entity Node',
        configObjectType: 'CompositeEntityNode',
        createdBy: '1135',
        isDeleted: 0,
        itemDescription: 'CompositeEntityNode',
        creationDate: null,
        projectId: 0,
        updatedBy: '1135',
        updationDate: null,
        deletionDate: null,
    },
];
//# sourceMappingURL=configitem.compositeentitynode.js.map