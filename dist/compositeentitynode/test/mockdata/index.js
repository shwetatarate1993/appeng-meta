"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var compositeentitynode_object_1 = require("./compositeentitynode.object");
exports.compositeEntityNodeObjectMockData = compositeentitynode_object_1.default;
var configitem_compositeentitynode_1 = require("./configitem.compositeentitynode");
exports.compositeEntityNodeConfigitemMockData = configitem_compositeentitynode_1.default;
var configitem_property_compositeentitynode_1 = require("./configitem.property.compositeentitynode");
exports.compositeEntityNodePropertyMockData = configitem_property_compositeentitynode_1.default;
var configitem_relation_compositeentitynode_1 = require("./configitem.relation.compositeentitynode");
exports.compositeEntityNodeRelationMockData = configitem_relation_compositeentitynode_1.default;
var configitem_privileges_compositeentitynode_1 = require("./configitem.privileges.compositeentitynode");
exports.compositeEntityNodePrivilegesMockData = configitem_privileges_compositeentitynode_1.default;
//# sourceMappingURL=index.js.map