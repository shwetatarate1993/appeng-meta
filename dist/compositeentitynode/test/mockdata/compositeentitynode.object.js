"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: 'f645ccab-8133-11e9-8548-0ee72c6ddce6',
        configObjectType: 'CompositeEntityNode',
        name: 'Composite Entity Node',
        projectId: 1,
        addToParentDisplay: false,
        addToParentGrid: false,
        addToParentEditForm: false,
        addToParentInsertForm: false,
        displayNodeName: 'Child Composite Entity',
        graphTemplate: '',
        accessibilityRegex: '',
        order: 5,
        expressionAvailble: false,
        expressionFieldString: '',
        createdBy: '1135',
        isDeleted: 0,
        itemDescription: null,
        creationDate: '2019-04-05T10:40:06.000Z',
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
        privileges: [],
        childRelations: [],
        parentRelations: [
            {
                relationId: '775364a2-8134-11e9-8548-0ee72c6ddce6',
                relationType: 'CompositeEntity_CompositeEntityNode',
                parentItemId: '996d6c36-8134-11e9-8548-0ee72c6ddce6',
                childItemId: 'f645ccab-8133-11e9-8548-0ee72c6ddce6',
                createdBy: '1135',
                isDeleted: 0,
                creationDate: '2018-02-26T00:00:00.000Z',
                updatedBy: '1135',
                updationDate: '2018-04-27T00:00:00.000Z',
                deletionDate: '2018-04-27T00:00:00.000Z',
            },
        ],
    },
];
//# sourceMappingURL=compositeentitynode.object.js.map