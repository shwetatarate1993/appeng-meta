"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const compositeentitynode_parser_1 = require("../compositeentitynode.parser");
const mockdata_1 = require("./mockdata");
test('parser CompositeEntityNode to config', () => {
    const compositeEntityNode = models_1.CompositeEntityNode.deserialize(mockdata_1.compositeEntityNodeObjectMockData[0]);
    const result = compositeentitynode_parser_1.parseCompositeEntityNodeToConfig(compositeEntityNode);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItem.configObjectType).toEqual('CompositeEntityNode');
    expect(result.configItemProperty[0].propertyName).toEqual('ADDTO_PARENT_DISPLAY');
    expect(result.configItemProperty[1].propertyName).toEqual('ADDTO_PARENT_GRID');
    expect(result.configItemProperty[2].propertyName).toEqual('ADDTO_PARENT_EDITFORM');
    expect(result.configItemProperty[3].propertyName).toEqual('ADDTO_PARENT_INSERTFORM');
    expect(result.configItemProperty[4].propertyName).toEqual('DISPLAY_NODE_NAME');
    expect(result.configItemProperty[5].propertyName).toEqual('GRAPH_TEMPLATE');
    expect(result.configItemProperty[6].propertyName).toEqual('ACCESSBILITY_REGEX');
    expect(result.configItemProperty[7].propertyName).toEqual('ORDER');
    expect(result.configItemProperty[8].propertyName).toEqual('IS_EXPRESSION_AVAILABLE');
    expect(result.configItemProperty[9].propertyName).toEqual('EXPRESSION_FIELD_STRING');
});
test('parser config to CompositeEntityNode', () => {
    const result = compositeentitynode_parser_1.parseConfigToCompositeEntityNode(mockdata_1.compositeEntityNodeConfigitemMockData[0], mockdata_1.compositeEntityNodePropertyMockData, mockdata_1.compositeEntityNodeRelationMockData, mockdata_1.compositeEntityNodePrivilegesMockData);
    expect(result).toHaveProperty('displayNodeName');
    expect(result).toHaveProperty('order');
    expect(result).toHaveProperty('addToParentDisplay');
    expect(result).toHaveProperty('addToParentGrid');
    expect(result).toHaveProperty('addToParentEditForm');
    expect(result).toHaveProperty('addToParentInsertForm');
    expect(result).toHaveProperty('graphTemplate');
    expect(result).toHaveProperty('accessibilityRegex');
    expect(result).toHaveProperty('expressionAvailable');
    expect(result).toHaveProperty('expressionFieldString');
});
//# sourceMappingURL=compositeentitynode.parser.test.js.map