import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { CompositeEntityNode } from '../models';
export declare const parseCompositeEntityNodeToConfig: (compositeEntityNode: CompositeEntityNode) => ConfigMetadata;
export declare const parseConfigToCompositeEntityNode: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => CompositeEntityNode;
//# sourceMappingURL=compositeentitynode.parser.d.ts.map