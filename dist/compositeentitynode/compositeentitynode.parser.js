"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseCompositeEntityNodeToConfig = (compositeEntityNode) => {
    const createdAt = compositeEntityNode.creationDate;
    const createdBy = compositeEntityNode.createdBy;
    const updatedBy = compositeEntityNode.updatedBy;
    const updatedAt = compositeEntityNode.updationDate;
    const property = [];
    let itemId;
    if (compositeEntityNode.configObjectId !== undefined && compositeEntityNode.configObjectId !== ''
        && compositeEntityNode.configObjectId !== null) {
        itemId = compositeEntityNode.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    // setting default value as false
    property.push(new meta_db_1.ConfigItemProperty('ADDTO_PARENT_DISPLAY', compositeEntityNode.addToParentDisplay !== undefined ?
        (compositeEntityNode.addToParentDisplay ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    property.push(new meta_db_1.ConfigItemProperty('ADDTO_PARENT_GRID', compositeEntityNode.addToParentGrid !== undefined ?
        (compositeEntityNode.addToParentGrid ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    property.push(new meta_db_1.ConfigItemProperty('ADDTO_PARENT_EDITFORM', compositeEntityNode.addToParentEditForm !== undefined ?
        (compositeEntityNode.addToParentEditForm ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    property.push(new meta_db_1.ConfigItemProperty('ADDTO_PARENT_INSERTFORM', compositeEntityNode.addToParentInsertForm !== undefined ?
        (compositeEntityNode.addToParentInsertForm ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    if (compositeEntityNode.displayNodeName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('DISPLAY_NODE_NAME', compositeEntityNode.displayNodeName, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (compositeEntityNode.graphTemplate !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('GRAPH_TEMPLATE', compositeEntityNode.graphTemplate, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (compositeEntityNode.accessibilityRegex !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ACCESSBILITY_REGEX', compositeEntityNode.accessibilityRegex, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (compositeEntityNode.order !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ORDER', compositeEntityNode.order, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    property.push(new meta_db_1.ConfigItemProperty('IS_EXPRESSION_AVAILABLE', compositeEntityNode.expressionAvailable !== undefined ?
        (compositeEntityNode.expressionAvailable ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    if (compositeEntityNode.editabilityRegex !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('EDITABILITY_REGEX', compositeEntityNode.editabilityRegex, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (compositeEntityNode.expressionFieldString !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('EXPRESSION_FIELD_STRING', compositeEntityNode.expressionFieldString, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, compositeEntityNode.name, configitemtype_enum_1.ConfigItemTypes.COMPOSITEENTITYNODE, compositeEntityNode.projectId, createdBy, compositeEntityNode.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const compositeEntityNodePrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(compositeEntityNode.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(compositeEntityNode.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(compositeEntityNode.privileges, compositeEntityNodePrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, compositeEntityNodePrivileges);
};
exports.parseConfigToCompositeEntityNode = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.CompositeEntityNode(details.get('ADDTO_PARENT_DISPLAY'), details.get('ADDTO_PARENT_GRID'), details.get('ADDTO_PARENT_EDITFORM'), details.get('ADDTO_PARENT_INSERTFORM'), details.get('DISPLAY_NODE_NAME'), details.get('GRAPH_TEMPLATE'), details.get('ACCESSBILITY_REGEX'), details.get('ORDER'), details.get('IS_EXPRESSION_AVAILABLE'), details.get('EDITABILITY_REGEX'), details.get('EXPRESSION_FIELD_STRING'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=compositeentitynode.parser.js.map