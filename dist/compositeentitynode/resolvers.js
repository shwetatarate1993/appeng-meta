"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const datagrid_parser_1 = require("../datagrid/datagrid.parser");
const form_parser_1 = require("../form/form.parser");
const logicalentity_parser_1 = require("../logicalentity/logicalentity.parser");
const nodebusinessrule_parser_1 = require("../nodebusinessrule/nodebusinessrule.parser");
const physicalentity_parser_1 = require("../physicalentity/physicalentity.parser");
const compositeentitynode_parser_1 = require("./compositeentitynode.parser");
exports.Query = {
    CompositeEntityNode: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, compositeentitynode_parser_1.parseConfigToCompositeEntityNode),
    ChildCompositeEntityNodes: (_, { id }, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(id, relationtype_enum_1.RelationType.PARENTNODE_CHILDNODE, compositeentitynode_parser_1.parseConfigToCompositeEntityNode),
    CombinedChildCompositeEntityNodes: (_, { id }, context) => common_meta_1.configService.fetchChildNodesAsPerDisplayType(id, relationtype_enum_1.RelationType.PARENTNODE_CHILDNODE, configitemtype_enum_1.DisplayTypeProp.ADDTOPARENTDISPLAY, configitemtype_enum_1.NodeDisplayType.COMBINED, compositeentitynode_parser_1.parseConfigToCompositeEntityNode),
    ChildNodesInTree: (_, { id }, context) => common_meta_1.configService.fetchChildNodesAsPerDisplayType(id, relationtype_enum_1.RelationType.PARENTNODE_CHILDNODE, configitemtype_enum_1.DisplayTypeProp.ADDTOPARENTDISPLAY, configitemtype_enum_1.NodeDisplayType.TREE, compositeentitynode_parser_1.parseConfigToCompositeEntityNode),
    HiddenNodes: (_, { id }, context) => common_meta_1.configService.fetchChildNodesAsPerDisplayType(id, relationtype_enum_1.RelationType.PARENTNODE_CHILDNODE, configitemtype_enum_1.DisplayTypeProp.ADDEDHIDDEN, configitemtype_enum_1.NodeDisplayType.COMBINED, compositeentitynode_parser_1.parseConfigToCompositeEntityNode),
};
exports.CompositeEntityNode = {
    physicalDataEntities: async (compositeEntityNode, _, context) => {
        const entityIds = compositeEntityNode.childRelations.filter((rel) => rel.relationType === relationtype_enum_1.RelationType.ROOTCOMPOSITEENTITYNODE_PHYSICALENTITY
            || rel.relationType === relationtype_enum_1.RelationType.COMPOSITEENTITYNODE_PHYSICALENTITY)
            .map((rel) => rel.childItemId);
        return entityIds ? await common_meta_1.configService.fetchConfigItemsForIdList(entityIds, physicalentity_parser_1.parseConfigToPhysicalEntity) : [];
    },
    entity: async (compositeEntityNode, _, context) => {
        const entityRel = compositeEntityNode.childRelations.find((rel) => rel.relationType === relationtype_enum_1.RelationType.ROOTCOMPOSITEENTITYNODE_CHILDLOGICALENTITY
            || rel.relationType === relationtype_enum_1.RelationType.COMPOSITEENTITYNODE_CHILDLOGICALENTITY);
        if (entityRel) {
            return await common_meta_1.configService.fetchConfigById(entityRel.childItemId, logicalentity_parser_1.parseConfigToLogicalEntity);
        }
        else {
            return null;
        }
    },
    insertForm: async (compositeEntityNode, _, context) => {
        const insertFormRel = compositeEntityNode.childRelations.find((rel) => rel.relationType === relationtype_enum_1.RelationType.ROOTCOMPOSITEENTITYNODE_INSERTFORM
            || rel.relationType === relationtype_enum_1.RelationType.COMPOSITEENTITYNODE_INSERTFORM);
        if (insertFormRel) {
            return await common_meta_1.configService.fetchConfigById(insertFormRel.childItemId, form_parser_1.parseConfigToForm);
        }
        else {
            return null;
        }
    },
    editForm: async (compositeEntityNode, _, context) => {
        const insertFormRel = compositeEntityNode.childRelations.find((rel) => rel.relationType === relationtype_enum_1.RelationType.ROOTCOMPOSITEENTITYNODE_INSERTFORM
            || rel.relationType === relationtype_enum_1.RelationType.COMPOSITEENTITYNODE_INSERTFORM);
        const editFormRel = compositeEntityNode.childRelations.find((rel) => rel.relationType === relationtype_enum_1.RelationType.ROOTCOMPOSITEENTITYNODE_EDITFORM
            || rel.relationType === relationtype_enum_1.RelationType.COMPOSITEENTITYNODE_EDITFORM);
        let editForm = null;
        if (!insertFormRel || (insertFormRel && editFormRel && insertFormRel.childItemId !== editFormRel.childItemId)) {
            editForm = await common_meta_1.configService.fetchConfigById(editFormRel.childItemId, form_parser_1.parseConfigToForm);
        }
        return editForm;
    },
    dataGrid: async (compositeEntityNode, _, context) => {
        const datagridRel = compositeEntityNode.childRelations.find((rel) => rel.relationType === relationtype_enum_1.RelationType.ROOTCOMPOSITEENTITYNODE_DATAGRID
            || rel.relationType === relationtype_enum_1.RelationType.COMPOSITEENTITYNODE_DATAGRID);
        return await common_meta_1.configService.fetchConfigById(datagridRel.childItemId, datagrid_parser_1.parseConfigToDataGrid);
    },
    nodeBusinessRules: async (compositeEntityNode, _, context) => {
        const nodeRulesIds = compositeEntityNode.childRelations.filter((rel) => rel.relationType === relationtype_enum_1.RelationType.ROOTCOMPOSITEENTITYNODE_NODEBUSINESSRULE
            || rel.relationType === relationtype_enum_1.RelationType.COMPOSITEENTITYNODE_NODEBUSINESSRULE)
            .map((rel) => rel.childItemId);
        return nodeRulesIds ?
            await common_meta_1.configService.fetchConfigItemsForIdList(nodeRulesIds, nodebusinessrule_parser_1.parseConfigToNodeBusinessRule) : [];
    },
    combinedNodes: (compositeEntityNode, _, context) => common_meta_1.configService.fetchChildNodesAsPerDisplayType(compositeEntityNode.configObjectId, relationtype_enum_1.RelationType.PARENTNODE_CHILDNODE, configitemtype_enum_1.DisplayTypeProp.ADDTOPARENTDISPLAY, configitemtype_enum_1.NodeDisplayType.COMBINED, compositeentitynode_parser_1.parseConfigToCompositeEntityNode),
    children: (compositeEntityNode, _, context) => common_meta_1.configService.fetchChildNodesAsPerDisplayType(compositeEntityNode.configObjectId, relationtype_enum_1.RelationType.PARENTNODE_CHILDNODE, configitemtype_enum_1.DisplayTypeProp.ADDTOPARENTDISPLAY, configitemtype_enum_1.NodeDisplayType.TREE, compositeentitynode_parser_1.parseConfigToCompositeEntityNode),
};
exports.Mutation = {
    createCompositeEntityNode: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, compositeentitynode_parser_1.parseCompositeEntityNodeToConfig, compositeentitynode_parser_1.parseConfigToCompositeEntityNode);
    },
};
//# sourceMappingURL=resolvers.js.map