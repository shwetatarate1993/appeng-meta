export declare const Query: {
    CompositeEntityNode: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").CompositeEntityNode>;
    ChildCompositeEntityNodes: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").CompositeEntityNode[]>;
    CombinedChildCompositeEntityNodes: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").CompositeEntityNode[]>;
    ChildNodesInTree: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").CompositeEntityNode[]>;
    HiddenNodes: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").CompositeEntityNode[]>;
};
export declare const CompositeEntityNode: {
    physicalDataEntities: (compositeEntityNode: any, _: any, context: any) => Promise<import("../models").PhysicalEntity[]>;
    entity: (compositeEntityNode: any, _: any, context: any) => Promise<import("../models").LogicalEntity>;
    insertForm: (compositeEntityNode: any, _: any, context: any) => Promise<import("../models").Form>;
    editForm: (compositeEntityNode: any, _: any, context: any) => Promise<import("../models").Form>;
    dataGrid: (compositeEntityNode: any, _: any, context: any) => Promise<import("../models").DataGrid>;
    nodeBusinessRules: (compositeEntityNode: any, _: any, context: any) => Promise<import("../models").NodeBusinessRule[]>;
    combinedNodes: (compositeEntityNode: any, _: any, context: any) => Promise<import("../models").CompositeEntityNode[]>;
    children: (compositeEntityNode: any, _: any, context: any) => Promise<import("../models").CompositeEntityNode[]>;
};
export declare const Mutation: {
    createCompositeEntityNode: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map