export { default as formMockData } from './form';
export { default as configitemMockDataForm } from './configitem.form';
export { default as formPropertyMockData } from './configitem.property.form';
export { default as formRelationMockData } from './configitem.relation.form';
export { default as formPrivilegeMockData } from './configitem.privilege.form';
//# sourceMappingURL=index.d.ts.map