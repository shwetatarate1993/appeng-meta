declare const _default: {
    configObjectId: string;
    configObjectType: string;
    name: string;
    isRepeatable: boolean;
    tabRequiredInFormsection: boolean;
    tab: string;
    order: string;
    formType: string;
    formLabel: string;
    accessibilityRegex: string;
    editabilityRegex: string;
    expressionAvailable: boolean;
    expressionFieldString: string;
    maxRepeatation: string;
    defaultRepeatation: string;
    repeatationStyle: string;
    isDeletionAllowed: boolean;
    privileges: {
        privilegeType: string;
        itemId: string;
        roleId: number;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: string;
        deletionDate: string;
    }[];
    parentRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        childItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: any;
        updatedBy: string;
        updationDate: any;
        deletionDate: any;
    }[];
    childRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        childItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: any;
        updatedBy: string;
        updationDate: any;
        deletionDate: any;
    }[];
}[];
export default _default;
//# sourceMappingURL=form.d.ts.map