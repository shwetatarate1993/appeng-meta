"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var form_1 = require("./form");
exports.formMockData = form_1.default;
var configitem_form_1 = require("./configitem.form");
exports.configitemMockDataForm = configitem_form_1.default;
var configitem_property_form_1 = require("./configitem.property.form");
exports.formPropertyMockData = configitem_property_form_1.default;
var configitem_relation_form_1 = require("./configitem.relation.form");
exports.formRelationMockData = configitem_relation_form_1.default;
var configitem_privilege_form_1 = require("./configitem.privilege.form");
exports.formPrivilegeMockData = configitem_privilege_form_1.default;
//# sourceMappingURL=index.js.map