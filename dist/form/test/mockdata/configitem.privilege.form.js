"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        privilegeId: '12d43324-e28e-4c27-ae2d-aa22321e2197',
        privilegeType: 'VIEW',
        itemId: '111111-0728-43c7-a228-18d0eba11111',
        roleId: 19,
        createdBy: '1111',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
    },
    {
        privilegeId: '12271fce-0a2a-4c47-8058-27220470bb18',
        privilegeType: 'EDIT',
        itemId: '111111-0728-43c7-a228-18d0eba11111',
        roleId: 17,
        createdBy: '1111',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
    },
    {
        privilegeId: '12212cb3-333e-4216-98c7-952219411efd',
        privilegeType: 'EDIT',
        itemId: '111111-0728-43c7-a228-18d0eba11111',
        roleId: 16,
        createdBy: '1111',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
    },
];
//# sourceMappingURL=configitem.privilege.form.js.map