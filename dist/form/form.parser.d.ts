import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { Form } from '../models';
export declare const parseFormToConfig: (form: Form) => ConfigMetadata;
export declare const parseConfigToForm: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => Form;
//# sourceMappingURL=form.parser.d.ts.map