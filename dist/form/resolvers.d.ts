export declare const Query: {
    Form: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").Form>;
};
export declare const Form: {
    formSections: (form: any, _: any, context: any) => Promise<import("../models").FormSection[]>;
    buttonPanels: (form: any, _: any, context: any) => Promise<import("../models").ButtonPanel[]>;
    columnDataPreprocessors: (form: any, _: any, context: any) => Promise<import("../models").ColumnDataPreprocessor[]>;
};
export declare const Mutation: {
    createForm: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map