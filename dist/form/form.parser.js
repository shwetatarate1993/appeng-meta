"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseFormToConfig = (form) => {
    const createdAt = form.creationDate;
    const createdBy = form.createdBy;
    const updatedBy = form.updatedBy;
    const updatedAt = form.updationDate;
    const property = [];
    let itemId;
    if (form.configObjectId !== undefined && form.configObjectId !== ''
        && form.configObjectId !== null) {
        itemId = form.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (form.isRepeatable !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_FORM_REPEATABLE', form.isRepeatable !== undefined ? (form.isRepeatable ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (form.tabRequiredInFormsection !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_TAB_REQUIRED_IN_FORMSECTION', form.tabRequiredInFormsection !== undefined ? (form.tabRequiredInFormsection ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (form.tab !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('TAB', form.tab, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (form.order !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ORDER', form.order, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (form.formType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('FORM_TYPE', form.formType, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (form.expressionAvailable !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_EXPRESSION_AVAILABLE', form.expressionAvailable !== undefined ? (form.expressionAvailable ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (form.accessibilityRegex !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ACCESSBILITY_REGEX', form.accessibilityRegex, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (form.editabilityRegex !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('EDITABILITY_REGEX', form.editabilityRegex, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (form.expressionFieldString !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('EXPRESSION_FIELD_STRING', form.expressionFieldString, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (form.formLabel !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('FORM_LABEL', form.formLabel, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (form.defaultDataString !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('DEFAULT_DATA_STRING', form.defaultDataString, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (form.maxRepeatation !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('MAX_REPEATATION', form.maxRepeatation, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (form.defaultRepeatation !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('DEFAULT_REPEATATION', form.defaultRepeatation, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (form.repeatationStyle !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('REPEATATION_STYLE', form.repeatationStyle, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (form.isDeletionAllowed !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_DELETION_ALLOWED', form.isDeletionAllowed !== undefined ? (form.isDeletionAllowed ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (form.isDeletionAllowed !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ADD_ALLOWED', form.isDeletionAllowed !== undefined ? (form.isDeletionAllowed ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, form.name, configitemtype_enum_1.ConfigItemTypes.FORM, form.projectId, createdBy, form.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const formPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(form.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(form.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(form.privileges, formPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, formPrivileges);
};
exports.parseConfigToForm = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.Form(details.get('IS_FORM_REPEATABLE'), details.get('IS_TAB_REQUIRED_IN_FORMSECTION'), details.get('TAB'), details.get('ORDER'), details.get('FORM_TYPE'), details.get('ACCESSBILITY_REGEX'), details.get('EDITABILITY_REGEX'), details.get('IS_EXPRESSION_AVAILABLE'), details.get('EXPRESSION_FIELD_STRING'), details.get('FORM_LABEL'), details.get('DEFAULT_DATA_STRING'), details.get('MAX_REPEATATION'), details.get('DEFAULT_REPEATATION'), details.get('REPEATATION_STYLE'), details.get('IS_DELETION_ALLOWED'), details.get('ADD_ALLOWED'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=form.parser.js.map