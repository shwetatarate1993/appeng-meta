"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const buttonpanel_parser_1 = require("../buttonpanel/buttonpanel.parser");
const columndatapreprocessor_parser_1 = require("../columndatapreprocessor/columndatapreprocessor.parser");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const formsection_parser_1 = require("../formsection/formsection.parser");
const form_parser_1 = require("./form.parser");
exports.Query = {
    Form: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, form_parser_1.parseConfigToForm),
};
exports.Form = {
    formSections: async (form, _, context) => {
        const formSectionIds = form.childRelations.filter((rel) => rel.relationType === relationtype_enum_1.RelationType.FORM_FORMSECTION)
            .map((rel) => rel.childItemId);
        return formSectionIds ?
            await common_meta_1.configService.fetchConfigItemsForIdList(formSectionIds, formsection_parser_1.parseConfigToFormSectionObject) : [];
    },
    buttonPanels: (form, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(form.configObjectId, relationtype_enum_1.RelationType.FORM_BUTTONPANEL, buttonpanel_parser_1.parseConfigToButtonPanel),
    columnDataPreprocessors: (form, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(form.configObjectId, relationtype_enum_1.RelationType.FORM_COLUMNDATAPREPROCESSOR, columndatapreprocessor_parser_1.parseConfigToColumnDataPreprocessor),
};
exports.Mutation = {
    createForm: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, form_parser_1.parseFormToConfig, form_parser_1.parseConfigToForm);
    },
};
//# sourceMappingURL=resolvers.js.map