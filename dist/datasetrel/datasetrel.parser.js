"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseDataSetRelToConfig = (dataSetRel) => {
    const createdAt = dataSetRel.creationDate;
    const createdBy = dataSetRel.createdBy;
    const updatedBy = dataSetRel.updatedBy;
    const updatedAt = dataSetRel.updationDate;
    const property = [];
    let itemId;
    if (dataSetRel.configObjectId !== undefined && dataSetRel.configObjectId !== ''
        && dataSetRel.configObjectId !== null) {
        itemId = dataSetRel.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, dataSetRel.name, configitemtype_enum_1.ConfigItemTypes.DATASETREL, dataSetRel.projectId, createdBy, dataSetRel.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const datasetrelPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(dataSetRel.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(dataSetRel.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(dataSetRel.privileges, datasetrelPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, datasetrelPrivileges);
};
exports.parseConfigToDataSetRel = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.DataSetRel(configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=datasetrel.parser.js.map