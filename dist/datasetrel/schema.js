"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = __importDefault(require("../base"));
const common_schema_1 = __importDefault(require("../common.schema"));
const DataSetRel = `

input DataSetRelInput {
    configObjectId: ID
    configObjectType: String
    name: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilegeInput]
    parentRelations : [ConfigItemRelationInput]
    childRelations : [ConfigItemRelationInput]
}

extend type Query {
    DataSetRel(id: ID!): DataSetRel
    DataSetRelEntityColumn(parentEntityId: ID!, childEntityId: ID!): DataSetRelEntityColumns
}

extend type Mutation {
    createDataSetRel (input: DataSetRelInput): DataSetRel
}

type DataSetRelEntityColumns{
    dataSetRelColumns: [DataSetRelColumn]
}

type DataSetRelColumn{
    parentColumn: LogicalColumn
    childColumn: LogicalColumn
}

type DataSetRel {
    configObjectId: ID
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilege]
    parentRelations : [ConfigItemRelation]
    childRelations : [ConfigItemRelation]
    logicalEntity : [LogicalEntity]
    dataSetRelProperties : [DataSetRelProperty]
}
`;
exports.default = () => [DataSetRel, base_1.default, common_schema_1.default];
//# sourceMappingURL=schema.js.map