"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const datasetrel_parser_1 = require("../datasetrel.parser");
const mockdata_1 = require("./mockdata");
test('parser DataSetRel to config', () => {
    const datasetrel = models_1.DataSetRel.deserialize(mockdata_1.dataSetRelMockData[0]);
    const result = datasetrel_parser_1.parseDataSetRelToConfig(datasetrel);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItem.configObjectType).toEqual('DataSetRel');
});
test('parser config to DataSetRel ', () => {
    const result = datasetrel_parser_1.parseConfigToDataSetRel(mockdata_1.configitemMockDataDataSetRel[0], mockdata_1.dataSetRelPropertiesMockData, mockdata_1.configRelationsMockDataSetRel, mockdata_1.configPrivilegesMockDataDataSetRel);
    expect(result).toHaveProperty('configObjectType', 'DataSetRel');
    expect(result).toHaveProperty('name');
    expect(result.parentRelations.length === 1).toBe(true);
    expect(result.childRelations.length === 2).toBe(true);
    expect(result.parentRelations[0].relationType === 'LogicalEntity_DataSetRel').toBe(true);
    expect(result.childRelations[0].relationType === 'DataSetRel_LogicalEntity').
        toBe(true);
    expect(result.childRelations[1].relationType === 'DataSetRel_DataSetRelProperty').toBe(true);
});
//# sourceMappingURL=datasetrel.parser.test.js.map