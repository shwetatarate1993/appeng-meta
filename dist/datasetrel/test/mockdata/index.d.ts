export { default as dataSetRelMockData } from './datasetrel.object';
export { default as configRelationsMockDataSetRel } from './configitem.relation.datasetrel';
export { default as dataSetRelPropertiesMockData } from './configitem.property.datasetrel';
export { default as configitemMockDataDataSetRel } from './configitem.datasetrel';
export { default as configPrivilegesMockDataDataSetRel } from './configitem.privileges.datasetrel';
//# sourceMappingURL=index.d.ts.map