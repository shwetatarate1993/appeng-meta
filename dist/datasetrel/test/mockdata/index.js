"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var datasetrel_object_1 = require("./datasetrel.object");
exports.dataSetRelMockData = datasetrel_object_1.default;
var configitem_relation_datasetrel_1 = require("./configitem.relation.datasetrel");
exports.configRelationsMockDataSetRel = configitem_relation_datasetrel_1.default;
var configitem_property_datasetrel_1 = require("./configitem.property.datasetrel");
exports.dataSetRelPropertiesMockData = configitem_property_datasetrel_1.default;
var configitem_datasetrel_1 = require("./configitem.datasetrel");
exports.configitemMockDataDataSetRel = configitem_datasetrel_1.default;
var configitem_privileges_datasetrel_1 = require("./configitem.privileges.datasetrel");
exports.configPrivilegesMockDataDataSetRel = configitem_privileges_datasetrel_1.default;
//# sourceMappingURL=index.js.map