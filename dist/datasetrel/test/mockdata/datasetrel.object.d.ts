declare const _default: {
    configObjectId: string;
    configObjectType: string;
    name: string;
    createdBy: string;
    isDeleted: number;
    creationDate: string;
    updatedBy: string;
    updationDate: any;
    deletionDate: string;
    parentRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: any;
        deletionDate: any;
    }[];
    childRelations: {
        relationId: string;
        relationType: string;
        childItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: any;
        deletionDate: any;
    }[];
}[];
export default _default;
//# sourceMappingURL=datasetrel.object.d.ts.map