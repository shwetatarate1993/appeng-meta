"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        relationId: '95cdd4c7-a6f1-4b28-8de0-20edfb6169fb',
        relationType: 'LogicalEntity_DataSetRel',
        parentItemId: '518f6728-d024-4766-ae9f-5ca933d3c196',
        childItemId: '67aaa0e6-77de-43e3-8b6a-8fa7de7cf64d',
        createdBy: '1124',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1115',
        updationDate: null,
        deletionDate: null,
    },
    {
        relationId: 'a4ec6903-6928-4bff-8e3d-3e6a5c4130bf',
        relationType: 'DataSetRel_LogicalEntity',
        parentItemId: '67aaa0e6-77de-43e3-8b6a-8fa7de7cf64d',
        childItemId: '6474f66a-baab-467f-93db-53ebee5dc798',
        createdBy: '1124',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1115',
        updationDate: null,
        deletionDate: null,
    },
    {
        relationId: 'aa1d12e7-417a-4086-9882-e795a4dea975',
        relationType: 'DataSetRel_DataSetRelProperty',
        parentItemId: '67aaa0e6-77de-43e3-8b6a-8fa7de7cf64d',
        childItemId: 'c65ae5ad-9a9d-4d8e-adad-00aef1754d99',
        createdBy: '1124',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1115',
        updationDate: null,
        deletionDate: null,
    },
];
//# sourceMappingURL=configitem.relation.datasetrel.js.map