import { DataSetRelEntityColumns } from '../models';
export declare const Query: {
    DataSetRel: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").DataSetRel>;
    DataSetRelEntityColumn: (_: any, { parentEntityId, childEntityId }: {
        parentEntityId: any;
        childEntityId: any;
    }, context: any) => Promise<DataSetRelEntityColumns>;
};
export declare const DataSetRel: {
    logicalEntity: (dataSetRel: any, _: any, context: any) => Promise<import("../models").LogicalEntity[]>;
    dataSetRelProperties: (dataSetRel: any, _: any, context: any) => Promise<import("../models").DataSetRelProperty[]>;
};
export declare const Mutation: {
    createDataSetRel: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map