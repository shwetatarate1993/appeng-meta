import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { DataSetRel } from '../models';
export declare const parseDataSetRelToConfig: (dataSetRel: DataSetRel) => ConfigMetadata;
export declare const parseConfigToDataSetRel: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => DataSetRel;
//# sourceMappingURL=datasetrel.parser.d.ts.map