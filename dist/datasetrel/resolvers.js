"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const datasetrelproperty_parser_1 = require("../datasetrelproperty/datasetrelproperty.parser");
const logicalcolumn_parser_1 = require("../logicalcolumn/logicalcolumn.parser");
const logicalentity_parser_1 = require("../logicalentity/logicalentity.parser");
const models_1 = require("../models");
const datasetrel_parser_1 = require("./datasetrel.parser");
exports.Query = {
    DataSetRel: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, datasetrel_parser_1.parseConfigToDataSetRel),
    DataSetRelEntityColumn: async (_, { parentEntityId, childEntityId }, context) => {
        // fetch all dataSetRel relations attached with parent entity
        const configObjects = await common_meta_1.configService.fetchConfigsByParentIdAndRelationType(parentEntityId, relationtype_enum_1.RelationType.LOGICALENTITY_DATASETREL, datasetrel_parser_1.parseConfigToDataSetRel);
        // fetch all parent relations(including datasetrel & others as well) with child entity
        const configItems = await common_meta_1.configService.fetchConfigsByChildId(childEntityId);
        // filter all dataSetRel relations attached with child entity
        const configItem = configItems.filter((item) => item.configObjectType === configitemtype_enum_1.ConfigItemTypes.DATASETREL);
        // Loop to find out the proper common dataSetRel among both parent & child entity
        let relation;
        allLoop: for (const configObject of configObjects) {
            for (const item of configItem) {
                relation = configObject.parentRelations.find((parentRelation) => parentRelation.childItemId === item.configObjectId);
                if (relation !== null && relation !== undefined) {
                    break allLoop;
                }
            }
        }
        // fetch all dataSetRelProperty attached with above finded dataSetRel
        const dataSetRelPropertyConfig = await common_meta_1.configService.fetchConfigsByParentIdAndRelationType(relation.childItemId, relationtype_enum_1.RelationType.DATASETREL_DATASETRELPROPERTY, datasetrelproperty_parser_1.parseConfigToDataSetRelPropertyObject);
        const relColumns = new models_1.DataSetRelEntityColumns();
        relColumns.dataSetRelColumns = [];
        for (const relProperty of dataSetRelPropertyConfig) {
            const dataSetRelColumn = new models_1.DataSetRelColumns();
            for (const relPropertyChildRelation of relProperty.childRelations) {
                const entityColumn = await common_meta_1.configService.fetchConfigById(relPropertyChildRelation.childItemId, logicalcolumn_parser_1.parseConfigToLogicalColumn);
                if (relPropertyChildRelation.relationType === relationtype_enum_1.RelationType.DATASETRELPROPERTY_ENTITYCOLUMN) {
                    dataSetRelColumn.parentColumn = entityColumn;
                }
                if (relPropertyChildRelation.relationType === relationtype_enum_1.RelationType.DATASETRELPROPERTY_ENTITYCOLUMN_CHILD) {
                    dataSetRelColumn.childColumn = entityColumn;
                }
            }
            relColumns.dataSetRelColumns.push(dataSetRelColumn);
        }
        return relColumns;
    },
};
exports.DataSetRel = {
    logicalEntity: (dataSetRel, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(dataSetRel.configObjectId, relationtype_enum_1.RelationType.DATASETREL_LOGICALENTITY, logicalentity_parser_1.parseConfigToLogicalEntity),
    dataSetRelProperties: (dataSetRel, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(dataSetRel.configObjectId, relationtype_enum_1.RelationType.DATASETREL_DATASETRELPROPERTY, datasetrelproperty_parser_1.parseConfigToDataSetRelPropertyObject),
};
exports.Mutation = {
    createDataSetRel: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, datasetrel_parser_1.parseDataSetRelToConfig, datasetrel_parser_1.parseConfigToDataSetRel);
    },
};
//# sourceMappingURL=resolvers.js.map