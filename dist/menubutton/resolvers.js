"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const menubutton_parser_1 = require("./menubutton.parser");
exports.Query = {
    MenuButton: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, menubutton_parser_1.parseConfigToMenuButton),
};
exports.Mutation = {
    createMenuButton: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, menubutton_parser_1.parseMenuButtonToConfig, menubutton_parser_1.parseConfigToMenuButton);
    },
};
//# sourceMappingURL=resolvers.js.map