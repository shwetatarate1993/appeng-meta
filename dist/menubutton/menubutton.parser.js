"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseMenuButtonToConfig = (menuButton) => {
    const createdAt = menuButton.creationDate;
    const createdBy = menuButton.createdBy;
    const updatedBy = menuButton.updatedBy;
    const updatedAt = menuButton.updationDate;
    const property = [];
    let itemId;
    if (menuButton.configObjectId !== undefined && menuButton.configObjectId !== ''
        && menuButton.configObjectId !== null) {
        itemId = menuButton.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (menuButton.defaultType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'DEFAULT_TYPE', menuButton.defaultType, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (menuButton.menuLabel !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'MENU_LABEL', menuButton.menuLabel, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (menuButton.position !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'POSITION', menuButton.position, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, menuButton.name, configitemtype_enum_1.ConfigItemTypes.MENUBUTTON, menuButton.projectId, createdBy, menuButton.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const menuPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(menuButton.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(menuButton.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(menuButton.privileges, menuPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, menuPrivileges);
};
exports.parseConfigToMenuButton = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.MenuButton(details.get('DEFAULT_TYPE'), details.get('MENU_LABEL'), details.get('POSITION'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=menubutton.parser.js.map