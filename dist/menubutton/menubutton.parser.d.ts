import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { MenuButton } from '../models';
export declare const parseMenuButtonToConfig: (menuButton: MenuButton) => ConfigMetadata;
export declare const parseConfigToMenuButton: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => MenuButton;
//# sourceMappingURL=menubutton.parser.d.ts.map