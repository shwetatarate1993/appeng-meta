"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const menubutton_parser_1 = require("../menubutton.parser");
const mockdata_1 = require("./mockdata");
test('parser menuButton object to config', () => {
    const menubutton = models_1.MenuButton.deserialize(mockdata_1.menuButtonMockData[0]);
    const result = menubutton_parser_1.parseMenuButtonToConfig(menubutton);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItem.configObjectType).toEqual('MenuButton');
});
test('parser config to menubutton object', () => {
    const result = menubutton_parser_1.parseConfigToMenuButton(mockdata_1.configitemMockDataMenuButton[0], mockdata_1.menuButtonPropertyMockData, mockdata_1.configRelationsMockDataMenuButton, mockdata_1.configPrivilegesMockData);
    expect(result).toHaveProperty('defaultType', 'dropdown');
    expect(result).toHaveProperty('menuLabel', 'Tools');
    expect(result).toHaveProperty('position', 'right');
    expect(result.privileges.length === 4).toBe(true);
    expect(result.parentRelations.length === 1).toBe(true);
    expect(result.childRelations.length === 1).toBe(true);
    expect(result.parentRelations[0].relationType === 'ButtonPanel_MenuButton').toBe(true);
    expect(result.childRelations[0].relationType === 'MenuButton_UserAction').toBe(true);
});
//# sourceMappingURL=menubutton.parser.test.js.map