declare const _default: {
    configObjectId: string;
    configObjectType: string;
    name: string;
    defaultType: string;
    menuLabel: string;
    position: string;
    privileges: {
        privilegeType: string;
        roleId: number;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: string;
        deletionDate: string;
    }[];
    parentRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: any;
        deletionDate: any;
    }[];
    childRelations: {
        relationId: string;
        relationType: string;
        childItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: any;
        deletionDate: any;
    }[];
}[];
export default _default;
//# sourceMappingURL=menubutton.object.d.ts.map