"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        propertyId: '8f801c10-1b1d-11e9-b2e1-ad13d7833e95',
        propertyName: 'DEFAULT_TYPE',
        propertyValue: 'dropdown',
        itemId: '20ae929e-ccb9-40db-b505-c0e61821903f',
        createdBy: '1124',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '8f801c10-1b1d-11e9-b2e1-ad1457833e96',
        propertyName: 'MENU_LABEL',
        propertyValue: 'Tools',
        itemId: '20ae929e-ccb9-40db-b505-c0e61821903f',
        createdBy: '1124',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '8f8c10-1b1d-11e9-b2e1-ad14d7633e96',
        propertyName: 'POSITION',
        propertyValue: 'right',
        itemId: '20ae929e-ccb9-40db-b505-c0e61821903f',
        createdBy: '1124',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
];
//# sourceMappingURL=configitem.property.menubutton.js.map