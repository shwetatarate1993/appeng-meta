export { default as menuButtonMockData } from './menubutton.object';
export { default as configRelationsMockDataMenuButton } from './configitem.relation.menubutton';
export { default as menuButtonPropertyMockData } from './configitem.property.menubutton';
export { default as configitemMockDataMenuButton } from './configitem.menubutton';
export { default as configPrivilegesMockData } from './configitem.privileges.menubutton';
//# sourceMappingURL=index.d.ts.map