"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var menubutton_object_1 = require("./menubutton.object");
exports.menuButtonMockData = menubutton_object_1.default;
var configitem_relation_menubutton_1 = require("./configitem.relation.menubutton");
exports.configRelationsMockDataMenuButton = configitem_relation_menubutton_1.default;
var configitem_property_menubutton_1 = require("./configitem.property.menubutton");
exports.menuButtonPropertyMockData = configitem_property_menubutton_1.default;
var configitem_menubutton_1 = require("./configitem.menubutton");
exports.configitemMockDataMenuButton = configitem_menubutton_1.default;
var configitem_privileges_menubutton_1 = require("./configitem.privileges.menubutton");
exports.configPrivilegesMockData = configitem_privileges_menubutton_1.default;
//# sourceMappingURL=index.js.map