export declare const Query: {
    MenuButton: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").MenuButton>;
};
export declare const Mutation: {
    createMenuButton: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map