"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const auditgrid_parser_1 = require("./auditgrid.parser");
exports.Query = {
    AuditGrid: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, auditgrid_parser_1.parseConfigToAuditGrid),
};
exports.Mutation = {
    createAuditGrid: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, auditgrid_parser_1.parseAuditGridToConfig, auditgrid_parser_1.parseConfigToAuditGrid);
    },
};
//# sourceMappingURL=resolvers.js.map