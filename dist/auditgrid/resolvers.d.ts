export declare const Query: {
    AuditGrid: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").AuditGrid>;
};
export declare const Mutation: {
    createAuditGrid: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map