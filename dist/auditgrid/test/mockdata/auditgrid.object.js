"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: '138fc4b2-d639-458c-9f1b-d7007b942074',
        configObjectType: 'AuditGrid',
        name: 'Button Grid',
        projectId: -1,
        selectQueryId: 'SELECT "Expand" as Expand , AUDIT_ID as id,VERSION as Version,'
            + 'ACTION as Action,USER as User,AUDIT_DATE as Date '
            + 'FROM CONFIGITEMRELATION_AUDIT WHERE CHILDITEMID=#{PRIV_ITEMID}',
        selectQueryDetail: 'SELECT AUDIT_DESC FROM CONFIGITEMRELATION_AUDIT WHERE AUDIT_ID=#{AUDIT_ID}',
        dataSourceName: 'PRIMARY_MD',
        tableType: 'PARENT_TABLE',
        createdBy: '1135',
        isDeleted: 0,
        itemDescription: null,
        creationDate: '2019-04-05T10:40:06.000Z',
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
        privileges: [],
        childRelations: [],
        parentRelations: [
            {
                relationId: '92be540e-9f08-4451-8ac3-3b64508a4a32',
                relationType: 'AuditEntity_AuditGrid',
                parentItemId: '0d4b4763-a267-4ec2-be8f-460ebe464f31',
                childItemId: '138fc4b2-d639-458c-9f1b-d7007b942074',
                createdBy: '1135',
                isDeleted: 0,
                creationDate: '2018-02-26T00:00:00.000Z',
                updatedBy: '1135',
                updationDate: '2018-04-27T00:00:00.000Z',
                deletionDate: '2018-04-27T00:00:00.000Z',
            },
        ],
    },
];
//# sourceMappingURL=auditgrid.object.js.map