"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        propertyId: '1bd14db5-4a26-4243-a519-d5ab27cec407',
        propertyName: 'SELECT_QUERY_SUMMARY',
        propertyValue: 'SELECT "Expand" as Expand , AUDIT_ID as id,VERSION as Version,'
            + 'ACTION as Action,USER as User,AUDIT_DATE as Date '
            + 'FROM CONFIGITEMRELATION_AUDIT WHERE CHILDITEMID=#{PRIV_ITEMID}',
        itemId: '138fc4b2-d639-458c-9f1b-d7007b942074',
        createdBy: '1135',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '2ef24081-59cd-4ede-bfe8-f89b2c302fbb',
        propertyName: 'SELECT_QUERY_DETAIL',
        propertyValue: 'SELECT AUDIT_DESC FROM CONFIGITEMRELATION_AUDIT WHERE AUDIT_ID=#{AUDIT_ID}',
        itemId: '138fc4b2-d639-458c-9f1b-d7007b942074',
        createdBy: '1135',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: 'fb2de8f1-c09d-4f9a-8819-f78cd2c5f583',
        propertyName: 'DATASOURCE_NAME',
        propertyValue: 'PRIMARY_MD',
        itemId: '138fc4b2-d639-458c-9f1b-d7007b942074',
        createdBy: '1135',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '665f62db-0041-4be3-a215-a3ea0cc4e7ce',
        propertyName: 'TABLETYPE',
        propertyValue: 'PARENT_TABLE',
        itemId: '138fc4b2-d639-458c-9f1b-d7007b942074',
        createdBy: '1135',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
];
//# sourceMappingURL=configitem.property.auditgrid.js.map