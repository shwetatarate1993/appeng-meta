"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var auditgrid_object_1 = require("./auditgrid.object");
exports.auditGridObjectMockData = auditgrid_object_1.default;
var configitem_auditgrid_1 = require("./configitem.auditgrid");
exports.auditGridConfigitemMockData = configitem_auditgrid_1.default;
var configitem_property_auditgrid_1 = require("./configitem.property.auditgrid");
exports.auditGridPropertyMockData = configitem_property_auditgrid_1.default;
var configitem_relation_auditgrid_1 = require("./configitem.relation.auditgrid");
exports.auditGridRelationMockData = configitem_relation_auditgrid_1.default;
var configitem_privileges_auditgrid_1 = require("./configitem.privileges.auditgrid");
exports.auditGridPrivilegesMockData = configitem_privileges_auditgrid_1.default;
//# sourceMappingURL=index.js.map