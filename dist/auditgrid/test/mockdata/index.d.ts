export { default as auditGridObjectMockData } from './auditgrid.object';
export { default as auditGridConfigitemMockData } from './configitem.auditgrid';
export { default as auditGridPropertyMockData } from './configitem.property.auditgrid';
export { default as auditGridRelationMockData } from './configitem.relation.auditgrid';
export { default as auditGridPrivilegesMockData } from './configitem.privileges.auditgrid';
//# sourceMappingURL=index.d.ts.map