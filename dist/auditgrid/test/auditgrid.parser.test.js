"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const auditgrid_parser_1 = require("../auditgrid.parser");
const mockdata_1 = require("./mockdata");
test('parser AuditGrid to config', () => {
    const auditGrid = models_1.AuditGrid.deserialize(mockdata_1.auditGridObjectMockData[0]);
    const result = auditgrid_parser_1.parseAuditGridToConfig(auditGrid);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItem.configObjectType).toEqual('AuditGrid');
    expect(result.configItemProperty[0].propertyName).toEqual('SELECT_QUERY_SUMMARY');
    expect(result.configItemProperty[1].propertyName).toEqual('SELECT_QUERY_DETAIL');
    expect(result.configItemProperty[2].propertyName).toEqual('DATASOURCE_NAME');
    expect(result.configItemProperty[3].propertyName).toEqual('TABLETYPE');
});
test('parser config to AuditGrid', () => {
    const result = auditgrid_parser_1.parseConfigToAuditGrid(mockdata_1.auditGridConfigitemMockData[0], mockdata_1.auditGridPropertyMockData, mockdata_1.auditGridRelationMockData, mockdata_1.auditGridPrivilegesMockData);
    expect(result).toHaveProperty('selectQueryId');
    expect(result).toHaveProperty('selectQueryDetail');
    expect(result).toHaveProperty('dataSourceName');
    expect(result).toHaveProperty('tableType');
});
//# sourceMappingURL=auditgrid.parser.test.js.map