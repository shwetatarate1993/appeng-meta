"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseAuditGridToConfig = (auditGrid) => {
    const createdAt = auditGrid.creationDate;
    const createdBy = auditGrid.createdBy;
    const updatedBy = auditGrid.updatedBy;
    const updatedAt = auditGrid.updationDate;
    const property = [];
    let itemId;
    if (auditGrid.configObjectId !== undefined && auditGrid.configObjectId !== ''
        && auditGrid.configObjectId !== null) {
        itemId = auditGrid.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (auditGrid.selectQueryId !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'SELECT_QUERY_SUMMARY', auditGrid.selectQueryId, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (auditGrid.selectQueryDetail !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'SELECT_QUERY_DETAIL', auditGrid.selectQueryDetail, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (auditGrid.dataSourceName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'DATASOURCE_NAME', auditGrid.dataSourceName, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (auditGrid.tableType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'TABLETYPE', auditGrid.tableType, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, auditGrid.name, configitemtype_enum_1.ConfigItemTypes.AUDITGRID, auditGrid.projectId, createdBy, auditGrid.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const auditGridPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(auditGrid.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(auditGrid.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(auditGrid.privileges, auditGridPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, auditGridPrivileges);
};
exports.parseConfigToAuditGrid = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.AuditGrid(details.get('SELECT_QUERY_SUMMARY'), details.get('SELECT_QUERY_DETAIL'), details.get('DATASOURCE_NAME'), details.get('TABLETYPE'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=auditgrid.parser.js.map