import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { AuditGrid } from '../models';
export declare const parseAuditGridToConfig: (auditGrid: AuditGrid) => ConfigMetadata;
export declare const parseConfigToAuditGrid: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => AuditGrid;
//# sourceMappingURL=auditgrid.parser.d.ts.map