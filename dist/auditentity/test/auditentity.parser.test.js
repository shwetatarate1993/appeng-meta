"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const auditentity_parser_1 = require("../auditentity.parser");
const mockdata_1 = require("./mockdata");
test('parser audit entity to config', () => {
    const auditEntity = models_1.AuditEntity.deserialize(mockdata_1.auditEntityMockData[0]);
    const result = auditentity_parser_1.parseAuditEntityToConfig(auditEntity);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItem.configObjectType).toEqual('AuditEntity');
    expect(result.configItemRelation.length === 1).toBe(true);
    expect(result.configItemPrivilege.length === 0).toBe(true);
    expect(result.configItemProperty.length === 3).toBe(true);
});
test('parser config to audit entity', () => {
    const result = auditentity_parser_1.parseConfigToAuditEntity(mockdata_1.auditEntityConfigitemMockData[0], mockdata_1.auditEntityPropertyMockData, mockdata_1.auditEntityConfigRelationsMockData, mockdata_1.auditEntityConfigPrivilegesMockData);
    expect(result).toHaveProperty('displayName');
    expect(result).toHaveProperty('displayName', 'Card audit entity');
    expect(result.privileges.length === 0).toBe(true);
});
//# sourceMappingURL=auditentity.parser.test.js.map