export { default as auditEntityMockData } from './auditentity';
export { default as auditEntityConfigitemMockData } from './configitem.auditentity';
export { default as auditEntityPropertyMockData } from './configitem.property.auditentity';
export { default as auditEntityConfigRelationsMockData } from './configitem.relation.auditentity';
export { default as auditEntityConfigPrivilegesMockData } from './configitem.privileges.auditentity';
//# sourceMappingURL=index.d.ts.map