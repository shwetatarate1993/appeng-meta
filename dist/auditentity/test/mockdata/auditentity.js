"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: 'b80f1be8-0d60-4c91-9f06-94eff3835547',
        name: 'Card Audit Entity',
        configObjectType: 'AuditEntity',
        createdBy: '1121',
        isDeleted: 0,
        itemDescription: 'test',
        creationDate: null,
        projectId: -1,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
        datasourceName: 'PrimaryMd',
        displayName: 'Card audit entity',
        insertQuery: 'test',
        privileges: [],
        childRelations: [
            {
                relationId: '6cf28e58-da13-43c6-99a2-a78df3131e6f',
                relationType: 'AuditEntity_AuditGrid',
                parentItemId: 'b80f1be8-0d60-4c91-9f06-94eff3835547',
                childItemId: '346e2bc4-4357-424f-87c9-84e97ca9582f',
                createdBy: '1121',
                isDeleted: 0,
                creationDate: null,
                updatedBy: '1121',
                updationDate: null,
                deletionDate: null,
            },
        ],
    },
];
//# sourceMappingURL=auditentity.js.map