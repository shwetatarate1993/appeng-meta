"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        propertyId: '297399c5-56f2-4e02-8d39-419cb9ac3197',
        propertyName: 'DISPLAY_NAME',
        propertyValue: 'Card audit entity',
        itemId: 'b80f1be8-0d60-4c91-9f06-94eff3835547',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    }, {
        propertyId: '6dfcd337-07bd-4992-b527-06a1a25249c6',
        propertyName: 'INSERT_QUERY',
        propertyValue: 'test',
        itemId: 'b80f1be8-0d60-4c91-9f06-94eff3835547',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    }, {
        propertyId: 'a593fdf9-8da4-42eb-b489-7da9aa7c34b7',
        propertyName: 'DATASOURCE_NAME',
        propertyValue: 'primaryMd',
        itemId: 'b80f1be8-0d60-4c91-9f06-94eff3835547',
        createdBy: '1115',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
];
//# sourceMappingURL=configitem.property.auditentity.js.map