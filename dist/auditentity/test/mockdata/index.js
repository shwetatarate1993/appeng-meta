"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var auditentity_1 = require("./auditentity");
exports.auditEntityMockData = auditentity_1.default;
var configitem_auditentity_1 = require("./configitem.auditentity");
exports.auditEntityConfigitemMockData = configitem_auditentity_1.default;
var configitem_property_auditentity_1 = require("./configitem.property.auditentity");
exports.auditEntityPropertyMockData = configitem_property_auditentity_1.default;
var configitem_relation_auditentity_1 = require("./configitem.relation.auditentity");
exports.auditEntityConfigRelationsMockData = configitem_relation_auditentity_1.default;
var configitem_privileges_auditentity_1 = require("./configitem.privileges.auditentity");
exports.auditEntityConfigPrivilegesMockData = configitem_privileges_auditentity_1.default;
//# sourceMappingURL=index.js.map