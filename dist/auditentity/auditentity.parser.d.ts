import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { AuditEntity } from '../models';
export declare const parseAuditEntityToConfig: (auditEntity: AuditEntity) => ConfigMetadata;
export declare const parseConfigToAuditEntity: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => AuditEntity;
//# sourceMappingURL=auditentity.parser.d.ts.map