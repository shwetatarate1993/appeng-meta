"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = __importDefault(require("../base"));
const common_schema_1 = __importDefault(require("../common.schema"));
const AuditEntity = `

input AuditEntityInput {

    name: String
    configObjectId: ID
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    datasourceName: String
    displayName: String
    insertQuery: String
    privileges : [ConfigItemPrivilegeInput]
    parentRelations : [ConfigItemRelationInput]
    childRelations : [ConfigItemRelationInput]
}

extend type Query {
    AuditEntity(id: ID!): AuditEntity
   }

extend type Mutation {
    createAuditEntity (input: AuditEntityInput): AuditEntity
}

type AuditEntity {

    name: String
    configObjectId: ID
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    datasourceName: String
    displayName: String
    insertQuery: String
    privileges : [ConfigItemPrivilege]
    parentRelations : [ConfigItemRelation]
    childRelations : [ConfigItemRelation]
    auditGrid : [AuditGrid]
}
`;
exports.default = () => [AuditEntity, base_1.default, common_schema_1.default];
//# sourceMappingURL=schema.js.map