"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseAuditEntityToConfig = (auditEntity) => {
    const createdAt = auditEntity.creationDate;
    const createdBy = auditEntity.createdBy;
    const updatedBy = auditEntity.updatedBy;
    const updatedAt = auditEntity.updationDate;
    const property = [];
    let itemId;
    if (auditEntity.configObjectId !== undefined && auditEntity.configObjectId !== ''
        && auditEntity.configObjectId !== null) {
        itemId = auditEntity.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (auditEntity.datasourceName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'DATASOURCE_NAME', auditEntity.datasourceName, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (auditEntity.displayName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'DISPLAY_NAME', auditEntity.displayName, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (auditEntity.insertQuery !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'INSERT_QUERY', auditEntity.insertQuery, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, auditEntity.name, configitemtype_enum_1.ConfigItemTypes.AUDITENTITY, auditEntity.projectId, createdBy, auditEntity.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const auditEntityPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(auditEntity.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(auditEntity.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(auditEntity.privileges, auditEntityPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, auditEntityPrivileges);
};
exports.parseConfigToAuditEntity = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations != null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.AuditEntity(configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, details.get('DATASOURCE_NAME'), details.get('DISPLAY_NAME'), details.get('INSERT_QUERY'), privileges, childRelations, parentRelations);
};
//# sourceMappingURL=auditentity.parser.js.map