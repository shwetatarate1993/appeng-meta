"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const auditgrid_parser_1 = require("../auditgrid/auditgrid.parser");
const auditentity_parser_1 = require("./auditentity.parser");
exports.Query = {
    AuditEntity: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, auditentity_parser_1.parseConfigToAuditEntity),
};
exports.AuditEntity = {
    auditGrid: (auditEntity, _, context) => common_meta_1.configService.fetchConfigsByParentId(auditEntity.configObjectId, auditgrid_parser_1.parseConfigToAuditGrid),
};
exports.Mutation = {
    createAuditEntity: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, auditentity_parser_1.parseAuditEntityToConfig, auditentity_parser_1.parseConfigToAuditEntity);
    },
};
//# sourceMappingURL=resolvers.js.map