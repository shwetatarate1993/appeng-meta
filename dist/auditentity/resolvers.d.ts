export declare const Query: {
    AuditEntity: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").AuditEntity>;
};
export declare const AuditEntity: {
    auditGrid: (auditEntity: any, _: any, context: any) => Promise<import("../models").AuditGrid[]>;
};
export declare const Mutation: {
    createAuditEntity: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map