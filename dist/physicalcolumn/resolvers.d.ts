export declare const Query: {
    PhysicalColumn: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").PhysicalColumn>;
};
export declare const Mutation: {
    createPhysicalColumn: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map