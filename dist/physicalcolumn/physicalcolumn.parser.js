"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parsePhysicalColumnToConfig = (physicalColumn) => {
    const createdAt = physicalColumn.creationDate;
    const createdBy = physicalColumn.createdBy;
    const updatedBy = physicalColumn.updatedBy;
    const updatedAt = physicalColumn.updationDate;
    const property = [];
    let itemId;
    if (physicalColumn.configObjectId !== undefined && physicalColumn.configObjectId !== ''
        && physicalColumn.configObjectId !== null) {
        itemId = physicalColumn.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (physicalColumn.isPrimaryKey !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_KEY', physicalColumn.isKey !== undefined ? (physicalColumn.isKey ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (physicalColumn.isDisplayColumn !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ISDISPLAYCOLUMN', physicalColumn.isDisplayColumn !== undefined ? (physicalColumn.isDisplayColumn ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (physicalColumn.isPrimaryKey !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ISPRIMARYKEY', physicalColumn.isPrimaryKey !== undefined ? (physicalColumn.isPrimaryKey ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (physicalColumn.dataType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('DATATYPE', physicalColumn.dataType, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (physicalColumn.dbCode !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('DBCODE', physicalColumn.dbCode, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (physicalColumn.length !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('LENGTH', physicalColumn.length, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (physicalColumn.dbType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('DBTYPE', physicalColumn.dbType, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (physicalColumn.jsonName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('JSONNAME', physicalColumn.jsonName, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (physicalColumn.isLogicalColumnRequired !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_LOGICAL_COLUMN_REQUIRED', physicalColumn.isLogicalColumnRequired !== undefined ?
            (physicalColumn.isLogicalColumnRequired ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (physicalColumn.isUnique !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_UNIQUE', physicalColumn.isUnique !== undefined ? (physicalColumn.isUnique ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (physicalColumn.isVirtualColumn !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_VIRTUAL_COLUMN', physicalColumn.isVirtualColumn !== undefined ? (physicalColumn.isVirtualColumn ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (physicalColumn.isMultiValueField !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_MULITIVALUE_MAPPING_FIELD', physicalColumn.isMultiValueField !== undefined ? (physicalColumn.isMultiValueField ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (physicalColumn.isPhysicalColumnMandatory !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_PHYSICAL_COLUMN_MANDATORY', physicalColumn.isPhysicalColumnMandatory !== undefined ?
            (physicalColumn.isPhysicalColumnMandatory ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, physicalColumn.name, configitemtype_enum_1.ConfigItemTypes.ENTITYCOLUMN, physicalColumn.projectId, createdBy, physicalColumn.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const physicalColumnPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(physicalColumn.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(physicalColumn.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(physicalColumn.privileges, physicalColumnPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, physicalColumnPrivileges);
};
exports.parseConfigToPhysicalColumn = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.PhysicalColumn(details.get('IS_KEY'), details.get('ISDISPLAYCOLUMN'), details.get('ISPRIMARYKEY'), details.get('DATATYPE'), details.get('DBCODE'), details.get('LENGTH'), details.get('DBTYPE'), details.get('JSONNAME'), details.get('IS_LOGICAL_COLUMN_REQUIRED'), details.get('IS_UNIQUE'), details.get('IS_VIRTUAL_COLUMN'), details.get('IS_MULITIVALUE_MAPPING_FIELD'), details.get('IS_PHYSICAL_COLUMN_MANDATORY'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=physicalcolumn.parser.js.map