"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = __importDefault(require("../base"));
const common_schema_1 = __importDefault(require("../common.schema"));
const PhysicalColumn = `

input PhysicalColumnInput {

    isKey: Boolean
    dbCode: String
    length: Int
    isDisplayColumn: Boolean
    isPrimaryKey: Boolean
    dataType: String
    dbType: String
    jsonName: String
    isLogicalColumnRequired: Boolean
    isUnique: Boolean
    isVirtualColumn: Boolean
    isMultiValueField: Boolean
    isPhysicalColumnMandatory:Boolean
    configObjectId: ID
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilegeInput]
    parentRelations : [ConfigItemRelationInput]
    childRelations : [ConfigItemRelationInput]
}

extend type Query {
    PhysicalColumn(id: ID!): PhysicalColumn
   }

extend type Mutation {
    createPhysicalColumn (input: PhysicalColumnInput): PhysicalColumn
}

type PhysicalColumn {

    isKey: Boolean
    dbCode: String
    length: Int
    isDisplayColumn: Boolean
    isPrimaryKey: Boolean
    dataType: String
    dbType: String
    jsonName: String
    isLogicalColumnRequired: Boolean
    isUnique: Boolean
    isVirtualColumn: Boolean
    isMultiValueField: Boolean
   isPhysicalColumnMandatory: Boolean
    configObjectId: ID
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilege]
    parentRelations : [ConfigItemRelation]
    childRelations : [ConfigItemRelation]
}
`;
exports.default = () => [PhysicalColumn, base_1.default, common_schema_1.default];
//# sourceMappingURL=schema.js.map