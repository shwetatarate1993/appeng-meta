import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { PhysicalColumn } from '../models';
export declare const parsePhysicalColumnToConfig: (physicalColumn: PhysicalColumn) => ConfigMetadata;
export declare const parseConfigToPhysicalColumn: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => PhysicalColumn;
//# sourceMappingURL=physicalcolumn.parser.d.ts.map