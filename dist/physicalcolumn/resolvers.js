"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const physicalcolumn_parser_1 = require("./physicalcolumn.parser");
exports.Query = {
    PhysicalColumn: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, physicalcolumn_parser_1.parseConfigToPhysicalColumn),
};
exports.Mutation = {
    createPhysicalColumn: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, physicalcolumn_parser_1.parsePhysicalColumnToConfig, physicalcolumn_parser_1.parseConfigToPhysicalColumn);
    },
};
//# sourceMappingURL=resolvers.js.map