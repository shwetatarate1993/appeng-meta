"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const dboperation_parser_1 = require("./dboperation.parser");
exports.Query = {
    DBOperation: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, dboperation_parser_1.parseConfigToDBOperation),
};
exports.Mutation = {
    createDBOperation: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, dboperation_parser_1.parseDBOperationToConfig, dboperation_parser_1.parseConfigToDBOperation);
    },
};
//# sourceMappingURL=resolvers.js.map