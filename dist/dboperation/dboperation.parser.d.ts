import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { DBOperation } from '../models';
export declare const parseDBOperationToConfig: (dbOperation: DBOperation) => ConfigMetadata;
export declare const parseConfigToDBOperation: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => DBOperation;
//# sourceMappingURL=dboperation.parser.d.ts.map