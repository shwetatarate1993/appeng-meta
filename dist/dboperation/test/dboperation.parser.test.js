"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const dboperation_parser_1 = require("../dboperation.parser");
const mockdata_1 = require("./mockdata");
test('parser DBOperation object to config', () => {
    const dboperation = models_1.DBOperation.deserialize(mockdata_1.dbOperationMockData[0]);
    const result = dboperation_parser_1.parseDBOperationToConfig(dboperation);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItem.configObjectType).toEqual('DBOperation');
});
test('parser config to DBOperation object', () => {
    const result = dboperation_parser_1.parseConfigToDBOperation(mockdata_1.configitemMockDataDBOperation[0], mockdata_1.dbOperationPropertyMockData, mockdata_1.configRelationsMockDBOperation, mockdata_1.configPrivilegesMockDataDBOperation);
    expect(result).toHaveProperty('selectId');
    expect(result).toHaveProperty('mode', '0');
    expect(result).toHaveProperty('gridSelectId');
    expect(result).toHaveProperty('filteredGridSelectQuery');
    expect(result).toHaveProperty('workAreaSessName', 'PRIMARY_MD');
    expect(result).toHaveProperty('customCss');
    expect(result).toHaveProperty('type', 'CUSTOM');
    expect(result.privileges.length === 0).toBe(true);
    expect(result.parentRelations.length === 1).toBe(true);
    expect(result.childRelations.length === 0).toBe(true);
    expect(result.parentRelations[0].relationType === 'LogicalEntity_DBOperation').toBe(true);
});
//# sourceMappingURL=dboperation.parser.test.js.map