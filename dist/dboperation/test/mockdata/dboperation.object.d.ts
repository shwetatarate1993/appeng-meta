declare const _default: {
    configObjectId: string;
    configObjectType: string;
    name: string;
    mode: string;
    selectId: string;
    gridSelectId: string;
    filteredGridSelectQuery: string;
    workAreaSessName: string;
    customCss: string;
    type: string;
    parentRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: any;
        deletionDate: any;
    }[];
}[];
export default _default;
//# sourceMappingURL=dboperation.object.d.ts.map