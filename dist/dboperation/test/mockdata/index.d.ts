export { default as dbOperationMockData } from './dboperation.object';
export { default as configRelationsMockDBOperation } from './configitem.relation.dboperation';
export { default as dbOperationPropertyMockData } from './configitem.property.dboperation';
export { default as configitemMockDataDBOperation } from './configitem.dboperation';
export { default as configPrivilegesMockDataDBOperation } from './configitem.privileges.dboperation';
//# sourceMappingURL=index.d.ts.map