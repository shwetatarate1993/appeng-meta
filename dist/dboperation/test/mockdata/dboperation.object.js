"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: '422b87d3-9038-4595-81e3-2be3ba70afa2',
        configObjectType: 'DBOperation',
        name: 'GRAPH_CHILD_DATA',
        mode: '0',
        selectId: 'SELECT IT.ITEMNAME as title, IT.ITEMID as primaryKey,'
            + '3476614c-f5ae-4dc2-963f-67f1a30371f1\' as logicalEntityId'
            + 'FROM  CONFIGITEM IT, CONFIGITEMRELATION REL'
            + 'WHERE IT.ITEMID = REL.CHILDITEMID'
            + 'AND REL.RELATIONTYPE = \'LogicalEntity_EntityColumn'
            + 'AND REL.PARENTITEMID = #{ MLE_ITEMID, jdbcType=VARCHAR }',
        gridSelectId: '',
        filteredGridSelectQuery: '',
        workAreaSessName: 'PRIMARY_MD',
        customCss: '',
        type: 'CUSTOM',
        parentRelations: [
            {
                relationId: '033ebb93-598c-4c8c-b450-2a22c9c6a6c2',
                relationType: 'LogicalEntity_DBOperation',
                parentItemId: '1d0e9bc2-3534-4405-b63a-bb3eac38e53e',
                createdBy: '1124',
                isDeleted: 0,
                creationDate: '2018-02-26T00:00:00.000Z',
                updatedBy: '1115',
                updationDate: null,
                deletionDate: null,
            },
        ],
    },
];
//# sourceMappingURL=dboperation.object.js.map