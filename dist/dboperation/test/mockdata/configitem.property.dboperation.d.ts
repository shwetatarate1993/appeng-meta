declare const _default: {
    propertyId: string;
    propertyName: string;
    propertyValue: string;
    itemId: string;
    createdBy: any;
    isDeleted: number;
    creationDate: any;
    updatedBy: any;
    updationDate: any;
    deletionDate: any;
}[];
export default _default;
//# sourceMappingURL=configitem.property.dboperation.d.ts.map