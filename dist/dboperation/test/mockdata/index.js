"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var dboperation_object_1 = require("./dboperation.object");
exports.dbOperationMockData = dboperation_object_1.default;
var configitem_relation_dboperation_1 = require("./configitem.relation.dboperation");
exports.configRelationsMockDBOperation = configitem_relation_dboperation_1.default;
var configitem_property_dboperation_1 = require("./configitem.property.dboperation");
exports.dbOperationPropertyMockData = configitem_property_dboperation_1.default;
var configitem_dboperation_1 = require("./configitem.dboperation");
exports.configitemMockDataDBOperation = configitem_dboperation_1.default;
var configitem_privileges_dboperation_1 = require("./configitem.privileges.dboperation");
exports.configPrivilegesMockDataDBOperation = configitem_privileges_dboperation_1.default;
//# sourceMappingURL=index.js.map