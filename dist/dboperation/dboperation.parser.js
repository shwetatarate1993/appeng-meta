"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseDBOperationToConfig = (dbOperation) => {
    const createdAt = dbOperation.creationDate;
    const createdBy = dbOperation.createdBy;
    const updatedBy = dbOperation.updatedBy;
    const updatedAt = dbOperation.updationDate;
    const property = [];
    let itemId;
    if (dbOperation.configObjectId !== undefined && dbOperation.configObjectId !== ''
        && dbOperation.configObjectId !== null) {
        itemId = dbOperation.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (dbOperation.mode !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'MODE', dbOperation.mode, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (dbOperation.selectId !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'QUERY', dbOperation.selectId, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (dbOperation.gridSelectId !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'GRID_QUERY', dbOperation.gridSelectId, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (dbOperation.filteredGridSelectQuery !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'FILTER_QUERY', dbOperation.filteredGridSelectQuery, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (dbOperation.workAreaSessName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'DATASOURCE_NAME', dbOperation.workAreaSessName, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (dbOperation.customCss !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'CUSTOM_CSS', dbOperation.customCss, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (dbOperation.type !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'TYPE', dbOperation.type, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, dbOperation.name, configitemtype_enum_1.ConfigItemTypes.DBOPERATION, dbOperation.projectId, createdBy, dbOperation.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const dbOperationPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(dbOperation.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(dbOperation.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(dbOperation.privileges, dbOperationPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, dbOperationPrivileges);
};
exports.parseConfigToDBOperation = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.DBOperation(details.get('MODE'), details.get('QUERY'), details.get('GRID_QUERY'), details.get('FILTER_QUERY'), details.get('DATASOURCE_NAME'), details.get('CUSTOM_CSS'), details.get('TYPE'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=dboperation.parser.js.map