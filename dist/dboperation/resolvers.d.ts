export declare const Query: {
    DBOperation: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").DBOperation>;
};
export declare const Mutation: {
    createDBOperation: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map