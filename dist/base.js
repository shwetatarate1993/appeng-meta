"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Base = `
type Query {
    dummy: Boolean
}

type Mutation {
    dummy: Boolean
}

type Meta {
    count: Int
}

scalar Url
scalar Date
`;
exports.default = () => [Base];
//# sourceMappingURL=base.js.map