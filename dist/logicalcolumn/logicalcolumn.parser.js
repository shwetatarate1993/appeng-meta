"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseLogicalColumnToConfig = (logicalColumn) => {
    const createdAt = logicalColumn.creationDate;
    const createdBy = logicalColumn.createdBy;
    const updatedBy = logicalColumn.updatedBy;
    const updatedAt = logicalColumn.updationDate;
    const property = [];
    let itemId;
    if (logicalColumn.configObjectId !== undefined && logicalColumn.configObjectId !== ''
        && logicalColumn.configObjectId !== null) {
        itemId = logicalColumn.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (logicalColumn.isPrimaryKey !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ISPRIMARYKEY', logicalColumn.isPrimaryKey !== undefined ? (logicalColumn.isPrimaryKey ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (logicalColumn.isDisplayColumn !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ISDISPLAYCOLUMN', logicalColumn.isDisplayColumn !== undefined ? (logicalColumn.isDisplayColumn ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (logicalColumn.dataType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('DATATYPE', logicalColumn.dataType, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (logicalColumn.isVerticalFieldKey !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_VERTICAL_FIELD_KEY', logicalColumn.isVerticalFieldKey !== undefined ? (logicalColumn.isVerticalFieldKey ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (logicalColumn.dbCode !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('DBCODE', logicalColumn.dbCode, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (logicalColumn.length !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('LENGTH', logicalColumn.length, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (logicalColumn.dbType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('DBTYPE', logicalColumn.dbType, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (logicalColumn.isMandatory !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_MANDATORY', logicalColumn.isMandatory !== undefined ? (logicalColumn.isMandatory ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (logicalColumn.jsonName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('JSONNAME', logicalColumn.jsonName, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (logicalColumn.mode !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('MODE', logicalColumn.mode, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (logicalColumn.isUnique !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_UNIQUE', logicalColumn.isUnique !== undefined ? (logicalColumn.isUnique ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (logicalColumn.isDerived !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ISDERIVED', logicalColumn.isDerived !== undefined ? (logicalColumn.isDerived ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, logicalColumn.name, configitemtype_enum_1.ConfigItemTypes.ENTITYCOLUMN, logicalColumn.projectId, createdBy, logicalColumn.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const logicalColumnPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(logicalColumn.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(logicalColumn.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(logicalColumn.privileges, logicalColumnPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, logicalColumnPrivileges);
};
exports.parseConfigToLogicalColumn = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.LogicalColumn(details.get('ISPRIMARYKEY'), details.get('ISDISPLAYCOLUMN'), details.get('DATATYPE'), details.get('IS_VERTICAL_FIELD_KEY'), details.get('DBCODE'), details.get('LENGTH'), details.get('DBTYPE'), details.get('IS_MANDATORY'), details.get('JSONNAME'), details.get('MODE'), details.get('IS_UNIQUE'), details.get('ISDERIVED'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=logicalcolumn.parser.js.map