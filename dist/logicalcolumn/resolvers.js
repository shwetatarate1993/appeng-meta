"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const databasevalidation_parser_1 = require("../databasevalidation/databasevalidation.parser");
const standardvalidation_parser_1 = require("../standardvalidation/standardvalidation.parser");
const logicalcolumn_parser_1 = require("./logicalcolumn.parser");
exports.Query = {
    LogicalColumn: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, logicalcolumn_parser_1.parseConfigToLogicalColumn),
};
exports.LogicalColumn = {
    standardValidations: async (logicalColumn, _, context) => {
        const standardValidation = await common_meta_1.configService.fetchConfigsByParentIdAndRelationType(logicalColumn.configObjectId, relationtype_enum_1.RelationType.ENTITYCOLUMN_VALIDATIONOBJECT, standardvalidation_parser_1.parseConfigToStandardValidation);
        if (standardValidation !== null && standardValidation !== undefined) {
            return standardValidation.filter((standardVal) => standardVal.configObjectType === configitemtype_enum_1.ConfigItemTypes.STANDARDVALIDATION);
        }
    },
    databaseValidations: async (logicalColumn, _, context) => {
        const databaseValidation = await common_meta_1.configService.fetchConfigsByParentIdAndRelationType(logicalColumn.configObjectId, relationtype_enum_1.RelationType.ENTITYCOLUMN_VALIDATIONOBJECT, databasevalidation_parser_1.parseConfigToDatabaseValidation);
        if (databaseValidation !== null && databaseValidation !== undefined) {
            return databaseValidation.filter((databaseVal) => databaseVal.configObjectType === configitemtype_enum_1.ConfigItemTypes.DATABASEVALIDATION);
        }
    },
    sourceColumn: async (logicalColumn, _, context) => {
        const sourceColumn = logicalColumn.isDerived ?
            await common_meta_1.configService.fetchConfigsByParentIdAndRelationType(logicalColumn.configObjectId, relationtype_enum_1.RelationType.ENTITYCOLUMN_ENTITYCOLUMN, logicalcolumn_parser_1.parseConfigToLogicalColumn) : null;
        return sourceColumn ? sourceColumn[0] : null;
    },
};
exports.Mutation = {
    createLogicalColumn: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, logicalcolumn_parser_1.parseLogicalColumnToConfig, logicalcolumn_parser_1.parseConfigToLogicalColumn);
    },
};
//# sourceMappingURL=resolvers.js.map