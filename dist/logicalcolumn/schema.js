"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = __importDefault(require("../base"));
const common_schema_1 = __importDefault(require("../common.schema"));
const LogicalColumn = `

input LogicalColumnInput {

    isPrimaryKey : Boolean
    isDisplayColumn : Boolean
    dataType : String
    isVerticalFieldKey : Boolean
    dbCode : String
    length : Int
    dbType: String
    isMandatory : Boolean
    jsonName: String
    mode: String
    isUnique: Boolean
    isDerived: Boolean
    configObjectId: ID
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilegeInput]
    parentRelations : [ConfigItemRelationInput]
    childRelations : [ConfigItemRelationInput]
}

extend type Query {
    LogicalColumn(id: ID!): LogicalColumn
   }

extend type Mutation {
    createLogicalColumn (input: LogicalColumnInput): LogicalColumn
}

type LogicalColumn {

    isPrimaryKey : Boolean
    isDisplayColumn : Boolean
    dataType : String
    isVerticalFieldKey : Boolean
    dbCode : String
    length : Int
    dbType: String
    isMandatory : Boolean
    jsonName: String
    mode: String
    isUnique: Boolean
    isDerived: Boolean
    configObjectId: ID
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilege]
    parentRelations : [ConfigItemRelation]
    childRelations : [ConfigItemRelation]
    sourceColumn : LogicalColumn
    standardValidations : [StandardValidation]
    databaseValidations : [DatabaseValidation]
}
`;
exports.default = () => [LogicalColumn, base_1.default, common_schema_1.default];
//# sourceMappingURL=schema.js.map