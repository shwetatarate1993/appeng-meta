import { DatabaseValidation, StandardValidation } from '../models';
export declare const Query: {
    LogicalColumn: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").LogicalColumn>;
};
export declare const LogicalColumn: {
    standardValidations: (logicalColumn: any, _: any, context: any) => Promise<StandardValidation[]>;
    databaseValidations: (logicalColumn: any, _: any, context: any) => Promise<DatabaseValidation[]>;
    sourceColumn: (logicalColumn: any, _: any, context: any) => Promise<import("../models").LogicalColumn>;
};
export declare const Mutation: {
    createLogicalColumn: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map