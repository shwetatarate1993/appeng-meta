"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const logicalcolumn_parser_1 = require("../logicalcolumn.parser");
const mockdata_1 = require("./mockdata");
test('parse LogicalColumn object to config', () => {
    const logicalColumn = models_1.LogicalColumn.deserialize(mockdata_1.logicalColumnMockData[0]);
    expect(logicalColumn.parentRelations.length === 3).toBe(true);
    expect(logicalColumn.childRelations.length === 0).toBe(true);
    expect(logicalColumn.dbCode).toEqual('EMPLOYEE_NAME');
    expect(logicalColumn.configObjectType).toEqual('EntityColumn');
    const result = logicalcolumn_parser_1.parseLogicalColumnToConfig(logicalColumn);
    expect(result.configItem.configObjectType).toEqual('EntityColumn');
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
});
test('parse config to LogicalColumn object', () => {
    const result = logicalcolumn_parser_1.parseConfigToLogicalColumn(mockdata_1.configitemMockDataLogicalColumn[0], mockdata_1.logicalColumnPropertyMockData, mockdata_1.logicalColumnRelationMockData, mockdata_1.logicalColumnPrivilegeMockData);
    expect(result).toHaveProperty('isPrimaryKey', false);
    expect(result).toHaveProperty('isVerticalFieldKey', false);
    expect(result).toHaveProperty('isMandatory', true);
    expect(result).toHaveProperty('dbCode', 'EMPLOYEE_NAME');
    expect(result).toHaveProperty('length', 128);
    expect(result).toHaveProperty('dataType', 'VARCHAR');
    expect(result).toHaveProperty('dbType');
    expect(result.privileges.length === 0).toBe(true);
    expect(result.parentRelations.length === 3).toBe(true);
    expect(result.childRelations.length === 0).toBe(true);
    expect(result.parentRelations[0].relationType === 'FormField_EntityColumn').toBe(true);
    expect(result.parentRelations[1].relationType === 'LogicalEntity_EntityColumn').toBe(true);
    expect(result.parentRelations[2].relationType === 'DataGridColumn_EntityColumn').toBe(true);
});
//# sourceMappingURL=logicalcolumn.parser.test.js.map