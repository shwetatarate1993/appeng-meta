"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        propertyId: '148c0c4f-441b-4e7a-b48b-8140450e823f',
        propertyName: 'ISPRIMARYKEY',
        propertyValue: false,
        itemId: '6ad8e5e5-4f29-474f-b336-535449f522a5',
        createdBy: null,
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '1cf4ce8a-74d4-4db1-aa60-87d4f79c34d2',
        propertyName: 'IS_VERTICAL_FIELD_KEY',
        propertyValue: false,
        itemId: '6ad8e5e5-4f29-474f-b336-535449f522a5',
        createdBy: '1135',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '6a8fa48c-f62a-41d3-870c-fe25ab28f3c8',
        propertyName: 'IS_MANDATORY',
        propertyValue: true,
        itemId: '6ad8e5e5-4f29-474f-b336-535449f522a5',
        createdBy: '1135',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '741188c7-284d-4184-8ab8-41dd081d13d3',
        propertyName: 'DBCODE',
        propertyValue: 'EMPLOYEE_NAME',
        itemId: '6ad8e5e5-4f29-474f-b336-535449f522a5',
        createdBy: '1135',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: 'ac135663-ef83-43a8-bde0-5e86532c28c4',
        propertyName: 'LENGTH',
        propertyValue: 128,
        itemId: '6ad8e5e5-4f29-474f-b336-535449f522a5',
        createdBy: '1111',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: 'cb732eee-3979-4bc5-b901-41b702b23317',
        propertyName: 'DATATYPE',
        propertyValue: 'VARCHAR',
        itemId: '6ad8e5e5-4f29-474f-b336-535449f522a5',
        createdBy: '1135',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '248c0c4f-441b-4e7a-b48b-8140450e823f',
        propertyName: 'DBTYPE',
        propertyValue: '',
        itemId: '6ad8e5e5-4f29-474f-b336-535449f522a5',
        createdBy: null,
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
];
//# sourceMappingURL=configitem.property.logicalcolumn.js.map