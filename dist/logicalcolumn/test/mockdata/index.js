"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var logicalcolumn_1 = require("./logicalcolumn");
exports.logicalColumnMockData = logicalcolumn_1.default;
var configitem_logicalcolumn_1 = require("./configitem.logicalcolumn");
exports.configitemMockDataLogicalColumn = configitem_logicalcolumn_1.default;
var configitem_property_logicalcolumn_1 = require("./configitem.property.logicalcolumn");
exports.logicalColumnPropertyMockData = configitem_property_logicalcolumn_1.default;
var configitem_relation_logicalcolumn_1 = require("./configitem.relation.logicalcolumn");
exports.logicalColumnRelationMockData = configitem_relation_logicalcolumn_1.default;
var configitem_privilege_logicalcolumn_1 = require("./configitem.privilege.logicalcolumn");
exports.logicalColumnPrivilegeMockData = configitem_privilege_logicalcolumn_1.default;
//# sourceMappingURL=index.js.map