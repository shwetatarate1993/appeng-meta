export { default as logicalColumnMockData } from './logicalcolumn';
export { default as configitemMockDataLogicalColumn } from './configitem.logicalcolumn';
export { default as logicalColumnPropertyMockData } from './configitem.property.logicalcolumn';
export { default as logicalColumnRelationMockData } from './configitem.relation.logicalcolumn';
export { default as logicalColumnPrivilegeMockData } from './configitem.privilege.logicalcolumn';
//# sourceMappingURL=index.d.ts.map