declare const _default: {
    configObjectId: string;
    configObjectType: string;
    name: string;
    isPrimaryKey: boolean;
    dataType: string;
    isVerticalFieldKey: boolean;
    dbCode: string;
    length: number;
    isMandatory: boolean;
    privileges: any[];
    parentRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        childItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: any;
        updatedBy: string;
        updationDate: any;
        deletionDate: any;
    }[];
    childRelations: any[];
}[];
export default _default;
//# sourceMappingURL=logicalcolumn.d.ts.map