import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { LogicalColumn } from '../models';
export declare const parseLogicalColumnToConfig: (logicalColumn: LogicalColumn) => ConfigMetadata;
export declare const parseConfigToLogicalColumn: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => LogicalColumn;
//# sourceMappingURL=logicalcolumn.parser.d.ts.map