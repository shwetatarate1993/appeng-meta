"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const relationtype_enum_1 = require("../constants/relationtype.enum");
exports.getupdatedRelations = (relations, allRelations, configItem, relationType) => {
    const newUpdatedRelations = [];
    if (relations !== undefined && relations !== null) {
        relations.forEach((relation) => {
            const newRel = new Map();
            if (relation.relationId === undefined ||
                relation.relationId === null || relation.relationId === '') {
                newRel.set('relationId', uuid_1.v4());
            }
            if (relationtype_enum_1.RelationType.PARENT_RELATIONS === relationType) {
                newRel.set('childItemId', configItem.configObjectId);
            }
            else {
                newRel.set('parentItemId', configItem.configObjectId);
            }
            newRel.set('creationDate', configItem.creationDate);
            newRel.set('updationDate', configItem.updationDate);
            newRel.set('isDeleted', 0);
            newRel.set('createdBy', configItem.createdBy);
            newRel.set('updatedBy', configItem.updatedBy);
            newUpdatedRelations.push(meta_db_1.ConfigItemRelation.clone(relation, newRel));
        });
        allRelations.push.apply(allRelations, newUpdatedRelations);
    }
};
exports.getUpdatedPrivileges = (inputPrivileges, updatedPrivileges, configItem) => {
    if (inputPrivileges !== undefined && inputPrivileges !== null
        && inputPrivileges.length > 0) {
        inputPrivileges.forEach((privilege) => {
            const newPriv = new Map();
            if (privilege.privilegeId === undefined ||
                privilege.privilegeId === null || privilege.privilegeId === '') {
                newPriv.set('privilegeId', uuid_1.v4());
            }
            newPriv.set('itemId', configItem.configObjectId);
            newPriv.set('creationDate', configItem.creationDate);
            newPriv.set('updationDate', configItem.updationDate);
            newPriv.set('isDeleted', 0);
            newPriv.set('createdBy', configItem.createdBy);
            newPriv.set('updatedBy', configItem.updatedBy);
            updatedPrivileges.push(meta_db_1.ConfigItemPrivilege.clone(privilege, newPriv));
        });
    }
};
//# sourceMappingURL=utilities.parser.js.map