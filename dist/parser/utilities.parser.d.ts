import { ConfigItemModel, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
import { RelationType } from '../constants/relationtype.enum';
export declare const getupdatedRelations: (relations: ConfigItemRelation[], allRelations: ConfigItemRelation[], configItem: ConfigItemModel, relationType: RelationType) => void;
export declare const getUpdatedPrivileges: (inputPrivileges: ConfigItemPrivilege[], updatedPrivileges: ConfigItemPrivilege[], configItem: ConfigItemModel) => void;
//# sourceMappingURL=utilities.parser.d.ts.map