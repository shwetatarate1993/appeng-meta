"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseDatabaseToConfig = (database) => {
    const createdAt = database.creationDate;
    const createdBy = database.createdBy;
    const updatedBy = database.updatedBy;
    const updatedAt = database.updationDate;
    const property = [];
    let itemId;
    if (database.configObjectId !== undefined && database.configObjectId !== ''
        && database.configObjectId !== null) {
        itemId = database.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (database.engine !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ENGINE', database.engine, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (database.details !== undefined) {
        if (database.details.host !== undefined) {
            property.push(new meta_db_1.ConfigItemProperty('HOST', database.details.host, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
        }
        if (database.details.port !== undefined) {
            property.push(new meta_db_1.ConfigItemProperty('PORT', database.details.port, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
        }
        if (database.details.dbname !== undefined) {
            property.push(new meta_db_1.ConfigItemProperty('DB_NAME', database.details.dbname, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
        }
        if (database.details.user !== undefined) {
            property.push(new meta_db_1.ConfigItemProperty('USER', database.details.user, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
        }
        if (database.details.password !== undefined) {
            property.push(new meta_db_1.ConfigItemProperty('PASSWORD', database.details.password, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
        }
    }
    if (database.features !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('FEATURES', database.features, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, database.name, 'Database', database.projectId, createdBy, database.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const databasePrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(database.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(database.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(database.privileges, databasePrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, databasePrivileges);
};
exports.parseConfigToDatabase = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.Database(configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, details.get('ENGINE'), details.get('FEATURES'), details.get('HOST'), details.get('PORT'), details.get('DB_NAME'), details.get('USER'), details.get('PASSWORD'), privileges, childRelations, parentRelations);
};
//# sourceMappingURL=database.parser.js.map