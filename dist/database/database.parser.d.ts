import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { Database } from '../models';
export declare const parseDatabaseToConfig: (database: Database) => ConfigMetadata;
export declare const parseConfigToDatabase: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => Database;
//# sourceMappingURL=database.parser.d.ts.map