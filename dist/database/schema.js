"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = __importDefault(require("../base"));
const common_schema_1 = __importDefault(require("../common.schema"));
const Database = `
input ConnectionDetailsInput {
    host:String,
    port:Int,
    dbname:String,
    user:String,
    password:String,
}

input DatabaseInput{

    configObjectId: ID
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    engine: String
    details : ConnectionDetailsInput
    features: String
    privileges : [ConfigItemPrivilegeInput]
    parentRelations : [ConfigItemRelationInput]
    childRelations : [ConfigItemRelationInput]
}

extend type Mutation {
    createDatabase (input: DatabaseInput): Database
}

extend type Query {
    Database(id: ID!): Database
}

type Database {
    configObjectId: ID
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    engine: String
    details : ConnectionDetails
    features: String
    privileges : [ConfigItemPrivilege]
    parentRelations : [ConfigItemRelation]
    childRelations : [ConfigItemRelation]
}

type ConnectionDetails {
    host:String,
    port:Int,
    dbname:String,
    user:String,
    password:String,
}
`;
exports.default = () => [Database, base_1.default, common_schema_1.default];
//# sourceMappingURL=schema.js.map