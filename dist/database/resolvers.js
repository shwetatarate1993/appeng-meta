"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const database_parser_1 = require("./database.parser");
exports.Query = {
    Database: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, database_parser_1.parseConfigToDatabase),
};
exports.Mutation = {
    createDatabase: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, database_parser_1.parseDatabaseToConfig, database_parser_1.parseConfigToDatabase);
    },
};
//# sourceMappingURL=resolvers.js.map