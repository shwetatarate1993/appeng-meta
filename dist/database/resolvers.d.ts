export declare const Query: {
    Database: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").Database>;
};
export declare const Mutation: {
    createDatabase: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map