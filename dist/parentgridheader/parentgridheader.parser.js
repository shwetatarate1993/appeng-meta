"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseParentGridHeaderObjectToConfig = (parentGridHeader) => {
    const createdAt = parentGridHeader.creationDate;
    const createdBy = parentGridHeader.createdBy;
    const updatedBy = parentGridHeader.updatedBy;
    const updatedAt = parentGridHeader.updationDate;
    const property = [];
    let itemId;
    if (parentGridHeader.configObjectId !== undefined && parentGridHeader.configObjectId !== ''
        && parentGridHeader.configObjectId !== null) {
        itemId = parentGridHeader.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (parentGridHeader.order !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ORDER', parentGridHeader.order, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (parentGridHeader.headerName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('HEADER_NAME', parentGridHeader.headerName, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, parentGridHeader.name, configitemtype_enum_1.ConfigItemTypes.PARENTGRIDHEADER, parentGridHeader.projectId, createdBy, parentGridHeader.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const ParentGridHeaderPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(parentGridHeader.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(parentGridHeader.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(parentGridHeader.privileges, ParentGridHeaderPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, ParentGridHeaderPrivileges);
};
exports.parseConfigToParentGridHeaderObject = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.ParentGridHeader(configItem.configObjectId, details.get('ORDER'), details.get('HEADER_NAME'), configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=parentgridheader.parser.js.map