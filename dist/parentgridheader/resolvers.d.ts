export declare const Query: {
    ParentGridHeader: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").ParentGridHeader>;
};
export declare const ParentGridHeader: {
    parentGridHeader: (parentGridHeader: any, _: any, context: any) => Promise<import("../models").ParentGridHeader[]>;
};
export declare const Mutation: {
    createParentGridHeader: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map