"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const parentgridheader_parser_1 = require("../parentgridheader.parser");
const mockdata_1 = require("./mockdata");
test('parser parentgridheader object to config', () => {
    const parentGridHeader = models_1.ParentGridHeader.deserialize(mockdata_1.parentGridHeaderMockData[0]);
    const result = parentgridheader_parser_1.parseParentGridHeaderObjectToConfig(parentGridHeader);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(parentGridHeader.configObjectType).toEqual('ParentGridHeader');
});
test('parser config to parentgridheader object', () => {
    const result = parentgridheader_parser_1.parseConfigToParentGridHeaderObject(mockdata_1.parentGridHeaderConfigItemMockData[0], mockdata_1.parentGridHeaderPropertyMockData, mockdata_1.parentGridHeaderRelationMockData, mockdata_1.parentGridHeaderPrivilegesMockData);
    expect(result).toHaveProperty('order', 14);
    expect(result).toHaveProperty('headerName', 'Total Demand');
});
//# sourceMappingURL=parentgridheader.parser.test.js.map