declare const _default: {
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: any;
    isDeleted: number;
    itemDescription: string;
    creationDate: any;
    projectId: number;
    updatedBy: any;
    updationDate: any;
    deletionDate: any;
}[];
export default _default;
//# sourceMappingURL=configitem.parentgridheader.d.ts.map