"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: '0139c285-c652-44af-9138-9222d2ad80ad',
        configObjectType: 'ParentGridHeader',
        name: 'Total Demand',
        order: '14',
        headerName: 'English:Total Demand;ગુજરાતી:કુલ માગણુ',
        privileges: [],
        parentRelations: [
            {
                relationId: 'd55af015-4258-4b2a-b6c7-7ecedcdbb11c',
                relationType: 'ReportObject_ParentGridHeader',
                parentItemId: '3bfe4a43-1946-4b3a-b9ef-7b8629c77be0',
                childItemId: '0139c285-c652-44af-9138-9222d2ad80ad',
                createdBy: '1115',
                isDeleted: 0,
                creationDate: '2018-02-26T00:00:00.000Z',
                updatedBy: '1115',
                updationDate: '2018-04-27T00:00:00.000Z',
                deletionDate: '2018-04-27T00:00:00.000Z',
            },
        ],
    },
];
//# sourceMappingURL=parentgridheader.object.js.map