"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var parentgridheader_object_1 = require("./parentgridheader.object");
exports.parentGridHeaderMockData = parentgridheader_object_1.default;
var configitem_parentgridheader_1 = require("./configitem.parentgridheader");
exports.parentGridHeaderConfigItemMockData = configitem_parentgridheader_1.default;
var property_configitem_parentgridheader_1 = require("./property.configitem.parentgridheader");
exports.parentGridHeaderPropertyMockData = property_configitem_parentgridheader_1.default;
var privileges_configitem_parentgridheader_1 = require("./privileges.configitem.parentgridheader");
exports.parentGridHeaderPrivilegesMockData = privileges_configitem_parentgridheader_1.default;
var relation_configitem_parentgridheader_1 = require("./relation.configitem.parentgridheader");
exports.parentGridHeaderRelationMockData = relation_configitem_parentgridheader_1.default;
//# sourceMappingURL=index.js.map