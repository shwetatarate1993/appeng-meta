export { default as parentGridHeaderMockData } from './parentgridheader.object';
export { default as parentGridHeaderConfigItemMockData } from './configitem.parentgridheader';
export { default as parentGridHeaderPropertyMockData } from './property.configitem.parentgridheader';
export { default as parentGridHeaderPrivilegesMockData } from './privileges.configitem.parentgridheader';
export { default as parentGridHeaderRelationMockData } from './relation.configitem.parentgridheader';
//# sourceMappingURL=index.d.ts.map