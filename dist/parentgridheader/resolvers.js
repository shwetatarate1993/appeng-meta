"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const parentgridheader_parser_1 = require("./parentgridheader.parser");
exports.Query = {
    ParentGridHeader: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, parentgridheader_parser_1.parseConfigToParentGridHeaderObject),
};
exports.ParentGridHeader = {
    parentGridHeader: (parentGridHeader, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(parentGridHeader.configObjectId, relationtype_enum_1.RelationType.PARENTGRIDHEADER_PARENTGRIDHEADER, parentgridheader_parser_1.parseConfigToParentGridHeaderObject),
};
exports.Mutation = {
    createParentGridHeader: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, parentgridheader_parser_1.parseParentGridHeaderObjectToConfig, parentgridheader_parser_1.parseConfigToParentGridHeaderObject);
    },
};
//# sourceMappingURL=resolvers.js.map