import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { ParentGridHeader } from '../models';
export declare const parseParentGridHeaderObjectToConfig: (parentGridHeader: ParentGridHeader) => ConfigMetadata;
export declare const parseConfigToParentGridHeaderObject: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => ParentGridHeader;
//# sourceMappingURL=parentgridheader.parser.d.ts.map