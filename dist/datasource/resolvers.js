"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const datasource_parser_1 = require("./datasource.parser");
exports.Query = {
    Datasource: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, datasource_parser_1.parseConfigToDatasource),
};
exports.Mutation = {
    createDatasource: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, datasource_parser_1.parseDatasourceToConfig, datasource_parser_1.parseConfigToDatasource);
    },
};
//# sourceMappingURL=resolvers.js.map