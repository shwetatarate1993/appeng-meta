import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { Datasource } from '../models';
export declare const parseDatasourceToConfig: (datasource: Datasource) => ConfigMetadata;
export declare const parseConfigToDatasource: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => Datasource;
//# sourceMappingURL=datasource.parser.d.ts.map