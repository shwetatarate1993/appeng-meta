"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var datasource_1 = require("./datasource");
exports.datasourceMockData = datasource_1.default;
var configitem_datasource_1 = require("./configitem.datasource");
exports.configitemMockDataDatasource = configitem_datasource_1.default;
var configitem_property_datasource_1 = require("./configitem.property.datasource");
exports.datasourcePropertyMockData = configitem_property_datasource_1.default;
var configitem_relation_datasource_1 = require("./configitem.relation.datasource");
exports.datasourceRelationMockData = configitem_relation_datasource_1.default;
var configitem_privilege_datasource_1 = require("./configitem.privilege.datasource");
exports.datasourcePrivilegeMockData = configitem_privilege_datasource_1.default;
//# sourceMappingURL=index.js.map