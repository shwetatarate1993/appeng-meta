declare const _default: {
    configObjectId: string;
    configObjectType: string;
    name: string;
    displayName: string;
    datasourceType: string;
    lookupKey: string;
    privileges: {
        privilegeType: string;
        itemId: string;
        roleId: number;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: string;
        deletionDate: string;
    }[];
    parentRelations: any[];
}[];
export default _default;
//# sourceMappingURL=datasource.d.ts.map