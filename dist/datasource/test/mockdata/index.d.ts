export { default as datasourceMockData } from './datasource';
export { default as configitemMockDataDatasource } from './configitem.datasource';
export { default as datasourcePropertyMockData } from './configitem.property.datasource';
export { default as datasourceRelationMockData } from './configitem.relation.datasource';
export { default as datasourcePrivilegeMockData } from './configitem.privilege.datasource';
//# sourceMappingURL=index.d.ts.map