"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        propertyId: '12301c10-1b1d-11e9-b2e1-ad14d7833e95',
        propertyName: 'DISPLAY_NAME',
        propertyValue: 'PrimaryDS',
        itemId: '1239d82d-0728-43c7-a228-5e0eba54321',
        createdBy: '1111',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '123410-1b1d-11e9-b2e1-ad14d7833e96',
        propertyName: 'DATASOURCE_TYPE',
        propertyValue: 'DYNAMIC_SQL',
        itemId: '1239d82d-0728-43c7-a228-5e0eba54321',
        createdBy: '1111',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '12301c10-1b1d-11e9-b2e1-ad14d7833e96',
        propertyName: 'LOOKUP_KEY',
        propertyValue: 'PRIMARY',
        itemId: '1239d82d-0728-43c7-a228-5e0eba54321',
        createdBy: '1111',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
];
//# sourceMappingURL=configitem.property.datasource.js.map