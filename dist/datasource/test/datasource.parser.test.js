"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const datasource_parser_1 = require("../datasource.parser");
const mockdata_1 = require("./mockdata");
test('parse datasource object to config', () => {
    const datasource = models_1.Datasource.deserialize(mockdata_1.datasourceMockData[0]);
    expect(datasource.datasourceType).toEqual('DYNAMIC_SQL');
    expect(datasource.configObjectType).toEqual('DataSource');
    const result = datasource_parser_1.parseDatasourceToConfig(datasource);
    expect(result.configItem.configObjectType).toEqual('DataSource');
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
});
test('parse config to datasource object', () => {
    const result = datasource_parser_1.parseConfigToDatasource(mockdata_1.configitemMockDataDatasource[0], mockdata_1.datasourcePropertyMockData, mockdata_1.datasourceRelationMockData, mockdata_1.datasourcePrivilegeMockData);
    expect(result).toHaveProperty('displayName', 'PrimaryDS');
    expect(result).toHaveProperty('datasourceType', 'DYNAMIC_SQL');
    expect(result).toHaveProperty('lookupKey', 'PRIMARY');
    expect(result.privileges.length === 4).toBe(true);
    expect(result.parentRelations.length === 0).toBe(true);
    expect(result.childRelations.length === 0).toBe(true);
});
//# sourceMappingURL=datasource.parser.test.js.map