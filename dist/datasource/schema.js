"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = __importDefault(require("../base"));
const common_schema_1 = __importDefault(require("../common.schema"));
const Datasource = `

input DatasourceInput {

    displayName : String
    datasourceType : String
    lookupKey : String
    configObjectId: ID
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilegeInput]
    parentRelations : [ConfigItemRelationInput]
    childRelations : [ConfigItemRelationInput]
}

extend type Query {
    Datasource(id: ID!): Datasource
   }

extend type Mutation {
    createDatasource (input: DatasourceInput): Datasource
}

type Datasource {
    displayName : String
    datasourceType : String
    lookupKey : String
    configObjectId: ID
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilege]
    parentRelations : [ConfigItemRelation]
    childRelations : [ConfigItemRelation]
}
`;
exports.default = () => [Datasource, base_1.default, common_schema_1.default];
//# sourceMappingURL=schema.js.map