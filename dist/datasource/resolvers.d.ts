export declare const Query: {
    Datasource: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").Datasource>;
};
export declare const Mutation: {
    createDatasource: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map