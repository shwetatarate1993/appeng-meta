"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseDatasourceToConfig = (datasource) => {
    const createdAt = datasource.creationDate;
    const createdBy = datasource.createdBy;
    const updatedBy = datasource.updatedBy;
    const updatedAt = datasource.updationDate;
    const property = [];
    let itemId;
    if (datasource.configObjectId !== undefined && datasource.configObjectId !== ''
        && datasource.configObjectId !== null) {
        itemId = datasource.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (datasource.displayName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'DISPLAY_NAME', datasource.displayName, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (datasource.datasourceType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'DATASOURCE_TYPE', datasource.datasourceType, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (datasource.lookupKey !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'LOOKUP_KEY', datasource.lookupKey, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, datasource.name, configitemtype_enum_1.ConfigItemTypes.DATASOURCE, datasource.projectId, createdBy, datasource.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const datasourcePrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(datasource.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(datasource.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(datasource.privileges, datasourcePrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, datasourcePrivileges);
};
exports.parseConfigToDatasource = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.Datasource(details.get('DISPLAY_NAME'), details.get('DATASOURCE_TYPE'), details.get('LOOKUP_KEY'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=datasource.parser.js.map