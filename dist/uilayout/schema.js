"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = __importDefault(require("../base"));
const common_schema_1 = __importDefault(require("../common.schema"));
const UILayout = `

input UILayoutInput {

    configObjectId: ID
    uiLayoutType: String
    targetDevice: String
    mode:String
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilegeInput]
    parentRelations : [ConfigItemRelationInput]
    childRelations : [ConfigItemRelationInput]
}

extend type Query {
    UILayout(id: ID!): UILayout
   }

extend type Mutation {
    createUILayout (input: UILayoutInput): UILayout
}

type UILayout {
    configObjectId: ID
    uiLayoutType: String
    targetDevice: String
    mode:String
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilege]
    parentRelations : [ConfigItemRelation]
    childRelations : [ConfigItemRelation]
    uICards : [UiCard]
}
`;
exports.default = () => [UILayout, base_1.default, common_schema_1.default];
//# sourceMappingURL=schema.js.map