export declare const Query: {
    UILayout: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").UILayout>;
};
export declare const UILayout: {
    uICards: (uILayout: any, _: any, context: any) => Promise<import("../models").UiCard[]>;
};
export declare const Mutation: {
    createUILayout: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map