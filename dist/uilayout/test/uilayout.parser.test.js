"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const uilayout_parser_1 = require("../uilayout.parser");
const mockdata_1 = require("./mockdata");
test('parser uilayout object to config', () => {
    const uilayout = models_1.UILayout.deserialize(mockdata_1.uiLayoutMockData[0]);
    const result = uilayout_parser_1.parseUILayoutToConfig(uilayout);
    expect(uilayout.targetDevice).toEqual('Web');
    expect(uilayout.uiLayoutType).toEqual('WIZARD');
    expect(uilayout.mode).toEqual('Both');
});
test('parser config to uilayout object', () => {
    const result = uilayout_parser_1.parseConfigToUILayout(mockdata_1.uiLayoutConfigitemMockData[0], mockdata_1.uiLayoutPropertyMockData, mockdata_1.uiLayoutRelationMockData, mockdata_1.uiLayoutPrivilegesMockData);
    expect(result).toHaveProperty('mode');
});
//# sourceMappingURL=uilayout.parser.test.js.map