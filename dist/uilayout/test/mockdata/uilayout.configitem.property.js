"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        propertyId: '1d8c73a6-02e5-4e94-9c10-cc3975e9c451',
        propertyName: 'TARGET_DEVICE',
        propertyValue: 'Web',
        itemId: '2db0c2f9-f356-4b71-8cbc-cdb8e2f46da3',
        createdBy: null,
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '58c223c2-f0d7-4132-b256-9778147fc395',
        propertyName: 'UI_LAYOUT_TYPE',
        propertyValue: 'WIZARD',
        itemId: '2db0c2f9-f356-4b71-8cbc-cdb8e2f46da3',
        createdBy: null,
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '997ad1b2-489d-4ce0-b0ae-d1eaf7ede4f9',
        propertyName: 'MODE',
        propertyValue: 'Both',
        itemId: '2db0c2f9-f356-4b71-8cbc-cdb8e2f46da3',
        createdBy: null,
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
];
//# sourceMappingURL=uilayout.configitem.property.js.map