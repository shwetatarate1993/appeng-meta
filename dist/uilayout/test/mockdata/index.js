"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var uilayout_object_1 = require("./uilayout.object");
exports.uiLayoutMockData = uilayout_object_1.default;
var uilayout_configitem_1 = require("./uilayout.configitem");
exports.uiLayoutConfigitemMockData = uilayout_configitem_1.default;
var uilayout_configitem_property_1 = require("./uilayout.configitem.property");
exports.uiLayoutPropertyMockData = uilayout_configitem_property_1.default;
var uilayout_configitem_relation_1 = require("./uilayout.configitem.relation");
exports.uiLayoutRelationMockData = uilayout_configitem_relation_1.default;
var uilayout_configitem_privileges_1 = require("./uilayout.configitem.privileges");
exports.uiLayoutPrivilegesMockData = uilayout_configitem_privileges_1.default;
//# sourceMappingURL=index.js.map