declare const _default: {
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: any;
    projectId: number;
    updatedBy: any;
    updationDate: any;
    deletionDate: any;
}[];
export default _default;
//# sourceMappingURL=uilayout.configitem.d.ts.map