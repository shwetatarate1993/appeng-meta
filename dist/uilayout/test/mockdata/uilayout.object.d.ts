declare const _default: {
    configObjectId: string;
    uiLayoutType: string;
    targetDevice: string;
    mode: string;
    configObjectType: string;
    name: string;
    projectId: number;
    privileges: any[];
    parentRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        childItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: string;
        deletionDate: string;
    }[];
}[];
export default _default;
//# sourceMappingURL=uilayout.object.d.ts.map