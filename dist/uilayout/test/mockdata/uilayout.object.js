"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: '2db0c2f9-f356-4b71-8cbc-cdb8e2f46da3',
        uiLayoutType: 'WIZARD',
        targetDevice: 'Web',
        mode: 'Both',
        configObjectType: 'UILayout',
        name: 'Modal',
        projectId: -1,
        privileges: [],
        parentRelations: [
            {
                relationId: '16ef391f-04c1-463f-93da-3d5ca197a541',
                relationType: 'CompositeEntity_UILayout',
                parentItemId: '79185363-6fae-4919-8d46-eacdf3a369ce',
                childItemId: '2db0c2f9-f356-4b71-8cbc-cdb8e2f46da3',
                createdBy: '1115',
                isDeleted: 0,
                creationDate: '2018-02-26T00:00:00.000Z',
                updatedBy: '1115',
                updationDate: '2018-04-27T00:00:00.000Z',
                deletionDate: '2018-04-27T00:00:00.000Z',
            },
        ],
    },
];
//# sourceMappingURL=uilayout.object.js.map