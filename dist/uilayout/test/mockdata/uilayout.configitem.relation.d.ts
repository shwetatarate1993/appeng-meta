declare const _default: {
    relationId: string;
    relationType: string;
    parentItemId: string;
    childItemId: string;
    createdBy: any;
    isDeleted: number;
    creationDate: any;
    updatedBy: any;
    updationDate: any;
    deletionDate: any;
}[];
export default _default;
//# sourceMappingURL=uilayout.configitem.relation.d.ts.map