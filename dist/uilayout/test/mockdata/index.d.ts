export { default as uiLayoutMockData } from './uilayout.object';
export { default as uiLayoutConfigitemMockData } from './uilayout.configitem';
export { default as uiLayoutPropertyMockData } from './uilayout.configitem.property';
export { default as uiLayoutRelationMockData } from './uilayout.configitem.relation';
export { default as uiLayoutPrivilegesMockData } from './uilayout.configitem.privileges';
//# sourceMappingURL=index.d.ts.map