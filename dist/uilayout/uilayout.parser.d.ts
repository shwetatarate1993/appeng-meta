import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { UILayout } from '../models';
export declare const parseUILayoutToConfig: (uiLayout: UILayout) => ConfigMetadata;
export declare const parseConfigToUILayout: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => UILayout;
//# sourceMappingURL=uilayout.parser.d.ts.map