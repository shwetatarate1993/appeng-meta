"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const uicard_parser_1 = require("../uicard/uicard.parser");
const uilayout_parser_1 = require("./uilayout.parser");
exports.Query = {
    UILayout: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, uilayout_parser_1.parseConfigToUILayout),
};
exports.UILayout = {
    uICards: (uILayout, _, context) => common_meta_1.configService.fetchConfigsByParentId(uILayout.configObjectId, uicard_parser_1.parseConfigToUiCard),
};
exports.Mutation = {
    createUILayout: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, uilayout_parser_1.parseUILayoutToConfig, uilayout_parser_1.parseConfigToUILayout);
    },
};
//# sourceMappingURL=resolvers.js.map