"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseUILayoutToConfig = (uiLayout) => {
    const createdAt = uiLayout.creationDate;
    const createdBy = uiLayout.createdBy;
    const updatedBy = uiLayout.updatedBy;
    const updatedAt = uiLayout.updationDate;
    const property = [];
    let itemId;
    if (uiLayout.configObjectId !== undefined && uiLayout.configObjectId !== ''
        && uiLayout.configObjectId !== null) {
        itemId = uiLayout.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (uiLayout.uiLayoutType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'UI_LAYOUT_TYPE', uiLayout.uiLayoutType, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (uiLayout.targetDevice !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'TARGET_DEVICE', uiLayout.targetDevice, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (uiLayout.mode !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'MODE', uiLayout.mode, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, uiLayout.name, configitemtype_enum_1.ConfigItemTypes.UILAYOUT, uiLayout.projectId, createdBy, uiLayout.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const uilayoutPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(uiLayout.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(uiLayout.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(uiLayout.privileges, uilayoutPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, uilayoutPrivileges);
};
exports.parseConfigToUILayout = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.UILayout(configItem.configObjectId, details.get('UI_LAYOUT_TYPE'), details.get('TARGET_DEVICE'), details.get('MODE'), configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=uilayout.parser.js.map