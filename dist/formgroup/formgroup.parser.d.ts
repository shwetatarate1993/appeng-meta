import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { FormGroup } from '../models';
export declare const parseFormGroupToConfig: (formGroup: FormGroup) => ConfigMetadata;
export declare const parseConfigToFormGroup: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => FormGroup;
//# sourceMappingURL=formgroup.parser.d.ts.map