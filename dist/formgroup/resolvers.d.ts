export declare const Query: {
    FormGroup: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").FormGroup>;
};
export declare const FormGroup: {
    forms: (formGroup: any, _: any, context: any) => Promise<import("../models").Form[]>;
};
export declare const Mutation: {
    createFormGroup: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map