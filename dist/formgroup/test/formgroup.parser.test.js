"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const formgroup_parser_1 = require("../formgroup.parser");
const mockdata_1 = require("./mockdata");
test('parser Formgroup object to config', () => {
    const formgroup = models_1.FormGroup.deserialize(mockdata_1.formGroupMockData[0]);
    const result = formgroup_parser_1.parseFormGroupToConfig(formgroup);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItem.configObjectType).toEqual('FormGroup');
});
test('parser config to FormGroup object', () => {
    const result = formgroup_parser_1.parseConfigToFormGroup(mockdata_1.configitemMockDataFormGroup[0], mockdata_1.formGroupPropertyMockData, mockdata_1.configRelationsMockFormGroup, mockdata_1.configPrivilegesMockDataFormGroup);
    expect(result).toHaveProperty('readOnlyAccessible', true);
    expect(result).toHaveProperty('formOrder', 'a4f914f5-324b-477f-a6ea-e08111f331c2,380db32b-8537-4b80-8127-30242862d48b');
    expect(result).toHaveProperty('repeatableForms');
    expect(result).toHaveProperty('readOnly', false);
    expect(result).toHaveProperty('formType', 'INSERT_FORM_GROUP');
    expect(result).toHaveProperty('tabbedStructure', true);
    expect(result).toHaveProperty('tabs', 'CARDGROUP');
    expect(result).toHaveProperty('formGroupName');
    expect(result.privileges.length === 4).toBe(true);
    expect(result).toHaveProperty('parentRelations');
    expect(result).toHaveProperty('childRelations');
});
//# sourceMappingURL=formgroup.parser.test.js.map