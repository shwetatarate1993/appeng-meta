export { default as formGroupMockData } from './formgroup.object';
export { default as configRelationsMockFormGroup } from './configitem.relation.formgroup';
export { default as formGroupPropertyMockData } from './configitem.property.formgroup';
export { default as configitemMockDataFormGroup } from './configitem.formgroup';
export { default as configPrivilegesMockDataFormGroup } from './configitem.privileges.formgroup';
//# sourceMappingURL=index.d.ts.map