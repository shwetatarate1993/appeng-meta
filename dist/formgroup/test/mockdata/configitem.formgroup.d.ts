declare const _default: {
    configObjectId: string;
    name: string;
    configObjectType: string;
    projectId: number;
    createdBy: string;
    itemDescription: any;
    creationDate: any;
    updatedBy: any;
    updationDate: any;
    deletionDate: any;
    isDeleted: number;
}[];
export default _default;
//# sourceMappingURL=configitem.formgroup.d.ts.map