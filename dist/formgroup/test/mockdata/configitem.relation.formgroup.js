"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        relationId: 'ad18470f-257a-4861-9a16-91bf8dcdac97',
        relationType: 'CompositeEntity_FormGroup',
        parentItemId: '1679aba5-b772-4858-a143-0a8dd8e4669a',
        childItemId: '06f67661-5a9c-4279-bc09-5899690aa7bd',
        createdBy: null,
        isDeleted: 0,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
    },
    {
        relationId: '6ada182c-a7fd-4283-a1a8-3c4b4bcc1544',
        relationType: 'FormGroup_Form',
        parentItemId: '06f67661-5a9c-4279-bc09-5899690aa7bd',
        childItemId: '380db32b-8537-4b80-8127-30242862d48b',
        createdBy: null,
        isDeleted: 0,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
    },
    {
        relationId: '72ace2a8-92eb-4baa-bdb3-3ace2833fff5',
        relationType: 'FormGroup_Form',
        parentItemId: '06f67661-5a9c-4279-bc09-5899690aa7bd',
        childItemId: 'a4f914f5-324b-477f-a6ea-e08111f331c2',
        createdBy: null,
        isDeleted: 0,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
    },
];
//# sourceMappingURL=configitem.relation.formgroup.js.map