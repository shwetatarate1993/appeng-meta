"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var formgroup_object_1 = require("./formgroup.object");
exports.formGroupMockData = formgroup_object_1.default;
var configitem_relation_formgroup_1 = require("./configitem.relation.formgroup");
exports.configRelationsMockFormGroup = configitem_relation_formgroup_1.default;
var configitem_property_formgroup_1 = require("./configitem.property.formgroup");
exports.formGroupPropertyMockData = configitem_property_formgroup_1.default;
var configitem_formgroup_1 = require("./configitem.formgroup");
exports.configitemMockDataFormGroup = configitem_formgroup_1.default;
var configitem_privileges_formgroup_1 = require("./configitem.privileges.formgroup");
exports.configPrivilegesMockDataFormGroup = configitem_privileges_formgroup_1.default;
//# sourceMappingURL=index.js.map