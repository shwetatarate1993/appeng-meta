"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        propertyId: 'e7c57e4b-5985-4cbf-af57-d6912e45fcda',
        propertyName: 'FORM_GROUP_NAME',
        propertyValue: null,
        itemId: '06f67661-5a9c-4279-bc09-5899690aa7bd',
        createdBy: null,
        isDeleted: 0,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
    },
    {
        propertyId: 'b5fe9f77-6794-4124-b051-3245119a3ecf',
        propertyName: 'FORMS_ORDER',
        propertyValue: 'a4f914f5-324b-477f-a6ea-e08111f331c2,380db32b-8537-4b80-8127-30242862d48b',
        itemId: '06f67661-5a9c-4279-bc09-5899690aa7bd',
        createdBy: null,
        isDeleted: 0,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
    },
    {
        propertyId: 'b7907d96-dffc-45c8-a1b7-8a2503b1e32e',
        propertyName: 'FORM_GROUP_TYPE',
        propertyValue: 'INSERT_FORM_GROUP',
        itemId: '06f67661-5a9c-4279-bc09-5899690aa7bd',
        createdBy: null,
        isDeleted: 0,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
    },
    {
        propertyId: '4c7431e1-9119-409a-b19c-dd4d407fbe9a',
        propertyName: 'REPEATABLE_FORMS',
        propertyValue: null,
        itemId: '06f67661-5a9c-4279-bc09-5899690aa7bd',
        createdBy: null,
        isDeleted: 0,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
    },
    {
        propertyId: 'd6dc9e86-ae78-4928-a42a-31b04646932b',
        propertyName: 'TABS',
        propertyValue: 'CARDGROUP',
        itemId: '06f67661-5a9c-4279-bc09-5899690aa7bd',
        createdBy: null,
        isDeleted: 0,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
    },
    {
        propertyId: 'da02689a-2842-4254-983f-ca73fca254ab',
        propertyName: 'IS_RO_ACCESSIBLE',
        propertyValue: '1',
        itemId: '06f67661-5a9c-4279-bc09-5899690aa7bd',
        createdBy: null,
        isDeleted: 0,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
    },
    {
        propertyId: '38264e79-47a3-453b-8f53-ae11092cf204',
        propertyName: 'IS_READONLY',
        propertyValue: '0',
        itemId: '06f67661-5a9c-4279-bc09-5899690aa7bd',
        createdBy: null,
        isDeleted: 0,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
    },
    {
        propertyId: '0cc410e6-d956-45a0-ab55-d3fc2467406e',
        propertyName: 'IS_TABBED_STRUCTURE',
        propertyValue: '1',
        itemId: '06f67661-5a9c-4279-bc09-5899690aa7bd',
        createdBy: null,
        isDeleted: 0,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
    },
];
//# sourceMappingURL=configitem.property.formgroup.js.map