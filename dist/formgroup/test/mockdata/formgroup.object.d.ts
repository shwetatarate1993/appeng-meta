declare const _default: {
    configObjectId: string;
    configObjectType: string;
    name: string;
    formGroupName: any;
    formOrder: string;
    formType: string;
    readOnlyAccessible: boolean;
    readOnly: boolean;
    tabbedStructure: boolean;
    tabs: string;
    repeatableForms: any;
    privileges: {
        privilegeType: string;
        roleId: number;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: string;
        deletionDate: string;
    }[];
    parentRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: any;
        deletionDate: any;
    }[];
    childRelations: ({
        relationId: string;
        relationType: string;
        childtemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: any;
        deletionDate: any;
        childItemId?: undefined;
    } | {
        relationId: string;
        relationType: string;
        childItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: any;
        deletionDate: any;
        childtemId?: undefined;
    })[];
}[];
export default _default;
//# sourceMappingURL=formgroup.object.d.ts.map