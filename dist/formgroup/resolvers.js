"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const form_parser_1 = require("../form/form.parser");
const formgroup_parser_1 = require("./formgroup.parser");
exports.Query = {
    FormGroup: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, formgroup_parser_1.parseConfigToFormGroup),
};
exports.FormGroup = {
    forms: (formGroup, _, context) => common_meta_1.configService.fetchConfigsByParentId(formGroup.configObjectId, form_parser_1.parseConfigToForm),
};
exports.Mutation = {
    createFormGroup: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, formgroup_parser_1.parseFormGroupToConfig, formgroup_parser_1.parseConfigToFormGroup);
    },
};
//# sourceMappingURL=resolvers.js.map