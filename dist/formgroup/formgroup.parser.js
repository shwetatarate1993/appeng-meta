"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseFormGroupToConfig = (formGroup) => {
    const createdAt = formGroup.creationDate;
    const createdBy = formGroup.createdBy;
    const updatedBy = formGroup.updatedBy;
    const updatedAt = formGroup.updationDate;
    const property = [];
    let itemId;
    if (formGroup.configObjectId !== undefined && formGroup.configObjectId !== ''
        && formGroup.configObjectId !== null) {
        itemId = formGroup.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (formGroup.formGroupName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'FORM_GROUP_NAME', formGroup.formGroupName, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formGroup.formOrder !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'FORMS_ORDER', formGroup.formOrder, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formGroup.formType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'FORM_GROUP_TYPE', formGroup.formType, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formGroup.repeatableForms !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'REPEATABLE_FORMS', formGroup.repeatableForms, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formGroup.tabs !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'TABS', formGroup.tabs, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    // for setting default value as true
    property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'IS_RO_ACCESSIBLE', formGroup.readOnlyAccessible !== undefined ? (formGroup.readOnlyAccessible ?
        '1' : '0') : '1', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    // for setting default value as false
    property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'IS_READONLY', formGroup.readOnly !== undefined ? (formGroup.readOnly ?
        '1' : '0') : '0', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    // for setting default value as true
    property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'IS_TABBED_STRUCTURE', formGroup.tabbedStructure !== undefined ? (formGroup.tabbedStructure ?
        '1' : '0') : '1', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    const configItem = new meta_db_1.ConfigItemModel(itemId, formGroup.name, configitemtype_enum_1.ConfigItemTypes.FORMGROUP, formGroup.projectId, createdBy, formGroup.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const formgroupPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(formGroup.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(formGroup.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(formGroup.privileges, formgroupPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, formgroupPrivileges);
};
exports.parseConfigToFormGroup = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.FormGroup(details.get('FORM_GROUP_NAME'), details.get('FORMS_ORDER'), details.get('FORM_GROUP_TYPE'), details.get('REPEATABLE_FORMS'), details.get('TABS'), details.get('IS_RO_ACCESSIBLE'), details.get('IS_READONLY'), details.get('IS_TABBED_STRUCTURE'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=formgroup.parser.js.map