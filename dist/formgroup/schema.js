"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = __importDefault(require("../base"));
const common_schema_1 = __importDefault(require("../common.schema"));
const FormGroup = `

input FormGroupInput {

   readOnlyAccessible : Boolean
    formOrder : String
    repeatableForms : String
    readOnly : Boolean
    formType :  String
    tabbedStructure : Boolean
    tabs : String
    formGroupName : String
    configObjectId: ID
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilegeInput]
    parentRelations : [ConfigItemRelationInput]
    childRelations : [ConfigItemRelationInput]
}

extend type Query {
    FormGroup(id: ID!): FormGroup
   }

extend type Mutation {
    createFormGroup (input:FormGroupInput): FormGroup
}

type FormGroup {

     readOnlyAccessible : Boolean
    formOrder : String
    repeatableForms : String
    readOnly : Boolean
    formType :  String
    tabbedStructure : Boolean
    tabs : String
    formGroupName : String
    configObjectId: ID
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilege]
    parentRelations : [ConfigItemRelation]
    childRelations : [ConfigItemRelation]
    forms : [Form]
}
`;
exports.default = () => [FormGroup, base_1.default, common_schema_1.default];
//# sourceMappingURL=schema.js.map