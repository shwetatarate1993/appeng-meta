export declare const Query: {
    FormSection: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").FormSection>;
};
export declare const FormSection: {
    formFields: (formSection: any, _: any, context: any) => Promise<import("../models").FormField[]>;
};
export declare const Mutation: {
    createFormSection: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map