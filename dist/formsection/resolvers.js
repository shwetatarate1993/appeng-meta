"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const formfield_parser_1 = require("../formfield/formfield.parser");
const formsection_parser_1 = require("./formsection.parser");
exports.Query = {
    FormSection: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, formsection_parser_1.parseConfigToFormSectionObject),
};
exports.FormSection = {
    formFields: async (formSection, _, context) => {
        const fieldIds = formSection.childRelations.filter((rel) => rel.relationType === relationtype_enum_1.RelationType.FORMSECTION_FORMFIELD)
            .map((rel) => rel.childItemId);
        return fieldIds ?
            await common_meta_1.configService.fetchConfigItemsForIdList(fieldIds, formfield_parser_1.parseConfigToFormField) : [];
    },
};
exports.Mutation = {
    createFormSection: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, formsection_parser_1.parseFormSectionObjectToConfig, formsection_parser_1.parseConfigToFormSectionObject);
    },
};
//# sourceMappingURL=resolvers.js.map