declare const _default: {
    configObjectId: string;
    configObjectType: string;
    name: string;
    projectId: number;
    displayName: string;
    headerLabel: string;
    order: number;
    privileges: {
        privilegeType: string;
        itemId: string;
        roleId: number;
        createdBy: any;
        isDeleted: number;
        creationDate: any;
        updatedBy: any;
        updationDate: any;
        deletionDate: any;
    }[];
    parentRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        childItemId: string;
        createdBy: any;
        isDeleted: number;
        creationDate: any;
        updatedBy: any;
        updationDate: any;
        deletionDate: any;
    }[];
}[];
export default _default;
//# sourceMappingURL=formsection.object.d.ts.map