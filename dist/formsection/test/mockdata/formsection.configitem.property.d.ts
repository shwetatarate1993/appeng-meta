declare const _default: ({
    propertyId: string;
    propertyName: string;
    propertyValue: number;
    itemId: string;
    createdBy: string;
    updatedBy: any;
    creationDate: any;
    isDeleted: number;
    deletionDate: any;
    updationDate: any;
} | {
    propertyId: string;
    propertyName: string;
    propertyValue: string;
    itemId: string;
    createdBy: string;
    updatedBy: any;
    creationDate: any;
    isDeleted: number;
    deletionDate: any;
    updationDate: any;
})[];
export default _default;
//# sourceMappingURL=formsection.configitem.property.d.ts.map