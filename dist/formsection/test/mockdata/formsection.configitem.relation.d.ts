declare const _default: {
    relationId: string;
    relationType: string;
    parentItemId: string;
    childItemId: string;
    createdBy: any;
    isDeleted: number;
    creationDate: any;
    updatedBy: string;
    updationDate: any;
    deletionDate: any;
}[];
export default _default;
//# sourceMappingURL=formsection.configitem.relation.d.ts.map