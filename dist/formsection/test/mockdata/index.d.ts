export { default as formSectionMockData } from './formsection.object';
export { default as formSectionConfigitemMockData } from './formsection.configitem';
export { default as formSectionPropertyMockData } from './formsection.configitem.property';
export { default as formSectionPrivilegesMockData } from './formsection.configitem.privilages';
export { default as formSectionRelationMockData } from './formsection.configitem.relation';
//# sourceMappingURL=index.d.ts.map