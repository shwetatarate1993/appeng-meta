declare const _default: {
    privilegeId: string;
    privilegeType: string;
    itemId: string;
    roleId: number;
    createdBy: any;
    isDeleted: number;
    creationDate: any;
    updatedBy: any;
    updationDate: any;
    deletionDate: any;
}[];
export default _default;
//# sourceMappingURL=formsection.configitem.privilages.d.ts.map