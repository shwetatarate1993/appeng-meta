"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        propertyId: '4f1d4efb-499d-4c80-9a7c-87984b7ede75',
        propertyName: 'IS_RENDER_ON_REPEAT',
        propertyValue: 1,
        itemId: 'a0d0370b-6e1d-430b-8852-7a254844189d',
        createdBy: 'SYSTEM',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: 'd40ea026-5e41-4bab-8136-0ea5fcb3f8d2',
        propertyName: 'EDITABILITY_REGEX',
        propertyValue: null,
        itemId: 'a0d0370b-6e1d-430b-8852-7a254844189d',
        createdBy: 'SYSTEM',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '8a4e22c3-27b0-4c1e-910a-3ee742ba5713',
        propertyName: 'ACCESSBILITY_REGEX',
        propertyValue: null,
        itemId: 'a0d0370b-6e1d-430b-8852-7a254844189d',
        createdBy: 'SYSTEM',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: 'd888ce19-91e9-4428-aa73-b8a6a5bb5c55',
        propertyName: 'IS_EXPRESSION_AVAILABLE',
        propertyValue: 0,
        itemId: 'a0d0370b-6e1d-430b-8852-7a254844189d',
        createdBy: 'SYSTEM',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: 'b8776889-fa1e-4f15-bc59-6ba0a29fea31',
        propertyName: 'COMPONENT_PER_ROW',
        propertyValue: 2,
        itemId: 'a0d0370b-6e1d-430b-8852-7a254844189d',
        createdBy: 'SYSTEM',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '93dbbe77-44fb-4c95-85d1-a21d67d4fb42',
        propertyName: 'HEADER_LABEL',
        propertyValue: 'Employee info',
        itemId: 'a0d0370b-6e1d-430b-8852-7a254844189d',
        createdBy: 'SYSTEM',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '9c81c277-1eea-4830-a852-cd1a45c543fd',
        propertyName: 'ORDER',
        propertyValue: 5,
        itemId: 'a0d0370b-6e1d-430b-8852-7a254844189d',
        createdBy: 'SYSTEM',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '8b6dcf27-d4bc-4507-b8f1-1a316655e933',
        propertyName: 'DISPLAY_NAME',
        propertyValue: 'FormSection_Building Branch-EmployeeInfo',
        itemId: 'a0d0370b-6e1d-430b-8852-7a254844189d',
        createdBy: null,
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '2a229f75-3188-4478-87e8-fd861e45e72b',
        propertyName: 'TAB_GROUP',
        propertyValue: null,
        itemId: 'a0d0370b-6e1d-430b-8852-7a254844189d',
        createdBy: 'SYSTEM',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
];
//# sourceMappingURL=formsection.configitem.property.js.map