"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var formsection_object_1 = require("./formsection.object");
exports.formSectionMockData = formsection_object_1.default;
var formsection_configitem_1 = require("./formsection.configitem");
exports.formSectionConfigitemMockData = formsection_configitem_1.default;
var formsection_configitem_property_1 = require("./formsection.configitem.property");
exports.formSectionPropertyMockData = formsection_configitem_property_1.default;
var formsection_configitem_privilages_1 = require("./formsection.configitem.privilages");
exports.formSectionPrivilegesMockData = formsection_configitem_privilages_1.default;
var formsection_configitem_relation_1 = require("./formsection.configitem.relation");
exports.formSectionRelationMockData = formsection_configitem_relation_1.default;
//# sourceMappingURL=index.js.map