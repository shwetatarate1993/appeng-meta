"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mockdata_1 = require("./mockdata");
const models_1 = require("../../models");
const formsection_parser_1 = require("../formsection.parser");
test('parser formsection object to config', () => {
    const formsection = models_1.FormSection.deserialize(mockdata_1.formSectionMockData[0]);
    const result = formsection_parser_1.parseFormSectionObjectToConfig(formsection);
    expect(formsection.displayName).toEqual('Employee Info');
    expect(formsection.configObjectType).toEqual('FormSection');
});
test('parser config to Formsection object', () => {
    const result = formsection_parser_1.parseConfigToFormSectionObject(mockdata_1.formSectionConfigitemMockData[0], mockdata_1.formSectionPropertyMockData, mockdata_1.formSectionRelationMockData, mockdata_1.formSectionPrivilegesMockData);
    expect(result).toHaveProperty('displayName', 'FormSection_Building Branch-EmployeeInfo');
    expect(result).toHaveProperty('order', 5);
    expect(result).toHaveProperty('headerLabel', 'Employee info');
    expect(result).toHaveProperty('accessibilityRegex', null);
    expect(result).toHaveProperty('editabilityRegex', null);
    expect(result).toHaveProperty('tabGroup', null);
    expect(result).toHaveProperty('isRenderOnRepeat', true);
    expect(result).toHaveProperty('componentsPerRow', 2);
    expect(result).toHaveProperty('expressionAvailable', true);
    expect(result.privileges.length === 5).toBe(true);
});
//# sourceMappingURL=formsection.parser.test.js.map