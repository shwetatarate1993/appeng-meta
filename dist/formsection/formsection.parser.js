"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseFormSectionObjectToConfig = (formSection) => {
    const createdAt = formSection.creationDate;
    const createdBy = formSection.createdBy;
    const updatedBy = formSection.updatedBy;
    const updatedAt = formSection.updationDate;
    const property = [];
    let itemId;
    if (formSection.configObjectId !== undefined && formSection.configObjectId !== ''
        && formSection.configObjectId !== null) {
        itemId = formSection.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (formSection.headerLabel !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('HEADER_LABEL', formSection.headerLabel, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formSection.order !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ORDER', formSection.order, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formSection.displayName !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('DISPLAY_NAME', formSection.displayName, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formSection.accessibilityRegex !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ACCESSBILITY_REGEX', formSection.accessibilityRegex, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formSection.editabilityRegex !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('EDITABILITY_REGEX', formSection.editabilityRegex, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formSection.tabGroup !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('TAB_GROUP', formSection.tabGroup, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    // for setting default value as true
    property.push(new meta_db_1.ConfigItemProperty('IS_RENDER_ON_REPEAT', formSection.isRenderOnRepeat !== undefined ? (formSection.isRenderOnRepeat ? '1' : '0') : '1', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    if (formSection.requiredFormfields !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('REQUIRED_FORM_FIELDS', formSection.requiredFormfields, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (formSection.componentsPerRow !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('COMPONENT_PER_ROW', formSection.componentsPerRow, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    // for setting default value as true
    property.push(new meta_db_1.ConfigItemProperty('IS_EXPRESSION_AVAILABLE', formSection.expressionAvailable !== undefined ? (formSection.expressionAvailable ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    const configItem = new meta_db_1.ConfigItemModel(itemId, formSection.name, configitemtype_enum_1.ConfigItemTypes.FORMSECTION, formSection.projectId, createdBy, formSection.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const formsectionPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(formSection.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(formSection.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(formSection.privileges, formsectionPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, formsectionPrivileges);
};
exports.parseConfigToFormSectionObject = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.FormSection(configItem.configObjectId, details.get('HEADER_LABEL'), details.get('ORDER'), details.get('DISPLAY_NAME'), details.get('ACCESSBILITY_REGEX'), details.get('EDITABILITY_REGEX'), details.get('TAB_GROUP'), details.get('IS_RENDER_ON_REPEAT'), details.get('REQUIRED_FORM_FIELDS'), details.get('COMPONENT_PER_ROW'), details.get('IS_EXPRESSION_AVAILABLE'), configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=formsection.parser.js.map