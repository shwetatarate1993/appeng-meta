import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { FormSection } from '../models';
export declare const parseFormSectionObjectToConfig: (formSection: FormSection) => ConfigMetadata;
export declare const parseConfigToFormSectionObject: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => FormSection;
//# sourceMappingURL=formsection.parser.d.ts.map