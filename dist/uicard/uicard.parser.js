"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseUiCardToConfig = (uiCard) => {
    const createdAt = uiCard.creationDate;
    const createdBy = uiCard.createdBy;
    const updatedBy = uiCard.updatedBy;
    const updatedAt = uiCard.updationDate;
    const property = [];
    let itemId;
    if (uiCard.configObjectId !== undefined && uiCard.configObjectId !== ''
        && uiCard.configObjectId !== null) {
        itemId = uiCard.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (uiCard.height !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('HEIGHT', uiCard.height, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (uiCard.width !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('WIDTH', uiCard.width, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (uiCard.order !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ORDER', uiCard.order, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (uiCard.accessibilityRegex !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('ACCESSBILITY_REGEX', uiCard.accessibilityRegex, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    // for setting default value as false
    property.push(new meta_db_1.ConfigItemProperty('IS_EXPRESSION_AVAILABLE', uiCard.expressionAvailble !== undefined ?
        (uiCard.expressionAvailble ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    if (uiCard.viewType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('VIEWTYPE', uiCard.viewType, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (uiCard.displayLabel !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('IS_DISPLAY_LABEL', uiCard.displayLabel, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (uiCard.expressionFields !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('EXPRESSION_FIELD_STRING', uiCard.expressionFields, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, uiCard.name, configitemtype_enum_1.ConfigItemTypes.UICARD, uiCard.projectId, createdBy, uiCard.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const uicardPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(uiCard.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(uiCard.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(uiCard.privileges, uicardPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, uicardPrivileges);
};
exports.parseConfigToUiCard = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.UiCard(details.get('HEIGHT'), details.get('WIDTH'), details.get('ORDER'), details.get('ACCESSBILITY_REGEX'), details.get('IS_EXPRESSION_AVAILABLE'), details.get('VIEWTYPE'), details.get('IS_DISPLAY_LABEL'), details.get('EXPRESSION_FIELD_STRING'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=uicard.parser.js.map