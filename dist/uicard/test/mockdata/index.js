"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var uicard_object_1 = require("./uicard.object");
exports.uiCardMockData = uicard_object_1.default;
var configitem_relation_uicard_1 = require("./configitem.relation.uicard");
exports.configRelationsMockUiCard = configitem_relation_uicard_1.default;
var configitem_property_uicard_1 = require("./configitem.property.uicard");
exports.uiCardPropertyMockData = configitem_property_uicard_1.default;
var configitem_uicard_1 = require("./configitem.uicard");
exports.configitemMockDataUiCard = configitem_uicard_1.default;
var configitem_privileges_uicard_1 = require("./configitem.privileges.uicard");
exports.configPrivilegesMockDataUiCard = configitem_privileges_uicard_1.default;
//# sourceMappingURL=index.js.map