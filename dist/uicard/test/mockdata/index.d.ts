export { default as uiCardMockData } from './uicard.object';
export { default as configRelationsMockUiCard } from './configitem.relation.uicard';
export { default as uiCardPropertyMockData } from './configitem.property.uicard';
export { default as configitemMockDataUiCard } from './configitem.uicard';
export { default as configPrivilegesMockDataUiCard } from './configitem.privileges.uicard';
//# sourceMappingURL=index.d.ts.map