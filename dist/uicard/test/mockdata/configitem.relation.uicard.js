"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        relationId: '6a80b8f4-8f88-4a95-addf-67f8ce6ad8f4',
        relationType: 'UILayout_UICard',
        parentItemId: '2db0c2f9-f356-4b71-8cbc-cdb8e2f46da3',
        childItemId: '8bef8f9f-c190-493a-85db-e3dd43241785',
        createdBy: null,
        isDeleted: 0,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
    },
    {
        relationId: '5a92d6f8-a6d9-4b64-981b-de9e8c0aa7ba',
        relationType: 'UICard_UITab',
        parentItemId: '8bef8f9f-c190-493a-85db-e3dd43241785',
        childItemId: '1899c67c-8997-423e-b426-b3dd4c9c3cd3',
        createdBy: null,
        isDeleted: 0,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
    },
    {
        relationId: '3d09f862-0776-44b7-abfc-6984b040c35e',
        relationType: 'UICard_CompositeEntityNode',
        parentItemId: '8bef8f9f-c190-493a-85db-e3dd43241785',
        childItemId: '06646fc4-6335-442c-8117-799234e9a018',
        createdBy: null,
        isDeleted: 0,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
    },
];
//# sourceMappingURL=configitem.relation.uicard.js.map