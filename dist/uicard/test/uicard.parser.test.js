"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const uicard_parser_1 = require("../uicard.parser");
const mockdata_1 = require("./mockdata");
test('parser uicard object to config', () => {
    const uicard = models_1.UiCard.deserialize(mockdata_1.uiCardMockData[0]);
    const result = uicard_parser_1.parseUiCardToConfig(uicard);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItem.configObjectType).toEqual('UICard');
});
test('parser config to uicard ', () => {
    const result = uicard_parser_1.parseConfigToUiCard(mockdata_1.configitemMockDataUiCard[0], mockdata_1.uiCardPropertyMockData, mockdata_1.configRelationsMockUiCard, mockdata_1.configPrivilegesMockDataUiCard);
    expect(result).toHaveProperty('height', 25);
    expect(result).toHaveProperty('width', 50);
    expect(result).toHaveProperty('order', 55);
    expect(result).toHaveProperty('accessibilityRegex');
    expect(result).toHaveProperty('expressionAvailble', false);
    expect(result).toHaveProperty('viewType', 'tabs');
    expect(result).toHaveProperty('displayLabel', false);
    expect(result).toHaveProperty('expressionFields', 'abcd,bcd');
    expect(result.privileges.length === 4).toBe(true);
    expect(result.parentRelations.length === 1).toBe(true);
    expect(result.childRelations.length === 2).toBe(true);
    expect(result.parentRelations[0].relationType === 'UILayout_UICard').toBe(true);
    expect(result.childRelations[0].relationType === 'UICard_UITab').toBe(true);
    expect(result.childRelations[1].relationType === 'UICard_CompositeEntityNode').toBe(true);
});
//# sourceMappingURL=uicard.parser.test.js.map