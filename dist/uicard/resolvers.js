"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const uitab_parser_1 = require("../uitab/uitab.parser");
const uicard_parser_1 = require("./uicard.parser");
exports.Query = {
    UiCard: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, uicard_parser_1.parseConfigToUiCard),
};
exports.UiCard = {
    uITabs: (uICard, _, context) => common_meta_1.configService.fetchConfigsByParentId(uICard.configObjectId, uitab_parser_1.parseConfigToUiTab),
};
exports.Mutation = {
    createUiCard: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, uicard_parser_1.parseUiCardToConfig, uicard_parser_1.parseConfigToUiCard);
    },
};
//# sourceMappingURL=resolvers.js.map