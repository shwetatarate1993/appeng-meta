export declare const Query: {
    UiCard: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").UiCard>;
};
export declare const UiCard: {
    uITabs: (uICard: any, _: any, context: any) => Promise<import("../models").UiTab[]>;
};
export declare const Mutation: {
    createUiCard: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map