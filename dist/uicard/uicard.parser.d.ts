import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { UiCard } from '../models';
export declare const parseUiCardToConfig: (uiCard: UiCard) => ConfigMetadata;
export declare const parseConfigToUiCard: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => UiCard;
//# sourceMappingURL=uicard.parser.d.ts.map