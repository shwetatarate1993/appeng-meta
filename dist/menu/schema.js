"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = __importDefault(require("../base"));
const common_schema_1 = __importDefault(require("../common.schema"));
const Menu = `

input MenuInput {

    displayLabel : String
    hrefBaseUrl : String
    menuClass : String
    header : String
    order : Int
    isLabel : Boolean
    objectId : String
    isOpenInNewTab : Boolean
    submenuContainerClass : String
    subMenu : Boolean
    report : String
    customUrl : String
    componentsPerRow : Int
    linkType : String
    configObjectId: ID
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilegeInput]
    parentRelations : [ConfigItemRelationInput]
    childRelations : [ConfigItemRelationInput]
}

extend type Query {
    Menu(id: ID!): Menu
    SubMenus(id: ID!): [Menu]
   }

extend type Mutation {
    createMenu (input: MenuInput): Menu
}

type Menu {

    displayLabel : String
    hrefBaseUrl : String
    menuClass : String
    header : String
    order : Int
    isLabel : Boolean
    objectId : String
    isOpenInNewTab : Boolean
    submenuContainerClass : String
    subMenu : Boolean
    report : String
    customUrl : String
    componentsPerRow : Int
    linkType : String
    configObjectId: ID
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilege]
    parentRelations : [ConfigItemRelation]
    childRelations : [ConfigItemRelation]
    menus : [Menu]
}
`;
exports.default = () => [Menu, base_1.default, common_schema_1.default];
//# sourceMappingURL=schema.js.map