import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { Menu } from '../models';
export declare const parseMenuToConfig: (menu: Menu) => ConfigMetadata;
export declare const parseConfigToMenu: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => Menu;
//# sourceMappingURL=menu.parser.d.ts.map