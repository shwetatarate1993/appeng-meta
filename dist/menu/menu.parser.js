"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseMenuToConfig = (menu) => {
    const createdAt = menu.creationDate;
    const createdBy = menu.createdBy;
    const updatedBy = menu.updatedBy;
    const updatedAt = menu.updationDate;
    const property = [];
    let itemId;
    if (menu.configObjectId !== undefined && menu.configObjectId !== ''
        && menu.configObjectId !== null) {
        itemId = menu.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (menu.displayLabel !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('MENU_LABEL', menu.displayLabel, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (menu.hrefBaseUrl !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('HREF_BASE_URL', menu.hrefBaseUrl, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (menu.menuClass !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('MENU_CLASS', menu.menuClass, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (menu.header !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('MENU_HEADER', menu.header, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (menu.order !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('MENU_ORDER', menu.order, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    // for setting default value as false
    property.push(new meta_db_1.ConfigItemProperty('IS_LABEL', menu.isLabel !== undefined ? (menu.isLabel ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    if (menu.objectId !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('REFRENCE_OBJECT_ID', menu.objectId, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    // for setting default value as false
    property.push(new meta_db_1.ConfigItemProperty('OPEN_IN_NEW_TAB', menu.isOpenInNewTab !== undefined ? (menu.isOpenInNewTab ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    if (menu.submenuContainerClass !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('SUBMENU_CONTAINER_CLASS', menu.submenuContainerClass, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    // for setting default value as false
    property.push(new meta_db_1.ConfigItemProperty('IS_SUBMENU', menu.subMenu !== undefined ? (menu.subMenu ? '1' : '0') : '0', itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    if (menu.report !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('REPORT', menu.report, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (menu.customUrl !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('CUSTOM_URL', menu.customUrl, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (menu.linkType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('LINK_TYPE', menu.linkType, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    // for setting default value as true //change condition
    property.push(new meta_db_1.ConfigItemProperty('COMPONENT_PER_ROW', menu.componentsPerRow === undefined || menu.componentsPerRow === null
        || menu.componentsPerRow === 0
        ? 1 : menu.componentsPerRow, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    const configItem = new meta_db_1.ConfigItemModel(itemId, menu.name, configitemtype_enum_1.ConfigItemTypes.MENU, menu.projectId, createdBy, menu.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const menuPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(menu.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(menu.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(menu.privileges, menuPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, menuPrivileges);
};
exports.parseConfigToMenu = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.Menu(details.get('MENU_LABEL'), details.get('HREF_BASE_URL'), details.get('MENU_CLASS'), details.get('MENU_HEADER'), details.get('MENU_ORDER'), details.get('IS_LABEL'), details.get('REFRENCE_OBJECT_ID'), details.get('OPEN_IN_NEW_TAB'), details.get('SUBMENU_CONTAINER_CLASS'), details.get('IS_SUBMENU'), details.get('REPORT'), details.get('CUSTOM_URL'), details.get('COMPONENT_PER_ROW'), details.get('LINK_TYPE'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=menu.parser.js.map