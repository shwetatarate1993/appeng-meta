"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const menu_parser_1 = require("./menu.parser");
exports.Query = {
    Menu: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, menu_parser_1.parseConfigToMenu),
    SubMenus: (_, { id }, context) => common_meta_1.configService.fetchConfigsByParentId(id, menu_parser_1.parseConfigToMenu),
};
exports.Menu = {
    menus: async (menu, _, context) => {
        let menus = null;
        if (menu.linkType === 'PARENT_MENU' || menu.linkType === 'SETTING_MENU') {
            const menuIds = menu.childRelations.filter((rel) => rel.relationType === relationtype_enum_1.RelationType.MENU_MENU)
                .map((rel) => rel.childItemId);
            menus = menuIds ?
                await common_meta_1.configService.fetchConfigItemsForIdList(menuIds, menu_parser_1.parseConfigToMenu) : [];
        }
        return menus;
    },
};
exports.Mutation = {
    createMenu: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, menu_parser_1.parseMenuToConfig, menu_parser_1.parseConfigToMenu);
    },
};
//# sourceMappingURL=resolvers.js.map