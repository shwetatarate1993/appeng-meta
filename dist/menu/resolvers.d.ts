export declare const Query: {
    Menu: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").Menu>;
    SubMenus: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").Menu[]>;
};
export declare const Menu: {
    menus: (menu: any, _: any, context: any) => Promise<import("../models").Menu[]>;
};
export declare const Mutation: {
    createMenu: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map