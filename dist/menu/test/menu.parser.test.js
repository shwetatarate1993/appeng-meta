"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const menu_parser_1 = require("../menu.parser");
const mockdata_1 = require("./mockdata");
test('parser menu object to config', () => {
    const menu = models_1.Menu.deserialize(mockdata_1.menuMockData[0]);
    const result = menu_parser_1.parseMenuToConfig(menu);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItemRelation[0]).toHaveProperty('relationType', 'Menu_Menu');
    expect(result.configItem.configObjectType).toEqual('Menu');
});
test('parser config to menu object', () => {
    const result = menu_parser_1.parseConfigToMenu(mockdata_1.configitemMockDataMenu[0], mockdata_1.menuPropertyMockData, mockdata_1.configRelationsMockDataMenu, mockdata_1.configPrivilegesMockData);
    expect(result).toHaveProperty('displayLabel');
    expect(result).toHaveProperty('hrefBaseUrl');
    expect(result).toHaveProperty('menuClass');
    expect(result).toHaveProperty('header');
    expect(result).toHaveProperty('order');
    expect(result).toHaveProperty('isLabel');
    expect(result).toHaveProperty('objectId');
    expect(result).toHaveProperty('isOpenInNewTab');
    expect(result).toHaveProperty('submenuContainerClass');
    expect(result).toHaveProperty('subMenu');
    expect(result).toHaveProperty('report');
    expect(result).toHaveProperty('customUrl');
    expect(result).toHaveProperty('componentsPerRow');
    expect(result).toHaveProperty('linkType');
    expect(result.privileges.length === 4).toBe(true);
    expect(result.parentRelations.length === 1).toBe(true);
    expect(result.childRelations.length === 0).toBe(true);
    expect(result.parentRelations[0].relationType === 'Menu_Menu').toBe(true);
});
//# sourceMappingURL=menu.parser.test.js.map