declare const _default: ({
    propertyId: string;
    propertyName: string;
    propertyValue: string;
    itemId: string;
    createdBy: any;
    isDeleted: number;
    creationDate: any;
    updatedBy: any;
    updationDate: any;
    deletionDate: any;
} | {
    propertyId: string;
    propertyName: string;
    propertyValue: number;
    itemId: string;
    createdBy: any;
    isDeleted: number;
    creationDate: any;
    updatedBy: any;
    updationDate: any;
    deletionDate: any;
})[];
export default _default;
//# sourceMappingURL=configitem.property.menu.d.ts.map