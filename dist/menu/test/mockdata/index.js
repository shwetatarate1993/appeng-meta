"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var menu_object_1 = require("./menu.object");
exports.menuMockData = menu_object_1.default;
var configitem_menu_1 = require("./configitem.menu");
exports.configitemMockDataMenu = configitem_menu_1.default;
var configitem_property_menu_1 = require("./configitem.property.menu");
exports.menuPropertyMockData = configitem_property_menu_1.default;
var configitem_relation_menu_1 = require("./configitem.relation.menu");
exports.configRelationsMockDataMenu = configitem_relation_menu_1.default;
var configitem_privileges_menu_1 = require("./configitem.privileges.menu");
exports.configPrivilegesMockData = configitem_privileges_menu_1.default;
//# sourceMappingURL=index.js.map