export { default as menuMockData } from './menu.object';
export { default as configitemMockDataMenu } from './configitem.menu';
export { default as menuPropertyMockData } from './configitem.property.menu';
export { default as configRelationsMockDataMenu } from './configitem.relation.menu';
export { default as configPrivilegesMockData } from './configitem.privileges.menu';
//# sourceMappingURL=index.d.ts.map