"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        privilegeId: '25d43324-e28e-4c27-ae2d-aaf232re2197',
        privilegeType: 'EDIT',
        itemId: '12485524-6271-45bc-b1f6-144342640bd2',
        roleId: 19,
        createdBy: '1115',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1115',
        updationDate: null,
        deletionDate: null,
    },
    {
        privilegeId: '6d271fce-0a2a-4c47-8058-279f047obb18',
        privilegeType: 'EDIT',
        itemId: '12485524-6271-45bc-b1f6-144342640bd2',
        roleId: 17,
        createdBy: '1115',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1115',
        updationDate: null,
        deletionDate: null,
    },
    {
        privilegeId: '83212cb3-333e-4216-98c7-955219h12efd',
        privilegeType: 'EDIT',
        itemId: '12485524-6271-45bc-b1f6-144342640bd2',
        roleId: 16,
        createdBy: '1115',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1115',
        updationDate: null,
        deletionDate: null,
    },
    {
        privilegeId: 'acb8d3ef-eb6f-4f9e-85a1-16d3ec55s87c',
        privilegeType: 'EDIT',
        itemId: '12485524-6271-45bc-b1f6-144342640bd2',
        roleId: 20,
        createdBy: '1115',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1115',
        updationDate: null,
        deletionDate: null,
    },
];
//# sourceMappingURL=configitem.privileges.menu.js.map