declare const _default: {
    configObjectId: string;
    configObjectType: string;
    name: string;
    displayLabel: string;
    hrefBaseUrl: string;
    menuClass: string;
    header: string;
    order: number;
    isLabel: boolean;
    objectId: string;
    isOpenInNewTab: boolean;
    submenuContainerClass: string;
    subMenu: boolean;
    report: string;
    customUrl: string;
    componentsPerRow: number;
    linkType: string;
    privileges: {
        privilegeType: string;
        roleId: number;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: string;
        deletionDate: string;
    }[];
    parentRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: any;
        deletionDate: any;
    }[];
}[];
export default _default;
//# sourceMappingURL=menu.object.d.ts.map