declare const _default: {
    configObjectId: string;
    configObjectType: string;
    usedInAllProjects: boolean;
    name: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: any;
    creationDate: any;
    projectId: number;
    updatedBy: string;
    updationDate: any;
    deletionDate: any;
    privileges: {
        privilegeId: string;
        privilegeType: string;
        itemId: string;
        roleId: number;
        createdBy: string;
        isDeleted: number;
        creationDate: any;
        updatedBy: string;
        updationDate: any;
        deletionDate: any;
    }[];
    childRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        childItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: any;
        updatedBy: string;
        updationDate: any;
        deletionDate: any;
    }[];
}[];
export default _default;
//# sourceMappingURL=menugroup.d.ts.map