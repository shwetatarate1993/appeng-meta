"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        privilegeId: '3bf46c4f-df14-48a6-aeeb-90250215c5ea',
        privilegeType: 'EDIT',
        itemId: '952ee82b-75ee-4a66-99f3-a72fadfbeb5f',
        roleId: 20,
        createdBy: '1111',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
    },
    {
        privilegeId: '6827de5d-2b98-11e9-9992-0e5a20b1cdd2',
        privilegeType: 'VIEW',
        itemId: '952ee82b-75ee-4a66-99f3-a72fadfbeb5f',
        roleId: -888,
        createdBy: '1111',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
    },
    {
        privilegeId: '82f91dd3-05ac-4885-9001-46129b1b4f19',
        privilegeType: 'EDIT',
        itemId: '952ee82b-75ee-4a66-99f3-a72fadfbeb5f',
        roleId: 359,
        createdBy: '1111',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
    },
    {
        privilegeId: '9f33d212-7684-11e6-86b0-1245e69fa1c1 ',
        privilegeType: 'EDIT',
        itemId: '952ee82b-75ee-4a66-99f3-a72fadfbeb5f',
        roleId: 16,
        createdBy: '1111',
        isDeleted: 0,
        creationDate: null,
        updatedBy: '1111',
        updationDate: null,
        deletionDate: null,
    },
];
//# sourceMappingURL=configitem.privileges.menugroup.js.map