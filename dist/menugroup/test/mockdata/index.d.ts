export { default as menuGroupMockData } from './menugroup';
export { default as menuGroupConfigitemMockData } from './configitem.menugroup';
export { default as menuGroupPropertyMockData } from './configitem.property.menugroup';
export { default as menuGroupConfigRelationsMockData } from './configitem.relation.menugroup';
export { default as menuGroupConfigPrivilegesMockData } from './configitem.privileges.menugroup';
//# sourceMappingURL=index.d.ts.map