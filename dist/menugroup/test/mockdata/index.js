"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var menugroup_1 = require("./menugroup");
exports.menuGroupMockData = menugroup_1.default;
var configitem_menugroup_1 = require("./configitem.menugroup");
exports.menuGroupConfigitemMockData = configitem_menugroup_1.default;
var configitem_property_menugroup_1 = require("./configitem.property.menugroup");
exports.menuGroupPropertyMockData = configitem_property_menugroup_1.default;
var configitem_relation_menugroup_1 = require("./configitem.relation.menugroup");
exports.menuGroupConfigRelationsMockData = configitem_relation_menugroup_1.default;
var configitem_privileges_menugroup_1 = require("./configitem.privileges.menugroup");
exports.menuGroupConfigPrivilegesMockData = configitem_privileges_menugroup_1.default;
//# sourceMappingURL=index.js.map