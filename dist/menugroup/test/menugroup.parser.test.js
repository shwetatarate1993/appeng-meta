"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const menugroup_parser_1 = require("../menugroup.parser");
const mockdata_1 = require("./mockdata");
test('parser menu group object to config', () => {
    const menuGroup = models_1.MenuGroup.deserialize(mockdata_1.menuGroupMockData[0]);
    const result = menugroup_parser_1.parseMenuGroupToConfig(menuGroup);
    expect(result).toHaveProperty('configItemProperty');
    expect(result.configItem.configObjectType).toEqual('MenuGroup');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItemRelation.length === 1).toBe(true);
});
test('parser config to menugroup object', () => {
    const result = menugroup_parser_1.parseConfigToMenuGroup(mockdata_1.menuGroupConfigitemMockData[0], mockdata_1.menuGroupPropertyMockData, mockdata_1.menuGroupConfigRelationsMockData, mockdata_1.menuGroupConfigPrivilegesMockData);
    expect(result).toHaveProperty('usedInAllProjects');
    expect(result).toHaveProperty('usedInAllProjects', true);
    expect(result.privileges.length === 4).toBe(true);
});
//# sourceMappingURL=menugroup.parser.test.js.map