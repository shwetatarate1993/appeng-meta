"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const menu_parser_1 = require("../menu/menu.parser");
const menugroup_parser_1 = require("./menugroup.parser");
exports.Query = {
    MenuGroup: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, menugroup_parser_1.parseConfigToMenuGroup),
};
exports.MenuGroup = {
    menus: async (menuGroup, _, context) => {
        const menuIds = menuGroup.childRelations.filter((rel) => rel.relationType === relationtype_enum_1.RelationType.MENUGROUP_MENU)
            .map((rel) => rel.childItemId);
        const menus = menuIds ?
            await common_meta_1.configService.fetchConfigItemsForIdList(menuIds, menu_parser_1.parseConfigToMenu) : [];
        return menus;
    },
};
exports.Mutation = {
    createMenuGroup: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, menugroup_parser_1.parseMenuGroupToConfig, menugroup_parser_1.parseConfigToMenuGroup);
    },
};
//# sourceMappingURL=resolvers.js.map