export declare const Query: {
    MenuGroup: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").MenuGroup>;
};
export declare const MenuGroup: {
    menus: (menuGroup: any, _: any, context: any) => Promise<import("../models").Menu[]>;
};
export declare const Mutation: {
    createMenuGroup: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map