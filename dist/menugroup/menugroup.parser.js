"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseMenuGroupToConfig = (meuGroup) => {
    const createdAt = meuGroup.creationDate;
    const createdBy = meuGroup.createdBy;
    const updatedBy = meuGroup.updatedBy;
    const updatedAt = meuGroup.updationDate;
    const property = [];
    let itemId;
    if (meuGroup.configObjectId !== undefined && meuGroup.configObjectId !== ''
        && meuGroup.configObjectId !== null) {
        itemId = meuGroup.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'USED_IN_ALL_PROJECTS', meuGroup.usedInAllProjects !== undefined ? (meuGroup.usedInAllProjects ? '1' : '0') : '0', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'IS_LEFT_MENU_REQUIRED', meuGroup.leftMenuRequired !== undefined ? (meuGroup.leftMenuRequired ? '1' : '0') : '1', itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    const configItem = new meta_db_1.ConfigItemModel(itemId, meuGroup.name, configitemtype_enum_1.ConfigItemTypes.MENUGROUP, meuGroup.projectId, createdBy, meuGroup.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const menuGroupPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(meuGroup.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(meuGroup.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(meuGroup.privileges, menuGroupPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, menuGroupPrivileges);
};
exports.parseConfigToMenuGroup = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations != null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.MenuGroup(details.get('USED_IN_ALL_PROJECTS'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, details.get('IS_LEFT_MENU_REQUIRED'), configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=menugroup.parser.js.map