"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = __importDefault(require("../base"));
const common_schema_1 = __importDefault(require("../common.schema"));
const MenuGroup = `

input MenuGroupInput {
    name: String
    leftMenuRequired : Boolean
    configObjectId: ID
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    usedInAllProjects: Boolean
    privileges : [ConfigItemPrivilegeInput]
    parentRelations : [ConfigItemRelationInput]
    childRelations : [ConfigItemRelationInput]
}

extend type Query {
    MenuGroup(id: ID!): MenuGroup
   }

extend type Mutation {
    createMenuGroup (input: MenuGroupInput): MenuGroup
}

type MenuGroup {
    name: String
    leftMenuRequired : Boolean
    configObjectId: ID
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    usedInAllProjects: Boolean
    privileges : [ConfigItemPrivilege]
    parentRelations : [ConfigItemRelation]
    childRelations : [ConfigItemRelation]
    menus : [Menu]
}`;
exports.default = () => [MenuGroup, base_1.default, common_schema_1.default];
//# sourceMappingURL=schema.js.map