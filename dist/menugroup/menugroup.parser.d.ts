import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { MenuGroup } from '../models';
export declare const parseMenuGroupToConfig: (meuGroup: MenuGroup) => ConfigMetadata;
export declare const parseConfigToMenuGroup: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => MenuGroup;
//# sourceMappingURL=menugroup.parser.d.ts.map