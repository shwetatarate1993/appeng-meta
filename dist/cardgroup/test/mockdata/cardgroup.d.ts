declare const _default: {
    configObjectId: string;
    type: string;
    cardPerRow: number;
    compositeEntityId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: any;
    creationDate: any;
    projectId: number;
    updatedBy: any;
    updationDate: any;
    deletionDate: any;
    privileges: {
        privilegeType: string;
        roleId: number;
        createdBy: string;
        isDeleted: number;
        creationDate: any;
        updatedBy: string;
        updationDate: any;
        deletionDate: any;
    }[];
    childRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        childItemId: string;
        createdBy: string;
        isDeleted: number;
        creationDate: string;
        updatedBy: string;
        updationDate: any;
        deletionDate: any;
    }[];
}[];
export default _default;
//# sourceMappingURL=cardgroup.d.ts.map