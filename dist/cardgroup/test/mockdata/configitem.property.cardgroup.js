"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        propertyId: '8f801c10-1b1d-11e9-b2e1-ad14d7833e95',
        propertyName: 'TYPE',
        propertyValue: 'Project',
        itemId: '64212e16-df9c-451a-aac1-496688d7629f',
        createdBy: '1119',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '8f801c10-1b1d-11e9-b2e1-ad14d7833e96',
        propertyName: 'CARD_PER_ROW',
        propertyValue: 2,
        itemId: '64212e16-df9c-451a-aac1-496688d7629f',
        createdBy: '1119',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '8f8c10-1b1d-11e9-b2e1-ad14d7833e96',
        propertyName: 'COMPOSITE_ENTITY_ID',
        propertyValue: '642e16-df9c-451a-aac1-496688d7629f',
        itemId: '64212e16-df9c-451a-aac1-496688d7629f',
        createdBy: '1119',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
];
//# sourceMappingURL=configitem.property.cardgroup.js.map