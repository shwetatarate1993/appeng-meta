export { default as cardGroupMockData } from './cardgroup';
export { default as configitemMockDataCardGroup } from './configitem.cardgroup';
export { default as cardGroupPropertyMockData } from './configitem.property.cardgroup';
export { default as configPrivilegesMockData } from './configitem.privileges.card';
export { default as configRelationsMockData } from './configitem.relation.card';
//# sourceMappingURL=index.d.ts.map