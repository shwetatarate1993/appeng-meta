"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var cardgroup_1 = require("./cardgroup");
exports.cardGroupMockData = cardgroup_1.default;
var configitem_cardgroup_1 = require("./configitem.cardgroup");
exports.configitemMockDataCardGroup = configitem_cardgroup_1.default;
var configitem_property_cardgroup_1 = require("./configitem.property.cardgroup");
exports.cardGroupPropertyMockData = configitem_property_cardgroup_1.default;
var configitem_privileges_card_1 = require("./configitem.privileges.card");
exports.configPrivilegesMockData = configitem_privileges_card_1.default;
var configitem_relation_card_1 = require("./configitem.relation.card");
exports.configRelationsMockData = configitem_relation_card_1.default;
//# sourceMappingURL=index.js.map