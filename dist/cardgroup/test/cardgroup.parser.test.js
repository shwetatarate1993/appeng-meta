"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const cardgroup_parser_1 = require("../cardgroup.parser");
const mockdata_1 = require("./mockdata");
test('parser card group object to config', () => {
    const cardGroup = models_1.CardGroup.deserialize(mockdata_1.cardGroupMockData[0]);
    const result = cardgroup_parser_1.parseCardGroupObjectToConfig(cardGroup);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItemRelation.length === 1).toBe(true);
});
test('parser config to Card object', () => {
    const result = cardgroup_parser_1.parseConfigToCardGroupObject(mockdata_1.configitemMockDataCardGroup[0], mockdata_1.cardGroupPropertyMockData, mockdata_1.configRelationsMockData, mockdata_1.configPrivilegesMockData);
    expect(result).toHaveProperty('type');
    expect(result).toHaveProperty('cardPerRow', 2);
    expect(result.privileges.length === 4).toBe(true);
});
//# sourceMappingURL=cardgroup.parser.test.js.map