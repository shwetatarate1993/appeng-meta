export declare const Query: {
    CardGroup: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").CardGroup>;
};
export declare const CardGroup: {
    cards: (cardgroup: any, _: any, context: any) => Promise<import("../models").Card[]>;
};
export declare const Mutation: {
    createCardGroup: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map