"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseCardGroupObjectToConfig = (cardGroup) => {
    const createdAt = cardGroup.creationDate;
    const createdBy = cardGroup.createdBy;
    const updatedBy = cardGroup.updatedBy;
    const updatedAt = cardGroup.updationDate;
    const property = [];
    let itemId;
    if (cardGroup.configObjectId !== undefined && cardGroup.configObjectId !== ''
        && cardGroup.configObjectId !== null) {
        itemId = cardGroup.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (cardGroup.type !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('TYPE', cardGroup.type, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (cardGroup.cardPerRow !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('CARD_PER_ROW', cardGroup.cardPerRow, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (cardGroup.compositeEntityId !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty('COMPOSITE_ENTITY_ID', cardGroup.compositeEntityId, itemId, uuid_1.v4(), createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, cardGroup.name, configitemtype_enum_1.ConfigItemTypes.CARDGROUP, cardGroup.projectId, createdBy, cardGroup.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const cardGroupPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(cardGroup.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(cardGroup.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(cardGroup.privileges, cardGroupPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, cardGroupPrivileges);
};
exports.parseConfigToCardGroupObject = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations != null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.CardGroup(details.get('TYPE'), details.get('CARD_PER_ROW'), details.get('COMPOSITE_ENTITY_ID'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=cardgroup.parser.js.map