import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { CardGroup } from '../models';
export declare const parseCardGroupObjectToConfig: (cardGroup: CardGroup) => ConfigMetadata;
export declare const parseConfigToCardGroupObject: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => CardGroup;
//# sourceMappingURL=cardgroup.parser.d.ts.map