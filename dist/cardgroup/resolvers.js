"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const card_parser_1 = require("../card/card.parser");
const cardgroup_parser_1 = require("./cardgroup.parser");
exports.Query = {
    CardGroup: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, cardgroup_parser_1.parseConfigToCardGroupObject),
};
exports.CardGroup = {
    cards: (cardgroup, _, context) => common_meta_1.configService.fetchConfigsByParentId(cardgroup.configObjectId, card_parser_1.parseConfigToCardObject),
};
exports.Mutation = {
    createCardGroup: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, cardgroup_parser_1.parseCardGroupObjectToConfig, cardgroup_parser_1.parseConfigToCardGroupObject);
    },
};
//# sourceMappingURL=resolvers.js.map