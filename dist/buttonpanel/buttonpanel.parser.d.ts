import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
import { ButtonPanel } from '../models';
export declare const parseButtonPanelToConfig: (buttonPanel: ButtonPanel) => ConfigMetadata;
export declare const parseConfigToButtonPanel: (configItem: ConfigItemModel, properties: ConfigItemProperty[], relations: ConfigItemRelation[], privileges: ConfigItemPrivilege[]) => ButtonPanel;
//# sourceMappingURL=buttonpanel.parser.d.ts.map