"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = __importDefault(require("../base"));
const common_schema_1 = __importDefault(require("../common.schema"));
const ButtonPanel = `

input ButtonPanelInput {
    configObjectId: ID
    defaultType: String
    buttonPanelPosition:String
    mode:String
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilegeInput]
    parentRelations : [ConfigItemRelationInput]
    childRelations : [ConfigItemRelationInput]
}

extend type Query {
    ButtonPanel(id: ID!): ButtonPanel
   }

extend type Mutation {
    createButtonPanel (input: ButtonPanelInput): ButtonPanel
}

type ButtonPanel {
    configObjectId: ID
    defaultType: String
    buttonPanelPosition:String
    mode:String
    name: String
    configObjectType: String
    createdBy: String
    isDeleted: Int
    itemDescription: String
    creationDate: Date
    projectId: Int
    updatedBy: String
    updationDate: Date
    deletionDate: Date
    privileges : [ConfigItemPrivilege]
    parentRelations : [ConfigItemRelation]
    childRelations : [ConfigItemRelation]
    buttons : [Button]
    menuButtons : [MenuButton]
}
`;
exports.default = () => [ButtonPanel, base_1.default, common_schema_1.default];
//# sourceMappingURL=schema.js.map