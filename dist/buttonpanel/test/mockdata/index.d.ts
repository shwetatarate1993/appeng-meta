export { default as buttonPanelObjectMockData } from './buttonpanel.object';
export { default as buttonPanelConfigitemMockData } from './configitem.buttonpanel';
export { default as buttonPanelPropertyMockData } from './configitem.property.buttonpanel';
export { default as buttonPanelRelationMockData } from './configitem.relation.buttonpanel';
export { default as buttonPanelPrivilegesMockData } from './configitem.privileges.buttonpanel';
//# sourceMappingURL=index.d.ts.map