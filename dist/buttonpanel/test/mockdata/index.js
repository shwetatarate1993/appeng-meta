"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var buttonpanel_object_1 = require("./buttonpanel.object");
exports.buttonPanelObjectMockData = buttonpanel_object_1.default;
var configitem_buttonpanel_1 = require("./configitem.buttonpanel");
exports.buttonPanelConfigitemMockData = configitem_buttonpanel_1.default;
var configitem_property_buttonpanel_1 = require("./configitem.property.buttonpanel");
exports.buttonPanelPropertyMockData = configitem_property_buttonpanel_1.default;
var configitem_relation_buttonpanel_1 = require("./configitem.relation.buttonpanel");
exports.buttonPanelRelationMockData = configitem_relation_buttonpanel_1.default;
var configitem_privileges_buttonpanel_1 = require("./configitem.privileges.buttonpanel");
exports.buttonPanelPrivilegesMockData = configitem_privileges_buttonpanel_1.default;
//# sourceMappingURL=index.js.map