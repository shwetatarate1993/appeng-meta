"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        propertyId: 'b66ff6ba-6f95-4b39-834f-b45c031e2787',
        propertyName: 'DEFAULT_TYPE',
        propertyValue: 'button',
        itemId: 'bfa2b624-5181-4ada-9b26-0d1b45ebde3c',
        createdBy: '1135',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: 'f0c743b5-044e-4c7e-9b79-693cbf7833b1',
        propertyName: 'BUTTON_PANEL_POSITION',
        propertyValue: 'BOTTOM',
        itemId: 'bfa2b624-5181-4ada-9b26-0d1b45ebde3c',
        createdBy: '1135',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: 'f11c6c31-4132-405c-99d9-77043233fb4b',
        propertyName: 'MODE',
        propertyValue: 'INSERT',
        itemId: 'bfa2b624-5181-4ada-9b26-0d1b45ebde3c',
        createdBy: '1135',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
];
//# sourceMappingURL=configitem.property.buttonpanel.js.map