"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const buttonpanel_parser_1 = require("../buttonpanel.parser");
const mockdata_1 = require("./mockdata");
test('parser button panel to config', () => {
    const buttonPanel = models_1.ButtonPanel.deserialize(mockdata_1.buttonPanelObjectMockData[0]);
    const result = buttonpanel_parser_1.parseButtonPanelToConfig(buttonPanel);
    expect(result).toHaveProperty('configItemProperty');
    expect(result).toHaveProperty('configItemPrivilege');
    expect(result.configItem.configObjectType).toEqual('ButtonPanel');
    expect(result.configItemProperty[0].propertyName).toEqual('DEFAULT_TYPE');
    expect(result.configItemProperty[1].propertyName).toEqual('BUTTON_PANEL_POSITION');
    expect(result.configItemProperty[2].propertyName).toEqual('MODE');
});
test('parser config to button panel', () => {
    const result = buttonpanel_parser_1.parseConfigToButtonPanel(mockdata_1.buttonPanelConfigitemMockData[0], mockdata_1.buttonPanelPropertyMockData, mockdata_1.buttonPanelRelationMockData, mockdata_1.buttonPanelPrivilegesMockData);
    expect(result).toHaveProperty('defaultType');
    expect(result).toHaveProperty('mode');
    expect(result).toHaveProperty('buttonPanelPosition');
});
//# sourceMappingURL=buttonpanel.parser.test.js.map