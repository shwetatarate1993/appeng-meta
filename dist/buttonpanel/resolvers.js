"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_meta_1 = require("common-meta");
const button_parser_1 = require("../button/button.parser");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const menubutton_parser_1 = require("../menubutton/menubutton.parser");
const buttonpanel_parser_1 = require("./buttonpanel.parser");
exports.Query = {
    ButtonPanel: (_, { id }, context) => common_meta_1.configService.fetchConfigById(id, buttonpanel_parser_1.parseConfigToButtonPanel),
};
exports.ButtonPanel = {
    buttons: async (buttonPanel, _, context) => {
        const buttonIds = buttonPanel.childRelations.filter((rel) => rel.relationType === relationtype_enum_1.RelationType.BUTTONPANEL_BUTTON)
            .map((rel) => rel.childItemId);
        return buttonIds ?
            await common_meta_1.configService.fetchConfigItemsForIdList(buttonIds, button_parser_1.parseConfigToButton) : [];
    },
    menuButtons: (buttonPanel, _, context) => common_meta_1.configService.fetchConfigsByParentIdAndRelationType(buttonPanel.configObjectId, relationtype_enum_1.RelationType.BUTTONPANEL_MENUBUTTON, menubutton_parser_1.parseConfigToMenuButton),
};
exports.Mutation = {
    createButtonPanel: async (parent, { input }, context) => {
        return await common_meta_1.configService.createConfig(input, buttonpanel_parser_1.parseButtonPanelToConfig, buttonpanel_parser_1.parseConfigToButtonPanel);
    },
};
//# sourceMappingURL=resolvers.js.map