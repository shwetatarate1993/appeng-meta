"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const uuid_1 = require("uuid");
const configitemtype_enum_1 = require("../constants/configitemtype.enum");
const relationtype_enum_1 = require("../constants/relationtype.enum");
const models_1 = require("../models");
const utilities_parser_1 = require("../parser/utilities.parser");
exports.parseButtonPanelToConfig = (buttonPanel) => {
    const createdAt = buttonPanel.creationDate;
    const createdBy = buttonPanel.createdBy;
    const updatedBy = buttonPanel.updatedBy;
    const updatedAt = buttonPanel.updationDate;
    const property = [];
    let itemId;
    if (buttonPanel.configObjectId !== undefined && buttonPanel.configObjectId !== ''
        && buttonPanel.configObjectId !== null) {
        itemId = buttonPanel.configObjectId;
    }
    else {
        itemId = uuid_1.v4();
    }
    if (buttonPanel.defaultType !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'DEFAULT_TYPE', buttonPanel.defaultType, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (buttonPanel.buttonPanelPosition !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'BUTTON_PANEL_POSITION', buttonPanel.buttonPanelPosition, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    if (buttonPanel.mode !== undefined) {
        property.push(new meta_db_1.ConfigItemProperty(uuid_1.v4(), 'MODE', buttonPanel.mode, itemId, createdBy, 0, createdAt, updatedBy, updatedAt, null));
    }
    const configItem = new meta_db_1.ConfigItemModel(itemId, buttonPanel.name, configitemtype_enum_1.ConfigItemTypes.BUTTONPANEL, buttonPanel.projectId, createdBy, buttonPanel.itemDescription, createdAt, updatedBy, updatedAt, null, 0);
    const buttonPanelPrivileges = [];
    const allRelations = [];
    utilities_parser_1.getupdatedRelations(buttonPanel.parentRelations, allRelations, configItem, relationtype_enum_1.RelationType.PARENT_RELATIONS);
    utilities_parser_1.getupdatedRelations(buttonPanel.childRelations, allRelations, configItem, relationtype_enum_1.RelationType.CHILD_RELATIONS);
    utilities_parser_1.getUpdatedPrivileges(buttonPanel.privileges, buttonPanelPrivileges, configItem);
    return new meta_db_1.ConfigMetadata(configItem, property, allRelations, buttonPanelPrivileges);
};
exports.parseConfigToButtonPanel = (configItem, properties, relations, privileges) => {
    const details = new Map();
    if (properties) {
        properties.forEach((property) => {
            details.set(property.propertyName, property.propertyValue);
        });
    }
    let parentRelations = [];
    let childRelations = [];
    if (relations !== undefined && relations !== null) {
        parentRelations = relations.filter((rel) => rel.childItemId === configItem.configObjectId);
        childRelations = relations.filter((rel) => rel.parentItemId === configItem.configObjectId);
    }
    return new models_1.ButtonPanel(details.get('DEFAULT_TYPE'), details.get('BUTTON_PANEL_POSITION'), details.get('MODE'), configItem.configObjectId, configItem.name, configItem.configObjectType, configItem.projectId, configItem.createdBy, configItem.itemDescription, configItem.creationDate, configItem.updatedBy, configItem.updationDate, configItem.deletionDate, configItem.isDeleted, privileges, childRelations, parentRelations);
};
//# sourceMappingURL=buttonpanel.parser.js.map