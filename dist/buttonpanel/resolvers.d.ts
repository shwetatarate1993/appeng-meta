export declare const Query: {
    ButtonPanel: (_: any, { id }: {
        id: any;
    }, context: any) => Promise<import("../models").ButtonPanel>;
};
export declare const ButtonPanel: {
    buttons: (buttonPanel: any, _: any, context: any) => Promise<import("../models").Button[]>;
    menuButtons: (buttonPanel: any, _: any, context: any) => Promise<import("../models").MenuButton[]>;
};
export declare const Mutation: {
    createButtonPanel: (parent: any, { input }: {
        input: any;
    }, context: any) => Promise<any>;
};
//# sourceMappingURL=resolvers.d.ts.map